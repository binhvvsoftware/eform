USE [FSB.EForm]
GO
/****** Object:  Table [dbo].[Cms_Campus]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Campus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[Status] [bit] NOT NULL,
	[ParentDepartmentId] [int] NOT NULL,
 CONSTRAINT [PK_Cms_Campus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_Department]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Department](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NOT NULL,
	[Description] [nvarchar](255) NULL,
	[CampusId] [int] NOT NULL,
	[Order] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Cms_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_EmailTemplates]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_EmailTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[Subject] [nvarchar](512) NOT NULL,
	[Body] [ntext] NOT NULL,
	[EmailAddress] [nvarchar](512) NULL,
	[CcEmailAddress] [nvarchar](512) NULL,
	[BccEmailAddress] [nvarchar](512) NULL,
	[Enabled] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Cms_EmailTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_FormTemplates]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_FormTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](255) NOT NULL,
	[TemplateContents] [ntext] NOT NULL,
	[TemplatePath] [nvarchar](512) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Cms_FormTemplates_IsActive]  DEFAULT ((0)),
	[FormType] [smallint] NULL DEFAULT ((1)),
 CONSTRAINT [PK_Cms_FormTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityRoleClaims]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_IdentityRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityRoles]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[NormalizedName] [nvarchar](50) NULL,
	[ConcurrencyStamp] [nvarchar](50) NULL,
	[MenuIds] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_IdentityRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityUserClaims]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_IdentityUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityUserLogins]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityUserLogins](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_Cms_IdentityUserLogins] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityUserRoles]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityUserRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_Cms_IdentityUserRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityUsers]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NormalizedUserName] [nvarchar](max) NULL,
	[NormalizedEmail] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[EmployeeCode] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[ContractType] [smallint] NOT NULL,
	[Gender] [smallint] NOT NULL CONSTRAINT [DF_Cms_IdentityUsers_Gender]  DEFAULT ((1)),
	[CampusId] [int] NOT NULL,
	[Disabled] [bit] NULL DEFAULT ((0)),
	[SeniorLeader] [bit] NULL DEFAULT ((0)),
	[UserType] [smallint] NULL DEFAULT ((1)),
	[IsManageRequestDocument ] [bit] NULL DEFAULT ((0)),
	[UpdateDate] [datetime] NULL,
	[IgnoreWorkingRule] [bit] NULL CONSTRAINT [DF_Cms_IdentityUsers_IgnoreWorkingRule]  DEFAULT ((0)),
 CONSTRAINT [PK_Cms_IdentityUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_IdentityUserTokens]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_IdentityUserTokens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[LoginProvider] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Cms_IdentityUserTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_MemberRelation]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_MemberRelation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ManagerId] [int] NOT NULL,
 CONSTRAINT [PK_Cms_MemberRelation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_Menus]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Menus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Url] [nvarchar](max) NOT NULL,
	[Icon] [nvarchar](max) NOT NULL,
	[Position] [int] NOT NULL,
	[Parent] [int] NOT NULL,
	[MenuAction] [tinyint] NOT NULL,
 CONSTRAINT [PK_Cms_Menus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_ParentDepartment]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_ParentDepartment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_ParentDepartment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_ReqDocumentAssignee]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_ReqDocumentAssignee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RequestDocumentId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Step] [int] NOT NULL,
	[SignatureImage] [nvarchar](max) NULL,
	[Comment] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ReqDocumentAssignee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_RequestDocuments]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cms_RequestDocuments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Status] [tinyint] NULL,
	[JsonContent] [nvarchar](max) NOT NULL,
	[HtmlContent] [nvarchar](max) NOT NULL,
	[SignatureImage] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CaptchaCode] [varchar](50) NULL,
	[Enabled] [bit] NULL DEFAULT ((1)),
	[CreateByUserId] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Cms_RequestDocuments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cms_Timesheets_raw]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Cms_Timesheets_raw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emp_code] [nvarchar](100) NULL,
	[checkin_time] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cms_UserLeaveApplications]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_UserLeaveApplications](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RequestDocumentId] [int] NOT NULL,
	[LeaveType] [tinyint] NOT NULL,
	[LeaveFrom] [datetime] NULL,
	[LeaveTo] [datetime] NULL,
	[TotalLeaveTime] [float] NOT NULL,
	[LeaveReason] [nvarchar](max) NULL,
	[LeaveArrangement] [nvarchar](max) NULL,
	[CreateAt] [datetime] NOT NULL CONSTRAINT [DF_Cms_UserLeaveApplications_CreateAt]  DEFAULT (getdate()),
	[ApplyStatus] [tinyint] NULL DEFAULT ((1)),
	[LeaveUnPaidFrom] [datetime] NULL,
	[LeaveUnPaidTo] [datetime] NULL,
	[TotalLeaveUnPaidTime] [float] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Cms_UserLeaveApplications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_Users]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cms_Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pin1] [int] NULL,
	[pin2] [int] NULL,
	[name] [nvarchar](250) NULL,
	[groupName] [int] NULL,
	[privilege] [int] NULL,
	[card] [int] NULL,
	[raw_data] [varchar](2000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cms_Versions_Entities]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Versions_Entities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Cms_Versions_Entities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_Versions_Histories]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Versions_Histories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[EntityId] [nvarchar](max) NOT NULL,
	[Values] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_Versions_Histories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_Workflows]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Workflows](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[CampusId] [int] NOT NULL,
	[FormTemplateId] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[WorkflowType] [smallint] NULL DEFAULT ((1)),
 CONSTRAINT [PK_Cms_Workflows] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_WorkflowSteps]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_WorkflowSteps](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Step] [int] NOT NULL,
	[UserIds] [nvarchar](500) NULL,
	[GroupManagement] [tinyint] NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Cms_WorkflowSteps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cms_WorkingLateRequest]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cms_WorkingLateRequest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TimeSheetId] [bigint] NOT NULL,
	[ManagerIds] [varchar](255) NULL,
	[RequesterId] [int] NOT NULL,
	[TotalLateMinutes] [int] NULL,
	[ActualWorkTime] [datetime2](7) NOT NULL,
	[PenaltyRule] [nvarchar](255) NULL,
	[LateReviewStatus] [tinyint] NULL CONSTRAINT [DF_Cms_WorkingLateRequest_LateReviewStatus]  DEFAULT ((1)),
	[ExpiredDate] [datetime2](7) NOT NULL DEFAULT (getdate()+(2)),
	[CreateDate] [datetime2](7) NOT NULL DEFAULT (getdate()),
	[UpdateDate] [datetime2](7) NOT NULL DEFAULT (getdate()),
 CONSTRAINT [Cms_WorkingLateRequest_pk] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cms_WorkingTimeSheets]    Script Date: 30/01/2020 08:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cms_WorkingTimeSheets](
	[UserId] [int] NOT NULL,
	[WorkingDate] [date] NULL,
	[StartTime] [varchar](50) NOT NULL,
	[EndTime] [varchar](50) NOT NULL,
	[CreateAt] [datetime] NULL,
	[CreateBy] [varchar](50) NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Cms_WorkingTimeSheets_pk] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Cms_Campus]  WITH CHECK ADD  CONSTRAINT [FK_Cms_Campus_Cms_ParentDepartment_ParentDepartmentId] FOREIGN KEY([ParentDepartmentId])
REFERENCES [dbo].[Cms_ParentDepartment] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Cms_Campus] CHECK CONSTRAINT [FK_Cms_Campus_Cms_ParentDepartment_ParentDepartmentId]
GO
ALTER TABLE [dbo].[Cms_Department]  WITH CHECK ADD  CONSTRAINT [FK_Cms_Department_Cms_Campus_CampusId] FOREIGN KEY([CampusId])
REFERENCES [dbo].[Cms_Campus] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Cms_Department] CHECK CONSTRAINT [FK_Cms_Department_Cms_Campus_CampusId]
GO
ALTER TABLE [dbo].[Cms_WorkingLateRequest]  WITH CHECK ADD  CONSTRAINT [Cms_WorkingLateRequest_Cms_WorkingTimeSheets_Id_fk] FOREIGN KEY([TimeSheetId])
REFERENCES [dbo].[Cms_WorkingTimeSheets] ([Id])
GO
ALTER TABLE [dbo].[Cms_WorkingLateRequest] CHECK CONSTRAINT [Cms_WorkingLateRequest_Cms_WorkingTimeSheets_Id_fk]
GO
