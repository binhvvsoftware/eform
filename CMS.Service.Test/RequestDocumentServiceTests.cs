using System.Linq;
using System;
using System.Threading.Tasks;
using Autofac;
using CMS.Core;
using CMS.Core.Repository;
using CMS.Dashboard;
using CMS.Services.Data;
using CMS.Services.Services;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMS.Core.Extensions;
using CMS.Shared.Data.Domain;
using CMS.Service.Test;

namespace CMS.Service.Tests
{
    [TestClass]
    public class RequestDocumentServiceTests
    {
        private IContainer Init(Action<ContainerBuilder> builder = null)
        {
            return BaseTestContainer.Init(builder);
        }

        [TestMethod]
        public async Task GetListApprovalDocumentsTest()
        {
            using (var c = Init(builder =>
            {
                builder.RegisterModule(new DashboardModule());
                builder.RegisterModule(new EntityModule<EformDbContext>("Server=.\\SQLEXPRESS; Database=FSB.EForm; Trusted_Connection=True;"));
            }))
            {
                var repoService =c.Resolve<IRepository<RequestDocument, int>>();
                var userService =c.Resolve<IUserService>();
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<ILogger>();
                var service = c.Resolve<IRequestDocumentService>();
                var records = await service.GetReviewDocuments(new Shared.Models.RequestDocumentFilterRequest
                {
                    UserId = 5
                });

                Assert.IsNotNull(records);
                Assert.AreNotEqual(0, records.Count);
                Assert.IsTrue(records.All(x => !x.RequestDocumentAssignees.IsNullOrEmpty()));
            }
        }

        public async Task GetCurrentReviewStepUserTest()
        {
            using (var c = Init(builder =>
            {
                builder.RegisterGeneric(typeof(Core.Entity.MemoryRepository<,>)).As(typeof(IRepository<,>)).SingleInstance();
            }))
            {
                var service = c.Resolve<IRequestDocumentService>();
                var records = await service.GetReviewDocuments(new Shared.Models.RequestDocumentFilterRequest
                {
                    UserId = 5
                });

                Assert.IsNotNull(records);
                Assert.AreNotEqual(0, records.Count);
                Assert.IsTrue(records.All(x => !x.RequestDocumentAssignees.IsNullOrEmpty()));


            }
        }
    }
}