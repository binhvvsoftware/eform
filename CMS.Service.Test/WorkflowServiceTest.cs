﻿using Autofac;
using CMS.Core.Repository;
using CMS.Services.Data;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Service.Tests
{
    [TestClass]
    public class WorkflowServiceTest
    {
        [TestMethod]
        public async Task RegisterWorkflow()
        {
            var connectionString = "Server=DUYNGUYEN\\SQLEXPRESS; Database=FSB.EForm; Trusted_Connection=True;";
            var builder = new ContainerBuilder();
            builder.RegisterType<WorkflowService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorkflowStepService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterModule(new EntityModule<EformDbContext>(connectionString));

            var context = builder.Build();
            var workflowService = context.Resolve<IWorkflowService>();
            var workflowStepService = context.Resolve<IWorkflowStepService>();

            var workflow = new Workflow()
            {
                CampusId = 1,
                FormTemplateId = 1,
                Name = "Gửi quản lý trực tiếp",
                CreateDate = DateTime.Now,
                CreateBy = "jymyct@gmail.com",
            };

            await workflowService.InsertAsync(workflow);

            var workflowSteps = new List<WorkflowStep>()
            {
                new WorkflowStep()
                {
                     WorkflowId = workflow.Id,
                     GroupManagement = GroupManagement.SuperiorManagement,
                     Name = "Gửi quản lý trực tiếp",
                     Step = 1,
                     CreateDate = DateTime.Now,
                     CreateBy = "jymyct@gmail.com",
                },
                 new WorkflowStep()
                 {
                     WorkflowId = workflow.Id,
                     GroupManagement = GroupManagement.UpSuperiorManagement,
                     Name = "Gửi quản lý cấp cao",
                     Step = 2,
                     CreateDate = DateTime.Now,
                     CreateBy = "jymyct@gmail.com",
                 },
            };

            await workflowStepService.InsertManyAsync(workflowSteps);
        }

        [TestMethod]
        public async Task ModifyDeparment()
        {
            var connectionString = "Server=.\\SQLEXPRESS; Database=FSB.EForm; User Id=sa; Password=fsb@123aB;";
            var builder = new ContainerBuilder();
            builder.RegisterType<DepartmentService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterModule(new EntityModule<EformDbContext>(connectionString));

            var context = builder.Build();
            var departmentService = context.Resolve<IDepartmentService>();
            var deparments = (await departmentService.GetRecordsAsync()).OrderBy(x => x.Id).ToList();

            for (int i = 0; i < deparments.Count; i++)
            {
                deparments[i].Order = i + 1;
            }

            await departmentService.UpdateManyAsync(deparments);
        }
    }
}
