using System.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Repository;
using CMS.Service.Test;
using CMS.Services.Services;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMS.Core.Linq;
using CMS.Core.Extensions;

namespace CMS.Service.Tests
{
    [TestClass]
    public class CollectWorkingTimeJobTest
    {
        [TestMethod]
        public async Task GetWorkingLateTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var service =
                    new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);

                var usersWorkedLate =
                    await service.GetUserWorkingTimes(new List<int>(), DateTime.ParseExact("2019-10-01", "yyyy-MM-dd", CultureInfo.CurrentCulture));

                Assert.IsNotNull(usersWorkedLate);
                Assert.AreEqual(17, usersWorkedLate.Count);
            }
        }

        [TestMethod]
        public async Task GetLateTimesheetTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var service = new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);

                var emailLogger = c.Resolve<ILoggerFactory>().CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), emailLogger);
                var lateService = new WorkingLateService(c.Resolve<IRepository<WorkingLateRequest, long>>(), emailService, c.Resolve<IMemberRelationService>(), c.Resolve<IUserService>(), options);

                var userService = c.Resolve<IUserService>();
                var users = await userService.GetRecordsAsync(x => !x.Disabled);

                var random = new Random();
                var entites = Enumerable.Range(1, 10).Select((x, i) =>
                {
                    var workingDate = DateTime.Now.AddDays(random.Next(-5, 5));
                    var workingTimeSheet = new WorkingTimesheet()
                    {
                        UserId = 2,
                        WorkingDate = workingDate.Date,
                    };

                    workingTimeSheet.StartTime = i % 2 == 0
                        ? (workingDate.Date + TimeSpan.Parse("09:30:00")).ToString("yyyy-MM-dd hh:mm:ss")
                        : workingDate.ToString("yyyy-MM-dd hh:mm:ss");
                    workingTimeSheet.EndTime = workingDate.ToString("yyyy-MM-dd hh:mm:ss");
                    workingTimeSheet.CreateAt = workingDate;
                    workingTimeSheet.CreateBy = "Test";

                    return workingTimeSheet;
                }).ToList();

                await service.InsertManyAsync(entites);

                var lateRequest = await lateService.TransformAndInsertRequests(users, entites);

                Assert.IsNotNull(lateRequest);
                Assert.AreNotEqual(0, lateRequest.Count());
                // Without User
                var usersWorkedLate = await service.GetUserWorkingTimes(new[] { 2 });
                await lateService.DeleteManyAsync(lateRequest);
                await service.DeleteManyAsync(entites);
                Assert.IsNotNull(usersWorkedLate);
                Assert.AreNotEqual(0, usersWorkedLate.Count);
                Assert.IsTrue(usersWorkedLate.Any(x => x.LateRequest != null));
                // With User
            }
        }

        [TestMethod]
        public async Task GetTimesheetIncludedRequests()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var userService = c.Resolve<IUserService>();
                var users = await userService.GetRecordsAsync(x => !x.Disabled);
                var emailLogger = c.Resolve<ILoggerFactory>().CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), emailLogger);

                var service =
                    new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);
                var lateService = new WorkingLateService(c.Resolve<IRepository<WorkingLateRequest, long>>(), emailService, c.Resolve<IMemberRelationService>(), c.Resolve<IUserService>(), options);

                var testEntities = await service.GetRecordsAsync(x => x.UserId == 2);
                var lates = await lateService.TransformAndInsertRequests(users, testEntities.ToList());

                var usersWorkedLate = await service.GetUserWorkingTimes(testEntities.Select(x => x.UserId).ToList());
                System.Console.WriteLine("Inserted total request = " + usersWorkedLate.Where(x => x.LateRequest != null).Count());
                System.Console.WriteLine("Total usersWorkedLate = " + usersWorkedLate.Count);
                Assert.IsNotNull(usersWorkedLate);
                Assert.AreNotEqual(0, usersWorkedLate.Count);
                Assert.IsTrue(usersWorkedLate.Any(x => x.LateRequest != null));
            }
        }


        [TestMethod]
        public async Task InsertAndGetUserWorkingTimeTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var service =
                    new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);

                var random = new Random(5);
                var entites = Enumerable.Range(1, 10).Select(x =>
                {
                    var workingDate = DateTime.Now.AddDays(random.Next(-5, 5));
                    var workingTimeSheet = new WorkingTimesheet()
                    {
                        UserId = 2,
                        WorkingDate = workingDate,
                    };

                    workingTimeSheet.StartTime = workingDate.ToString("yyyy-MM-dd hh:mm:ss");
                    workingTimeSheet.EndTime = workingDate.AddHours(8).ToString("yyyy-MM-dd hh:mm:ss");
                    workingTimeSheet.CreateAt = DateTime.Now;
                    workingTimeSheet.CreateBy = "Test";

                    return workingTimeSheet;
                }).ToList();

                await service.InsertManyAsync(entites);
                // Without User
                var usersWorkedLate =
                    await service.GetUserWorkingTimes(Array.Empty<int>());

                await service.DeleteManyAsync(entites);

                Assert.IsNotNull(usersWorkedLate);
                Assert.AreNotEqual(0, usersWorkedLate.Count);

                var ids = entites.Select(x => x.Id).ToArray();
                usersWorkedLate = usersWorkedLate.Where(x => ids.Contains(x.Id)).ToList();

                Assert.IsNotNull(usersWorkedLate);
                Assert.AreNotEqual(0, usersWorkedLate.Count);
            }
        }

        [TestMethod]
        public async Task GetWorkingLateByUserIdTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var service = new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);

                var usersWorkedLate = await service.GetUserWorkingTimes(new[] { 2 });

                Assert.IsNotNull(usersWorkedLate);
                Assert.AreNotEqual(0, usersWorkedLate.Count);
            }
        }

        [TestMethod]
        public async Task CreateDummyData()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logger = c.Resolve<ILoggerFactory>().CreateLogger<WorkingTimeSheetService>();
                var options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
                var workingTimeSheetService = new WorkingTimeSheetService(c.Resolve<IRepository<WorkingTimesheet, long>>(), options);

                var random = new Random();
                var expectedWorkingTime = new TimeSpan(8, 30, 0);
                var expectedLeaveTime = new TimeSpan(17, 30, 0);
                // Own timesheet
                var ownTimeSheets = Enumerable.Range(1, 5).Select(x =>
                {
                    var workingTime = new WorkingTimesheet()
                    {
                        UserId = 2,
                        CreateAt = DateTime.Now,
                        CreateBy = "System",
                        WorkingDate = DateTime.Now.AddDays(random.Next(1, 5) * -1),
                    };
                    workingTime.StartTime = (workingTime.WorkingDate.Date + expectedWorkingTime).AddMinutes(random.Next(1, 120)).ToString("yyyy-MM-dd HH:mm:ss");
                    workingTime.EndTime = (workingTime.WorkingDate.Date + expectedLeaveTime).ToString("yyyy-MM-dd HH:mm:ss");
                    return workingTime;
                }).ToList();

                await workingTimeSheetService.InsertManyAsync(ownTimeSheets);

                // Member Timesheet
                var member1TimeSheets = Enumerable.Range(1, 5).Select(x =>
                {
                    var workingTime = new WorkingTimesheet()
                    {
                        UserId = 3,
                        CreateAt = DateTime.Now,
                        CreateBy = "System",
                        WorkingDate = DateTime.Now.AddDays(random.Next(1, 5) * -1),
                    };
                    workingTime.StartTime = (workingTime.WorkingDate.Date + expectedWorkingTime).AddMinutes(random.Next(1, 120)).ToString("yyyy-MM-dd HH:mm:ss");
                    workingTime.EndTime = (workingTime.WorkingDate.Date + expectedLeaveTime).ToString("yyyy-MM-dd HH:mm:ss");
                    return workingTime;
                }).ToList();

                await workingTimeSheetService.InsertManyAsync(member1TimeSheets);

                var member2TimeSheets = Enumerable.Range(1, 5).Select(x =>
                {
                    var workingTime = new WorkingTimesheet()
                    {
                        UserId = 4006,
                        CreateAt = DateTime.Now,
                        CreateBy = "System",
                        WorkingDate = DateTime.Now.AddDays(random.Next(1, 5) * -1),
                    };
                    workingTime.StartTime = (workingTime.WorkingDate.Date + expectedWorkingTime).AddMinutes(random.Next(1, 120)).ToString("yyyy-MM-dd HH:mm:ss");
                    workingTime.EndTime = (workingTime.WorkingDate.Date + expectedLeaveTime).ToString("yyyy-MM-dd HH:mm:ss");

                    return workingTime;
                }).ToList();

                await workingTimeSheetService.InsertManyAsync(member2TimeSheets);
                var userRelationService = c.Resolve<IMemberRelationService>();

                var userIds = new [] { 2, 3, 4006};

                var filterExpression = PredicateBuilder.True<WorkingTimesheet>();

                if (!userIds.IsNullOrEmpty())
                {
                    filterExpression = filterExpression.And(x => userIds.Contains(x.UserId));
                }

                var lateTimeRecords = await workingTimeSheetService.GetUserWorkingTimes(filterExpression);

                Assert.IsNotNull(lateTimeRecords);
                Assert.AreNotEqual(0,lateTimeRecords.Count);
                var emailLogger = c.Resolve<ILoggerFactory>().CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), emailLogger);

                var workingLateService = new WorkingLateService(c.Resolve<IRepository<WorkingLateRequest, long>>(), emailService, c.Resolve<IMemberRelationService>(), c.Resolve<IUserService>(), options);
                var userService = c.Resolve<IUserService>();
                var users = await userService.GetRecordsAsync(x => !x.Disabled);
                await workingLateService.TransformAndInsertRequests(users, lateTimeRecords);
                var lateRequests = await workingLateService.GetRecordsAsync();
                Assert.IsNotNull(lateRequests);
                Assert.AreNotEqual(0, lateRequests.Count());
            }
        }
    }
}