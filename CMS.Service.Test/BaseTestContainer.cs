using System;
using Autofac;
using CMS.Core;
using CMS.Core.Repository;
using CMS.Dashboard;
using CMS.Service.Test;
using CMS.Services.Data;
using Microsoft.Extensions.Logging;

namespace CMS.Service.Tests
{
    public static class BaseTestContainer
    {
        public static IContainer Init(Action<ContainerBuilder> builder = null)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule(new DashboardModule())
                .RegisterModule(new CoreModule())
                .RegisterModule(new EntityModule<EformDbContext>("Server=.\\SQLEXPRESS; Database=FSB.EForm; Trusted_Connection=True;"));

            containerBuilder.RegisterType<LoggerFactory>().AsImplementedInterfaces().SingleInstance();
            containerBuilder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();

            builder?.Invoke(containerBuilder);

            return containerBuilder.Build();
        }
    }
}