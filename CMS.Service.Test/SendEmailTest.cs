﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Repository;
using CMS.Dashboard.Extensions;
using CMS.Service.Test;
using CMS.Services.Data;
using CMS.Services.Extensions;
using CMS.Services.Services;
using CMS.Services.Templates;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemplateParser = CMS.Services.Templates.TemplateParser;

namespace CMS.Service.Tests
{
    [TestClass]
    public class EmailServiceTest
    {
        private string host = "smtp.gmail.com";
        private string email = "eform@fsb.edu.vn";
        private string password = "fsb@123aB";
        private bool enableSsl = true;
        private int port = 587;


        //        [TestMethod]
        //        public async Task SendEmailTestAsync()
        //        {
        //            var builder = new ContainerBuilder();
        //            builder.RegisterType<TemplateParser>().AsImplementedInterfaces().SingleInstance();
        //            var context = builder.Build();
        //
        //            var service = new EmailService(context, new OptionsWrapper<EmailSettings>(new EmailSettings()
        //            {
        //                Host = "smtp.gmail.com",
        //                Email = "eform@fsb.edu.vn",
        //                Password = "fsb@123aB",
        //                EnableSsl = true,
        //                Port = 587
        //            }));
        //
        //            var templateParser = context.Resolve<ITemplateParser>();
        //            var subject = "Email Confirm";
        //            var body = "Kính gửi @Model.Name<br/><br/>Dưới đây là thông tin chúng tôi cần bạn xác nhận:<br/>" +
        //                       "- Tên đầy đủ: @Model.Name <br/> " +
        //                       "- Tuổi: @Model.Age <br/> " +
        //                       "- Địa chỉ: @Model.Address <br/>" +
        //                       "- Số điện thoại: @Model.Tel <br/>" +
        //                       "- Đây là template khi chưa Compile: <br/><br/>" +
        //                       "@Model.Template";
        //            var tokens = new Dictionary<string, object>
        //            {
        //                {"Name", "Nguyễn Quang Duy"},
        //                {"Age", "24"},
        //                {"Address", "222 Âu Cơ"},
        //                {"Tel", "09087083373"},
        //                {"Template", body}
        //            };
        //
        //            try
        //            {
        //                var mail = new MailMessage();
        //                mail.To.Add("nguyenquangduy0310@gmail.com");
        ////                mail.CC.Add("jymyct@gmail.com");
        ////                mail.Bcc.Add("vuthanh20286@gmail.com");
        //                mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens);
        //                mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
        //                mail.IsBodyHtml = true;
        //                mail.SubjectEncoding = Encoding.Unicode;
        //
        //                var smtpConfig = new SmtpConfig()
        //                {
        //                    Id = 0,
        //                    Email = email,
        //                    EnableSsl = enableSsl,
        //                    Host = host,
        //                    Name = "SMTP",
        //                    Password = password,
        //                    Port = port,
        //                    Username = email
        //                };
        //                await service.SendMailAsync(mail, smtpConfig, true);
        //                Console.WriteLine("Send email successful");
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine(ex);
        //            }
        //        }

        [TestMethod]
        public async Task NewFormRequestEmailTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<EmailService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<TemplateParser>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<EmailTemplateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().As<ILoggerFactory>().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
                builder.RegisterModule(new EntityModule<EformDbContext>("Server=210.245.95.111; Database=FSB.EForm; User Id=sa;Password=fsb@123aB;"));
            }))
            {
                var logFac = c.Resolve<ILoggerFactory>();
                var logger = logFac.CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), logger);


                var users = await c.Resolve<IRepository<IdentityUser, int>>().FindAsync(x => x.Id == 2);
                var formRequest = new RequestDocument()
                {
                    WorkflowId = 79
                };
                await emailService.SendNewRequestDocument(formRequest, new[] { new RequestDocumentAssignee() { UserId = 1 }  , new RequestDocumentAssignee() { UserId = 6 }  }, users.First());
            }
        }


        [TestMethod]
        public async Task ApprovedFormRequestEmailTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<EmailService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<TemplateParser>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<EmailTemplateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().As<ILoggerFactory>().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logFac = c.Resolve<ILoggerFactory>();
                var logger = logFac.CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), logger);


                var users = await c.Resolve<IRepository<IdentityUser, int>>().FindAsync(x => x.Id == 2);

                await emailService.SendRequestDocumentApproved(new RequestDocument(), new RequestDocumentAssignee(), users.First());
            }
        }

        [TestMethod]
        public async Task RejectedFormRequestEmailTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<EmailService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<TemplateParser>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<EmailTemplateService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<WorkflowService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<RequestDocumentService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<RequestDocumentAssigneeService>().AsImplementedInterfaces().SingleInstance();
                builder.RegisterType<LoggerFactory>().As<ILoggerFactory>().SingleInstance();
                builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                var logFac = c.Resolve<ILoggerFactory>();
                var logger = logFac.CreateLogger<EmailService>();

                var emailService = new EmailService(c, new OptionsWrapper<EmailSettings>(new EmailSettings()
                {
                    Host = "smtp.gmail.com",
                    Email = "eform@fsb.edu.vn",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                }), logger);
                var users = await c.Resolve<IRepository<IdentityUser, int>>().FindAsync(x => x.Id == 2);
                var request = await c.Resolve<IRequestDocumentService>().GetRecordAsync();
                var requestAssign = await c.Resolve<IRequestDocumentAssigneeService>().GetRecordAsync();
                requestAssign.SignatureImage = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test\Images\capcha-2.png";
                await emailService.SendRequestDocumentReject(request, requestAssign, users.First());
            }
        }
    }
}