using System;
using Microsoft.Extensions.Logging;

namespace CMS.Service.Test
{
    public class TestLogger<T> : ILogger<T>
    {
        private readonly ILogger internalLogger;

        public TestLogger(ILoggerFactory loggerFactory)
        {
            internalLogger = loggerFactory.CreateLogger(GetType());
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            internalLogger.Log(logLevel, eventId, state, exception, formatter);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return internalLogger.IsEnabled(logLevel);
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return internalLogger.BeginScope(state);
        }
    }
}