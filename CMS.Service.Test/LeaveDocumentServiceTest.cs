using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using Autofac;
using CMS.Service.Tests;
using CMS.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMS.Core.Extensions;
using System;
using CMS.Shared.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Microsoft.Extensions.Logging;

namespace CMS.Service.Test
{
    [TestClass]
    public class LeaveDocumentServiceTest
    {
        [TestMethod]
        public async Task StoreLeaveApplicationTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<LeaveDocumentService>().AsImplementedInterfaces().SingleInstance();
                // builder.RegisterModule(new LogModule());
                //  builder.RegisterGeneric(typeof(TestLogger<>)).As(typeof(ILogger<>)).SingleInstance();
            }))
            {
                using (var fileStream = File.OpenRead(@"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test\FileInputs\DXNP_Content.json"))
                using (var dataStream = new StreamReader(fileStream))
                {
                    var content = await dataStream.ReadToEndAsync();
                    Assert.IsNotNull(content);
                    try
                    {
//                      var model = JsonConvert.DeserializeObject<AbsentModel>(content, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                        var model = content.JsonDeserialize<AbsentModel>();
                        Assert.IsNotNull(model);
                        Assert.IsTrue(model.IsValidateModel());

                        var entity = model.ToEntity();
                        var service = c.Resolve<ILeaveDocumentService>();
                        await service.StoreAbsentRequest(entity);

                        var storedEntity = service.GetRecordAsync();

                        Assert.AreEqual(entity, storedEntity);
                    }
                    catch (System.Exception e)
                    {
                        System.Console.WriteLine(e.StackTrace);
                        throw;
                    }

                }
            }
        }

        [TestMethod]
        public async Task GenerateCapchaStringTest()
        {
            using (var c = BaseTestContainer.Init(builder =>
            {
                builder.RegisterType<CapchaGenerator>().AsImplementedInterfaces().SingleInstance();
            }))
            {
                var capchaService = c.Resolve<ICapchaGenerator>();
                var img = capchaService.GenerateCapchaImage();
                Assert.IsNotNull(img);
                Console.WriteLine(img);
                var validate = capchaService.ValidateCapcha(img, img);
                Assert.IsNotNull(validate);
                Assert.IsTrue(validate.AttemptSucceeded);
                var img2 = capchaService.GenerateCapchaImage(4);

                validate = capchaService.ValidateCapcha(img, img2);
                Assert.IsNotNull(validate);
                Assert.IsFalse(validate.AttemptSucceeded);

                Console.WriteLine("\r\n\r\n\r\n");

                // img = await  capchaService.GenerateCapchaImageAsync(4);
                // Assert.IsNotNull(img);
                // Console.WriteLine(img);
                //
                // validate = await capchaService.ValidateCapchaAsync(img, img);
                // Assert.IsNotNull(validate);
                // Assert.IsTrue(validate.AttemptSucceeded);
                //
                // validate = await capchaService.ValidateCapchaAsync(img, img + "xxxxx");
                // Assert.IsNotNull(validate);
                // Assert.IsFalse(validate.AttemptSucceeded);
            }
        }
    }
}