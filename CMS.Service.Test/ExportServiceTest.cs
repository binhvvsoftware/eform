﻿using System.Text;
using CMS.Services.Word;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CMS.Core.Extensions;
using CMS.Sharp.Docx;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using Wp = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using CMS.Services.Services;

namespace CMS.Service.Tests
{
    [TestClass]
    public class ExportServiceTest
    {
        [TestMethod]
        public async Task Yeu_Cau_Su_Dung_Tai_San_Word_Template_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };

            //            await Complex_Export_Process(baseBody, baseSignature);
            //            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await Yeu_Cau_Tuyen_Dung_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await De_Nghi_Ky_Hop_Dong_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Cap_Phat_VPP_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await To_Trinh_HD_Thu_Viec_Export_Process(new Dictionary<string, string>(), baseSignature);
            await Yeu_Cau_Su_Dung_Tai_San_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        [TestMethod]
        public async Task Yeu_Cau_Tuyen_Dung_Word_Template_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };

            //            await Complex_Export_Process(baseBody, baseSignature);
            //            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
            await Yeu_Cau_Tuyen_Dung_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await De_Nghi_Ky_Hop_Dong_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Cap_Phat_VPP_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await To_Trinh_HD_Thu_Viec_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Yeu_Cau_Su_Dung_Tai_San_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        [TestMethod]
        public async Task Complex_Word_Template_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };

            await Complex_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
            //    await Yeu_Cau_Tuyen_Dung_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await De_Nghi_Ky_Hop_Dong_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Cap_Phat_VPP_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await To_Trinh_HD_Thu_Viec_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Yeu_Cau_Su_Dung_Tai_San_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        [TestMethod]
        public async Task To_Trinh_HD_Thu_Viec_Word_Template_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };

            // await Complex_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
            //    await Yeu_Cau_Tuyen_Dung_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await De_Nghi_Ky_Hop_Dong_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Cap_Phat_VPP_Export_Process(new Dictionary<string, string>(), baseSignature);
            await To_Trinh_HD_Thu_Viec_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Yeu_Cau_Su_Dung_Tai_San_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        [TestMethod]
        public async Task De_Nghi_Ky_Hop_Dong_Export_Word_Template_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };

            //    await Complex_Export_Process(new Dictionary<string, string>(), baseSignature);
            //            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
            //    await Yeu_Cau_Tuyen_Dung_Export_Process(new Dictionary<string, string>(), baseSignature);
            await De_Nghi_Ky_Hop_Dong_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Cap_Phat_VPP_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await To_Trinh_HD_Thu_Viec_Export_Process(new Dictionary<string, string>(), baseSignature);
            // await Yeu_Cau_Su_Dung_Tai_San_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        [TestMethod]
        public async Task Don_Xin_Nghi_Phep_Export_Test()
        {
            var baseSignature = new Dictionary<string, string>
            {
                {"dean_image_sign", "test3.png"},
                {"user_image_sign", "test3.png"}
            };
            await Don_Xin_Nghi_Phep_Export_Process(new Dictionary<string, string>(), baseSignature);
        }

        private async Task Complex_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\complex-action-template-input.docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\complex-action-template-output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature,
                ExportTables = new List<ExportTable>
                {
                    new ExportTable
                    {
                        ItemNumber = 1,
                        ItemName = "Bút bi Thiên Long",
                        ItemUnit = "Chiếc",
                        Total = 100
                    },
                    new ExportTable
                    {
                        ItemNumber = 2,
                        ItemName = "Giấy in A4",
                        ItemUnit = "Thếp",
                        Total = 50
                    },
                    new ExportTable
                    {
                        ItemNumber = 3,
                        ItemName = "Bút viết bảng Thiên Long",
                        ItemUnit = "Chiếc",
                        Total = 20
                    }
                }
            };
            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task Don_Xin_Nghi_Phep_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Update_Đơn_xin_nghỉ_phép.docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Update_Đơn_xin_nghỉ_phép-output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            baseBody.Add("user_name", "Vũ Xuân Thành");
            baseBody.Add("user_code", "");
            baseBody.Add("position", "Administrator");
            baseBody.Add("contract_type", "Học việc");
            baseBody.Add("room", "11.FSB HN MD - IT");
            baseBody.Add("parent_room", "FSB HN");
            baseBody.Add("parent_user", "Nguyễn Quang Duy");
            baseBody.Add("create_date", "15/10/2019");
            baseBody.Add("from-absent-session", "morning");
            baseBody.Add("from_absent_date", "24/10/2019");
            baseBody.Add("to-absent-session", "afternoon");
            baseBody.Add("to_absent_date", "27/10/2019");
            baseBody.Add("total_absent", "4.0");
            baseBody.Add("leave_type", "0");
            baseBody.Add("reason_absent", "dasdsadsa");
            baseBody.Add("orther_nogotiation", "dsadsadasdsad");
            baseBody.Add("FORM_NAME", "ABSENT");
            baseBody.Add("itemNumber1", "1");
            baseBody.Add("absent_type1", "Nghỉ phép");
            baseBody.Add("time1", "");
            baseBody.Add("accumulated1", "");
            baseBody.Add("itemNumber2", "2");
            baseBody.Add("absent_type2", "Nghỉ kết hôn");
            baseBody.Add("time2", "");
            baseBody.Add("accumulated2", "");
            baseBody.Add("itemNumber3", "3");
            baseBody.Add("absent_type3", "Nghỉ con kết hôn");
            baseBody.Add("time3", "");
            baseBody.Add("accumulated3", "");
            baseBody.Add("itemNumber4", "4");
            baseBody.Add("absent_type4", "Nghỉ khám thai");
            baseBody.Add("time4", "");
            baseBody.Add("accumulated4", "");
            baseBody.Add("itemNumber5", "5");
            baseBody.Add("absent_type5", "Nghỉ sinh con");
            baseBody.Add("time5", "");
            baseBody.Add("accumulated5", "");
            baseBody.Add("itemNumber6", "6");
            baseBody.Add("absent_type6", "Nghỉ sảy thai");
            baseBody.Add("time6", "");
            baseBody.Add("accumulated6", "");
            baseBody.Add("itemNumber7", "7");
            baseBody.Add("absent_type7", "Nghỉ ốm");
            baseBody.Add("time7", "");
            baseBody.Add("accumulated7", "");
            baseBody.Add("itemNumber8", "8");
            baseBody.Add("absent_type8", "Nghỉ chăm sóc con ốm");
            baseBody.Add("time8", "");
            baseBody.Add("accumulated8", "");
            baseBody.Add("itemNumber9", "9");
            baseBody.Add("absent_type9", "Nghỉ tang");
            baseBody.Add("time9", "");
            baseBody.Add("accumulated9", "");
            baseBody.Add("itemNumber10", "10");
            baseBody.Add("absent_type10", "Nghỉ khác [3]");
            baseBody.Add("time10", "");
            baseBody.Add("accumulated10", "");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "test-capcha-1.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToLongVnDateString() },
                {"reviewer_step_1_sign_img", "test-capcha-1.png" },
                {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToLongVnDateString() },
                {"reviewer_step_2_sign_img", "blank_signature.png" },
                {"reviewer_step_2_sign_name", "Lê Linh Tinh" },
                {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() },
                {"reviewer_step_3_sign_img", "test-capcha-1.png" },
                {"reviewer_step_3_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_3_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() }
            };
            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature
            };

            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task Yeu_Cau_Tuyen_Dung_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Yêu_cầu_tuyển_dụng.docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Yêu_cầu_tuyển_dụng_output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";

            baseBody.Add("user_fullname","Hoàng Thị Mai");
            baseBody.Add("role_name","Cán bộ tuyển sinh");
            baseBody.Add("number_person","2");
            baseBody.Add("room_description","HN – Tuyển sinh MBA");
            baseBody.Add("date_start","16/12/2019");
            baseBody.Add("reason","Bổ sung nhân sự thử việc nghỉ việc");
            baseBody.Add("degree","-\tTốt nghiệp đại học trở lên\r\nƯu tiên tốt nghiệp chuyên ngành QTKD, Marketing. \r\n-\tCó kinh nghiệm ít nhất 1 năm kinh nghiệm tư vấn cho khách hàng qua điện thoại\r\n");
            baseBody.Add("qualities_other_priority_conditions","");
            baseBody.Add("qualities","- Yêu cầu: nhanh nhẹn, tự tin, giao tiếp tốt, nhiệt huyết, biết lắng nghe khách hàng, có kinh nghiệm làm sales.\r\n- Biết lên kế hoạch cho các chiến dịch tuyển sinh.\r\n");
            baseBody.Add("c1","normal");
            baseBody.Add("work_description","1. Tham gia hoạch định kế hoạch hoạt động tuyển sinh của phòng\r\n2. Tư vấn tuyển sinh cho khách hàng mục tiêu thông qua các kênh giao tiếp trực tiếp, qua điện thoại, facebook, fanpage...\r\n3. Tìm kiếm học viên tiềm năng, tư vấn và chốt học viên. \r\n4. Phối hợp tổ chức các hoạt động quảng bá hình ảnh Viện, chương trình MBA\r\n5. Các công việc khác theo sự phân công của cán bộ quản lý.\r\n");
            baseBody.Add("salary","7200000");
            baseBody.Add("direct_manager_fullname","Nguyễn Thị Thu Hà");
            baseBody.Add("current_datetime","25/11/2019");
            baseBody.Add("VerifyCaptchaCode","ACC6");
            baseBody.Add("CaptchaCode","ACC6");
            baseBody.Add("Id","");
            baseBody.Add("WorkflowId","1");
            baseBody.Add("FormTemplateId","1");
            baseBody.Add("Status","Inprogress");
            baseBody.Add("CurrentStepId","0");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "test-capcha-1.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToLongVnDateString() },
                {"reviewer_step_1_sign_img", "test-capcha-1.png" },
                {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToLongVnDateString() },
                {"reviewer_step_2_sign_img", "test-capcha-1.png" },
                {"reviewer_step_2_sign_name", "Lê Linh Tinh" },
                {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() },
                {"reviewer_step_3_sign_img", "test-capcha-1.png" },
                {"reviewer_step_3_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_3_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() }
            };

            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature,
                AllComments = "Test"
            };
            var document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task De_Nghi_Ky_Hop_Dong_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Đề_nghị_ký_hợp_đồng.docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Đề_nghị_ký_hợp_đồng_output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            baseBody.Add("name", "Vũ Xuân Thành");
            baseBody.Add("room_description", "11.FSB HN MD - PR");
            baseBody.Add("purpose_user", "Id pariatur Quia facere sint placeat voluptatibus laboris");
            baseBody.Add("purpose_user_dob", "");
            baseBody.Add("purpose_room_description", "11.FSB HN MD - CSD");
            baseBody.Add("purpose_user_position", "Sequi et dolore sequi id odit enim aspernatur a dolores anim aut est nihil quo nulla consequuntur reprehenderit eum veniam");
            baseBody.Add("iq", "63");
            baseBody.Add("gmat", "24");
            baseBody.Add("eng", "43");
            baseBody.Add("pr", "100");
            baseBody.Add("contract_type", "htthang");
            baseBody.Add("htfrom", "18/03/2020");
            baseBody.Add("htto", "18/03/2020");
            baseBody.Add("htsalary", "37");
            baseBody.Add("htlevel", "Possimus dolore rep");
            baseBody.Add("htwork", "Commodo aut deleniti");
            baseBody.Add("result", "Sed dolore nostrud a");
            baseBody.Add("gpa", "Veritatis autem mole");
            baseBody.Add("next_contract_type", "Chọn loại hợp đồng");
            baseBody.Add("confrom", "");
            baseBody.Add("conto", "");
            baseBody.Add("consalary", "");
            baseBody.Add("conlevel", "");
            baseBody.Add("conwork", "");
            baseBody.Add("textarea-,82831794466", "");
            baseBody.Add("VerifyCaptchaCode", "m1usm");
            baseBody.Add("CaptchaCode", "m1usm");
            baseBody.Add("Id", "198");
            baseBody.Add("WorkflowId", "4");
            baseBody.Add("FormTemplateId", "4");
            baseBody.Add("Status", "Approved");
            baseBody.Add("CurrentStepId", "445");
            baseBody.Add("Comment", "ok ok");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "test-capcha-1.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToLongVnDateString() },
                {"reviewer_step_1_sign_img", "test-capcha-1.png" },
                {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToLongVnDateString() },
                {"reviewer_step_2_sign_img", "blank_signature.png" },
                {"reviewer_step_2_sign_name", "Lê Linh Tinh" },
                {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() },
                {"reviewer_step_3_sign_img", "test-capcha-1.png" },
                {"reviewer_step_3_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_3_sign_date", DateTime.Now.AddDays(4).ToLongVnDateString() }
            };

            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature,
                AllComments = "this is comment"
            };

            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task Cap_Phat_VPP_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Phiếu_yêu_cầu_cấp_phát_VPP.docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Phiếu_yêu_cầu_cấp_phát_VPP_Output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            baseBody.Add("roomName", "test");
            baseBody.Add("location", "test");
            baseBody.Add("createDate", "test");
            baseBody.Add("manager_image_sign", "test");
            baseBody.Add("user_image_sign", "test");
            baseBody.Add("manager_sign_name", "test");
            baseBody.Add("book_stationery_directManager_sign_date", "test");
            baseBody.Add("user_sign_name", "test");
            baseBody.Add("create_date", "test3.png");
            baseBody.Add("namecomment0", "test");
            baseBody.Add("datecoment0", "test");
            baseBody.Add("commentcontent0", "test");
            baseBody.Add("tableContent", "[{\"itemNumber\":\"1\",\"itemName\":\"Bút chì\",\"unitOfItem\":\"Chiếc\",\"quantity\":\"100\",\"note\":\"Số lượng có hạn\"},{\"itemNumber\":\"2\",\"itemName\":\"Bút máy\",\"unitOfItem\":\"Chiếc\",\"quantity\":\"200\",\"note\":\"Loại chất lượng tốt, không bị tõe ngòi\"}]");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "vt-signature.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToString("D") },
                {"reviewer_step_1_sign_img", "hr-signature.png" },
                {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToString("D") },
                {"reviewer_step_2_sign_img", "dean-signature.png" },
                {"reviewer_step_2_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToString("D") },
            };

            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature
            };

            if (baseBody.ContainsKey("tableContent"))
            {
                exportModel.ExportTables = baseBody["tableContent"].JsonDeserialize<List<ExportTable>>();
            }


            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task To_Trinh_HD_Thu_Viec_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Tờ_trình_(HĐ_thử_việc_CTV_đặc_biệt_từ_12_tr_trở_lên).docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Tờ_trình_(HĐ_thử_việc_CTV_đặc_biệt_từ_12_tr_trở_lên)_Output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            baseBody.Add("room_name", "1.FSB HN MD - IT");
            baseBody.Add("user_fullname", "Vũ Xuân Thành");
            baseBody.Add("number_person", "712");
            baseBody.Add("reason", "Quaerat est do conse");
            baseBody.Add("suggest", "Eiusmod recusandae ");
            baseBody.Add("start_time", "18/12/2019");
            baseBody.Add("end_time", "21/02/2020");
            baseBody.Add("salary", "Et quis quae nesciun");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "vt-signature.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToString("D") },
                {"reviewer_step_1_sign_img", "dean-signature.png" },
                {"reviewer_step_1_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(4).ToString("D") },
            };

            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature
            };

            if (baseBody.ContainsKey("tableContent"))
            {
                exportModel.ExportTables = baseBody["tableContent"].JsonDeserialize<List<ExportTable>>();
            }

            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task To_Trinh_Cong_Tac_NH_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
            var viewPath = $"{Path.Combine(basePath, @"FileInputs\Tờ_trình_(HĐ_thử_việc_CTV_đặc_biệt_từ_12_tr_trở_lên).docx")}";
            var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Tờ_trình_(HĐ_thử_việc_CTV_đặc_biệt_từ_12_tr_trở_lên)_Output.docx")}";
            //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
            var imageDirectory = $"{Path.Combine(basePath, "Images")}";
            baseBody.Add("room_name", "1.FSB HN MD - IT");
            baseBody.Add("user_fullname", "Vũ Xuân Thành");
            baseBody.Add("number_person", "712");
            baseBody.Add("reason", "Quaerat est do conse");
            baseBody.Add("suggest", "Eiusmod recusandae ");
            baseBody.Add("start_time", "18/12/2019");
            baseBody.Add("end_time", "21/02/2020");
            baseBody.Add("salary", "Et quis quae nesciun");

            baseSignature = new Dictionary<string, string>
            {
                {"requester_sign_img", "vt-signature.png" },
                {"requester_sign_name", "Vũ Xuân Thành" },
                {"requester_sign_date", DateTime.Now.ToString("D") },
                {"reviewer_step_1_sign_img", "hr-signature.png" },
                {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToString("D") },
                {"reviewer_step_2_sign_img", "dean-signature.png" },
                {"reviewer_step_2_sign_name", "Lê Giám Đốc" },
                {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToString("D") },
            };

            var exportModel = new ExportModel
            {
                Bodys = baseBody,
                Signatures = baseSignature
            };

            if (baseBody.ContainsKey("tableContent"))
            {
                exportModel.ExportTables = baseBody["tableContent"].JsonDeserialize<List<ExportTable>>();
            }

            DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
            document.ImageDirectory = imageDirectory;
            document.Generate(documentPath);
            await Task.CompletedTask;
        }

        private async Task Yeu_Cau_Su_Dung_Tai_San_Export_Process(Dictionary<string, string> baseBody, Dictionary<string, string> baseSignature)
        {
            try
            {
                var basePath = @"C:\Works\SideProjects\Fsb.Eform\CMS.Service.Test";
                var viewPath = $"{Path.Combine(basePath, @"FileInputs\Phiếu_yêu_cầu_sử_dụng_tài_sản.docx")}";
                var documentPath = $"{Path.Combine(basePath, @"FileOutputs\Phiếu_yêu_cầu_sử_dụng_tài_sản_Output.docx")}";
                //            var documentPath = $"{BasePath}/Documents/test-insert-image-input.docx";
                var imageDirectory = $"{Path.Combine(basePath, "Images")}";
                baseBody.Add("room_name", "Leandra Ray");
                baseBody.Add("reason", "Fuga Eius hic duis ");
                baseBody.Add("content", "Culpa pariatur Vel ");
                baseBody.Add("end_time", "");
                baseBody.Add("SignatureImage", "iVBORw0KGgoAAAANSUhEUgAAAmYAAADICAYAAABCpS09AAAgAElEQVR4nO3dT6hi5/0G8JOMmdjUTGywqSE2SDGJdIRYMNQSCwZsaosZpFhqqQGhUgy11LZSJAiVChUqxMUhtcWFtEJdnIWhLmyxID9cuLBUigsXDrhw4cKFCxdncRbPbzE9p/eM987cO3OvHvX5wCGTuXf01fvHx+/7vt9XABEREREZgrDvARARERHRAwxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZERERkEAxmRERERAbBYEZ0whRFgaIo+x4GERH9F4MZ0YmaTqew2+2w2+3w+/2Ix+NIp9NIJBJIpVLI5/PodruQZXnfQyUiOhkMZkQnql6vQxCEx15WqxW/+tWvGNCIiHaAwYzoRK1WK8RiMXi93ksFtGg0ivV6ve9hExEdNQYzIsJms8FsNsNoNEKv10Oj0UA4HD43nHFNGp2a9XqNer2Of/3rX/seCp0ABjMiutAvf/lL3Lp1SxfORFHc97CIdma9XuOll17Svv8LhcK+h0RHjsGMiB6pVqvpgpnNZsNms9n3sIh2IpfLbVWOq9XqvodFR4zBjIgeK5VK6V6Y6vX6vodEB2wymWC5XO57GI/V6/W07/lnnnlG+7PJZEKv19v38OhIMZgR0WNNJhNdMEsmk/seEuHB2sDJZIJ+v49ms4lKpYJcLod4PI5wOAyfzwe32621RbFardql/p3L5YLP54Pf70c4HEY8Hkcmk0E+n0exWES1WkWj0UCr1UK73Uav18NoNMJoNMJkMsF0OsV0OsVsNsNqtcJ8PsdkMsFiscB0OkW/30er1UKpVEIoFILZbIYgCLh16xbm8/m+n8Ity+USf/rTnxCJRLSxulwu/PGPf9yqHHMzDN0EBjMieqz79+/rXpRisdi+h3QyVqsVxuMx+v0+isUi0uk0IpEIHA4HTCaT9jUxm81wOBzwer0IBoOIx+NIpVLIZDLI5XIolUool8sol8solUqoVCooFArIZrPIZDJIJBKIRCKIRqPw+Xy6UGe1WmGxWGA2m3X3abFYYLVaIQiCFvI8Hg8EQcCPfvQjeL1eOJ1OZLNZFItFVCoVxONx7d//+9//3vnzOZ1O0ev1UKvVUCqVkMlkEIvFEA6H4Xa7t6YtnU4nptMpACCfz+s+ViqVdj5+On4MZkT0SLIsw+fz8QXphqzXa0ynU3Q6HZTLZWQyGYRCIQSDQbhcLgiCgEAgAKfTCafTiXg8jkKhgEqlglarhfF4jNVqtZPdspvNBrIs6671eq1d6udctAZRFEXteyibzd74eGVZRq/Xw+9//3vE43EtRD7uslgseP/991Eul7du8+zPgsPh4C5lunYMZkR0ocVigVAotPXCZcQpKCNTFAXT6RSSJKFUKiGVSiEYDG5VvdT1S16vF6lUCuVyGZIkYb1eH3yDX0mStKnBWCx2I4FmPp+j3W4jl8shEAhoz+1zzz2nPb8ulwuhUAipVAq5XA75fB6lUgmiKKLdbmMymTzyPtrttu7rxbVmdN0YzIhIZz6fo9PpIJVKnVth+PGPf7zvIRrebDaDKIpIJpOPbODrcDgQCoVQKBTQarUwGo2OcserJEnaYw4Gg9cSMhVFwWg00qZH7Xb7uc+xz+fDxx9/jG63i9Vq9dT3K8uyLkzvovJHp4XBjOiEybKM//znP6hUKgiHw7Db7VsVnLNXJBLZ95ANablcolarIRaLwWaznfvcqdOQlUoF/X7/WkLCIRBFUfueCgaDT7VgfrlcotFoIBqNnvumwWQywe/3I5PJXFsQO8/ZsB0IBG7kPuh0MZgRGcjZNTyr1Qqz2QyTyQTj8Rij0Qjdbhe9Xg/tdhudTgedTge9Xg/D4RDj8fjcF731eo3ZbIbhcIhWq4VyuYxkMgm3261rnPmoy2QyIZPJHPx02nWazWYol8sIBAIXBoR8Pg9Jkg6iNcR1UxQF5XJZC2WBQOCJQpnadT8UCp077RsIBJDL5dDtdndWbTx7KobT6dzJfdLpYDAj2iNZlvHxxx/j9ddf19YbqbvfLhOYHr6eeeYZ3Lp1CyaTCc8//zyeffbZJ7od9bJarchkMtqutFO32WxQq9XODWMOhwPJZBLtdvtkqmEXkWUZiURCV2m96pqy0WiERCKx9bOg7vLcZRB72Pe//31tPG+99dZexkDHi8GMaA8URUGj0bhw2mtXl9VqhdPpxPvvv49EIoFcLodKpYJ2u43xeMwdZ/81HA6RSCRgsVi2QkI+n8dwONz3EA1jPp/rpvrS6fSlK62bzQatVgvBYHAr9OZyOcM8z2+99ZY2tpdeemnfw6Ejw2BGtAeZTAaCIOD555/HO++8g6985Stwu9348pe/jFdeeQWf//znt86ofJLq2dlu5eddHo8HP/vZz9BoNDAej/f9tBjKer2GKIpaXy71slgsSCQS3I13jna7rYVXs9l86aOL5vM5fv3rX2vtQdQrGo1CkiTDvUF44403tDF+8Ytf3Pdw6MgwmBHdMLVVQqfTQaVSwb179yAIAm7fvn2pipbL5UIkEtG29EuShNFohNlshsVigfV6jc1mo7VUeLiPlCzLmM/n6PV6EEURmUwG0WhUa6Z5dhG11WpFMBhEOp1GrVY7ySnMyWSCbDa7tbjc5/OhXq8f5a7JpyXLsvZmQxAeNJvt9/uP/Xe9Xg+RSER7I6FWx4rFoqHX5Z2tdHPxP103BjOia6QoCsbjMURRRDab1R1Bc9F1+/ZtOBwORCIR5HI51Go1dLtdLJfLG68UqM1Ne70estksfD7fVtsBq9WKcDiMYrGIXq93tMfQtNtt3aJutTqWSqVYTXyE6XSqqyqGw+HHrrHrdrtbTYvffvttNBqN3Qz6KciyrJvSjsfj+x4SHRkGM6KnMJvNIEkSMpkMfD7fI1tNOJ1OhMNhJJNJ/OY3v8HPf/5zrWrl8XhQLpcNsWhclmWMx2NUq1Ukk0m4XK6tx+X1epHNZg9+oftyuYQoinA4HLrH53K5IIoid6E+hiiKWkgxmUyoVCqP/PzZbLbVsNjlcqHdbu9oxE9vuVzqxp9Op/c9JDoyDGZEl7TZbDCdTlGr1RCNRi9saGm32xEMBpHL5dBoNB7ZNFRRFPT7fcTjce0cwlgsBkmSDDVltlwutQDq9/t1VUCTyYRgMIhyuXwwU5+z2Qw/+clPdIHMYrEgEolw7dglLBYLxGIxXbh61NTlZrNBsVjUfd9YrVaUy+WDC7+TyUT3814sFvc9JDoyDGZEF1DXZX366acIBoNbOygtFgt8Ph8SiQSq1Sr6/T4Wi8VT3Z8oilorBjUo1Ot1zGaza3xkT2+9XqPb7SKTyWwd/Ox2u1EsFg0X0hRFQa/X0wUKNSDk83nDPcdG1e12dYE2Ho8/cnp7PB5vbaAIh8MHe6xXt9vVPZZ6vb7vIdENmc/nkCQJhUIBqVQK9+7dw5///Ocbv18GMyI8qKA0m02USiXE43HdC88rr7wCs9mMSCSCfD6PZrP52PP0ntZisYAoigiHwzCZTDCZTFrn+Hq9brgXNbXzfTAY1FVF3G43KpXKXhdyy7KMarW6FSD9fj9fVK9AlmXkcjnt+bPZbI9dE1ar1XTfD2azGaIoGm6X5VWcPYhdEAR0u919D4muwWazwXA4RLVaRSwW014DnE4nQqGQ1uB4Fw2FGczo5CyXS3S7XRQKBd0P4NkKitfrRaFQQKfT2XslRZZl9Pt95PN5BAIBbU2P1WpFLBZDuVxGv983zNTner1Go9HQQqU63RmNRtHpdHb2orxer7emzwThQbPTy+wYpP8ZDoe6xfp+v/+RFdHFYrG1lszhcBzF857NZnWPy2iVYbocRVG0ilg0GoXH44HL5UIikYAoirrfqc1mE1arFTabDYPB4MbHxmBGR02WZfR6PVQqFUQikXPXhXm9XqRSKdTrdYzHY8MEnEcZj8doNptIp9O6TQc+nw+5XA69Xs8Q7QZWqxUqlYpuKstms6FQKNxY1W82myGdTusCmbq7ki+iV1ev17VpfJPJhHw+f+G6MEVRdBsCzraUMML343WIRqO69ZWHtkbulK1WK3Q6HZRKJRQKBW0WpNvtnvt1HAwG8Pv9EAQBoVBoZ2/SGczo6AwGAxSLxXNbVTidTsRiMYiiiMFgcDS/VDebDbrdLvL5PILBIBwOB+x2O9xuN9LpNEaj0d6nP8fjMXK5nC4cRyKRC38pXtVwOEQ0GtXtILXZbMjn83t/7IdIURTE43Ht+XQ6nRdWCxRFQbPZ3JouNplMKJVKBz11+TCn06l7U0fGpp4zXK/X4XQ6Ua/X0el0HrmbfDwea0eKORwOtFqtnX4PM5jRwVsul6jX64jFYrqmoGcPOJYk6aDbOlyVLMtYLBaQJAmxWAzhcBiRSAQ2mw3ZbBb1en2v05/9fh/JZFILzk6nE+Vy+YnekU4mE10VQ/1lWq1Wj7bn2k0bDoe6Kf5QKHRuxUuWZYiiuNWxX63eHlv/t/V6rXuM7GFmTMvlEpPJBMlkEvl8Hu12+1JvzkajkfZmxOFwoFKp7OVNBYMZHaTJZIJyuayVmc++GOTzefR6vYOYktwVRVG0I4ai0SicTqe2qcDv9yOTyaDRaGA6ne60iqgoClqtlrZ2zmw2IxgMXmpR/nw+11V01IDH/mNPbrFYIB6Pb7W0OO/zfvGLX+DFF1/cCmQOh+MgGsU+Ce7INCb15JNerwePx6NVxS7zxkxRFHQ6HW03vMvlQq1W2+vvEAYzOhi9Xg/5fH5rvVI8Hkez2TypitjTkmUZg8EAuVwObrdbN+Vrs9kQDodRKBR2WmmcTqeoVCraAdgejweffvrpVtVFURTk83ldILPb7ahUKgxkT0iWZZTLZV1LGL/fv3Vo+EWHuRvlBe2m5fN53WPmFPn+KIqC1WqFbDaLbDaLZrOJWq126Tfkq9UKoijC7XbDZDLB4/EY5g0FgxkZWr/fx+9+9zstjJlMJq3r/DHs8DKK+XyOVquFdDqtW0NztgoSjUZRKpXQ7XZvdIpQPVs0m80iGAzCarXC4/Gg1Wrhs88+002bmc1m5HI5hvInpCgKGo3G1tqwSqWifY3VXbYPV6cF4cFxYl//+tcNedD4TTj7HLjd7n0P5+QoioLJZIJUKoW//OUvkCQJ1Wr1SrcxmUyQTqfhcDi05titVuuGRvxkGMzIUJbLJXq9HjKZDCwWCywWC77yla/A7/ej0Wgczc4uI1N/+VWrVUQika3DvM9WSM42172JsLZerzEYDPCDH/wAt27d2mp7wV2WT67dbuuqzyaTCZFIBPP5XJveiUaj51bH1J21+24ls0vL5VJXpS2VSvse0sno9/solUrIZrOIRCJoNptXqsyu12tIkqSdhWsymZBKpQz75p7BjPZOlmU0m01tcbpaCYnH45detEk35+zZmWe/Rg9fZrMZbrcbyWQS9Xodg8HgWqooo9FIVyX73Oc+p1XxstkshsPhUU+fXbd2u61NF59d3P9///d/aLfbSCaT536NzWYzksnkyTZUPdtY1mQyPdUpH/RosixjOByiWCzCYrEgk8mg0+lc+bVgNBohm81qvz8sFstB7NJmMKO9mUwmSCQS2rtQ9Qew1+txN53Bjcdj1Go1xOPxC4Pa2ZYCZzcXXCWsNRoNXZUikUhgvV5jtVqhWCzC4/HAbDbD5XIhn89jNBrd4KM+bKPRSFvgrF5vvfUWPvzwQ3z7298+tzJmsVgQDofRbDZPYqryUYLBoPa8RKPRfQ/n6KhHpiWTSfh8Pni93ic6f3e5XKLVamnVMXVT2CGdacpgRju3XC6RSqV0i4xbrRarHgdsuVyi0Wggm81uVWMevqxWK6LRKMrlMgaDwYUhPJfL6UL7RTvg1P5o6to4r9erWyNFwP3793VTwbdv396aGj4bxpLJJNrtNnc2/9disdC9QTDqFNih2Ww2WpXW7XbD7XajVCpdecmK2kg8Ho/r3mCkUqkbPz7vJjCY0U7NZjOtrOz1etHr9fY9JLoBm81GO3EhGo2ee+LC2Skyj8eDVCoFSZJw//59pNNp7eNOp/NSlTBFUbS+bep0UywW2+kxUEb1hz/84cLn32KxIBAIoFAoMHBcoFwu695I0pNbr9doNpuIRqOw2Wxwu90oFAoYjUZX+jlVFAXD4RDJZFK31MHj8dzoySK7wGBGO6X+AIXD4ZN/sTw10+kUkiQhnU7D7/dvncpw3vXqq6/i73//+5Xva7Va6Q4udzqdqFarJ1sBkmUZ3/ve9/CNb3wDsVgM2WwWtVrtYI4g27ezO5VPdY3d01DDWDweh9lshs1mQy6X22rHchlqhfztt9/Wvbnw+XyQJOkoZl4YzGhnznbN/uc//7nv4dCebTYbbbfVReeYnt0F+O677+KTTz7Bcrm8UqjvdDqIRCLaNGqxWOQ0J13aYDBgtewJbDYbrTJmNptht9uRSqXQ6/Wu/KZ8NpuhXC5vLZN44YUXkM/nj+6ECQYz2hlZlrXWC5FI5KS22tPjtdtt3Tq0t99+W7eu5+yOOLfbjXg8jm63e+n1KKPRCJFIRDtupdPp3PAjomOQTCa1772Lzgql/1HXepnNZlgsFsRiMUiSdOXbmc1mKJVK8Pv9W78HIpGIYZrB3gQGM9qps1vOBUFAIBBAsVhEv9/HdDrFdDrFZrPBZrM5ipI0Xc50OtWmNm02m7Y+RJZldLtd5HI5fPDBB7h79y5sNpvuF7XVaoXdbkc8HkelUnns4fT9fl+b4kwkEvw+owttNhvtzWQwGNz3cAxrNpuhWCzC5XJpTVsbjcaVpsnVNWOFQmGrMqZ25q9WqyfRy5LBjHau0+mce+ixet26dUvbWaMuDnW5XPB4PAgGg4hGo0gkEkgkEshkMsjlcigWiyiXy6hWq6jVahBFEfV6HZIkQZIktFot7VL/v9lsotlsot1uax+r1+uoVquoVCra7ZTLZe2qVCqoVqtbf6f+vfrfarWKer2uu31JktDpdDCZTDAejzEcDjGfzzGfz3V/Ho/HGI1GmM1mmE6nmEwmGI1G517D4fDcazAYaNdwOES/39eusx9Xb2c8HmM6nWI2m2l/nkwmmEwm2p/V8aifN51OMZ/PMZvNMJvNMJ/PsVqtsFwusV6vsV6voSiKFrQVRdH+X1EUyLKM1WqFzWaDr371q9ov4MdVJTabDVarFbrdLqrVKgqFAkKhECwWixbYbt26hWg0emHoUhRF2xkci8W43pHO1Wg0Lv19eWpkWYYkSQiFQhCEB30Fi8XilYKT2h9R3Qjw8GuB2+1GsVg8yJ2VT4PBjPZCURR0u13EYrFz+ycd82WxWOBwOCAIAn74wx8iHA7DbDYjHA4jGAzC6XTCYrHA4/HA7XbDZrNppyA8fKnTBTabDWazWbvUA8pNJtPWIvuzHz/vz+p/z7sf9b6sVitsNhtcLhd8Ph8ikQgCgQBSqRQKhQJ8Ph/C4TDS6TTS6TQCgQA+++wzdDodhEIh1Go1FAoFxONx+Hw+bWzvvvuutkMzmUwiFoshkUggFoshlUohEAggHo8jEokgHA4jFAohFArB5/PB7Xbjzp07uoD/uOnyTCYDQRCQyWR29J1Ph+S9997Tps7ogeVyiUqlolXHotHopTZEnG1UHY1Gzz1RxGQyIRAIoFwun/RSFwYz2rvNZoPRaIRKpYIPPvgAb7zxBlwulxYy9h2kzv7SUEPV2epdMplEJpNBNptFsVhEtVpFo9GAJEno9XpaRWo0GmGxWECWZa2KRA/6lanP72effQZZliHLMpbLJVarFdbrtVbJ63Q6kCQJjUYDoihqVU1RFLUqZ7PZRK/Xu9T5mbIs4+WXX8YXvvCFHTxSOiSLxUL72b9///6+h7N3ZxuC2+32x/YbUxvGFotFhMPhC9+Au91urVUON+U8wGBGhqUGGFmWsVgsMBqNMBgM0O/30W63Ua/XtRfkYrGIfD6PbDaLTCaDdDqNZDKJeDyOWCyGWCyGaDSqBal0Oo1UKoVsNot8Pq+bBm02m2i1WhgOh9o03imsa9iXXq+n+0Vtt9uRSCRQqVTQ6XQwnU5v5JDy8XiMfD4Pi8UCp9N57bdPh01d9J9IJPY9lL3q9Xraphmfz/fIZuCz2QzValWbBXg4hJnNZkQiEeRyOUiSdCM/18eAwYyI9q7Vaj32aCer1QqXy6VVLEOhEAKBAL773e8im82iVCqhUCigWCxqawyr1SpKpRLK5TKy2SxisRh8Pp/u3bvb7eZRTqQznU61Kf4n6bV16NT1Y2q/wXg8fuHzMBgMUCqVdEsS1Mvj8Whn547HY67lvCQGMyIyBFmW0W63kclk4PV6HxvUnnQ62mazIRwOI5/P8+QJOpe69jAQCOx7KDslyzIajQa8Xi/MZjMymczWWZXq8UelUknXe9BqtcLv9yObzaLb7bIa9hQYzIjIkNRdm6PRSLeTtlKpaJWxn/70p8hkMiiVStoOzUqlou2ybbfb6Pf7mM1mWCwWXNdHjzWbzbSK6ql0+VcDmbq2t1gs6n5WZrMZ6vU6YrGYtvb3S1/6knb8Ub/f5/qwa8RgRkRE9F+JRAKCcBpd/tfrNWq1GlwuF+x2O0RRBPAgqA2HQ5RKJXg8Hm29mHr+bKvVOuldkzeNwYyIiAj/W1smCMJRH+i+Xq9RrVa1NjO1Wg2bzQatVgvJZFI3RWmz2RCPx7lYf4cYzIiIiABEo1EIgoBwOLzvodyYUqkEp9OJYDCotZsJBoO6XZROpxO5XA79fp8nY+wBgxkREZ28brerTdcd26HYi8UCrVYLDocD3/rWt/DRRx/hm9/8pu5oM6/Xi1KptLXYn3aPwYyIiE6aLMu681OPSS6XQzQaxd27d+F2u7UwZjabEQwGUa1WsVgs9j1MOoPBjIiITlqpVNLWUx1DM+nFYoHf/va3eOGFF2Cz2XDr1i2tMhYKhdBoNLiL0sAYzIiI6GQNh0OtiqTuSjxUy+US0WgUL7zwgvaYLBYLwuEw6vU6F+8fCAYzIiI6Sev1WpvCPORmsv/4xz/w3nvvaZUxi8WCWCyGZrPJytgBYjAjIqKTlMvltPVW8/l838O5kslkgnK5jDt37uCZZ57Bs88+izfffJOVsSPAYEZERCen2+1q033NZnPfw7mUzWaDWq0Gv9+vO2rs3XffxWAw2Pfw6JowmBER0UlZr9dwOBwHswtzOp0ilUppvcaee+45CIKAu3fvHnUj3FPFYEZERCdFbSTr8XiufH7qrtZsKYqCdruNUCikVcbu3LmjrSETRRGKouxkLLRbDGZERHQyRFGEIAiwWq1XOu9RlmVtCjGZTN7Y+BaLBYrFolbRM5lMuHv3Lm7fvg1BEBCLxdh37MgxmBER0UmYz+ew2WwQBAGNRuNK/7bT6ejWdV1nh/z1eo1Go6GrjlksFty7dw9vvvmmdkySJEnXdp9kXAxmRER0EtQpzCdZV7ZcLnXnSebz+acay3w+R7Vaxb1793SBLxgM4pNPPsFHH32k/V0mk7nylCsdLgYzIiI6eo1GA4IgwOFwPPHB3LFYTBeiLlPBWi6XGI1G6HQ6EEURyWRS650mCAJu3bqF1157DeVyGYvFAr1eD3a7XauScbfl6WEwIyKio7ZarWCxWGA2m59qF+NkMoHFYtGFs2QyqeuBNp1OUSqVEAgE8PLLL+s+9+zldDqRy+XQ7XYBPFjDlkwmtY+n02lWyU4UgxkRER01NfBks9mnvq12u631P1OvZ599Fl/72tfg8/kuDGIOhwPRaBSVSgXj8Vh3m4PBAE6nE4IgwG63o9PpPPU46XAxmBER0dHqdrswm83wer1PPIX5sH6/D4/Hc2EIUytiH374If76179eeDC6LMvI5XJa0ItEIkdxiDo9HQYzIiI6SoqiwO/3w2w2a1OG10WWZdRqNW2Xp3pFo9FLTZeORiN4vV6tJUa1Wr3W8dHhYjAjIqKj9Le//Q137txBOp2+sftQFAWLxQKj0ejSZ1SWy2Vth6fX68VkMrmx8dHhYTAjIqKjox679Prrr+97KJrZbIZAIKBVydgGg87DYEZEREcnk8lAEARDnCWpKApEUdSqZE6n89qnVul4MJgREdFR6Xa72nqvfZvP5wgGg9oatFQqdekpTzpNDGZERHQ0NpsNrFYr7Hb7tR6b9CTK5bK249LhcLANBl0KgxkRER0NtTt/rVbb2xgGg4Fux2U6nWaVjC6NwYyIiI5CtVrVzpvch8VigWQyqa0lc7vdGA6HexkLHS4GMyIiOnjj8Rgmkwl2u113RNIuqIv71Z5mJpMJlUplp2Og48FgRkREB02WZbhcLgiCsPPdjmenLQVBQCwWw2Kx2OkY6LgwmBER0cFSFAWJRAKCICCfz+/sfpfLJWKxmLa4ny0w6LowmBER0cGqVCoQBAE+n28n97dcLlEoFHSBTBTFndw3nZo59moAAANOSURBVAYGMyIiOkjT6RQWiwVWq3Un04eiKGqHl5tMJmSzWR46TteOwYyIiA6OekC5IAiQJOnG7mez2aBer8Nut+P27dswmUxIJBIMZHRjGMyIiOjg1Go1CIKAcDh8I7cvyzLq9bpWIRMEAe+88w7XkdGNYzAjIqKDIssy7HY7zGYzxuPxtd++JElwu91aILPZbBBFEbIsX/t9ET2MwYyIiA5Ko9HQzp28Tv1+X5seZfsL2hcGMyIiOihq37DRaHQttydJEnw+ny6QOZ1OtNvta7l9oqtgMCMiooMxm80gCAI8Hs9T31ar1dI1h1V3W+ZyOZ5tSXvDYEZERAdD7VtWLBaf6N+v12uIoqhbQ6Ze0WgUs9nsmkdMdDUMZkREdDACgQAEQbjSov/NZoN2u414PK4dMH72crvd3G1JhsFgRkREB+O1117Diy+++NjPW61WaLVaiMfjsFgsW2FMDWTNZhOKouxg5ESXw2BGREQHw263w2QyYTKZ6P5+s9lgOByiWCzC5/NpRyY9fJlMJsRiMVbIyLAYzIiI6GDk83kIggCz2YxkMolisYjvfOc7505Rnr28Xi8qlQo79pPhMZgREdHBUBQF6XT6kSFMrYxFIhFUq1X2IaODwmBGREQHZzweo1AoIBQK4dVXX4XdbofP50OhUECv12OXfjpYDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBsFgRkRERGQQDGZEREREBvH/zTNwQU1LEQcAAAAASUVORK5CYII=");
                baseBody.Add("Id", "");
                baseBody.Add("WorkflowId", "3");
                baseBody.Add("Status", "Inprogress");
                baseBody.Add("CurrentStepId", "0");
                baseBody.Add("tableContent", "[{\"itemNumber\":\"1\",\"itemName\":\"ABC\",\"unitOfItem\":\"XYZ\",\"quantity\":\"123\",\"itemType\":\"HEHEHEHE\"}]");
                baseBody.Add("Submit", "Inprogress");

                baseSignature = new Dictionary<string, string>
                {
                    {"requester_sign_img", "vt-signature.png" },
                    {"requester_sign_name", "Vũ Xuân Thành" },
                    {"requester_sign_date", DateTime.Now.ToString("D") },
                    {"reviewer_step_1_sign_img", "hr-signature.png" },
                    {"reviewer_step_1_sign_name", "Nguyễn Hát Rờ" },
                    {"reviewer_step_1_sign_date", DateTime.Now.AddDays(2).ToString("D") },
                    {"reviewer_step_2_sign_img", "dean-signature.png" },
                    {"reviewer_step_2_sign_name", "Lê Giám Đốc" },
                    {"reviewer_step_2_sign_date", DateTime.Now.AddDays(4).ToString("D") },
                };
                var exportModel = new ExportModel
                {
                    Bodys = baseBody,
                    Signatures = baseSignature,
                    AllComments = "this is comment"
                };

                if (baseBody.ContainsKey("tableContent"))
                {
                    exportModel.ExportTables = baseBody["tableContent"].JsonDeserialize<List<ExportTable>>();
                }

                DocumentBase document = DocumentFactory.Create(viewPath, exportModel);
                document.ImageDirectory = imageDirectory;
                document.Generate(documentPath);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.StackTrace);
                throw;
            }
            await Task.CompletedTask;
        }

        [TestMethod]
        public void DateTimeTest()
        {
            var vnDate = DateTime.Now.ToShortVnDateString();
            System.Console.WriteLine(vnDate);

            vnDate = DateTime.Now.ToLongVnDateString();
            System.Console.WriteLine(vnDate);
        }


        [TestMethod]
        public void CapChaTest()
        {
            var capchaGenerator = new CapchaGenerator();
            var capchaImg = capchaGenerator.GenerateCapchaImage("qhtpd", 250, 100);

            Assert.IsNotNull(capchaImg);
            var bytes = Convert.FromBase64String(capchaImg);
            using (var imageFile = new FileStream("test-capcha-1.png", FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
        }
    }
}