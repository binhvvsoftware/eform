﻿using System;

namespace CMS.Core.IoC
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoInjectAttribute : Attribute
    {
    }
}
