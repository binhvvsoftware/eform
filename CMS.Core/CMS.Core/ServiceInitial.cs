using System;

namespace CMS.Core
{
    public class ServiceInitial
    {
        public object Data { get; set; }
        public Guid ServiceId { get; set; }
    }
}