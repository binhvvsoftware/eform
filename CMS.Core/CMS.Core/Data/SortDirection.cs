﻿namespace CMS.Core.Data
{
    public enum SortDirection : byte
    {
        Ascending,
        Descending
    }
}
