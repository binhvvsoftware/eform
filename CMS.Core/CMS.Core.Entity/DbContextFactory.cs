﻿using Autofac;
using Microsoft.EntityFrameworkCore;

namespace CMS.Core.Entity
{
    public interface IDbContextFactory
    {
        DbContext CreateDbContext();
    }

    public class DbContextFactory : IDbContextFactory
    {
        private readonly IComponentContext componentContext;

        public DbContextFactory(IComponentContext componentContext)
        {
            this.componentContext = componentContext;

            //using (var ctx = CreateDbContext())
            //{
            //    //ctx.Database.Migrate();
            //    ctx.Database.EnsureCreated();
            //}
        }

        public DbContext CreateDbContext()
        {
            return componentContext.Resolve<DbContext>();
        }
    }

    public class EntityModule<TDbContext> : Module where TDbContext : DbContext
    {
        private readonly string connectionString;

        public EntityModule(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<DbContextFactory>().As<IDbContextFactory>().SingleInstance().AutoActivate();
            builder.RegisterType<TDbContext>().As<DbContext>().As<TDbContext>().InstancePerDependency();
            builder.RegisterType<EntityQueue>().As<IEntityQueue>().SingleInstance();
            builder.RegisterGeneric(typeof(Repository<,>)).As(typeof(IRepository<,>)).SingleInstance();

            var dbOptionBuilder = new DbContextOptionsBuilder();
            
            //dbOptionBuilder.UseNpgsql(connectionString);
            builder.Register(s => dbOptionBuilder.Options).As<DbContextOptions>().SingleInstance();
        }
    }

    //public class DbContext<T> : DbContext where T : class
    //{
    //    public DbSet<T> DbSet { get; set; }

    //    protected override void OnModelCreating(ModelBuilder modelBuilder)
    //    {
    //        base.OnModelCreating(modelBuilder);
    //        modelBuilder.HasDefaultSchema("dbo");
    //        modelBuilder.HasPostgresExtension("uuid-ossp");
    //    }

    //    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //    //{
    //    //    base.OnConfiguring(optionsBuilder);
    //    //    optionsBuilder.UseNpgsql("Server=localhost;port=5432;Database=GoQuoEngine;User Id=postgres;Password=Admin@123;CommandTimeout=60; Pooling=true;MinPoolSize=1;MaxPoolSize=100;");
    //    //}
    //}
}
