﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CMS.Core.Data;
using Microsoft.EntityFrameworkCore;

namespace CMS.Core.Entity
{
    public class MemoryRepository<T, TKey> : IRepository<T, TKey> where T : class, IBaseEntity<TKey>
    {
        private readonly ConcurrentDictionary<TKey, T> records = new ConcurrentDictionary<TKey, T>();

        #region Count

        public DbContext OpenConnection()
        {
            throw new NotSupportedException();
        }

        public int Count(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate == null)
            {
                return records.Count;
            }

            var func = predicate.Compile();
            return records.Count(x => func(x.Value));
        }

        public Task<int> CountAsync(Expression<Func<T, bool>> predicate = null)
        {
            return Task.FromResult(Count(predicate));
        }

        public long LongCount(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate == null)
            {
                return records.Count;
            }

            var func = predicate.Compile();
            return records.LongCount(x => func(x.Value));
        }

        public Task<long> LongCountAsync(Expression<Func<T, bool>> predicate = null)
        {
            return Task.FromResult(LongCount(predicate));
        }

        #endregion Count

        #region Delete

        public void Delete(T entity)
        {
            records.TryRemove(entity.Id, out _);
        }

        public Task DeleteAsync(T entity)
        {
            return Task.Run(() => { Delete(entity); });
        }

        public void DeleteMany(Expression<Func<T, bool>> filterExpression)
        {
            if (filterExpression == null)
            {
                records.Clear();
                return;
            }

            var func = filterExpression.Compile();
            var removed = new List<TKey>();

            foreach (var entry in records)
            {
                if (func(entry.Value))
                    removed.Add(entry.Key);
            }

            foreach (var id in removed)
            {
                records.TryRemove(id, out _);
            }
        }

        public void DeleteMany(IEnumerable<T> items)
        {
            var ids = items.Select(x => x.Id).ToArray();
            DeleteMany(r => ids.Contains(r.Id));
        }

        public Task DeleteManyAsync(Expression<Func<T, bool>> filterExpression)
        {
            return Task.Run(() => DeleteMany(filterExpression));
        }

        public Task DeleteManyAsync(IEnumerable<T> items)
        {
            return Task.Run(() => DeleteMany(items));
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null, params Expression<Func<T, dynamic>>[] includes)
        {
            if (includes.Length > 0)
                throw new NotSupportedException();

            return Find(filterExpression, findOptions);
        }

        #endregion Delete

        #region Find

        public IEnumerable<T> Find(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null)
        {
            var func = filterExpression?.Compile() ?? (t => true);

            if (findOptions == null)
                findOptions = new FindOptions<T>();

            var skip = findOptions.Skip ?? 0;
            var take = findOptions.Limit ?? records.Count;
            var found = records.Values.Where(func).Skip(skip).Take(take);

            return found;
        }

        public IEnumerable<TProjection> Find<TProjection>(Expression<Func<T, bool>> filterExpression, Expression<Func<T, TProjection>> projection, FindOptions<T> findOptions = null)
        {
            var func = projection.Compile();
            return Find(filterExpression, findOptions).Select(x => func(x));
        }

        public Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null)
        {
            return Task.Run(() => Find(filterExpression, findOptions));
        }

        public Task<IEnumerable<TProjection>> FindAsync<TProjection>(Expression<Func<T, bool>> filterExpression, Expression<Func<T, TProjection>> projection, FindOptions<T> findOptions = null)
        {
            return Task.Run(() => Find(filterExpression, projection, findOptions));
        }

        public T FindOne(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null, params Expression<Func<T, dynamic>>[] includes)
        {
            if (includes == null || includes.Length == 0)
            {
                return FindOne(filterExpression, findOptions);
            }
            throw new NotImplementedException();
        }

        public T FindOne(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null)
        {
            if (filterExpression == null)
                return records.Select(x => x.Value).FirstOrDefault();

            var func = filterExpression.Compile();

            foreach (var entry in records)
            {
                if (func(entry.Value)) return entry.Value;
            }

            return null;
        }

        public Task<T> FindOneAsync(Expression<Func<T, bool>> filterExpression, FindOptions<T> findOptions = null)
        {
            return Task.Run(() => FindOne(filterExpression, findOptions));
        }

        public async Task<TProjection> FindOneAsync<TProjection>(Expression<Func<T, bool>> filterExpression, Expression<Func<T, TProjection>> projection)
        {
            var record = await FindOneAsync(filterExpression);
            if (record == null) return default(TProjection);

            return projection.Compile()(record);
        }

        public T GetById(TKey id)
        {
            if (records.TryGetValue(id, out T e))
                return e;
            return null;
        }

        public Task<T> GetByIdAsync(TKey id)
        {
            return Task.Run(() => GetById(id));
        }

        #endregion Find

        #region Insert

        public void Insert(T entity)
        {
            records.TryAdd(entity.Id, entity);
        }

        public Task InsertAsync(T entity)
        {
            return Task.Run(() => Insert(entity));
        }

        public void InsertMany(IEnumerable<T> items)
        {
            foreach (var item in items)
                Insert(item);
        }

        public Task InsertManyAsync(IEnumerable<T> items)
        {
            return Task.Run(() => InsertMany(items));
        }

        #endregion Insert

        #region Update

        public void Update(T entity)
        {
            records.AddOrUpdate(entity.Id, entity, (k, old) => entity);
        }

        public Task UpdateAsync(T entity)
        {
            return Task.Run(() => Update(entity));
        }

        public void UpdateMany(IEnumerable<T> items)
        {
            foreach (var item in items)
                Update(item);
        }

        public void UpdateMany(Expression<Func<T, bool>> filterExpression, Expression<Func<T, T>> updateExpression)
        {
            var filter = filterExpression?.Compile() ?? (t => true);
            var factory = updateExpression.Compile();

            foreach (var entry in records)
            {
                var r = entry.Value;
                if (!filter(r)) continue;
                Update(factory(r));
            }
        }

        public Task UpdateManyAsync(IEnumerable<T> items)
        {
            return Task.Run(() => UpdateMany(items));
        }

        public Task UpdateManyAsync(Expression<Func<T, bool>> filterExpression, Expression<Func<T, T>> updateExpression)
        {
            return Task.Run(() => UpdateMany(filterExpression, updateExpression));
        }

        #endregion Update
    }
}
