﻿using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace NotificationJobSchduler.Services
{
    public interface INotificationService
    {
        Task ExecuteNotify(DateTime notifyOnDate);
    }

    public class NotificationService : INotificationService
    {
        private readonly AppSettings _appSettings;

        public NotificationService(IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
        }
        public async Task ExecuteNotify(DateTime notifyOnDate)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.ExpectContinue = false;
                httpClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMinutes(1);
                
                using (var response = await httpClient.GetAsync($"{_appSettings.ApiUrl}/sync-working-timesheet/{notifyOnDate.ToString("yyyy-MM-dd")}", CancellationToken.None))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        throw new HttpRequestException(
                            $"BadRequest:{(int) response.StatusCode} {responseContent}");
                    }
                }
            }
        }
    }
}