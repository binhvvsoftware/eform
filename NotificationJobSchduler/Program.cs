﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using NotificationJobSchduler.Services;
using Serilog;

namespace NotificationJobSchduler
{
    public class Program
    {
        private static IConfigurationRoot Configuration { get; set; }

        public static void Main(string[] args)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
                .AddJsonFile("appsettings.json")
                .Build();
            
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .Enrich.FromLogContext()
                .CreateLogger();
            var serviceCollection = new ServiceCollection();
            var serviceProvider = ConfigureServices(serviceCollection);
            var syncService = serviceProvider.GetService<INotificationService>();
            Console.WriteLine($"Working time start sync: {DateTime.Now:G}");
            syncService.ExecuteNotify(DateTime.Now).GetAwaiter().GetResult();
            Console.WriteLine($"Working time end sync: {DateTime.Now:G}");
        }

        static IServiceProvider ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            var builder = new ContainerBuilder();
            builder.RegisterType<NotificationService>().AsImplementedInterfaces().SingleInstance();
            builder.Populate(serviceCollection);

            var container = builder.Build();

            return new AutofacServiceProvider(container);
        }
    }
}