﻿using CMS.Shared.Data.Domain;
using System.Collections.Generic;

namespace CMS.Dashboard.ViewModels.MenuViewModels
{
    public class MenuViewModel
    {
        public List<Menu> Menus { get; set; }

        public List<Menu> MenuChilds { get; set; }
    }
}
