﻿using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.ViewModels.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
