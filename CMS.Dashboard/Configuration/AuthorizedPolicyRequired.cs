﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace CMS.Dashboard.Configuration
{
    public class AuthorizedPolicyRequired : IAuthorizationRequirement
    {
        public AuthorizedPolicyRequired(string permission)
        {
        }

        public string Permission { get; set; }
    }

    public class AuthorizedPolicyHandler : AuthorizationHandler<AuthorizedPolicyRequired>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AuthorizedPolicyRequired requirement)
        {
            if (context.User.IsInRole(requirement.Permission))
                context.Succeed(requirement);
            else
                context.Fail();
            return Task.FromResult(0);
        }
    }
}