﻿using Autofac;
using CMS.Dashboard.Web.UI.RoboUI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using CMS.Dashboard.Models;
using CMS.Services.Services;
using CMS.Dashboard.Controllers.Common;
using CMS.Shared.Data.Domain;
using System.Threading.Tasks;
using CMS.Dashboard.Extensions;
using CMS.Core.Extensions;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("menu")]
    public class MenuController : BaseRoboController<int, Menu, MenuModel>
    {
        private readonly IComponentContext _componentContext;

        private readonly IMenuService service;

        public MenuController(IComponentContext componentContext, IMenuService service) : base(componentContext, service)
        {
            this.service = service;
            this._componentContext = componentContext;
        }

        protected override string Name => "Menu";

        protected override string PluralizedName => "Quản lý menu";

        protected override bool EnableTracking => true;

        protected override MenuModel ConvertToModel(Menu entity)
        {
            return entity;
        }

        protected override string ConfirmDeleteMessage => "Bạn chắc chắn muốn xóa  menu?";

        protected override void OnViewIndex(RoboUIGridResult<Menu> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.ClientId = "tblMenus";
            roboGrid.EnableSearch = true;
            roboGrid.Title = "Quản lý Menus";
            roboGrid.ViewData = ViewData;
            roboGrid.FetchAjaxSource = GetRecords;
            roboGrid.AddColumn(x => x.Name).HasHeaderText(T("Tên menu")).EnableFilter();
            roboGrid.AddColumn(x => x.MenuParentName).HasHeaderText(T("Menu gốc"));
            roboGrid.AddColumn(x => x.Position).HasHeaderText(T("Vị trí")).RenderAsHtml(x => x.Position.ToString()).EnableSorting(true);
            roboGrid.AddColumn(x => x.Icon).HasHeaderText(T("Icon")).RenderAsHtml(x => $"<i title=\"{x.Name}\" class=\"{ x.Icon}\"></i>");
            ViewBag.Layout = "Layout";
        }

        protected override async Task OnCreatingAsync(RoboUIFormResult<MenuModel> roboForm)
        {
            roboForm.RegisterExternalDataSource(x => x.IsViewManage, EnumExtensions.GetValues<MenuAction>().ToSelectList(v => (short)v, t => t.GetDisplayName()));
            roboForm.RegisterExternalDataSource(x => x.Parent, await service.GetRecordsAsync().ToSelectListAsync(k => k.Id, v => v.Name));
        }

        protected override async Task OnEditingAsync(RoboUIFormResult<MenuModel> roboForm)
        {
            roboForm.RegisterExternalDataSource(x => x.IsViewManage, EnumExtensions.GetValues<MenuAction>().ToSelectList(v => (short)v, t => t.GetDisplayName()));
            roboForm.RegisterExternalDataSource(x => x.Parent, await service.GetRecordsAsync(x => x.Id != roboForm.FormModel.Id).ToSelectListAsync(k => k.Id, v => v.Name));
        }

        protected override void ConvertFromModel(MenuModel model, Menu entity)
        {
            entity.Name = model.Name.Trim();
            entity.Url = model.Url.Trim();
            entity.Icon = model.Icon;
            entity.Parent = model.Parent;
            entity.MenuAction = model.IsViewManage;
            entity.Position = model.Position;
            //entity.IsActive = model.IsActive;
        }
    }
}