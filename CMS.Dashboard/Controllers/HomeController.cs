﻿using Autofac;
using CMS.Dashboard.Controllers.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Dashboard.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(IComponentContext componentContext) : base(componentContext)
        {
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "RequestDocument");
        }
    }
}
