﻿using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Extensions;
using CMS.Core.Linq;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Dashboard.Web.UI.RoboUI.Filters;
using CMS.Services.Services;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Logging;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("working-timesheet")]
    public class WorkingTimeController : BaseRoboController<long, WorkingTimesheet, WorkingTimesheetModel>
    {
        private readonly IComponentContext _componentContext;
        private readonly IWorkingTimeSheetService _service;
        private readonly ILogger<WorkingTimeController> _logger;
        private readonly AppSettings _settings;

        public WorkingTimeController(IComponentContext componentContext, IWorkingTimeSheetService service,
            IOptions<AppSettings> options,
            ILogger<WorkingTimeController> logger) : base(
            componentContext, service)
        {
            _componentContext = componentContext;
            _service = service;
            _logger = logger;

            if (options == null)
            {
                options = new OptionsWrapper<AppSettings>(new AppSettings
                {
                    WorkingTime = TimeSpan.Parse("08:30:00")
                });
            }
            _settings = options.Value;
        }

        protected override bool ShowEditModalDialog => false;

        protected override Task<bool> IsEnableCreate() => Task.FromResult(false);

        protected override Task<bool> IsEnableEdit() => Task.FromResult(false);

        protected override Task<bool> IsEnableDelete() => Task.FromResult(false);


        protected override string PluralizedName => "Quản lý thời gian làm việc";

        protected override Task OnViewIndexAsync(RoboUIGridResult<WorkingTimesheet> roboGrid)
        {
            ViewBag.Layout = "Layout";
            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.ClientId = "tblWorkingTimesheets";

            var searchForm = BuildSearchForm(roboGrid, false);

            roboGrid.FilterForm = searchForm;
            roboGrid.EnableSearch = true;
            roboGrid.ViewData = ViewData;
            roboGrid.FetchAjaxSource = GetRecords;

            roboGrid.AddColumn(x => x.Requester)
                .HasHeaderText("Họ và tên")
                .RenderAsHtml(x => x.Requester?.FullName ?? string.Empty);
            roboGrid.AddColumn(x => x.StartTime)
               .RenderAsHtml(x =>
               {
                   var start = DateTime.ParseExact(x.StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                   CultureInfo.CurrentCulture = new CultureInfo("vi-VN", false);
                   return start.ToString("dddd, dd MMM yyyy, HH:mm:ss");
               })
               .HasHeaderText("Thời gian đến").EnableSorting();
            roboGrid.AddColumn(x => x.TotalLateTime)
                .HasHeaderText("Số phút đi muộn");
            roboGrid.AddColumn(x => x.ConfirmStatusString)
                .RenderAsHtml(x => x.ConfirmStatusString)
                .HasHeaderText("Trạng thái duyệt");
            roboGrid.AddColumn(x => x.PenaltyRule)
                .RenderAsHtml(x => x.PenaltyRule)
                .HasHeaderText("Mức phạt");
            return Task.CompletedTask;
        }

        private RoboUIFormResult BuildSearchForm(RoboUIGridResult<WorkingTimesheet> roboGrid, bool includeStatus)
        {
            var searchForm = new RoboUIFormResult(ControllerContext)
            {

                ViewData = ViewData,
                ShowCancelButton = false,
                ShowSubmitButton = false,
            };

            var headerText = includeStatus ? "Thời gian đi làm của CBNV" : "Thời gian đi làm của tôi";

            searchForm.AddProperty("lblHeader",
                new RoboDivAttribute
                {
                    HasLabelControl = false, ContainerRowIndex = 0
                },
                $"<div class=\"form-group col-md-12 col-sm-12\"><fieldset><legend>{headerText}</legend></fieldset></div>");

            searchForm.AddProperty("FromDate",
                new RoboDatePickerAttribute
                {
                    LabelText = "Từ ngày",
                    ContainerCssClass = "col-md-3 col-xs-6",
                    ContainerRowIndex = 1
                });

            searchForm.AddProperty("ToDate",
                new RoboDatePickerAttribute
                {
                    LabelText = "Tới ngày",
                    ParentStartDateControl = "FromDate",
                    ContainerCssClass = "col-md-3 col-xs-6",
                    ContainerRowIndex = 1
                });

            if (includeStatus)
            {
                searchForm.AddProperty("Status",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Trạng thái",
                    ContainerCssClass = "col-md-2 col-xs-2",
                    ContainerRowIndex = 1,
                    OptionLabel = "-=Trạng thái=-",
                    AllowEmptyOption = false,
                    SelectListItems = EnumExtensions.GetValues<ConfirmStatus>()
                        .Where(x => x == ConfirmStatus.Inprogress || x == ConfirmStatus.Expired)
                        .ToSelectList(v => v, t => t.GetDisplayName()),
                }, ConfirmStatus.Inprogress);

                roboGrid.AddCustomVariable("Status", "$('#Status').val();", true);
            }

            searchForm.AddProperty("ButtonSearch",
                new RoboButtonAttribute
                {
                    OnClick = roboGrid.GetReloadClientScript(),
                    ContainerCssClass = "col-md-1 col-xs-3",
                    ContainerRowIndex = 1,
                    LabelText = "Tìm kiếm",
                    HideLabelControl = true,
                    HasLabelControl = true,
                    HtmlAttributes = new Dictionary<string, object> { { "class", "btn btn-primary show" } }
                });

            roboGrid.AddCustomVariable("FromDate", "$('#FromDate').val();", true);
            roboGrid.AddCustomVariable("ToDate", "$('#ToDate').val();", true);

            return searchForm;
        }

        [Route("review-working-times")]
        public async Task<IActionResult> WorkingLateApprovementList()
        {
            ViewBag.Layout = "Layout";

            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T("Duyệt xin phép đi muộn"));

            var currentUser = await WorkContext.GetCurrentUser();

            var userRelationService = _componentContext.Resolve<IMemberRelationService>();

            var roboGrid = new RoboUIGridResult<WorkingTimesheet>(ControllerContext)
            {
                Title = "Duyệt xin phép đi muộn",
                ViewData = ViewData,
                FetchAjaxSource = options => GetUsersWorkLates(options, currentUser.Id)
            };

            roboGrid.FilterForm = BuildSearchForm(roboGrid, true);
            roboGrid.EnableSearch = true;

            roboGrid.AddColumn(x => x.Requester)
                .HasHeaderText("Họ và tên")
                .RenderAsHtml(x => x.Requester?.FullName ?? string.Empty)
                .EnableSorting();
            roboGrid.AddColumn(x => x.ActualStartTime)
                .HasHeaderText("Thời gian đến").EnableSorting();
            roboGrid.AddColumn(x => x.TotalLateTime).HasHeaderText("Số phút đi muộn");
            roboGrid.AddColumn(x => x.ConfirmStatusString).RenderAsHtml(x => x.ConfirmStatusString).HasHeaderText("Trạng thái duyệt");
            roboGrid.AddColumn(x => x.PenaltyRule).RenderAsHtml(x => x.PenaltyRule).HasHeaderText("Mức phạt");
            roboGrid.AddRowAction()
                .HasUrl(x => Url.Action("ReviewWorkLateRequestNew", new { timesheetId = x.Id }))
                .HasAttribute("title", "Xác nhận đi muộn")
                .HasText("Xác nhận")
                .HasButtonSize(ButtonSize.ExtraSmall)
                .HasButtonStyle(ButtonStyle.Primary)
                .EnableWhen(x => x.CanApproval)
                .ShowModalDialog();

            return roboGrid;

        }

        [Themed(false)]
        [HttpGet, Route("new-work-late-request-detail/{timesheetId}")]
        public async Task<IActionResult> ReviewWorkLateRequestNew(int timesheetId)
        {
            var timesheetRecord = await Service.GetRecordAsync(x => x.Id == timesheetId, null, i => i.Requester, i => i.LateRequest);

            if (timesheetRecord == null || timesheetRecord.LateRequest == null)
                return new AjaxResult().Alert("Có lỗi trong quá trình duyệt đơn").CloseModalDialog();

            var model = new WorkingLateRequestModel
            {
                ActualWorkingTime = timesheetRecord.StartTime,
                CurrentStatus = timesheetRecord.LateRequest.ConfirmedStatus,
                CreateDate = timesheetRecord.LateRequest.CreateDate,
                WorkingDate = timesheetRecord.WorkingDate,
                Requester = timesheetRecord.Requester,
                RequesterId = timesheetRecord.UserId,
                ManagerIds = timesheetRecord.LateRequest.ManagerIds,
                UserFullName = timesheetRecord.Requester.FullName,
                TimeSheetId = timesheetRecord.Id,
                Id = timesheetRecord.LateRequest.Id
            };

            var roboForm = new RoboUIFormResult<WorkingLateRequestModel>(model, ControllerContext)
            {
                Title = "Xác nhận đi làm muộn của nhân viên",
                ViewData = ViewData,
                FormActionUrl = Url.Action("ConfirmLateRequest"),
                FormMethod = FormMethod.Post,
                ShowSubmitButton = false,
                ShowCloseButton = true,
                ShowCancelButton = false,
                ReadOnly = true,
            };

            roboForm.AddAction(true)
                .HasText("Từ chối")
                .HasAttribute("formaction", Url.Action("RejectedLateRequest"))
                .HasButtonSize(ButtonSize.Default)
                .HasButtonStyle(ButtonStyle.Danger)
                .HasName("BtnRejected")
                .HasValue("Reject")
                .HasConfirmMessage("Bạn đã lựa chọn từ chối xác nhận ?");

            roboForm.AddAction(true)
                .HasText("Xác nhận quên điểm danh")
                .HasAttribute("formaction", Url.Action("ApproveWithReasonLateRequest"))
                .HasButtonSize(ButtonSize.Default)
                .HasButtonStyle(ButtonStyle.Warning)
                .HasName("BtnApproveWithReason")
                .HasValue("ApproveWithReason")
                .HasConfirmMessage("Bạn đã lựa chọn xác nhận quên điểm danh ?");

            roboForm.AddAction(true).HasText("Xác nhận")
                .HasAttribute("formaction", Url.Action("ApprovedLateRequest"))
                .HasButtonSize(ButtonSize.Default)
                .HasButtonStyle(ButtonStyle.Success)
                .HasName("BtnApproved")
                .HasValue("Approved")
                .HasConfirmMessage("Bạn đã lựa chọn đồng ý xác nhận ?");

            return roboForm;
        }

        [HttpPost, Route("approved-late-request")]
        [FormButton("BtnApproved")]
        public async Task<IActionResult> ApprovedLateRequest(WorkingLateRequestModel model)
        {
            try
            {
                var lateService = _componentContext.Resolve<IWorkingLateService>();

                await lateService.UpdateRequestStatus(model.Id, ConfirmStatus.Approved);

                return new AjaxResult().Alert("Đã xác nhận yêu cầu duyệt đi muộn.").CloseModalDialog().Reload(true);

            }
            catch (System.Exception e)
            {
                _logger.Error("WorkongTimeController.ApprovedLateRequest", e);
                throw e;
            }
        }

        [HttpPost, Route("rejected-late-request")]
        [FormButton("BtnRejected")]
        public async Task<IActionResult> RejectedLateRequest(WorkingLateRequestModel model)
        {
            try
            {
                var lateService = _componentContext.Resolve<IWorkingLateService>();

                await lateService.UpdateRequestStatus(model.Id, ConfirmStatus.Rejected);

                return new AjaxResult().Alert("Đã từ chối yêu cầu duyệt đi muộn.").CloseModalDialog().Reload(true);

            }
            catch (System.Exception e)
            {
                _logger.Error("WorkongTimeController.RejectedLateRequest", e);
                throw e;
            }
        }

        [HttpPost, Route("approve-with-reason-late-request")]
        [FormButton("BtnApproveWithReason")]
        public async Task<IActionResult> ApproveWithReasonLateRequest(WorkingLateRequestModel model)
        {
            try
            {
                var lateService = _componentContext.Resolve<IWorkingLateService>();

                await lateService.UpdateRequestStatus(model.Id, ConfirmStatus.ApproveWithReason);

                return new AjaxResult().Alert("Đã xác nhận quên điểm danh yêu cầu duyệt đi muộn.").CloseModalDialog().Reload(true);

            }
            catch (System.Exception e)
            {
                _logger.Error("WorkongTimeController.RejectedLateRequest", e);
                throw e;
            }
        }

        protected async Task<RoboUIGridAjaxData<WorkingTimesheet>> GetUsersWorkLates(RoboUIGridRequest options, int userId)
        {
            var userRelationService = _componentContext.Resolve<IMemberRelationService>();

            var userIds = await userRelationService.GetRecordsAsync(x => x.ManagerId == userId, projection => projection.UserId);

            if (userIds.IsNullOrEmpty())
                return new RoboUIGridAjaxData<WorkingTimesheet>(Array.Empty<WorkingTimesheet>());

            var filterExpression = PredicateBuilder.True<WorkingTimesheet>();

            if (!userIds.IsNullOrEmpty())
            {
                filterExpression = filterExpression.And(x => userIds.Contains(x.UserId));
            }

            var records = await BuildAndGetCommonRecords(filterExpression);

            return new RoboUIGridAjaxData<WorkingTimesheet>(records.Take(options.PageSize).Skip(options.PageIndex - 1 * options.PageSize), records.Count);
        }

        private async Task<ICollection<WorkingTimesheet>> BuildAndGetCommonRecords(Expression<Func<WorkingTimesheet, bool>> filterExpression)
        {
            var currentDate = DateTime.Now;
            var minDate = new DateTime(currentDate.Year, currentDate.Month, 1);
            var maxDate = minDate.AddMonths(1).AddDays(-1);

            if (Request.Form.TryGetValue<DateTime>("FromDate", out var requestFrom))
                minDate = requestFrom.Date;

            filterExpression = filterExpression.And(x => DateTime.ParseExact(x.StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture).Date >= minDate.Date);

            if (Request.Form.TryGetValue<DateTime>("ToDate", out var requestTo))
                maxDate = requestTo.Date;

            filterExpression = filterExpression.And(x => DateTime.ParseExact(x.StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture).Date <= maxDate.Date);

            var records = await _service.GetUserWorkingTimes(filterExpression);

            if (Request.Form.TryGetValue<string>("Status", out var status))
                records = records.Where(x => x.ConfirmStatusValue == ParseRequestStatus(status)).ToList();

            return records;
        }

        protected override async Task<RoboUIGridAjaxData<WorkingTimesheet>> GetRecords(RoboUIGridRequest options)
        {
            var user = await WorkContext.GetCurrentUser();

            var filterExpression = PredicateBuilder.Create<WorkingTimesheet>(x => x.UserId == user.Id);

            var records = await BuildAndGetCommonRecords(filterExpression);

            return new RoboUIGridAjaxData<WorkingTimesheet>(records.Take(options.PageSize).Skip(options.PageIndex - 1 * options.PageSize), records.Count);
        }

        private int ParseRequestStatus(string statusStr)
        {
            int defStatus;
            switch (statusStr)
            {
                case "Inprogress":
                    defStatus = 1;
                    break;
                case "Expired":
                    defStatus = 5;
                    break;
                default:
                    defStatus = 0;
                    break;
            }
            return defStatus;
        }

        protected override WorkingTimesheetModel ConvertToModel(WorkingTimesheet entity)
        {
            return entity;
        }
    }
}