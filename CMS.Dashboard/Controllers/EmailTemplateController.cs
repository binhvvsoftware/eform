﻿using Autofac;
using Microsoft.AspNetCore.Mvc;
using CMS.Dashboard.Models;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using CMS.Dashboard.Extensions;
using CMS.Shared;
using System;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("config/email-templates")]
    public class EmailTemplateController : BaseRoboController<int, EmailTemplate, EmailTemplateModel>
    {
        private readonly IComponentContext componentContext;
        public EmailTemplateController(IComponentContext componentContext, IEmailTemplateService service) : base(componentContext, service)
        {
            this.componentContext = componentContext;
        }

        protected override void OnViewIndex(RoboUIGridResult<EmailTemplate> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));
            roboGrid.AddColumn(x => x.Name);
            roboGrid.AddColumn(x => x.Subject);
            roboGrid.AddColumn(x => x.Enabled).RenderAsStatusImage().AlignCenter();
        }

        protected override async Task OnValidate(EmailTemplateModel model)
        {
            var oldEntity = await Service.GetRecordAsync(x => x.Name == model.Name);
            if (oldEntity != null && oldEntity.Id != model.Id)
            {
                throw new ArgumentException("Mẫu email này đã tồn tại.");
            }
        }

        protected override void OnCreating(RoboUIFormResult<EmailTemplateModel> roboForm)
        {
            roboForm.RegisterExternalDataSource(x => x.Name, Constants.EmailTemplates.GetEmailTemplates().ToSelectList(x => x, x => x));
        }

        protected override void OnEditing(RoboUIFormResult<EmailTemplateModel> roboForm)
        {
            roboForm.RegisterExternalDataSource(x => x.Name, Constants.EmailTemplates.GetEmailTemplates().ToSelectList(x => x, x => x));
        }

        protected override EmailTemplateModel ConvertToModel(EmailTemplate entity)
        {
            return entity;
        }

        protected override void ConvertFromModel(EmailTemplateModel model, EmailTemplate entity)
        {
            entity.Name = model.Name;
            entity.Subject = model.Subject;
            entity.Body = model.Body;
            entity.EmailAddress = model.EmailAddress;
            entity.CcEmailAddress = model.CcEmailAddress;
            entity.BccEmailAddress = model.BccEmailAddress;
            entity.Enabled = model.Enabled;
        }
    }
}