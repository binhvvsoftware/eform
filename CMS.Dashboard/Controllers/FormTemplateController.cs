﻿using Autofac;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CMS.Shared;
using CMS.Shared.Extensions;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("form-template")]
    public class FormTemplateController : BaseRoboController<int, FormTemplate, FormTemplateModel>
    {
        private readonly IFormTemplateService service;

        public FormTemplateController(IComponentContext componentContext, IFormTemplateService service) : base(
            componentContext, service)
        {
            this.service = service;
        }

        protected override string Name => "Form template";

        protected override string PluralizedName => "Quản lý form templates";

        protected override bool EnableTracking => true;

        protected override bool ShowModalDialog => false;

        protected override Task<bool> IsEnableCreate()
        {
            return Task.FromResult(false);
        }

        protected override FormTemplateModel ConvertToModel(FormTemplate entity)
        {
            return entity;
        }

        protected override string ConfirmDeleteMessage => "Bạn chắc chắn muốn xóa  menu?";

        protected override void OnViewIndex(RoboUIGridResult<FormTemplate> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.ClientId = "tblFormTemplates";
            roboGrid.EnableSearch = true;
            roboGrid.Title = "Quản lý form templates";
            roboGrid.ViewData = ViewData;
            roboGrid.FetchAjaxSource = GetRecords;
            roboGrid.AddColumn(x => x.TemplateName).HasHeaderText(T("Tên form")).EnableFilter();
            roboGrid.AddColumn(x => x.TemplatePath).RenderAsHtml(x => x.TemplatePath.GeLastDisplay()).HasHeaderText(T("Đường dẫn"));
            roboGrid.AddColumn(x => x.IsActive).HasHeaderText(T("Trạng thái")).RenderAsStatusImage(x => x.IsActive);
        }

        protected override async Task OnViewIndexAsync(RoboUIGridResult<FormTemplate> roboGrid)
        {
            if (await WorkContext.CheckPermission(MenuAction.IsManage))
            {
                roboGrid.AddAction()
                    .HasIconCssClass("fa fa-plus")
                    .HasUrl(Url.Action("CreateFormTemplate", "FormTemplate"))
                    .HasButtonStyle(ButtonStyle.Default);
            }
        }

        [Themed(true)]
        [HttpGet, Route("create-form-template")]
        public async Task<IActionResult> CreateFormTemplate(FormTemplateModel model = null)
        {
            ViewBag.Title = "Tạo mẫu văn bản";
            WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb("Tạo mẫu văn bản");

            if (!await CheckManagePermissions())
                throw new UnauthorizedAccessException();

            return View("CreateUpdateFormTemplate", model ?? new FormTemplateModel());
        }

        [HttpPost]
        [Route("save"), Themed(false)]
        [FormButton("Save")]
        public async Task<IActionResult> SaveFormTemplate(FormTemplateModel model)
        {
            var entity = new FormTemplate();
            var isNew = true;
            model.IsActive = Request.Form["IsActive"] == "on" ? true : false;

            if (model.Id > 0)
            {
                entity = await GetByIdAsync(model.Id);

                if (entity != null)
                {
                    isNew = false;
                }
            }

            var file = HttpContext.Request.Form.Files?.FirstOrDefault();
            if (file is null && string.IsNullOrEmpty(model.TemplatePath))
                return new AjaxResult().Alert("File văn bản mẫu không được để trống.").Redirect(Url.Action("CreateFormTemplate", model));

            if (file != null && !file.FileName.EndsWith(".docx") && string.IsNullOrEmpty(model.TemplatePath))
                return new AjaxResult().Alert("Không hỗ trợ định dạng file văn bản mẫu.").Redirect(Url.Action("CreateFormTemplate", model));

            if (file != null)
            {
                var uploadDir = Path.Combine(Directory.GetCurrentDirectory(), Constants.TemplatePath);
                model.TemplatePath = Path.Combine(uploadDir, file.FileName);

                if (!Directory.Exists(uploadDir))
                {
                    Directory.CreateDirectory(uploadDir);
                }

                using (var fileStream = new FileStream(model.TemplatePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            if (isNew)
            {
                entity = new FormTemplate
                {
                    TemplateName = model.TemplateName,
                    IsActive = model.IsActive,
                    TemplateContents = model.TemplateContent,
                    TemplatePath = $@"{Constants.TemplatePath}\{file.FileName}"
                };

                await service.InsertAsync(entity);

                return new AjaxResult().Alert("Mẫu văn bản đã được tạo.").Redirect(Url.Action("Index"));
            }
            else
            {
                ConvertFromModel(model, entity);

                await service.UpdateAsync(entity);

                return new AjaxResult().Alert("Cập nhật mẫu văn bản đã được tạo.").Redirect(Url.Action("Index"));
            }
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public override async Task<ActionResult> Edit(int id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException("Mẫu văn bản không tồn tại");

            return View("CreateUpdateFormTemplate", new FormTemplateModel
            {
                Id = entity.Id,
                TemplateContent = entity.TemplateContents,
                TemplateName = entity.TemplateName,
                IsActive = entity.IsActive,
                TemplatePath = entity.TemplatePath
            });
        }

        protected override void ConvertFromModel(FormTemplateModel model, FormTemplate entity)
        {
            entity.Id = model.Id;
            entity.TemplateName = model.TemplateName;
            entity.IsActive = model.IsActive;
            entity.TemplatePath = model.TemplatePath;
            entity.TemplateContents = model.TemplateContent;
        }
    }
}