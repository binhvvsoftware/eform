﻿using Autofac;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CMS.Dashboard.Controllers
{
    [Route("department")]
    public class DepartmentController : BaseRoboController<int, Department, DepartmentModel>
    {
        private readonly IComponentContext componentContext;

        public DepartmentController(IComponentContext componentContext, IDepartmentService service)
            : base(componentContext, service)
        {
            this.componentContext = componentContext;
        }

        protected override string Name => "Phòng ban";

        protected override string PluralizedName => "Phòng ban";

        protected override string CreateText => "Thêm";

        protected override string DeleteText => "Xóa";

        protected override string EditText => "Sửa";

        protected override string ActionText => "Thao tác";

        protected override bool EnableTracking => true;

        protected override void OnViewIndex(RoboUIGridResult<Department> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T(PluralizedName));
            roboGrid.AddColumn(x => x.Order, "STT").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Name, "Phòng ban").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Description, "Mô tả").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.CampusName, "Cơ sở").HasWidth(150).EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Status, "Trạng Thái").RenderAsStatusImage().AlignCenter().HasWidth(100);
        }

        protected override DepartmentModel ConvertToModel(Department entity)
        {
            return entity;
        }

        protected override void ConvertFromModel(DepartmentModel model, Department entity)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;
            entity.Status = model.Status;
            entity.CampusId = model.CampusId;
            entity.Order = model.Order;
        }

        protected override async Task OnCreatingAsync(RoboUIFormResult<DepartmentModel> roboForm)
        {
            var campusService = componentContext.Resolve<ICampusService>();
            var rootCampuses = await campusService.GetRecordsAsync();
            roboForm.RegisterExternalDataSource(x => x.CampusId, rootCampuses.ToSelectList(v => v.Id, t => t.Name));
        }

        protected override async Task OnEditingAsync(RoboUIFormResult<DepartmentModel> roboForm)
        {
            var campusService = componentContext.Resolve<ICampusService>();
            var rootCampuses = await campusService.GetRecordsAsync();
            roboForm.RegisterExternalDataSource(x => x.CampusId, rootCampuses.ToSelectList(v => v.Id, t => t.Name));
        }

        protected override async Task<RoboUIGridAjaxData<Department>> GetRecords(RoboUIGridRequest options)
        {
            var records = await Service.GetRecordsInclude(options, null, x => x.RootCampus);
            return new RoboUIGridAjaxData<Department>(records, records.ItemCount);
        }
    }
}