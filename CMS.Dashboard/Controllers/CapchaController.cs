using System.Threading.Tasks;
using Autofac;
using CMS.Dashboard.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Dashboard.Controllers
{
    public class CapchaController : BaseController
    {
        public CapchaController(IComponentContext componentContext) : base(componentContext)
        {
        }
        
        
        public async Task<IActionResult> GenerateCapchaImage()
        {
            return Ok();
        }
    }
}