﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using CMS.Core;
using CMS.Core.Extensions;
using CMS.Dashboard.Common;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.Notify;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using CMS.Shared.Models;
using CMS.Sharp.Docx;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("request-document")]
    public class RequestDocumentController : BaseRoboController<int, RequestDocument, RequestDocumentModel>
    {
        private const int Width = 200;
        private const int Height = 50;
        private readonly IComponentContext _componentContext;
        private readonly IRequestDocumentService _requestDocumentService;
        static readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);

        public RequestDocumentController(IComponentContext componentContext, IRequestDocumentService service) : base(
            componentContext, service)
        {
            _componentContext = componentContext;
            _requestDocumentService = service;
        }

        protected override bool ShowEditModalDialog => false;

        protected override Task<bool> IsEnableCreate() => Task.FromResult(false);

        protected override Task<bool> IsEnableEdit() => Task.FromResult(false);

        protected override Task<bool> IsEnableDelete() => Task.FromResult(false);

        protected override string Name => "Tạo mới văn bản";

        protected override string PluralizedName => "Phê duyệt văn bản";


        [Themed(false)]
        [HttpGet, Route("create-request")]
        public async Task<IActionResult> CreateNewRequest()
        {
            var user = await WorkContext.GetCurrentUser();
            var workflows = await WorkContext.GetWorkFlow()
                .Continue(x => x.Where(item => item.WorkflowType == user.UserType));

            RoboUIFormResult wiget = null;

            if (!workflows.IsNullOrEmpty())
            {
                wiget = new RoboUIFormResult(ControllerContext)
                {
                    ViewData = ViewData,
                    ShowCancelButton = false,
                    ShowSubmitButton = false,
                    DisableGenerateForm = true
                };

                var html = await RenderRazorViewToString("RequestProcedure", workflows);
                wiget.AddProperty("ReuqestProcedureDiv", new RoboDivAttribute { HasLabelControl = false }, html);
            }

            return wiget;
        }

        protected override async Task OnViewIndexAsync(RoboUIGridResult<RequestDocument> roboGrid)
        {
            roboGrid.ClientId = "tblRequestDocuments";

            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T(PluralizedName));
            WorkContext.AddBreadcrumb(T("Tạo văn bản"), Url.Action("CreateNewRequest", "RequestDocument"));

            var user = await WorkContext.GetCurrentUser();
            var filterForm = await BuilFilterForm(roboGrid, user);

            if (filterForm != null)
            {
                roboGrid.FilterForm = filterForm;
                roboGrid.AddCustomVariable("RequestFrom", "$('#RequestFrom').val();", true);
                roboGrid.AddCustomVariable("RequestTo", "$('#RequestTo').val();", true);
                roboGrid.AddCustomVariable("RequestStatus", "$('#RequestStatus').val();", true);
                roboGrid.AddCustomVariable("WorkflowId", "$('#WorkflowId').val();", true);
                roboGrid.FilterForm = filterForm;
                roboGrid.EnableSearch = true;
            }

            roboGrid.Title = PluralizedName;
            roboGrid.ViewData = ViewData;
            roboGrid.FetchAjaxSource = GetRecords;

            roboGrid.AddColumn(x => x.Workflow)
                .HasHeaderText("Văn bản")
                .RenderAsHtml(x =>
                    $"<a class= \"label label-primary\" style=\"cursor: pointer;\" href=\"{Url.Action("Edit", new { id = x.Id })}\">{x.Workflow.Name}</a>");

            roboGrid.AddColumn(x => x.CreateByUser)
                .HasHeaderText("Người trình ký")
                .RenderAsHtml(x => x.CreateByUser?.FullName)
                .AlignCenter();

            roboGrid.AddColumn(b => b.Status)
                .HasHeaderText("Trạng thái văn bản")
                .RenderAsHtml(b => BuildFormStatus(b, user));

            roboGrid.AddColumn(x => x.CurrentReviewSteps)
                .HasHeaderText("Người xử lý tiếp")
                .RenderAsHtml(x => BuildNextReviewer(x, user))
                .AlignCenter();

            roboGrid.AddColumn(x => x.CreateDate)
                .HasHeaderText("Ngày trình ký")
                .AlignCenter();

            roboGrid.AddRowAction()
                .HasCssClass("fa fa-eye")
                .HasUrl(x => Url.Action("Edit", new { id = x.Id }))
                .HasAttribute("title", "Chỉnh sửa/Duyệt văn bản")
                .HasButtonSize(ButtonSize.ExtraSmall)
                .HasButtonStyle(ButtonStyle.Success);

            roboGrid.AddRowAction()
                .HasUrl(x => Url.Action("DownloadFormRequest",
                    new { requestDocId = x.Id, templateId = x.Workflow.FormTemplateId }))
                .VisibleWhen(x => x.Status == RequestStatus.Approved ||
                                  user.IsManageRequestDocument)
                .HasButtonSize(ButtonSize.ExtraSmall)
                .HasCssClass("fa fa-download")
                .HasAttribute("title", "Tải về văn bản")
                .HasButtonStyle(ButtonStyle.Success);

            roboGrid.AddRowAction()
                .HasUrl(x => Url.Action("RemoveFormRequest",
                    new { x.Id }))
                .HasButtonSize(ButtonSize.ExtraSmall)
                .HasCssClass("fa fa-remove")
                .VisibleWhen(x => x.Status == RequestStatus.Saved ||
                                  x.Status == RequestStatus.Approved ||
                                  user.IsManageRequestDocument)
                .HasButtonStyle(ButtonStyle.Danger)
                .HasAttribute("title", "Xóa văn bản")
                .HasConfirmMessage("Bạn chắc chắn muốn xóa văn bản này?");

            WorkContext.AddScript("/js/req-tooltip.js");
        }

        private static string BuildNextReviewer(RequestDocument requestDocument, IdentityUser currentUser)
        {
            var sb = new StringBuilder();
            try
            {
                var records = requestDocument.RequestDocumentAssignees.ToArray();

                if (requestDocument.Status == RequestStatus.Approved ||
                    requestDocument.Status == RequestStatus.Rejected)
                    return "<span></span>";

                if (!requestDocument.CurrentReviewSteps.IsNullOrEmpty())
                {
                    records = requestDocument.CurrentReviewSteps.ToArray();
                }

                var groupSteps = records
                    .OrderBy(o => o.Step)
                    .GroupBy(i => i.Step);

                foreach (var g in groupSteps)
                {
                    sb.Append(string.Join(",", g.Where(gx => gx != null)
                        .Select(x => x.User?.FullName ?? x.User?.UserName)
                        .Where(x => !x.IsNullOrEmpty())
                        .ToArray()));
                }
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }

            return sb.ToString();
        }

        private string BuildFormStatus(RequestDocument requestDocument, IdentityUser currentUser)
        {
            var sb = new StringBuilder();
            try
            {
                var requestSteps = requestDocument.RequestDocumentAssignees.ToArray();

                sb.Append(!requestDocument.CurrentReviewSteps.IsNullOrEmpty()
                    ? StatusDisplay(requestDocument.Status,
                        requestDocument.CurrentReviewSteps.Any(x => x.UserId == currentUser.Id))
                    : StatusDisplay(requestDocument.Status, requestDocument.CreateByUserId == currentUser.Id));

                var groupSteps = requestSteps
                    .OrderBy(o => o.Step).GroupBy(i => i.Step)
                    .Select(grp => new
                    {
                        step = grp.Key,
                        status = grp.Any(f => f.Status == RequestStatus.Approved || f.Status == RequestStatus.Rejected),
                        result = grp.ToList(),
                        countStep = grp.Count()
                    }).ToList();

                var tooltipTarget = $"request-tooltip-document-{requestDocument.Id}";

                // tooltip talbe display
                sb.Append($"<div id=\"{tooltipTarget}\" style=\"display: none;\" class=\"panel-body\">");
                sb.Append("<table class=\"table\">");
                sb.Append("<thead><tr><th>Bước</th><th>Người xử lý</th></tr></thead><tbody>");
                sb.Append("<tr><td rowspan=\"2\">0</td></tr>");
                sb.Append(
                    $"<tr><td> {requestDocument.CreateByUser?.FullName}<span class=\"label label-primary\">Người làm đơn</span></td></tr>");

                foreach (var group in groupSteps)
                {
                    sb.Append($"<tr><td rowspan=\"{group.countStep + 1}\">{group.step}</td></tr>");
                    foreach (var item in group.result)
                    {
                        sb.Append(
                            $"<tr><td> {item.User?.FullName} {StatusDisplay(item.Status, item.UserId == currentUser.Id)}</td></tr>");
                    }
                }

                sb.Append("</tbody></table></div>");

                // tooltip icon
                sb.Append(
                    $"<a style=\"cursor: pointer;padding-left:5px;\" data-target=\"#{tooltipTarget}\" class=\"hover-tooltip\" data-toggle=\"popover\" data-original-title=\"\" title=\"\">");
                sb.Append(
                    $"<span class=\"badge badge-light\">{groupSteps.Count(x => x.status)}/{groupSteps.Count}<i class=\"fa fa-fw fa-tasks\"></i></span></a>");
            }
            catch (Exception ex)
            {
                Logger.LogError($"BuildFormStatus.Error", ex);
                return "Lấy thông tin người duyệt kế tiếp có lỗi.";
            }

            return sb.ToString();
        }

        private async Task<RoboUIFormResult> BuilFilterForm(RoboUIGridResult<RequestDocument> roboGrid,
            IdentityUser user)
        {
            var searchForm = new RoboUIFormResult(ControllerContext)
            {
                ViewData = ViewData,
                ShowCancelButton = false,
                ShowSubmitButton = false,
                DisableGenerateForm = true
            };

            searchForm.AddProperty("lblHeader",
                new RoboDivAttribute { HasLabelControl = false, ContainerRowIndex = 0 },
                "<div class=\"form-group col-md-12 col-sm-12\"><fieldset><legend>Phê duyệt văn bản</legend></fieldset></div>");

            searchForm.AddProperty("RequestFrom",
                new RoboDatePickerAttribute
                {
                    LabelText = "Ngày tạo",
                    ContainerCssClass = "form-group col-md-3 col-sm-6 col-xs-12",
                    ContainerRowIndex = 1
                });

            searchForm.AddProperty("RequestTo",
                new RoboDatePickerAttribute
                {
                    LabelText = "Tới ngày",
                    ParentStartDateControl = "BookingFrom",
                    ContainerCssClass = "form-group col-md-3 col-sm-6 col-xs-12",
                    ContainerRowIndex = 1
                });

            var requestStatus = Enum.GetValues(typeof(FilterDocumentStatus)).ToListOf<FilterDocumentStatus>()
                .ToSelectList(x => x, x => x.GetDisplayName());

            searchForm.AddProperty("RequestStatus",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Trạng thái",
                    ContainerCssClass = "form-group col-md-2 col-sm-6 col-xs-12 ",
                    ContainerRowIndex = 1,
                    OptionLabel = "Tất cả",
                    SelectListItems = requestStatus,
                    Value = FilterDocumentStatus.Inprogress,
                    HtmlAttributes = new Dictionary<string, object> { { "class", "booking-filter" } }
                });

            var workflowService = _componentContext.Resolve<IWorkflowService>();

            var workflows = await workflowService
                .GetRecordsAsync(x => x.CampusId == user.CampusId && x.WorkflowType == user.UserType)
                .ToSelectListAsync(k => k.Id, v => v.Name);

            searchForm.AddProperty("WorkflowId",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Loại văn bản",
                    ContainerCssClass = "form-group col-md-2 col-sm-6 col-xs-12",
                    ContainerRowIndex = 1,
                    OptionLabel = "Tất cả",
                    Value = "",
                    PropertyType = typeof(string),
                    SelectListItems = workflows
                });

            searchForm.AddProperty("ButtonSearch",
                new RoboButtonAttribute
                {
                    OnClick = roboGrid.GetReloadClientScript(),
                    ContainerCssClass = "form-group col-md-2 col-sm-6 col-xs-10",
                    ContainerRowIndex = 1,
                    LabelText = "Tìm kiếm",
                    HideLabelControl = true,
                    HasLabelControl = true,
                    HtmlAttributes = new Dictionary<string, object> { { "class", "btn btn-primary show" } }
                });

            return searchForm;
        }

        private static string StatusDisplay(RequestStatus status, bool isRequester)
        {
            switch (status)
            {
                case RequestStatus.Saved:
                    return
                        $"<span class=\"label label-warning\">{status.GetDisplayName()}</span>";
                case RequestStatus.Inprogress:
                    return $"<span class=\"label label-info\">{status.GetDisplayName()}</span>";
                case RequestStatus.Approved:
                    return $"<span class=\"label label-success\">{status.GetDisplayName()}</span>";
                case RequestStatus.Rejected:
                    return $"<span class=\"label label-danger\">{status.GetDisplayName()}</span>";
                default:
                    return $"<span class=\"label label-warning\">{status.GetDisplayName()}</span>";
            }
        }

        protected override async Task<RoboUIGridAjaxData<RequestDocument>> GetRecords(RoboUIGridRequest options)
        {
            var currentUser = await WorkContext.GetCurrentUser();
            var filterRequest = new RequestDocumentFilterRequest();

            if (Request.Form.TryGetValue<DateTime>("RequestFrom", out var requestFrom))
            {
                filterRequest.FromDate = requestFrom;
            }

            if (Request.Form.TryGetValue<DateTime>("RequestTo", out var requestTo))
            {
                filterRequest.ToDate = requestTo;
            }

            RequestStatus? myFilterStatus = null;

            if (Request.Form.TryGetValue<FilterDocumentStatus>("RequestStatus", out var requestStatus))
            {
                switch (requestStatus)
                {
                    case FilterDocumentStatus.Approved:
                        filterRequest.FormFilterStatus = RequestStatus.Approved;
                        break;
                    case FilterDocumentStatus.Rejected:
                        filterRequest.FormFilterStatus = RequestStatus.Rejected;
                        break;
                    case FilterDocumentStatus.Inprogress:
                        filterRequest.FormFilterStatus = RequestStatus.Inprogress;
                        break;
                    case FilterDocumentStatus.Saved:
                        filterRequest.FormFilterStatus = RequestStatus.Saved;
                        break;
                    case FilterDocumentStatus.MyApproved:
                        myFilterStatus = RequestStatus.Approved;
                        break;
                    case FilterDocumentStatus.MyRejected:
                        myFilterStatus = RequestStatus.Rejected;
                        break;
                    default:
                        // filterRequest.FormFilterStatus = RequestStatus.Inprogress;
                        break;
                }
            }
            else
            {
                filterRequest.FormFilterStatus = RequestStatus.Inprogress;
            }

            if (myFilterStatus != null)
            {
                filterRequest.CustomFilterStatus = myFilterStatus;
            }

            if (Request.Form.TryGetValue<int>("WorkflowId", out var workflowId))
            {
                filterRequest.WorkflowId = workflowId;
            }

            filterRequest.UserId = currentUser.Id;

            if (options.Sorts != null && options.Sorts.IsNullOrEmpty())
            {
                options.Sorts.Add(new Core.Data.SortDescriptor
                {
                    Member = nameof(RequestDocument.CreateDate),
                    SortDirection = Core.Data.SortDirection.Descending
                });
            }

            var records = await _requestDocumentService.GetReviewDocuments(filterRequest);

            return new RoboUIGridAjaxData<RequestDocument>(
                records.Skip(options.PageSize * (options.PageIndex - 1)).Take(options.PageSize * options.PageIndex),
                records.Count);
        }

        protected override void ConvertFromModel(RequestDocumentModel model, RequestDocument entity)
        {
            if (Enum.TryParse<RequestStatus>(model.Status, out var status))
            {
                entity.Status = status;
                entity.UpdateDate = DateTime.Now;
            }

            if (entity.SignatureImage.IsNullOrEmpty())
            {
                // entity.SignatureImage = SignatureImageFileName(model);
                entity.SignatureImage = StoreSignatureImages(model);
                entity.CaptchaCode = model.CaptchaCode;
            }
            entity.JsonContent = model.JsonContent;
            entity.HtmlContent = model.HtmlContent;
            entity.UpdateDate = DateTime.Now;
        }

        private string StoreSignatureImages(RequestDocumentModel model)
        {
            var uploadDir = Path.Combine(Directory.GetCurrentDirectory(), Constants.ImagePath);

            var capchaGenerator = new CapchaGenerator();
            var capchaImg = capchaGenerator.GenerateCapchaImage(model.CaptchaCode, 250, 100);

            if (capchaImg.IsNullOrEmpty())
                throw new ArgumentException("Có lỗi trong quá trình lưu chữ ký !");

            var bytes = Convert.FromBase64String(capchaImg);

            var fileName = $"signature-{model.User?.FullName}-{DateTime.Now.Ticks}.png";

            using (var imageFile = new FileStream(Path.Combine(uploadDir, fileName), FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return fileName;
        }
        private string StoreSignatureImages(string capchaCode, string fileName)
        {
            var uploadDir = Path.Combine(Directory.GetCurrentDirectory(), Constants.ImagePath);

            var capchaGenerator = new CapchaGenerator();
            var capchaImg = capchaGenerator.GenerateCapchaImage(capchaCode, 250, 100);

            if (capchaImg.IsNullOrEmpty())
                throw new ArgumentException("Có lỗi trong quá trình lưu chữ ký !");

            var bytes = Convert.FromBase64String(capchaImg);

            using (var imageFile = new FileStream(Path.Combine(uploadDir, fileName), FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return fileName;
        }

        protected override RequestDocumentModel ConvertToModel(RequestDocument entity)
        {
            return new RequestDocumentModel()
            {
            };
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public override async Task<ActionResult> Edit(int id)
        {
            var user = await WorkContext.GetCurrentUser();
            var requestDocument = await _componentContext.Resolve<IRequestDocumentService>()
                .GetReviewDocumentById(id, user.Id);
            if (requestDocument == null)
                throw new ArgumentException("Văn bản đăng ký  không tồn tại");

            var requestDocumentAssignees = requestDocument.RequestDocumentAssignees;
            var currentStepReview = requestDocument.CurrentReviewSteps.FirstOrDefault(x => x.UserId == user.Id);
            if (requestDocument == null)
                throw new ArgumentException("Nguời duyệt văn bản không khả dụng.");
            var canApprove = requestDocument.Status == RequestStatus.Inprogress &&
                             currentStepReview != null &&
                             requestDocument.Status != RequestStatus.Approved &&
                             requestDocument.Status != RequestStatus.Rejected;

            return View("WorkFormTemplate", new RequestDocumentModel
            {
                RequestDocument = requestDocument,
                Workflow = requestDocument.Workflow,
                RequestDocumentAssignees = requestDocumentAssignees,
                User = requestDocument.CreateByUser,
                CurrentStepId = currentStepReview?.Id ?? 0,
                CanApprove = canApprove,
                CurrentStepRequestDocumentAssignee = currentStepReview
            });
        }

        [Themed(false)]
        [Route("work/{id}/{templateId}")]
        public async Task<ActionResult> WorkTemplate(int id, int templateId)
        {
            var createByUser = await WorkContext.GetCurrentUser();
            var relations = await _componentContext.Resolve<IMemberRelationService>()
                .GetRecordsAsync(s => createByUser.Id == s.UserId);
            var departments = await _componentContext.Resolve<IDepartmentService>()
                .GetRecordsAsync()
                .Continue(x => x.ToList());

            createByUser.Department = departments.FirstOrDefault(x => x.Id == createByUser.DepartmentId);
            createByUser.Campus = await _componentContext.Resolve<ICampusService>().GetByIdAsync(createByUser.CampusId);
            createByUser.Managers = (ICollection<IdentityUser>)await _componentContext.Resolve<IUserService>()
                .GetRecordsAsync(x => relations.Select(s => s.ManagerId).Contains(x.Id));

            var workFlow = await _componentContext.Resolve<IWorkflowService>().GetByIdAsync(id);
            if (workFlow == null)
                throw new ArgumentException("Quy trình không tồn tại");

            var template = await _componentContext.Resolve<IFormTemplateService>().GetByIdAsync(templateId);
            if (template == null)
                throw new ArgumentException("Mẫu văn bản không tồn tại");

            return View("WorkFormTemplate", new RequestDocumentModel
            {
                FormTemplate = template,
                Workflow = workFlow,
                User = createByUser,
                Departments = departments
            });
        }

        [Themed(false)]
        [Route("remove-form-request/{id}")]
        public async Task<ActionResult> RemoveFormRequest(int id)
        {
            var formRequest = await _componentContext.Resolve<IRequestDocumentService>().GetByIdAsync(id);

            formRequest.Enabled = false;

            await Service.UpdateAsync(formRequest);

            return new AjaxResult().Alert("Đã xóa văn bản thành công.").Redirect(Url.Action("Index"), true);
        }

        [Themed(false)]
        [Route("download-request/{requestDocId}/{templateId}")]
        public async Task<IActionResult> DownloadFormRequest(int requestDocId, int templateId)
        {
            var requestDocAssignService = _componentContext.Resolve<IRequestDocumentAssigneeService>();
            var template = await _componentContext.Resolve<IFormTemplateService>().GetByIdAsync(templateId);
            var request = await _componentContext.Resolve<IRequestDocumentService>().GetRecordAsync(x => x.Id == requestDocId, null, i => i.CreateByUser);

            if (template == null || request == null)
                return new AjaxResult().Alert("Có lỗi trong quá trình tải về. Xin hãy thử lại").CloseModalDialog(Url.Action("Index"));
            try
            {
                _semaphoreSlim.Wait(TimeSpan.FromMinutes(1));

                var outDirPath = Path.Combine(Directory.GetCurrentDirectory(), Constants.GeneratePath);
                if (!Directory.Exists(outDirPath))
                {
                    Directory.CreateDirectory(outDirPath);
                }

                var imageDirectory = $"{Path.Combine(Directory.GetCurrentDirectory(), Constants.ImagePath)}";
                if (!Directory.Exists(imageDirectory))
                {
                    return new AjaxResult().Alert("Không tìn thấy thư mục chứa chữ ký của người duyệt văn bản! Xin hãy thử lại.").CloseModalDialog(Url.Action("Index"));
                }
                var templatePath = Path.Combine(Directory.GetCurrentDirectory(), $"{template.TemplatePath}");
                if (!System.IO.File.Exists(templatePath))
                {
                    return new AjaxResult().Alert("Không tìn thấy file template cho văn bản này ! Xin hãy thử lại. !").CloseModalDialog(Url.Action("Index"));
                }

                var outputPath = Path.Combine(Directory.GetCurrentDirectory(),
                    $@"{Constants.GeneratePath}\{template.TemplateName}_output.docx");

                if (System.IO.File.Exists(outputPath))
                {
                    System.IO.File.Delete(outputPath);
                }

                byte[] fileResults = null;

                var exportTbl = new List<ExportTable>();
                var reviewerComments = new StringBuilder();
                var baseBody = request.JsonContent?.JsonDeserialize<Dictionary<string, string>>();
                baseBody["create_at"] = $".........., ngày {request.CreateDate.Day}, tháng {request.CreateDate.Month}, năm {request.CreateDate.Year}";

                var signatures = new Dictionary<string, string>
                {
                    {"requester_sign_img", request.SignatureImage},
                    {"requester_sign_name", request.CreateByUser.FullName},
                    {"requester_sign_date", request.UpdateDate.ToLongVnDateString()},
                };

                if (!System.IO.File.Exists(Path.Combine(imageDirectory, request.SignatureImage)))
                    StoreSignatureImages(request.CaptchaCode, Path.Combine(imageDirectory, request.SignatureImage));

                var reviewerSigns = await requestDocAssignService.GetReviewerSignatureAndComment(requestDocId);

                foreach (var reviewer in reviewerSigns)
                {
                    signatures[$"reviewer_step_{reviewer.Step}_sign_img"] =
                        reviewer.SignatureImage ??
                        throw new ArgumentException("Không tìm thấy chữ ký của người duyệt văn bản. Vui lòng thử lại.");
                    signatures[$"reviewer_step_{reviewer.Step}_sign_date"] = reviewer.UpdateDate.ToLongVnDateString();
                    signatures[$"reviewer_step_{reviewer.Step}_sign_name"] = reviewer.User.FullName;

                    if (!reviewer.Comment.IsNullOrEmpty())
                        reviewerComments.AppendLine(reviewer.GetReviewerComment());

                    if (!System.IO.File.Exists(Path.Combine(imageDirectory, reviewer.SignatureImage)))
                    {
                        if (reviewer.CaptchaCode.IsNullOrEmpty())
                            signatures[$"reviewer_step_{reviewer.Step}_sign_img"] = "blank_signature.png";
                        else
                            StoreSignatureImages(reviewer.CaptchaCode, Path.Combine(imageDirectory, reviewer.SignatureImage));
                    }
                }

                if (baseBody.ContainsKey("tableContent") && baseBody["tableContent"].Trim().Length > 0)
                    exportTbl = baseBody["tableContent"].JsonDeserialize<List<ExportTable>>();

                var model = new ExportModel
                {
                    Bodys = baseBody,
                    Signatures = signatures,
                    ExportTables = exportTbl,
                    AllComments = reviewerComments.ToString()
                };

                var document = DocumentFactory.Create(templatePath, model);
                document.ImageDirectory = imageDirectory;
                fileResults = document.GenerateToBytes(outputPath);

                if (fileResults != null)
                {
                    Response.Headers.Add("Content-Disposition", fileResults?.Length.ToString());
                    Response.Headers.Add("Content-Lenght", fileResults?.Length.ToString());
                    return File(fileResults, "application/vnd.ms-word",
                        $"{template.TemplateName}_{DateTime.Now.UnixTimeStamp()}.docx");
                }
            }
            catch (Exception e)
            {
                Logger.Error("Tải về form lỗi", e);
            }
            finally
            {
                _semaphoreSlim.Release();
            }

            return new AjaxResult().Alert("Đã có lỗi trong quá trình duyệt văn bản.").CloseModalDialog(Url.Action("Index"));
        }

        [Themed(false)]
        [HttpPost, Route("update-status")]
        public async Task<ActionResult> UpdateStatusWorkTemplate(RequestDocumentModel model)
        {
            if (Request.Form.IsNullOrEmpty())
                return new AjaxResult().Alert("Dữ liệu không hợp lệ.").Redirect(Url.Action("Index"), true);

            var documentRequestService = _componentContext.Resolve<IRequestDocumentService>();
            var documentAssigneeService = _componentContext.Resolve<IRequestDocumentAssigneeService>();
            var emailService = _componentContext.Resolve<IEmailService>();
            var userService = _componentContext.Resolve<IUserService>();

            var user = await WorkContext.GetCurrentUser();
            var excludeds = new[] { "HtmlContent", "tableContent", "table" };
            var documentRequestToReview = await documentRequestService.GetReviewDocumentById(model.Id, user.Id);
            if (documentRequestToReview == null)
                return new AjaxResult().Alert("Bạn không có quyền duyệt văn bản này !").Redirect(Url.Action("Index"), true);

            var userInStep = documentRequestToReview.CurrentReviewSteps.OrderByDescending(x => x.Step).FirstOrDefault(x => x.UserId == user.Id && x.Status == RequestStatus.Inprogress);

            if (userInStep == null || documentRequestToReview.Status == RequestStatus.Approved || documentRequestToReview.Status == RequestStatus.Rejected)
            {
                return Json(new
                {
                    Status = false,
                    Message = "Bạn không thể duyệt văn bản này.",
                    RedirectUrl = Url.Action("Index")
                });
            }

            try
            {
                if (!string.Equals(Request.Form["FORM_NAME"], "ABSENT", StringComparison.OrdinalIgnoreCase))
                {
                    var isUpdated = false;
                    var oldJsonData = documentRequestToReview.JsonContent.JsonDeserialize<Dictionary<string, string>>();
                    foreach (var f in Request.Form)
                    {
                        var currentFormValue = Convert.ToString(f.Value);
                        if (string.IsNullOrEmpty(currentFormValue) || excludeds.Contains(f.Key, StringComparer.OrdinalIgnoreCase))
                            continue;

                        if (!oldJsonData.ContainsKey(f.Key) || !string.Equals(oldJsonData[f.Key], currentFormValue, StringComparison.OrdinalIgnoreCase))
                        {
                            oldJsonData[f.Key] = currentFormValue;
                            isUpdated = true;
                        }
                    }

                    if (isUpdated)
                        documentRequestToReview.JsonContent = oldJsonData.JsonSerialize();

                    if (!model.HtmlContent.IsNullOrEmpty()
                            && !string.Equals(documentRequestToReview.HtmlContent, model.HtmlContent, StringComparison.OrdinalIgnoreCase)
                            && HtmlContent.TryParse(model.HtmlContent))
                        documentRequestToReview.HtmlContent = model.HtmlContent;
                }

                userInStep.CaptchaCode = model.CaptchaCode;

                if (Enum.TryParse<RequestStatus>(model.Status, out var status))
                    userInStep.Status = status;

                if (status != RequestStatus.Rejected)
                {
                    userInStep.SignatureImage = userInStep.SignatureImage ?? StoreSignatureImages(model);

                    if (string.IsNullOrEmpty(userInStep.SignatureImage))
                    {
                        return Json(new
                        {
                            Status = false,
                            Message = "Chữ ký không được để trống.",
                            RedirectUrl = Url.Action("Edit", new { id = model.Id })
                        });
                    }
                }

                if (!model.Comment.IsNullOrEmpty())
                    userInStep.Comment = model.Comment;

                userInStep.UpdateDate = DateTime.Now;

                await documentAssigneeService.UpdateGroupReviewStatus(userInStep);

                documentRequestToReview.UpdateDate = DateTime.Now;
                if (status == RequestStatus.Rejected)
                {
                    documentRequestToReview.Status = userInStep.Status;

                    await Service.UpdateAsync(documentRequestToReview);
                    // await emailService.SendRequestDocumentReject(documentRequestToReview, userInStep, userRequestor);
                    TaskPool.Queue(() =>
                        emailService.SendRequestDocumentReject(documentRequestToReview, userInStep, documentRequestToReview.CreateByUser));
                    return Json(new
                    {
                        Status = true,
                        Message = "Đã từ chối duyệt văn bản !",
                        RedirectUrl = Url.Action("Index")
                    });
                }

                if (status == RequestStatus.Approved)
                {
                    var nextSteps = documentRequestToReview.RequestDocumentAssignees
                        .Where(x => x.Step == userInStep.Step + 1 && x.Status == RequestStatus.Inprogress)
                        .ToArray();

                    if (!nextSteps.IsNullOrEmpty())
                    {
                        documentRequestToReview.Status = RequestStatus.Inprogress;
                        var userNextSteps = nextSteps.Select(x => x.User).Where(x => x != null).ToArray();
                        if (!userNextSteps.IsNullOrEmpty())
                        {
                            // Gửi email thông báo tới ng duyệt kế tiếp
                            // await emailService.SendNewRequestDocument(documentRequestToReview, nextSteps, userRequestor);
                            TaskPool.Queue(() =>
                                emailService.SendNewRequestDocument(documentRequestToReview, nextSteps,
                                    documentRequestToReview.CreateByUser));
                        }
                    }
                    else
                    {
                        documentRequestToReview.Status = RequestStatus.Approved;
                        // await UpdateAbsentRequest(Request.Form, documentRequestToReview, model.User ?? user);
                    }

                    await Service.UpdateAsync(documentRequestToReview);
                    // Gửi email thông báo cho ng gửi và ng duyệt form
                    TaskPool.Queue(() =>
                            emailService.SendRequestDocumentApproved(documentRequestToReview, userInStep, documentRequestToReview.CreateByUser));
                    return Json(new
                    {
                        Status = true,
                        Message = "Duyệt văn bản thành công !",
                        RedirectUrl = Url.Action("Index")
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"UpdateStatusWorkTemplate.Error", ex);
            }

            return Json(new
            {
                Status = false,
                Message = "Có lỗi trong quá trình duyệt văn bản ! Xin hãy thử lại sau.",
                RedirectUrl = Url.Action("Index")
            });
        }

        [Themed(false)]
        [HttpPost, Route("save")]
        public async Task<ActionResult> SaveWorkTemplate(RequestDocumentModel model)
        {
            if (Request.Form.IsNullOrEmpty())
            {
                return Json(new
                {
                    Status = false,
                    Message = "Dữ liệu không hợp lệ. Xin vui lòng thử lại !",
                    RedirectUrl = Url.Action("Index")
                });
            }

            RequestDocument requestDocument;
            var currentUser = await WorkContext.GetCurrentUser();
            var isNew = false;

            if (model.Id > 0)
            {
                requestDocument = await Service.GetRecordAsync(s => s.Id == model.Id, null, x => x.Workflow);

                if (requestDocument == null)
                {
                    return Json(new
                    {
                        Status = false,
                        Message = "Văn bản không tồn tại. Xin vui lòng thử lại !",
                        RedirectUrl = Url.Action("Index")
                    });
                }
            }
            else
            {
                requestDocument = new RequestDocument
                {
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    CreateBy = User.Identity.Name,
                    CreateByUserId = currentUser.Id,
                    WorkflowId = model.WorkflowId,
                    Enabled = true
                };
                isNew = true;
            }

            if (string.IsNullOrEmpty(requestDocument.SignatureImage) && string.IsNullOrEmpty(model.CaptchaCode))
            {
                return Json(new
                {
                    Status = false,
                    Message = "Chữ ký không được để trống.",
                    RedirectUrl = Url.Action("WorkTemplate", "RequestDocument", new { id = model.WorkflowId, templateId = model.FormTemplateId })
                });
            }

            try
            {
                model.JsonContent = Request.Form.ToDictionary().JsonSerialize();
                ConvertFromModel(model, requestDocument);

                if (Request.Form.IsNullOrEmpty() || model.HtmlContent.IsNullOrEmpty())
                {
                    return Json(new
                    {
                        Status = false,
                        Message = "Nội dung văn bản không hợp lệ.",
                        RedirectUrl = Url.Action("WorkTemplate", "RequestDocument", new { id = model.WorkflowId, templateId = model.FormTemplateId })
                    });
                }

                await SaveEntity(requestDocument, isNew);
                requestDocument.CreateByUser = currentUser;

                var documentAssigneeService = _componentContext.Resolve<IRequestDocumentAssigneeService>();
                var documentAssignees = await documentAssigneeService.GetRecordsAsync(x => x.RequestDocumentId == requestDocument.Id).Continue(x => x.ToList());

                // await UpdateAbsentRequest(Request.Form, requestDocument, model.User ?? currentUser);

                if (requestDocument.Status != RequestStatus.Saved && documentAssignees.IsNullOrEmpty())
                {
                    await RegisterRequestAssignee(requestDocument, model.WorkflowId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"SaveWorkTemplate.{Name}.Error", ex);

                return Json(new
                {
                    Status = false,
                    Message = "Có lỗi khi cập nhật dữ liệu ! Xin hãy thử lại",
                    RedirectUrl = Url.Action("Index")
                });
            }

            return Json(new
            {
                Status = true,
                Message = requestDocument.Status == RequestStatus.Saved
                    ? "Đã lưu nháp văn bản !"
                    : "Đã gửi yêu cầu duyệt văn bản !",
                RedirectUrl = Url.Action("Index")
            });
        }

        private async Task UpdateAbsentRequest(IFormCollection form, RequestDocument requestDocument,
            IdentityUser userRequest)
        {
            var leaveApplyService = _componentContext.Resolve<ILeaveDocumentService>();

            var absentRequest = await leaveApplyService.GetRecordAsync(x => x.RequestDocumentId == requestDocument.Id);

            if (absentRequest == null)
            {
                if (form.TryGetValue("FORM_NAME", out var formType))
                {
                    if (!formType.IsNullOrEmpty() &&
                        string.Equals(formType, "ABSENT", StringComparison.OrdinalIgnoreCase))
                    {
                        var absentModel = requestDocument.JsonContent.JsonDeserialize<AbsentModel>();

                        if (!absentModel.IsValidateModel())
                            Logger.Error("Absent model is not valid to storing.");

                        absentModel.RequestDocumentId = requestDocument.Id;
                        absentModel.UserId = userRequest.Id;
                        absentModel.IsNew = true;
                        await leaveApplyService.StoreAbsentRequest(absentModel);
                    }
                }
            }
            else
            {
                if (requestDocument.Status == RequestStatus.Approved &&
                    absentRequest.ApplyStatus == ApplyStatus.NewApply)
                {
                    absentRequest.ApplyStatus = ApplyStatus.Approved;
                    await leaveApplyService.UpdateAsync(absentRequest);
                }
            }
        }

        private async Task RegisterRequestAssignee(RequestDocument requestDocument, int workflowId)
        {
            var emailService = _componentContext.Resolve<IEmailService>();
            var workflowSteps =
                await _componentContext.Resolve<IWorkflowStepService>().GeStepsByWorkflowsId(workflowId);

            if (!workflowSteps.IsNullOrEmpty())
            {
                var documentAssignees = new List<RequestDocumentAssignee>();
                var userService = _componentContext.Resolve<IUserService>();

                var users = await userService.GetRecordsAsync(x => !x.Disabled);
                var user = await WorkContext.GetCurrentUser();

                var superiorManagements = new List<MemberRelation>();
                var hasConfigSuperiorManagement = workflowSteps.Any(x =>
                    x.GroupManagement.HasValue && x.GroupManagement.Value == GroupManagement.SuperiorManagement);

                if (hasConfigSuperiorManagement)
                {
                    superiorManagements = (await _componentContext.Resolve<IMemberRelationService>()
                        .GetRecordsAsync(s => s.UserId == user.Id)).ToList();
                }

                foreach (var workflowStep in workflowSteps)
                {
                    if (!superiorManagements.IsNullOrEmpty() &&
                        workflowStep.GroupManagement.HasValue &&
                        workflowStep.GroupManagement.Value == GroupManagement.SuperiorManagement)
                    {
                        foreach (var item in superiorManagements)
                        {
                            documentAssignees.Add(new RequestDocumentAssignee
                            {
                                UserId = item.ManagerId,
                                RequestDocumentId = requestDocument.Id,
                                Step = workflowStep.Step,
                                Status = RequestStatus.Inprogress,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                            });
                        }
                    }
                    else if (!superiorManagements.IsNullOrEmpty() &&
                             workflowStep.GroupManagement.HasValue &&
                             workflowStep.GroupManagement.Value == GroupManagement.UpSuperiorManagement)
                    {
                        if (hasConfigSuperiorManagement)
                        {
                            var memberRelationService = _componentContext.Resolve<IMemberRelationService>();
                            var upSuperiorManagements = (await memberRelationService.GetRecordsAsync(s =>
                                    superiorManagements.Select(x => x.ManagerId).Contains(s.UserId)))
                                .ToList();
                            documentAssignees.AddRange(upSuperiorManagements.Select(item => new RequestDocumentAssignee
                            {
                                UserId = item.ManagerId,
                                RequestDocumentId = requestDocument.Id,
                                Step = workflowStep.Step,
                                Status = RequestStatus.Inprogress,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                            }));
                        }
                    }
                    else if (!string.IsNullOrEmpty(workflowStep.UserIds))
                    {
                        var parseUserIds = workflowStep.UserIds.SafeSplit().Select(x => int.Parse(x));
                        foreach (var userId in parseUserIds)
                        {
                            documentAssignees.Add(new RequestDocumentAssignee
                            {
                                UserId = userId,
                                RequestDocumentId = requestDocument.Id,
                                Step = workflowStep.Step,
                                Status = RequestStatus.Inprogress,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                            });
                        }
                    }

                    if (workflowStep.Step == 1)
                    {
                        var assgineesFirstStep = documentAssignees.Where(d => d.Step == 1);
                        // await emailService.SendNewRequestDocument(requestDocument, assgineesFirstStep,
                        //     requestDocument.CreateByUser);
                        TaskPool.Queue(() =>
                            emailService.SendNewRequestDocument(requestDocument, assgineesFirstStep,
                                requestDocument.CreateByUser));
                    }
                    // await emailService.SendRequestDocumentCreated(requestDocument, requestDocument.CreateByUser);
                    TaskPool.Queue(() => emailService.SendRequestDocumentCreated(requestDocument, requestDocument.CreateByUser));
                }

                await _componentContext.Resolve<IRequestDocumentAssigneeService>().InsertManyAsync(documentAssignees);
            }
        }

        [HttpGet, Route("get-captcha")]
        public FileStreamResult GetImage()
        {
            _semaphoreSlim.Wait();
            Stream s;
            CaptchaResult result;
            try
            {
                var captchaCode = Captcha.GenerateCaptchaCode();

                result = Captcha.GenerateCaptchaImage(Width, Height, captchaCode);

                s = new MemoryStream(result.CaptchaByteData);
            }
            catch (Exception ex)
            {
                Logger.Error("Generate Captcha encountered an error", ex);
                throw;
            }
            finally
            {
                _semaphoreSlim.Release();
            }

            HttpContext.Session.SetString($"CaptchaCode", result.CaptchaCode);

            return new FileStreamResult(s, new MediaTypeHeaderValue("image/png"));
        }

        [HttpGet, Route("get-captcha-code")]
        public string GetCaptchaCode()
        {
            return HttpContext.Session.GetString($"CaptchaCode");
        }

        [HttpGet, Route("generate-captcha-img/{captchaCode}")]
        public string GenerateCaptchaImg(string captchaCode)
        {
            if (captchaCode.IsNullOrEmpty())
                throw new ArgumentNullException("Captcha code khong hop le.");
            try
            {
                var captchaGenerator = new CapchaGenerator();
                var imgContent = captchaGenerator.GenerateCapchaImage(captchaCode, 200, 100);

                if (imgContent.IsNullOrEmpty())
                    throw new ArgumentNullException("Captcha image khong hop le.");

                return $"data:image/png;base64, {imgContent}";
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}