﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using CMS.Core;
using CMS.Core.Extensions;
using CMS.Dashboard.Controllers.Common;
using CMS.Services.Services;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CMS.Dashboard.Controllers
{
    [Route("scheduler")]
    public class SchedulerController : BaseController
    {
        private readonly ILogger<SchedulerController> _logger;
        private readonly IWorkingTimeSheetService _workingTimeSheetService;
        private readonly IWorkingLateService _workingLateService;
        private readonly IMemberRelationService _memberRelationService;
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;
        private AppSettings _appSetting;
        private SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        public SchedulerController(IComponentContext componentContext, ILogger<SchedulerController> logger, IOptions<AppSettings> options) : base(componentContext)
        {
            _logger = logger;
            _workingTimeSheetService = componentContext.Resolve<IWorkingTimeSheetService>();
            _emailService = componentContext.Resolve<IEmailService>();
            _workingLateService = componentContext.Resolve<IWorkingLateService>();
            _memberRelationService = componentContext.Resolve<IMemberRelationService>();
            _userService = componentContext.Resolve<IUserService>();

            if (options == null)
            {
                options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
            }

            _appSetting = options.Value;
        }

        [HttpGet, Route("sync-working-timesheet/{workingDate?}")]
        public async Task<IActionResult> SyncStaffWorkingTimes(DateTime? workingDate = null)
        {
            try
            {
                if (!workingDate.HasValue)
                    workingDate = DateTime.Now.Date;

                await _semaphoreSlim.WaitAsync();
                var userLists = await _userService.GetRecordsAsync(x => !x.Disabled);
                // get user timesheets
                var userLates = await _workingTimeSheetService.GetLateTimes(workingDate.Value);

                if (!userLates.IsNullOrEmpty())
                {
                    var lateRequests = await _workingLateService.TransformAndInsertRequests(userLists, userLates);
                    // send request email notification
                    var emailRequestTasks = new List<Task>();

                    foreach (var batch in lateRequests.Batches(10))
                    {
                        emailRequestTasks.AddRange(batch.Select(b => _emailService.SendEmailWorkingLate(b)).ToArray());
                    }
                    await Task.WhenAll(emailRequestTasks);
                }
                // get expired late requests
                var validPoint = workingDate.Value.Date + TimeSpan.Parse("12:00:00");

                var expiredRequests = await _workingLateService.GetOrUpdateExpiredRequests(userLists, validPoint);

                if (!expiredRequests.IsNullOrEmpty())
                {
                    // send expired email notification
                    var emailExpiredTasks = new List<Task>();

                    foreach (var batch in expiredRequests.Batches(10))
                    {
                        emailExpiredTasks.AddRange(batch.Select(b => _emailService.SendLateRequestExpiredNotification(b)).ToArray());
                    }
                    await Task.WhenAll(emailExpiredTasks);
                }
            }
            catch (Exception e)
            {
                _logger.Error("SyncStaffWorkingTimes.Error", e);
                return Json(new
                {
                    Status = false,
                    Message = $"Sync working time error. \n{e.Message}.\n{e.StackTrace}."
                });
            }
            finally
            {
                _semaphoreSlim.Release();
            }

            return Json(new
            {
                Status = true,
                Message = $"Working day: {workingDate.Value.ToLongVnDateString()} timesheet was synced."
            });
        }
    }
}
