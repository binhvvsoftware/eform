using Autofac;
using Microsoft.AspNetCore.Mvc;
using CMS.Dashboard.Models;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Authorization;
using CMS.Dashboard.Web.UI;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using CMS.Core.Extensions;
using CMS.Dashboard.Web;
using System.Linq;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("workflow")]
    public class WorkflowController : BaseRoboController<int, Workflow, WorkflowModel>
    {
        private readonly IComponentContext _componentContext;

        public WorkflowController(IComponentContext componentContext, IWorkflowService service) : base(componentContext,
            service)
        {
            _componentContext = componentContext;
        }

        protected override bool ShowModalDialog => false;

        protected override void OnViewIndex(RoboUIGridResult<Workflow> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));
            roboGrid.AddColumn(x => x.Name).HasHeaderText("Tên quy trình").EnableFilter();
            roboGrid.AddColumn(x => x.WorkflowType).HasHeaderText("Loại quy trình").RenderAsHtml(x => x.WorkflowType.GetDisplayName()).EnableSorting();
            roboGrid.AddColumn(x => x.CreateDate).RenderAsHtml(x => x.CreateDate.ToShortDateString())
                .HasHeaderText("Ngày tạo").EnableSorting();
            roboGrid.AddColumn(x => x.CreateBy).HasHeaderText("Người tạo");
        }

        protected override void ConvertFromModel(WorkflowModel model, Workflow entity)
        {
            entity.Name = model.Name;
            entity.CampusId = model.CampusId;
            entity.FormTemplateId = model.FormTemplateId;
            entity.WorkflowType = model.WorkflowType;
            if (entity.Id <= 0)
            {
                entity.CreateDate = DateTime.Now;
                entity.CreateBy = User.Identity.Name;
            }
        }

        protected override WorkflowModel ConvertToModel(Workflow entity)
        {
            var model = new WorkflowModel
            {
                Id = entity.Id,
                Name = entity.Name,
                CampusId = entity.CampusId,
                FormTemplateId = entity.FormTemplateId,
                WorkflowType = entity.WorkflowType
            };
            return model;
        }

        [Themed(false)]
        [Route("create")]
        public override async Task<ActionResult> Create()
        {
            if (!await CheckManagePermissions()) throw new UnauthorizedAccessException();

            WorkContext.AddBreadcrumb(T("Quy trình"), Url.Action("Index", "Workflow"));
            WorkContext.AddBreadcrumb(T("Tạo quy trình"));
            ViewBag.Title = "Tạo quy trình";

            return View("CreateUpdate", new WorkflowModel()
            {
                FormTemplates = await _componentContext.Resolve<IFormTemplateService>().GetRecordsAsync(),
                Campuses = await _componentContext.Resolve<ICampusService>().GetRecordsAsync(),
            });
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public override async Task<ActionResult> Edit(int id)
        {
            if (!await CheckManagePermissions())
                throw new UnauthorizedAccessException();

            var workflow = await Service.GetByIdAsync(id);
            if (workflow == null)
                return new AjaxResult().Alert("Quy trình không tồn tại xin vui lòng thử lại.")
                    .Redirect(Url.Action("Index", "Workflow"));

            WorkContext.AddBreadcrumb(T("Tạo quy trình"));
            ViewBag.Title = "Chỉnh sửa quy trình";
            var currentSteps = await _componentContext.Resolve<IWorkflowStepService>()
                    .GetRecordsAsync(x => x.WorkflowId == id);
            var workflowModel = new WorkflowModel()
            {
                FormTemplates = await _componentContext.Resolve<IFormTemplateService>().GetRecordsAsync(),
                Campuses = await _componentContext.Resolve<ICampusService>().GetRecordsAsync(),
                Workflow = workflow,
                WorkflowSteps = currentSteps,
                SelectedUsers = currentSteps.SafeToDictionary(k => k.Step, v => new KeyValuePair<object, object>(v.GroupManagement.HasValue ? "Group" : "User", (object)v.GroupManagement ?? v.UserIds)),
                Users = await _componentContext.Resolve<IUserService>().GetRecordsAsync(x => (x.SeniorLeader || x.CampusId == workflow.CampusId) && !x.Disabled),
            };
            return View("CreateUpdate", workflowModel);
        }

        [Themed(false)]
        [Route("work/{id}")]
        public async Task<ActionResult> WorkTemplate(int workId, int workflowId)
        {
            var model = new Dictionary<string, string>();

            if (workflowId <= 0)
            {
                var workFlow = await _componentContext.Resolve<IFormTemplateService>().GetByIdAsync(workId);
                if (workFlow == null)
                    throw new ArgumentException("Quy trình không tồn tại");

                var teamplate = await _componentContext.Resolve<IFormTemplateService>().GetByIdAsync(workId);
                if (teamplate == null)
                    throw new ArgumentException("Mẫu văn bản không tồn tại");
            }
            else
            {
                var workflow = await Service.GetByIdAsync(workflowId);
                if (workflow == null)
                    throw new ArgumentException("Yêu cầu văn bản không tồn tại");
            }

            return View("WorkFormTemplate", model);
        }

        [Themed(false)]
        [HttpPost, Route("save")]
        public async Task<ActionResult> SaveWorkTemplate(WorkflowModel model)
        {
            if (Request.Form.IsNullOrEmpty())
                return new AjaxResult().Alert("Dữ liệu không hợp lệ.");

            if (model.GroupManage == null && model.UserIds == null)
                return new AjaxResult().Alert("Vui lòng tạo các bước và thêm người xử lý cho cho từng bước.")
                    .Redirect(Url.Action("Index", "Workflow"));

            Workflow workflow = new Workflow();
            bool isNew = false;

            if (model.Id > 0)
            {
                workflow = await Service.GetRecordAsync(s => s.Id == model.Id);

                if (workflow == null) return new AjaxResult().Alert("Quy trình không tồn tại xin vui lòng thử lại.");
            }
            else
            {
                isNew = true;
            }

            using (var tracking = BeginTracking(workflow, isNew))
            {
                try
                {
                    ConvertFromModel(model, workflow);

                    await SaveEntity(workflow, isNew);
                    await tracking.Complete(User.Identity.Name);
                    model.Id = workflow.Id;
                    var workflowSteps = GetWorkFlowSteps(model);
                    var workflowStepService = _componentContext.Resolve<IWorkflowStepService>();
                    var oldSteps = await workflowStepService.GetRecordsAsync(x => x.WorkflowId == workflow.Id);
                    if (!oldSteps.IsNullOrEmpty())
                    {
                        await workflowStepService.DeleteManyAsync(oldSteps);
                    }

                    if (!workflowSteps.IsNullOrEmpty())
                    {
                        await _componentContext.Resolve<IWorkflowStepService>().InsertManyAsync(workflowSteps);
                    }
                }
                catch (Exception ex)
                {
                    tracking.Abort();
                    Logger.Error("Have exception when update entity " + Name, ex);
                    return StatusCode(500, "Có vấn đề khi cập nhật dữ liệu, xin vui lòng thử lại sau.");
                }
            }

            ;

            return new AjaxResult().Alert($"Lưu quy trình {workflow.Name} thành công.")
                .Redirect(Url.Action("Index"), true);
        }

        private List<WorkflowStep> GetWorkFlowSteps(WorkflowModel model)
        {
            var steps = new List<WorkflowStep>();
            if (!model.GroupManage.IsNullOrEmpty())
                steps.AddRange(model.GroupManage.Select(x => GetWorkflowStep(model.Id, x)).ToList());
            if (!model.UserIds.IsNullOrEmpty())
                steps.AddRange(model.UserIds.Select(x => GetWorkflowStep(model.Id, x, true)).ToList());

            return steps;
        }

        private WorkflowStep GetWorkflowStep(int workflowId, KeyValuePair<int, int[]> keyValue,
            bool toggleUserAndGroup = false)
        {
            var (key, value) = keyValue;
            var step = new WorkflowStep
            {
                Name = $"Step {key}",
                Step = key,
                WorkflowId = workflowId,
                CreateDate = DateTime.Now,
                CreateBy = User.Identity.Name,
            };

            if (!toggleUserAndGroup)
            {
                step.GroupManagement = (GroupManagement) value.FirstOrDefault();
            }
            else
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var userId = value[i];
                    step.UserIds += i == value.Length - 1 ? userId.ToString() : userId + ",";
                }
            }

            return step;
        }
    }
}