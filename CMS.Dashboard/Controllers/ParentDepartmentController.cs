﻿using Autofac;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Dashboard.Controllers
{
    [Route("parent-department")]
    public class ParentDepartmentController : BaseRoboController<int, ParentDepartment, ParentDepartmentModel>
    {
        private readonly IComponentContext componentContext;

        public ParentDepartmentController(IComponentContext componentContext, IParentDepartmentService service)
            : base(componentContext, service)
        {
            this.componentContext = componentContext;
        }

        protected override string Name => "Cơ sở";

        protected override string PluralizedName => "Cơ sở";

        protected override string CreateText => "Thêm";

        protected override string DeleteText => "Xóa";

        protected override string EditText => "Sửa";

        protected override string ActionText => "Thao tác";

        protected override bool EnableTracking => true;

        protected override void OnViewIndex(RoboUIGridResult<ParentDepartment> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.AddColumn(x => x.Name, "Tên Cơ Sở").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Description, "Mô tả").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Status, "Trạng Thái").RenderAsStatusImage().AlignCenter().HasWidth(100);
        }

        protected override ParentDepartmentModel ConvertToModel(ParentDepartment entity)
        {
            return entity;
        }

        protected override void ConvertFromModel(ParentDepartmentModel model, ParentDepartment entity)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;
            entity.Status = model.Status;
        }
    }
}