﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Extensions;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using CMS.Shared.Web.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using IdentityRole = CMS.Shared.Data.Domain.IdentityRole;

namespace CMS.Dashboard.Controllers
{
    [Authorize, Route("role")]
    public class RoleController : BaseRoboController<int, IdentityRole, IdentityRoleModel>
    {
        private readonly IComponentContext componentContext;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IRoleService service;

        public RoleController(IComponentContext componentContext, IRoleService service, RoleManager<IdentityRole> roleManager) : base(componentContext, service)
        {
            this.service = service;
            this.componentContext = componentContext;
            this.roleManager = roleManager;
        }
        protected override string PluralizedName => "Quản lý vai trò";

        protected override bool ShowModalDialog => false;

        protected class PermissionComparer : IComparer<string>
        {
            private readonly IComparer<string> baseComparer;

            public PermissionComparer(IComparer<string> baseComparer)
            {
                this.baseComparer = baseComparer;
            }

            public int Compare(string x, string y)
            {
                var value = string.Compare(x, y, StringComparison.Ordinal);

                if (value == 0)
                    return 0;

                if (baseComparer.Compare(x, "System") == 0)
                    return -1;

                if (baseComparer.Compare(y, "System") == 0)
                    return 1;

                return value;
            }
        }

        protected override string Name => "Vai trò";

        protected override void OnViewIndex(RoboUIGridResult<IdentityRole> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));
            ViewBag.Layout = "Layout";

            roboGrid.Title = T(PluralizedName);
            roboGrid.ViewData = ViewData;
            roboGrid.FormActionUrl = Url.Action("Update");
            roboGrid.AddColumn(x => x.Name);
        }


        [Themed(false)]
        [Route("create")]
        public override async Task<ActionResult> Create()
        {
            if (!await CheckManagePermissions()) throw new UnauthorizedAccessException();

            WorkContext.AddBreadcrumb(T("Vai trò"), Url.Action("Index", "Role"));
            WorkContext.AddBreadcrumb(T("Tạo vai trò"));

            ViewBag.Title = "Tạo vai trò";

            var menuService = componentContext.Resolve<IMenuService>();

            return View("CreateUpdate", new RolePermissionsModel()
            {
                Menus = await menuService.GetRecordsAsync(),
                MenuSelecteds = new List<Menu>()
            });
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public override async Task<ActionResult> Edit(int id)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException("Vai trò không tồn tại");

            var menuService = componentContext.Resolve<IMenuService>();
            var menus = await menuService.GetRecordsAsync();

            return View("CreateUpdate", new RolePermissionsModel()
            {
                Menus = menus,
                Id = entity.Id,
                Name = entity.Name,
                MenuSelecteds = menus.Where(s => entity.MenuIds.SafeSplit().Select(y => int.Parse(y)).Contains(s.Id))
            });
        }

        protected override async Task OnValidate(IdentityRoleModel model)
        {
            var oldEntity = await service.GetRecordAsync(x => x.Name == model.Name);
            if (oldEntity != null && oldEntity.Id != model.Id)
            {
                throw new ArgumentException("Vai trò này đã tồn tại.");
            }
        }

        [Route("save"), Themed(false)]
        [HttpPost, FormButton("Save")]
        public async Task<ActionResult> UpdatePermissions(RolePermissionsModel model)
        {
            if (model.Permissions.IsNullOrEmpty())
                return new AjaxResult().Alert("Bạn phải gán ít nhất 1 permission cho role.");

            IdentityRole role = null;
            bool isNew = false;

            if (model.Id > 0)
            {
                role = await service.GetRecordAsync(s => s.Id == model.Id);

                if (role == null)
                {
                    return new AjaxResult().Alert("Vai trò không tồn tại xin vui lòng thử lại");
                }
            }
            else
            {
                role = new IdentityRole();
                isNew = true;
            }


            using (var tracking = BeginTracking(role, false))
            {
                try
                {
                    role.NormalizedName = model.Name.ToUpperInvariant();
                    role.Name = model.Name;
                    role.MenuIds = model.Permissions.Where(s => s.Value == "on").Select(s => s.Key).SafeJoin();
                    await SaveEntity(role, isNew);
                    await tracking.Complete(User.Identity.Name);
                }
                catch (Exception ex)
                {
                    tracking.Abort();
                    Logger.Error("Have exception when update entity " + Name, ex);
                    return StatusCode(500, "Có vấn đề khi cập nhật dữ liệu, xin vui lòng thử lại sau.");
                }
            };

            return new AjaxResult().Alert("Gán quyền cho vai trò thành công").Redirect(Url.Action("Index"), true);
        }

        private IEnumerable<Permission> GetPermissions()
        {
            return componentContext.Resolve<IEnumerable<IPermissionProvider>>()
                .SelectMany(x => x.GetPermissions()).ToList();
        }

        protected override IdentityRoleModel ConvertToModel(IdentityRole entity)
        {
            return new IdentityRoleModel
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        protected override void ConvertFromModel(IdentityRoleModel model, IdentityRole entity)
        {
            entity.Name = model.Name;
            entity.Id = model.Id;
            entity.NormalizedName = model.Name.ToUpperInvariant();
        }
    }
}