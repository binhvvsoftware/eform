﻿using Microsoft.AspNetCore.Mvc;

namespace CMS.Dashboard.Controllers
{
    public class ErrorController : Controller
    {
        [Route("error/{code:int}")]
        public IActionResult Error(int code)
        {
            switch (code)
            {
                case 403:
                    return View("403");
                case 404:
                    return View("404");
                default:
                    return View("500");
            }
        }
    }
}