﻿using Autofac;
using CMS.Core.Entity;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.Notify;
using CMS.Dashboard.Web.UI.RoboUI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CMS.Services.Services;
using CMS.Shared;
using CMS.Shared.Extensions;
using CMS.Core.Extensions;
using CMS.Services.CompareObjects;
using CMS.Services.Tracking;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Controllers.Common
{
    public abstract class BaseRoboController<TKey, TEntity, TModel> : BaseController
        where TModel : BaseModel<TKey>, new()
        where TEntity : class, IBaseEntity<TKey>, new()
        where TKey : struct
    {
        private readonly IComponentContext componentContext;
        private readonly IGenericService<TEntity, TKey> service;
        private string name;
        private string pluralizeName;

        protected BaseRoboController(IComponentContext componentContext, IGenericService<TEntity, TKey> service)
            : base(componentContext)
        {
            this.componentContext = componentContext;
            this.service = service;
        }

        protected virtual string Name => name ?? (name = typeof(TEntity).Name.SpacePascal());

        protected virtual string PluralizedName => pluralizeName ?? (pluralizeName = Name + "s");

        protected virtual IGenericService<TEntity, TKey> Service => service;

        protected virtual Task<bool> IsEnableCreate()
        {
            return CheckManagePermissions();
        }

        protected virtual Task<bool> IsEnableEdit()
        {
            return CheckManagePermissions();
        }

        protected virtual Task<bool> IsEnableDelete()
        {
            return CheckManagePermissions();
        }

        protected virtual string CreateText => "Tạo";

        protected virtual string EditText => "Sửa";

        protected virtual string DeleteText => "Xóa";

        protected virtual string ActionText => "Thao tác";

        protected virtual string ConfirmDeleteMessage => Constants.LocalizedStrings.ConfirmDeleteRecord;

        protected virtual bool ShowModalDialog => true;

        protected virtual bool ShowCreateModalDialog => ShowModalDialog;

        protected virtual bool ShowEditModalDialog => ShowModalDialog;

        protected virtual int DialogModalWidth => 600;

        protected virtual bool EnablePaginate => true;

        protected virtual string CancelButtonUrl => null;

        protected virtual bool EnableTracking => false;

        protected virtual bool EnableVersion => false;

        protected virtual IDictionary<string, object> GetTrackingData(TEntity entity)
        {
            return new Dictionary<string, object>();
        }

        [Route("")]
        public virtual async Task<ActionResult> Index()
        {
            if (!await CheckViewPermissions())
            {
                throw new UnauthorizedAccessException();
            }

            return await IndexCore();
        }

        protected virtual async Task<RoboUIGridResult<TEntity>> IndexCore()
        {
            var result = new RoboUIGridResult<TEntity>(ControllerContext)
            {
                Title = string.Format(Constants.LocalizedStrings.ManageFor, PluralizedName),
                FetchAjaxSource = GetRecords,
                FormActionUrl = Url.Action("Update"),
                DefaultPageSize = 15,
                EnablePaginate = EnablePaginate,
                ActionsHeaderText = ActionText,
                ViewData = ViewData,
                TempData = TempData,
                ActionsColumnWidth = 80
            };

            OnViewIndex(result);
            await OnViewIndexAsync(result);

            if (Request.Method == "GET")
            {
                if (await IsEnableCreate())
                {
                    var createAction = result.AddAction()
                        .HasIconCssClass("fa fa-plus")
                        .HasUrl(Url.Action("Create", RouteData.Values))
                        .HasDescription("Thêm mới")
                        .HasButtonStyle(ButtonStyle.Success);
                    if (ShowCreateModalDialog)
                    {
                        createAction.ShowModalDialog(DialogModalWidth);
                    }

                    OnCreateCreateButton(createAction);
                }

                result.AddReloadEvent("UPDATE_ENTITY_COMPLETE");
                result.AddReloadEvent("DELETE_ENTITY_COMPLETE");
            }

            if (await IsEnableEdit())
            {
                var editAction = result.AddRowAction()
                    .HasUrl(x => Url.Action("Edit", new { id = x.Id }))
                    .HasButtonStyle(ButtonStyle.Primary)
                    .HasDescription("Chỉnh sửa")
                    .HasCssClass("fa fa-edit");

                if (ShowEditModalDialog)
                {
                    editAction.ShowModalDialog(DialogModalWidth);
                }

                OnCreateEditButton(editAction);
            }

            //if (EnableVersion)
            //{
            //    var versionHistoryService = componentContext.Resolve<IVersionHistoryService>();
            //    var typeId = versionHistoryService.GetEntityTypeId(typeof(TEntity));

            //    result.AddRowAction()
            //        .HasText(T("Versions"))
            //        .HasUrl(x => Url.Action("Versions", new { typeId, entityId = x.Id.ToString() }))
            //        .HasButtonStyle(ButtonStyle.Info)
            //        .HasButtonSize(ButtonSize.ExtraSmall)
            //        .ShowModalDialog();
            //}

            if (await IsEnableDelete())
            {
                var deleteAction = result.AddRowAction(true)
                    .HasAttribute("formaction", Url.Action("Delete"))
                    .HasName("Delete")
                    .HasValue(x => x.Id.ToString())
                    .HasButtonStyle(ButtonStyle.Danger)
                    .HasCssClass("fa fa-trash")
                    .HasDescription("Xóa")
                    .HasConfirmMessage(ConfirmDeleteMessage);
                OnCreateDeleteButton(deleteAction);
            }

            return result;
        }

        protected virtual void OnCreateCreateButton(RoboUIFormAction createButton)
        {
        }

        protected virtual void OnCreateEditButton(RoboUIGridRowAction<TEntity> editButton)
        {
        }

        protected virtual void OnCreateDeleteButton(RoboUIGridRowAction<TEntity> deleteButton)
        {
        }

        protected virtual Task<bool> CheckManagePermissions()
        {
            return WorkContext.CheckPermission(MenuAction.IsManage);
        }

        protected virtual Task<bool> CheckViewPermissions()
        {
            return WorkContext.CheckPermission(MenuAction.IsView);
        }

        protected virtual void OnViewIndex(RoboUIGridResult<TEntity> roboGrid)
        {

        }

        protected virtual Task OnViewIndexAsync(RoboUIGridResult<TEntity> roboGrid)
        {
            return Task.CompletedTask;
        }

        protected virtual async Task<RoboUIGridAjaxData<TEntity>> GetRecords(RoboUIGridRequest options)
        {
            var records = await Service.GetRecords(options);
            return new RoboUIGridAjaxData<TEntity>(records, records.ItemCount);
        }

        [Themed(false)]
        [Route("create")]
        public virtual async Task<ActionResult> Create()
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            var model = CreateNewModel();

            var result = new RoboUIFormResult<TModel>(model, ControllerContext)
            {
                Title = string.Format(Constants.LocalizedStrings.CreateFor, Name),
                FormActionUrl = Url.Action("Update"),
                ShowCloseButton = true,
                CancelButtonUrl = CancelButtonUrl,
                ViewData = ViewData,
                TempData = TempData
            };

            result.SubmitButtonHtmlAttributes.Add("name", "Save");

            OnCreating(result);
            await OnCreatingAsync(result);

            return result;
        }

        protected virtual TModel CreateNewModel()
        {
            return new TModel();
        }

        protected virtual void OnCreating(RoboUIFormResult<TModel> roboForm)
        {
        }

        protected virtual Task OnCreatingAsync(RoboUIFormResult<TModel> roboForm)
        {
            return Task.CompletedTask;
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public virtual async Task<ActionResult> Edit(TKey id)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            if (id.Equals(default(TKey)))
                throw new ArgumentException("No records found with the specified id");

            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException("No record found with the specified id");

            var model = ConvertToModel(entity);

            var result = new RoboUIFormResult<TModel>(model, ControllerContext)
            {
                Title = string.Format(Constants.LocalizedStrings.EditFor, Name),

                FormActionUrl = Url.Action("Update"),
                ShowCloseButton = true,
                CancelButtonUrl = CancelButtonUrl,
                ViewData = ViewData,
                TempData = TempData
            };

            result.SubmitButtonHtmlAttributes.Add("name", "Save");

            OnEditing(result);
            await OnEditingAsync(result);

            return result;
        }

        protected virtual void OnEditing(RoboUIFormResult<TModel> roboForm)
        {
        }

        protected virtual Task OnEditingAsync(RoboUIFormResult<TModel> roboForm)
        {
            return Task.CompletedTask;
        }

        [FormButton("Delete")]
        [HttpPost, Route("delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Delete(TKey id)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            try
            {
                if (id.Equals(default(TKey)))
                    throw new ArgumentException("No records found with the specified id");

                var entity = await Service.GetByIdAsync(id);
                if (entity == null)
                    throw new ArgumentException("No records found with the specified id");

                await DeleteAsync(entity);

                if (EnableTracking)
                {
                    var trackingEntityService = componentContext.Resolve<ITrackingEntityService>();
                    if (trackingEntityService != null)
                    {
                        var compareLogic = new CompareLogic(new ComparisonConfig
                        {
                            MaxDifferences = 9999
                        });

                        var result = compareLogic.Compare(entity, new TEntity());
                        result.EntityName = Name;
                        result.Data = GetTrackingData(entity);

                        await trackingEntityService.OnDelete(result, User.Identity.Name);
                    }
                }

                return await OnDeleteSuccess(entity);
            }
            catch (Exception ex)
            {
                return new AjaxResult().Alert(ex.Message);
            }

        }

        protected virtual Task<TEntity> GetByIdAsync(TKey key)
        {
            return Service.GetByIdAsync(key);
        }

        protected virtual async Task DeleteAsync(TEntity entity)
        {
            await Service.DeleteAsync(entity);
        }

        protected virtual async Task<ActionResult> OnDeleteSuccess(TEntity entity)
        {
            return await Task.FromResult(new AjaxResult().NotifyMessage("DELETE_ENTITY_COMPLETE"));
        }

        [Route("update")]
        [HttpPost, FormButton("Save")]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Update(TModel model)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            try
            {
                await OnValidate(model);
            }
            catch (Exception ex)
            {
                WorkContext.Notifier.Add(NotifyType.Error, ex.Message);
                return new AjaxResult();
            }

            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return new AjaxResult();
            }

            TEntity entity;
            bool isNew = false;
            if (model.Id.Equals(default(TKey)))
            {
                entity = new TEntity();
                isNew = true;
            }
            else
            {
                entity = await GetEntityForUpdate(model.Id);
            }

            using (var tracking = BeginTracking(entity, isNew))
            {
                try
                {
                    ConvertFromModel(model, entity);
                    await ConvertFromModelAsync(model, entity);
                    await SaveEntity(entity, isNew);
                    await tracking.Complete(User.Identity.Name);
                }
                catch (Exception ex)
                {
                    tracking.Abort();
                    Logger.Error("Have exception when update entity " + Name, ex);

                    return StatusCode(500, "Có vấn đề khi cập nhật dữ liệu, xin vui lòng thử lại sau.");
                }
            };

            return OnUpdateSuccess(entity, isNew);
        }

        protected virtual async Task OnValidate(TModel model)
        {
            await Task.FromResult(0);
        }

        protected virtual async Task SaveEntity(TEntity entity, bool isNew)
        {
            if (isNew)
            {
                await Service.InsertAsync(entity);
            }
            else
            {
                await Service.UpdateAsync(entity);
            }
        }

        protected virtual async Task<TEntity> GetEntityForUpdate(TKey id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException("No record found with the specified id");

            return entity;
        }

        protected virtual ActionResult OnUpdateSuccess(TEntity entity, bool isNew)
        {
            return (isNew ? ShowCreateModalDialog : ShowEditModalDialog)
                ? new AjaxResult().NotifyMessage("UPDATE_ENTITY_COMPLETE").CloseModalDialog()
                : new AjaxResult().Redirect(Url.Action("Index"), true);
        }

        protected abstract TModel ConvertToModel(TEntity entity);

        protected virtual void ConvertFromModel(TModel model, TEntity entity)
        {

        }

        protected virtual Task ConvertFromModelAsync(TModel model, TEntity entity)
        {
            return Task.CompletedTask;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await base.OnActionExecutionAsync(context, next);

            ViewBag.Layout = "Layout";

            var actionDescriptor = (ControllerActionDescriptor)context.ActionDescriptor;

            if (actionDescriptor.ActionName.Equals("Create") || actionDescriptor.ActionName.Equals("Edit"))
            {
                if (ShowCreateModalDialog && actionDescriptor.ActionName.Equals("Create"))
                {
                    ViewBag.Layout = "Layout-Minimal";
                }
                else if (ShowEditModalDialog && actionDescriptor.ActionName.Equals("Edit"))
                {
                    ViewBag.Layout = "Layout-Minimal";
                }
            }
            else
            {
                var attributes = actionDescriptor.MethodInfo.GetCustomAttributes(typeof(ThemedAttribute), false);
                foreach (var attribute in attributes)
                {
                    var themedAttribute = (ThemedAttribute)attribute;
                    if (themedAttribute.Minimal)
                    {
                        ViewBag.Layout = "Layout-Minimal";
                    }
                }
            }
        }

        #region Tracking 
        protected TrackingEntityManager<TEntity> BeginTracking(TEntity entity, bool isNew)
        {
            var trackingEntityManager = new TrackingEntityManager<TEntity>(componentContext, entity, isNew)
            {
                Name = Name,
                EnableTracking = EnableTracking,
                EnableVersion = EnableVersion,
                GetTrackingData = GetTrackingData
            };

            return trackingEntityManager;
        }
        #endregion
    }

    public abstract class BaseRoboController<TKey, TEntity, TCreateModel, TEditModel> : BaseRoboController<TKey, TEntity, TCreateModel>
        where TEntity : class, IBaseEntity<TKey>, new()
        where TKey : struct
        where TCreateModel : BaseModel<TKey>, new()
        where TEditModel : BaseModel<TKey>, new()
    {
        protected BaseRoboController(IComponentContext componentContext, IGenericService<TEntity, TKey> service) : base(componentContext, service)
        {
        }

        [Themed(false)]
        [Route("create")]
        public override async Task<ActionResult> Create()
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            var model = CreateNewModel();

            var result = new RoboUIFormResult<TCreateModel>(model, ControllerContext)
            {
                Title = string.Format(Constants.LocalizedStrings.CreateFor, Name),
                FormActionUrl = Url.Action("Insert"),
                ShowCloseButton = true,
                CancelButtonUrl = CancelButtonUrl,
                ViewData = ViewData,
                TempData = TempData
            };

            result.SubmitButtonHtmlAttributes.Add("name", "Insert");

            OnCreating(result);
            await OnCreatingAsync(result);

            return result;
        }

        [Themed(false)]
        [Route("edit/{id}")]
        public override async Task<ActionResult> Edit(TKey id)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            if (id.Equals(default(TKey)))
                throw new ArgumentException("No records found with the specified id");

            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException("No records found with the specified id");

            var model = ConvertToEditModel(entity);

            var result = new RoboUIFormResult<TEditModel>(model, ControllerContext)
            {
                Title = string.Format(Constants.LocalizedStrings.EditFor, Name),
                FormActionUrl = Url.Action("Update"),
                ShowCloseButton = true,
                CancelButtonUrl = CancelButtonUrl,
                ViewData = ViewData,
                TempData = TempData
            };

            result.SubmitButtonHtmlAttributes.Add("name", "Update");

            OnEditing(result);

            return result;
        }

        protected sealed override TCreateModel ConvertToModel(TEntity entity)
        {
            throw new NotSupportedException();
        }

        protected sealed override void ConvertFromModel(TCreateModel model, TEntity entity)
        {
            ConvertFromCreateModel(model, entity);
        }

        protected virtual void OnEditing(RoboUIFormResult<TEditModel> roboForm)
        {
        }

        protected abstract TEditModel ConvertToEditModel(TEntity entity);

        protected abstract void ConvertFromCreateModel(TCreateModel model, TEntity entity);

        protected abstract void ConvertFromEditModel(TEditModel model, TEntity entity);

        [NonAction]
        public sealed override Task<ActionResult> Update(TCreateModel model)
        {
            throw new NotSupportedException();
        }

        [Route("insert")]
        [HttpPost, FormButton("Insert")]
        public virtual Task<ActionResult> Insert(TCreateModel model)
        {
            return base.Update(model);
        }

        [Route("update")]
        [HttpPost, FormButton("Update")]
        public virtual async Task<ActionResult> Update(TEditModel model)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            var entity = await GetEntityForUpdate(model.Id);
            ConvertFromEditModel(model, entity);
            await Service.UpdateAsync(entity);
            return OnUpdateSuccess(entity, false);
        }
    }
}