﻿using System;
using System.IO;
using System.Threading.Tasks;
using Autofac;
using CMS.Dashboard.Web.UI.Notify;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;

namespace CMS.Dashboard.Controllers.Common
{
    public abstract class BaseController : Controller
    {
        public ILogger Logger
        {
            get; set;
        }

        public WorkContext WorkContext
        {
            get; set;
        }

        protected BaseController(IComponentContext componentContext)
        {
            var loggerFactory = componentContext.Resolve<ILoggerFactory>();
            Logger = loggerFactory.CreateLogger(GetType());
            WorkContext = componentContext.Resolve<WorkContext>();
        }

        protected string T(string value)
        {
            return value;
        }

        protected string T(string value, params object[] args)
        {
            return string.Format(value, args);
        }

        protected async Task<string> RenderRazorViewToString(string viewName, object model)
        {
            var actionContext = new ActionContext(ControllerContext.HttpContext, ControllerContext.RouteData, ControllerContext.ActionDescriptor);

            var razorViewEngine = (IRazorViewEngine) HttpContext.RequestServices.GetService(typeof(IRazorViewEngine));
            var tempDataProvider = (ITempDataProvider) HttpContext.RequestServices.GetService(typeof(ITempDataProvider));

            var viewResult = razorViewEngine.FindView(actionContext, viewName, false);
            if(viewResult.View == null)
            {
                throw new ArgumentNullException($"{viewName} does not match any available view.");
            }

            var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
            {
                Model = model
            };

            using(var sw = new StringWriter())
            {
                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }

        protected void NotifyModelStateErrors()
        {
            if(ModelState.IsValid)
            {
                return;
            }

            foreach(var modelState in ModelState.Values)
            {
                foreach(var modelError in modelState.Errors)
                {
                    WorkContext.Notifier.Add(NotifyType.Error, modelError.ErrorMessage);
                }
            }
        }
    }
}
