﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Extensions;
using CMS.Core.Linq;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI;
using CMS.Dashboard.Web.UI.Notify;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using CMS.Shared.Validate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Distributed;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;

namespace CMS.Dashboard.Controllers
{
    [Authorize]
    [Route("user")]
    public class UserController : BaseRoboController<int, IdentityUser, IdentityUserModel>
    {
        private readonly IComponentContext _componentContext;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserService _service;
        private readonly IDepartmentService _departmentService;
        private readonly ICampusService _campusService;
        private readonly IMemberRelationService _memberRelationService;

        public UserController(IComponentContext componentContext, IUserService service,
            UserManager<IdentityUser> userManager, IDepartmentService departmentService,
            IMemberRelationService memberRelationService, ICampusService campusService) : base(componentContext,
            service)
        {
            _service = service;
            _componentContext = componentContext;
            _userManager = userManager;
            _departmentService = departmentService;
            _memberRelationService = memberRelationService;
            _campusService = campusService;
        }

        protected override string Name => "Người dùng";

        protected override string PluralizedName => "Người dùng";

        protected override string ConfirmDeleteMessage => "Bạn có chắc chắn muốn xóa người dùng này?";

        protected override async Task OnViewIndexAsync(RoboUIGridResult<IdentityUser> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.ClientId = "tblUsers";

            var searchForm = new RoboUIFormResult(ControllerContext)
            {
                ViewData = ViewData,
                ShowCancelButton = false,
                ShowSubmitButton = false,
                DisableGenerateForm = true
            };

            var campus = await _componentContext.Resolve<ICampusService>().GetRecordsAsync()
                .ToSelectListAsync(k => k.Id, v => v.Name);
            searchForm.AddProperty("CampusId",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Cơ sở",
                    ContainerCssClass = "col-md-3 col-xs-3",
                    ContainerRowIndex = 1,
                    OptionLabel = "Tất cả",
                    Value = "",
                    PropertyType = typeof(string),
                    SelectListItems = campus,
                    HtmlAttributes = new Dictionary<string, object> {{"class", "booking-filter"}}
                });

            var userRoles = await _componentContext.Resolve<IRoleService>().GetRecordsAsync()
                .ToSelectListAsync(k => k.Id, v => v.Name);
            searchForm.AddProperty("UserRoleId",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Vai trò",
                    ContainerCssClass = "col-md-3 col-xs-3",
                    ContainerRowIndex = 1,
                    OptionLabel = "Tất cả",
                    Value = "",
                    PropertyType = typeof(string),
                    SelectListItems = userRoles,
                    HtmlAttributes = new Dictionary<string, object> {{"class", "booking-filter"}}
                });

            var userTypes = Enum.GetValues(typeof(UserType)).ToListOf<UserType>()
                .ToSelectList(x => x, x => x.GetDisplayName());
            searchForm.AddProperty("UserType",
                new RoboChoiceAttribute(RoboChoiceType.DropDownList)
                {
                    LabelText = "Đối tượng",
                    ContainerCssClass = "col-md-3 col-xs-6",
                    ContainerRowIndex = 1,
                    OptionLabel = "Tất cả",
                    SelectListItems = userTypes,
                    HtmlAttributes = new Dictionary<string, object> {{"class", "booking-filter"}}
                });

            searchForm.AddProperty("ButtonSearch",
                new RoboButtonAttribute
                {
                    OnClick = roboGrid.GetReloadClientScript(),
                    ContainerCssClass = "col-md-1 col-xs-1",
                    ContainerRowIndex = 1,
                    LabelText = "Tìm kiếm",
                    HideLabelControl = true,
                    HasLabelControl = true,
                    HtmlAttributes = new Dictionary<string, object> {{"class", "btn btn-primary show"}}
                });

            roboGrid.AddCustomVariable("CampusId", "$('#CampusId').val();", true);
            roboGrid.AddCustomVariable("UserRoleId", "$('#UserRoleId').val();", true);
            roboGrid.AddCustomVariable("UserType", "$('#UserType').val();", true);

            roboGrid.FilterForm = searchForm;

            roboGrid.EnableSearch = true;
            roboGrid.Title = "Quản lý Người dùng";
            roboGrid.ViewData = ViewData;
            roboGrid.FetchAjaxSource = GetRecords;
            roboGrid.AddColumn(x => x.FullName).HasHeaderText(T("Tên")).EnableFilter().EnableSorting();
            roboGrid.AddColumn(x => x.Email).EnableFilter().EnableSorting();
            roboGrid.AddColumn(x => x.DepartmentName).EnableSorting().HasHeaderText(T("Bộ phận"));
            roboGrid.AddColumn(x => x.Managers)
                .RenderAsHtml(x => string.Join("<br/>", x.Managers.Select(y => y.FullName)))
                .HasHeaderText(T("Quản lý"));

            if (await WorkContext.CheckPermission(MenuAction.IsManage))
            {
                var btnActions = roboGrid.AddRowAction().HasCssClass("fa fa-wrench").HasButtonStyle(ButtonStyle.Info);
                btnActions.AddChildAction()
                    .HasText(T("Vai trò"))
                    .HasUrl(x => Url.Action("EditRoles", new
                    {
                        userId = x.Id
                    }))
                    .HasButtonSize(ButtonSize.Small)
                    .HasButtonStyle(ButtonStyle.Info)
                    .HasCssClass("btn-block")
                    .ShowModalDialog();

                btnActions.AddChildAction()
                    .HasText(T("Thay đổi mật khẩu"))
                    .HasUrl(x => Url.Action("ChangePassword", new
                    {
                        userId = x.Id
                    }))
                    .HasButtonSize(ButtonSize.Small)
                    .HasButtonStyle(ButtonStyle.Warning)
                    .HasCssClass("btn-block")
                    .ShowModalDialog();
            }

            ViewBag.Layout = "Layout";
        }

        [Themed(false)]
        [HttpGet, Route("admin/change-password/{userId}")]
        public async Task<ActionResult> ChangePassword(int userId)
        {
            var user = await Service.GetByIdAsync(userId);

            var model = new ChangePasswordModel
            {
                UserId = user.Id,
                UserName = user.UserName,
                Email = user.Email
            };

            var retult = new RoboUIFormResult<ChangePasswordModel>(model, ControllerContext)
            {
                FormId = "fChangePassword",
                Title = T("Đổi mật khẩu"),
                ViewData = ViewData,
                FormActionUrl = Url.Action("UpdatePassword"),
                Encrypted = true
            };

            retult.ExcludeProperty(x => x.OldPassword);

            WorkContext.AddInlineScript("$('#fChangePassword').disableAutoFill();");

            return retult;
        }

        [Themed(false), FormButton("Save")]
        [HttpPost, Route("update-password")]
        public async Task<ActionResult> UpdatePassword(ChangePasswordModel model)
        {
            if (!await CheckManagePermissions())
            {
                throw new UnauthorizedAccessException();
            }

            var user = await _userManager.FindByNameAsync(model.UserName);

            var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);

            var result = await _userManager.ResetPasswordAsync(user, resetToken, model.Password);

            if (result.Succeeded)
                return new AjaxResult().Alert("Đổi mật khẩu thành công.").CloseModalDialog();

            foreach (var error in result.Errors)
                WorkContext.Notifier.Add(NotifyType.Error, error.Description);

            return new AjaxResult();
        }


        [HttpPost, IgnoreAntiforgeryToken]
        [Route("remove-current-loggedin")]
        public async Task<IActionResult> RemoveCurrentLoggedIn(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return BadRequest();
            }

            var distributedCache =
                (IDistributedCache) HttpContext.RequestServices.GetService(typeof(IDistributedCache));
            await distributedCache.RemoveAsync("IsUserLoggedIn:" + username.ToLowerInvariant());

            return Ok();
        }

        protected override IdentityUserModel ConvertToModel(IdentityUser entity)
        {
            return entity;
        }

        protected override void OnCreating(RoboUIFormResult<IdentityUserModel> roboForm)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboForm.Title = T("Tạo tài khoản");
            roboForm.ViewData = ViewData;
            roboForm.FormActionUrl = Url.Action("Update");
            roboForm.ShowCloseButton = true;
            roboForm.CancelButtonUrl = CancelButtonUrl;
            roboForm.Encrypted = true;

            var callbackUrl = Request.Query["callbackUrl"];

            if (!string.IsNullOrEmpty(callbackUrl))
                roboForm.AddHiddenValue("CallbackUrl", callbackUrl);
        }

        protected override void OnEditing(RoboUIFormResult<IdentityUserModel> roboForm)
        {
            base.OnEditing(roboForm);

            roboForm.ExcludeProperty(x => x.Password);
            roboForm.ExcludeProperty(x => x.ConfirmPassword);
            roboForm.ReadOnlyProperties = new List<string>() {nameof(IdentityUserModel.UserName)};
        }

        protected override async Task OnCreatingAsync(RoboUIFormResult<IdentityUserModel> roboForm)
        {
            var user = await WorkContext.GetCurrentUser();
            // Bind contract type value
            roboForm.RegisterExternalDataSource(x => x.ContractType, EnumExtensions.GetValues<ContractType>()
                .ToSelectList(k => (short) k, v => v.GetDisplayName()));
            // Bind gender value
            roboForm.RegisterExternalDataSource(x => x.Gender, EnumExtensions.GetValues<Gender>()
                .ToSelectList(k => (short) k, v => v.GetDisplayName()));
            // Bind campus value
            roboForm.RegisterExternalDataSource(x => x.CampusId,
                await _campusService.GetRecordsAsync().ToSelectListAsync(k => k.Id, v => v.Name));
            roboForm.RegisterCascadingDropDownDataSource(x => x.DepartmentId, Url.Action("GetDepartmentByCampus"));
            // Bind managers value
            roboForm.RegisterExternalDataSource(x => x.ManagerIds,
                await Service.GetRecordsAsync(x => x.CampusId == user.CampusId || x.SeniorLeader)
                    .ToSelectListAsync(k => k.Id.ToString(), v => v.UserName));

            roboForm.RegisterExternalDataSource(x => x.UserType,
                EnumExtensions.GetValues<UserType>().ToSelectList(v => (short) v, t => t.GetDisplayName()));
        }

        protected override async Task OnEditingAsync(RoboUIFormResult<IdentityUserModel> roboForm)
        {
            roboForm.FormModel.ManagerIds = await _componentContext.Resolve<IMemberRelationService>()
                .GetRecordsAsync(x => x.UserId == roboForm.FormModel.Id)
                .Continue(x => x.Select(c => c.ManagerId).ToArray());
            // Bind contract type value
            roboForm.RegisterExternalDataSource(x => x.ContractType, EnumExtensions.GetValues<ContractType>()
                .ToSelectList(k => (short) k, v => v.GetDisplayName()));
            // Bind gender value
            roboForm.RegisterExternalDataSource(x => x.Gender, EnumExtensions.GetValues<Gender>()
                .ToSelectList(k => (short) k, v => v.GetDisplayName()));
            // Bind campus value
            roboForm.RegisterExternalDataSource(x => x.CampusId,
                await _campusService.GetRecordsAsync().ToSelectListAsync(k => k.Id, v => v.Name));
            roboForm.RegisterCascadingDropDownDataSource(x => x.DepartmentId, Url.Action("GetDepartmentByCampus"));
            roboForm.RegisterExternalDataSource(x => x.ManagerIds, await Service.GetRecordsAsync(x =>
                x.CampusId == roboForm.FormModel.CampusId || x.SeniorLeader)
                .ToSelectListAsync(k => k.Id.ToString(), v => v.UserName));

            roboForm.RegisterExternalDataSource(x => x.UserType,
                EnumExtensions.GetValues<UserType>().ToSelectList(v => (short) v, t => t.GetDisplayName()));
        }

        [HttpPost, Route("get-department-by-campus")]
        public async Task<ActionResult> GetDepartmentByCampus()
        {
            var sender = Convert.ToString(Request.Form["sender"]);

            int.TryParse(Request.Form[sender], out var campusId);

            if (campusId <= 0) return Json(null);

            var departments = await _departmentService.GetRecordsAsync(x => x.CampusId == campusId);

            return Json(departments.Select(x => new SelectListItem() {Text = $"{x.Name}", Value = $"{x.Id}"}));
        }

        [FormButton("Save")]
        [HttpPost]
        [Route("update")]
        public override async Task<ActionResult> Update(IdentityUserModel model)
        {
            await OnValidate(model);

            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return new AjaxResult();
            }

            var user = new IdentityUser();
            var isNew = false;

            if (model.Id <= 0)
            {
                ConvertFromModel(model, user);
                //Check password rules
                if (!PasswordValidator.IsPasswordValidate(model.Password, model.UserName))
                {
                    return new AjaxResult().Alert(PasswordValidator.PasswordAlert);
                }

                var createUserResult = await _userManager.CreateAsync(user, model.Password);
                if (!createUserResult.Succeeded)
                    foreach (var error in createUserResult.Errors)
                    {
                        WorkContext.Notifier.Add(NotifyType.Error, error.Description);
                        return new AjaxResult();
                    }

                isNew = true;
            }
            else
            {
                user = await _userManager.FindByIdAsync(model.Id.ToString());

                ConvertFromModel(model, user);

                // Validate duplicate user name & email
                var oldUser = await _userManager.FindByNameAsync(model.UserName);
                if (oldUser != null && oldUser.Id != user.Id)
                {
                    WorkContext.Notifier.Add(NotifyType.Error, $"Tài khoản '{user.UserName}' đã tồn tại");
                    return new AjaxResult();
                }

                oldUser = await _userManager.FindByEmailAsync(model.Email);
                if (oldUser != null && oldUser.Id != user.Id)
                {
                    WorkContext.Notifier.Add(NotifyType.Error, $"Email '{user.Email}' đã tồn tại.");
                    return new AjaxResult();
                }

                await _service.UpdateAsync(user);
            }

            var memberRelation = _componentContext.Resolve<IMemberRelationService>();
            await memberRelation.DeleteManyAsync(await memberRelation.GetRecordsAsync(x => x.UserId == user.Id));

            if (!model.ManagerIds.IsNullOrEmpty())
            {
                await memberRelation.InsertManyAsync(model.ManagerIds.Select(x => new MemberRelation()
                {
                    UserId = user.Id,
                    ManagerId = x
                }));
            }

            var callbackUrl = Request.Form["CallbackUrl"];

            return !string.IsNullOrEmpty(callbackUrl)
                ? new AjaxResult().Redirect(callbackUrl + "?userId=" + user.Id, true)
                : OnUpdateSuccess(user, isNew);
        }

        protected override Task DeleteAsync(IdentityUser entity)
        {
            entity.Disabled = true;
            //            entity.UpdateDate = DateTime.Now;
            return Service.UpdateAsync(entity);
        }

        protected override void ConvertFromModel(IdentityUserModel model, IdentityUser entity)
        {
            if (model.Id <= 0)
            {
                entity.UserName = model.UserName.Trim().ToLowerInvariant();
                entity.CreateDate = DateTime.Now;
            }

            entity.NormalizedUserName = model.UserName.Trim().ToUpperInvariant();
            entity.Email = model.Email.Trim().ToLowerInvariant();
            entity.NormalizedEmail = model.Email.Trim().ToUpperInvariant();
            entity.PhoneNumber = model.PhoneNumber;
            entity.EmailConfirmed = true;
            entity.PhoneNumberConfirmed = true;
            entity.DepartmentId = model.DepartmentId;
            entity.FullName = model.FullName;
            entity.CampusId = model.CampusId;
            entity.ContractType = model.ContractType;
            entity.Gender = model.Gender;
            entity.Address = model.Address;
            entity.EmployeeCode = model.EmployeeCode;
            entity.SeniorLeader = model.SeniorLeader;
            entity.Disabled = model.Disabled;
            entity.IsManageRequestDocument = model.IsManageRequestDocument;
            entity.UserType = model.UserType;
            //            entity.UpdateDate = DateTime.Now;
        }

        [Themed(false)]
        [HttpGet, Route("edit-roles/{userId}")]
        public virtual async Task<ActionResult> EditRoles(int userId)
        {
            var user = await _service.GetByIdAsync(userId);
            var roleService = _componentContext.Resolve<IRoleService>();
            var roles = await roleService.GetRecordsAsync();
            var userInRoles = await _userManager.GetRolesAsync(user);

            var model = new UpdateRolesModel
            {
                UserId = userId,
                Roles = userInRoles.ToArray()
            };

            var retult = new RoboUIFormResult<UpdateRolesModel>(model, ControllerContext)
            {
                Title = T("Cập nhật vai trò"),
                ViewData = ViewData,
                FormActionUrl = Url.Action("UpdateRoles")
            };

            retult.RegisterExternalDataSource(x => x.Roles,
                roles.OrderBy(x => x.Name).ToSelectList(k => k.Name, v => v.Name));

            return retult;
        }

        [Themed(false), FormButton("Save")]
        [HttpPost, Route("update-roles")]
        public async Task<ActionResult> UpdateRoles(UpdateRolesModel model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId.ToString());

            var userRoles = await _userManager.GetRolesAsync(user);

            if (model.Roles == null || model.Roles.Length == 0)
            {
                if (userRoles.Count > 0)
                {
                    await _userManager.RemoveFromRolesAsync(user, userRoles);
                }
            }
            else
            {
                foreach (var role in model.Roles)
                {
                    if (!userRoles.Contains(role))
                    {
                        await _userManager.AddToRoleAsync(user, role);
                    }
                }

                foreach (var role in userRoles)
                {
                    if (!model.Roles.Contains(role))
                    {
                        await _userManager.RemoveFromRoleAsync(user, role);
                    }
                }
            }

            return new AjaxResult().Alert("Cập nhật vai trò cho người dùng thành công").CloseModalDialog().Reload(true);
        }

        [HttpPost, Route("get-by-campus/{id}")]
        public async Task<ActionResult> GetUserByCampus(int id)
        {
            var users = await Service.GetRecordsAsync(x => x.CampusId == id || x.SeniorLeader && !x.Disabled);
            return Json(users.ToSelectList(x => x.Id, x => x.FullName));
        }

        protected override async Task<RoboUIGridAjaxData<IdentityUser>> GetRecords(RoboUIGridRequest options)
        {
            var filterExpression = PredicateBuilder.True<IdentityUser>();

            if (Request.Form.TryGetValue<int>("CampusId", out var campusId))
            {
                filterExpression = filterExpression.And(x => x.CampusId == campusId);
            }

            if (Request.Form.TryGetValue<int>("UserRoleId", out var userRoleId))
            {
                var userRoleService = _componentContext.Resolve<IUserRoleService>();
                var userRoles = await userRoleService.GetRecordsAsync(r => r.RoleId == userRoleId);

                filterExpression = filterExpression.And(x => userRoles.Select(r => r.UserId).Contains(x.Id));
            }

            if (Request.Form.TryGetValue<UserType>("UserType", out var userType))
            {
                filterExpression = filterExpression.And(x => x.UserType == userType);
            }

            filterExpression.And(x => !x.Disabled);

            var records = await Service.GetRecordsInclude(options, filterExpression, x => x.Department);
            var userIds = records.Select(s => s.Id);
            var relations = await _memberRelationService.GetRecordsAsync(s => userIds.Contains(s.UserId))
                .Continue(x => x.ToList());
            var managers = await Service.GetRecordsAsync(x => relations.Select(s => s.ManagerId).Contains(x.Id));

            foreach (var item in records)
            {
                var managerIds = relations.Where(s => s.UserId == item.Id).Select(x => x.ManagerId);
                item.Managers = managers.Where(x => managerIds.Contains(x.Id)).ToList();
            }

            return new RoboUIGridAjaxData<IdentityUser>(records, records.ItemCount);
        }
    }
}