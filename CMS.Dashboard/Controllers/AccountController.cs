﻿using CMS.Dashboard.ViewModels.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CMS.Core.Extensions;
using CMS.Dashboard.Web;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;
using CMS.Dashboard.Models;
using CMS.Dashboard.Web.UI.RoboUI;
using Autofac;
using CMS.Shared.Validate;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Google;
using CMS.Shared.Extensions;

namespace CMS.Dashboard.Controllers
{
    [Authorize]
    [Route("account")]
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IComponentContext _componentContext;
        private readonly ILogger _logger;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILoggerFactory loggerFactory, IComponentContext componentContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _componentContext = componentContext;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        [AllowAnonymous]
        [HttpGet, Route("login")]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            _logger.Info($"{DateTime.Now:G}: Request to login");
            ViewData["ReturnUrl"] = returnUrl;
            return View(new LoginViewModel()
            {
                Schemes = await HttpContext.GetExternalProvidersAsync()
            });
        }

        [AllowAnonymous]
        [HttpPost, Route("login")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return new AjaxResult().Alert("Invalid login attempt.");
            }

            try
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe,
                    lockoutOnFailure: true);

                if (result.RequiresTwoFactor)
                {
                    return new AjaxResult().Alert(
                        "User account required 2 Factor Password! Please contact system administrator!.");
                }
                
                if (result.IsLockedOut)
                {
                    _logger.Info($"{DateTime.Now:G}: User account locked out.");
                    return new AjaxResult().Alert("User account is locked out! Please contact system administrator!.");
                }
                
                if (result.Succeeded)
                {
                    _logger.LogInformation(1, "User logged in.");
                    return RedirectToLocal(returnUrl);
                }
            }
            catch (Exception e)
            {
                _logger.Error($"AccountController was error:{e.Message}", e);
                return new AjaxResult().Alert($"Login error:{e.Message}.{Environment.NewLine}{e.StackTrace}.");
            }

            return new AjaxResult().Alert("Invalid login attempt.");
        }

        [HttpGet, Route("logoff")]
        public async Task<IActionResult> LogOff()
        {
            var distributedCache =
                (IDistributedCache) HttpContext.RequestServices.GetService(typeof(IDistributedCache));

            await distributedCache.RemoveAsync("IsUserLoggedIn:" + User.Identity.Name.ToLowerInvariant());

            await _signInManager.SignOutAsync();

            HttpContext.Session.Clear();

            _logger.LogInformation(4, "User logged out.");
            return Redirect("~/");
        }

        [HttpGet, Route("register")]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            _logger.Info($"{DateTime.Now:G}: User account locked out.");
            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost, Route("register")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            _logger.Info($"{DateTime.Now:G}: Request Register");
            if (ModelState.IsValid)
            {
                var user = new IdentityUser {UserName = model.Username, Email = model.Email};
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Json(new
                        {
                            status = true,
                            redirectUrl = returnUrl
                        });
                    }

                    return Json(new
                    {
                        status = true,
                        redirectUrl = Url.Action(nameof(Login), "Account")
                    });
                }

                return Json(new
                {
                    status = false,
                    message = string.Join(Environment.NewLine, result.Errors.Select(x => x.Description))
                });
            }

            return Json(new
            {
                status = false,
                message = "Invalid register attempt."
            });
        }

        [HttpGet, Route("forgot-password")]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await _userManager.FindByNameAsync(model.Email);

            if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
            {
                return View($"ForgotPasswordConfirmation");
            }

            return View(model);
        }

        [Route("lockout")]
        public IActionResult Lockout()
        {
            return View("Lockout");
        }

        [Route("access-denied")]
        public IActionResult AccessDenied()
        {
            return View("AccessDenied");
        }

        [HttpGet, Route("change-password")]
        public async Task<ActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user == null) return null;

            var model = new ChangePasswordModel
            {
                UserId = user.Id,
                UserName = user.UserName,
                Email = user.Email
            };

            var result = new RoboUIFormResult<ChangePasswordModel>(model, ControllerContext)
            {
                FormId = "fChangePassword",
                Title = "Đổi mật khẩu",
                ViewData = ViewData,
                FormActionUrl = Url.Action("UpdatePassword"),
                Encrypted = true
            };

            var workContext = _componentContext.Resolve<WorkContext>();
            workContext.AddInlineScript("$('#fChangePassword').disableAutoFill();");

            ViewBag.Layout = "Layout-Minimal";

            return result;
        }

        [FormButton("Save")]
        [HttpPost, Route("update-password")]
        public async Task<ActionResult> UpdatePassword(ChangePasswordModel model)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (!PasswordValidator.IsPasswordValidate(model.Password, user.UserName))
            {
                return new AjaxResult().Alert(PasswordValidator.PasswordAlert);
            }

            if (model.OldPassword == model.Password)
            {
                return new AjaxResult().Alert("New password must be different from previous passwords");
            }

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.Password);

            if (result.Succeeded)
                return new AjaxResult().Alert("Update password successful.").CloseModalDialog();

            return new AjaxResult().Alert(result.Errors.FirstOrDefault()?.Description ??
                                          "Password change was unsuccessful. Please correct the errors and try again.");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            return new AjaxResult().Redirect(Url.IsLocalUrl(returnUrl)
                ? returnUrl
                : Url.Action("Index", "Home", new {area = string.Empty}));
        }

        [AllowAnonymous]
        [HttpPost, Route("login-google")]
        public IActionResult LoginGoogle(string returnUrl)
        {
            return new ChallengeResult(
                GoogleDefaults.AuthenticationScheme,
                new AuthenticationProperties
                {
                    RedirectUri = Url.Action(nameof(LoginCallback), new {returnUrl})
                });
        }

        [AllowAnonymous]
        [HttpGet, Route("login-callback")]
        public async Task<IActionResult> LoginCallback(string returnUrl)
        {
            var authenticateResult = await HttpContext.AuthenticateAsync("External");

            if (!authenticateResult.Succeeded)
                return BadRequest();

            var email = authenticateResult.Principal.FindFirst(ClaimTypes.Email).Value;
            var user = await _userManager.FindByEmailAsync(email) ?? await _userManager.FindByNameAsync(email);

            if (user == null || user.Disabled)
            {
                return View("EmailFound");
            }

            await _signInManager.SignInAsync(user, false);

            return RedirectToLocal(returnUrl);
        }
    }
}