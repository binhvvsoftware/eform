﻿using Autofac;
using Microsoft.AspNetCore.Mvc;
using CMS.Dashboard.Models;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Identity;

namespace CMS.Dashboard.Controllers
{
    [Route("config/smtp-config")]
    public class SmtpConfigController : BaseRoboController<int, SmtpConfig, SmtpConfigModel>
    {
        private readonly IComponentContext componentContext;

        public SmtpConfigController(IComponentContext componentContext, ISmtpConfigService smtpConfigService)
            : base(componentContext, smtpConfigService)
        {
            this.componentContext = componentContext;
        }

        protected override string Name => "Smtp Configs";

        protected override string PluralizedName => "Smtp Configs";

        protected override string CreateText => "Thêm";

        protected override string DeleteText => "Xóa";

        protected override string EditText => "Sửa";

        protected override string ActionText => "Thao tác";

        protected override bool EnableTracking => true;

        protected override void OnViewIndex(RoboUIGridResult<SmtpConfig> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.AddColumn(x => x.Name, "Name").EnableFilter();
            roboGrid.AddColumn(x => x.Email, "Email").EnableFilter();
            roboGrid.AddColumn(x => x.Host, "Host");
            roboGrid.AddColumn(x => x.Port, "Port");
            roboGrid.AddColumn(x => x.EnableSsl, "EnableSsl").RenderAsStatusImage().AlignCenter().HasWidth(130);
            roboGrid.AddColumn(x => x.UseDefaultCredentials, "UseDefaultCredentials").RenderAsStatusImage().AlignCenter().HasWidth(130);
        }


        protected override SmtpConfigModel ConvertToModel(SmtpConfig entity)
        {
            return entity;
        }

        protected override void ConvertFromModel(SmtpConfigModel model, SmtpConfig entity)
        {
            var passwordHasher = new PasswordHasher<SmtpConfig>();
            entity.Id = model.Id;
            entity.Name = model.Name;
            entity.Host = model.Host;
            entity.Password = passwordHasher.HashPassword(new SmtpConfig(), model.Password);
            entity.UseDefaultCredentials = model.UseDefaultCredentials;
            entity.EnableSsl = model.EnableSsl;
            entity.Email = model.Email;
            entity.Username = model.Username;
            entity.Port = model.Port;
        }
    }
}