﻿using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Mvc;
using CMS.Dashboard.Models;
using CMS.Dashboard.Controllers.Common;
using CMS.Dashboard.Extensions;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Controllers
{
    [Route("campus")]
    public class CampusController : BaseRoboController<int, Campus, CampusModel>
    {
        private readonly IComponentContext componentContext;

        public CampusController(IComponentContext componentContext, ICampusService parentDepartmentService)
            : base(componentContext, parentDepartmentService)
        {
            this.componentContext = componentContext;
        }

        protected override string Name => "Campus";

        protected override string PluralizedName => "Campuses";

        protected override string CreateText => "Thêm";

        protected override string DeleteText => "Xóa";

        protected override string EditText => "Sửa";

        protected override string ActionText => "Thao tác";

        protected override bool EnableTracking => true;

        protected override void OnViewIndex(RoboUIGridResult<Campus> roboGrid)
        {
            WorkContext.Breadcrumbs.Clear();
            // WorkContext.AddBreadcrumb(T("Dashboard"), Url.Action("Index", "Home"));
            WorkContext.AddBreadcrumb(T(PluralizedName));

            roboGrid.AddColumn(x => x.Name, "Tên").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Address, "Địa chỉ").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Phone, "Số điện thoại").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.RootDepartmentName, "RootDepartment").EnableFilter().HasWidth(150);
            roboGrid.AddColumn(x => x.Status, "Trạng thái").RenderAsStatusImage().AlignCenter().HasWidth(100);
        }

        protected override async Task OnCreatingAsync(RoboUIFormResult<CampusModel> roboForm)
        {
            var departmentService = componentContext.Resolve<IParentDepartmentService>();
            var rootDepartments = await departmentService.GetRecordsAsync();
            roboForm.RegisterExternalDataSource(x => x.RootDepartment, rootDepartments.ToSelectList(v => v.Id, t => t.Name));
        }

        protected override async Task OnEditingAsync(RoboUIFormResult<CampusModel> roboForm)
        {
            var departmentService = componentContext.Resolve<IParentDepartmentService>();
            var rootDepartments = await departmentService.GetRecordsAsync();
            roboForm.RegisterExternalDataSource(x => x.RootDepartment, rootDepartments.ToSelectList(v => v.Id, t => t.Name));
        }

        protected override CampusModel ConvertToModel(Campus entity)
        {
            return entity;
        }

        protected override void ConvertFromModel(CampusModel model, Campus entity)
        {
            entity.Name = model.Name;
            entity.Address = model.Address;
            entity.Phone = model.Phone;
            entity.Status = model.Status;
            entity.ParentDepartmentId = model.RootDepartment;
        }
        protected override async Task<RoboUIGridAjaxData<Campus>> GetRecords(RoboUIGridRequest options)
        {
            var records = await Service.GetRecordsInclude(options, null, x => x.RootDepartment);
            return new RoboUIGridAjaxData<Campus>(records, records.ItemCount);
        }
    }
}