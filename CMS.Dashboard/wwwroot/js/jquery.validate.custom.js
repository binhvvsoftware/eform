$.validator.addMethod('captchacode', function (value, element) {
    let captchaCode = value || $('#CaptchaCode').val();
    return captchaCode.toUpperCase() === $('#VerifyCaptchaCode').val().toUpperCase();
});
