var options = {
    inputSets: [
        {
            label: 'Creator information',
            icon: '👥',
            name: 'user-details',
            fields: [
                {
                    type: 'text',
                    label: 'Tên người lập',
                    className: 'form-control'
                },
                {
                    type: 'text',
                    label: 'Ngày tạo',
                    className: 'form-control datepicker'
                }
            ]
        },
        // {
        //     label: 'Table gridview',
        //     icon: '🧮',
        //     fields: [
        //         {
        //             label: 'Table gridview',
        //             type: 'text',
        //             className: 'form-control hidden',
        //             name: 'table-grid'
        //         }
        //     ]
        // }
    ],
    typeUserAttrs: {
        text: {
            sum:{
                label: 'sum',
                value: ''
            },
            sumType:{
                label: 'sumtype',
                value: ''
            },
            disabled: {
                label: 'disabled',
                value: false,
                type: 'checkbox'
            },
        },
        number: {
            sum:{
                label: 'sum',
                value: ''
            },
            sumType:{
                label: 'sumtype',
                value: ''
            },
            disabled: {
                label: 'disabled',
                value: false,
                type: 'checkbox'
            }
        }
    }
};

const initalizeOptions = () => options;
