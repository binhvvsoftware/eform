var strings = {
  desc : 'Gestionnaire de fichiers',
  // Basic
 	close: 'Fermer',
 	cancel: 'Annuler',
 	insert: 'Insérer',
 	custom: 'Custom',
 	original_size: 'Taille d\'origine',
 	filename: 'Nom du fichier',
 	remove: 'Supprimer',
 	empty: 'vide',
 	root: 'Racine',
 	download: 'Télécharger',
 	custom: 'Personnalisée',
 	lock_aspect_ratio: 'Garder les proportions',
  		
  // Messages
 	rename_not_exists: 'Le fichier n\'existe plus',
 	rename_target_exists: 'Un fichier avec le même nom existe déjà',
 	rename_error: 'Impossible de renommer le fichier',
 	upload_dir_not_exist: 'Le répertoire de destination n\'existe pas',
 	upload_dir_not_writable: 'Le répertoire d\'upload n\'est pas modifiable',
 	cache_dir_not_exist: 'Le répertoire de cache n\'existe pas',
 	cache_dir_not_writable: 'Le répertoire de cache n\'est pas modifiable',
 	config_not_exist: 'Le fichier de configuration n\'existe pas',
 	config_not_writable: 'Le fichier de configuration n\'est pas inscriptible',
 	insert_upload_dir: 'Veuillez renseigner un chemin d\'upload valide',
 	max_file_size: 'La taille maximale d\'upload est de',
 	confirm_remove_dir: 'Voulez-vous vraiment supprimer ce dossier',
 	confirm_remove_file: 'Voulez-vous vraiment supprimer ce fichier',
 	dir_name_reserved_chars: 'Le nom du dossier contient des caractères réservés',
 	not_logged: 'Vous devez être connecté pour continuer',
 	ajax_error: 'Une erreur est survenue lors de la communication avec le serveur',
 	file_doesnt_exists: 'Le fichier selectionné n\'existe pas',
 	cant_save_settings: 'Impossible d\'enregistrer les paramètres. Veuillez réessayer.',
 	fill_dir_name: 'Veuillez renseigner le nom du dossier',
 	fill_filename: 'Veuillez renseigner le nom du fichier',
 	new_dimension_error: 'Les dimensions doivent être des nombres entiers',
	fill_link_title: 'Veuillez renseigner le nom du lien',
 	forbidden_extension: 'Ce type d\'extension n\'est pas autorisé',
 	remove_folder: 'Supprimer le dossier',
 	remove_file: 'Supprimer le fichier',
 	cannot_remove_file: 'Impossible de supprimer le fichier',
 	cannot_remove_dir: 'Impossible de supprimer le dossier',
  
  // File info
 	file: 'Fichier',
 	last_modified: 'Modification',
 	size: 'Taille',
  	
  // View
 	view_as_thumbs: 'Apercu',
 	view_as_table: 'Liste',
  	
  // About
 	about: 'A propos',
  	
  // Context menu
 	open_folder: 'Ouvrir le dossier',
 	insert_as_file: 'Insérer en tant que fichier',
 	rename: 'Renommer',
 	old_name: 'Nom précédant',
 	new_name: 'Nouveau nom',
 	insert_as_image: 'Insérer en tant qu\'image',
 	insert_as_zoom_image: 'Insérer en tant qu\'agrandissement',
 	view_image: 'Voir l\'image',
 	
  // Lightbox
 	alt: 'Texte de remplacement',
 	title: 'Titre',
 	thumb_size: 'Taille de l\'image',
 	w: 'L',
 	h: 'H',
 	full_size: 'Taille originale',
 	crop: 'Rogner',
 	insert_filesize: 'Insérer la taille',
 	insert_extension: 'Insérer l\'extension',
 	anchor_text: 'Nom du lien',
 	image_class: 'Classe de l\'image',
 	choose_insert_method: 'Choisissez la méthode d\'insertion',
 	insert_as_image_tab: 'Insérer en tant<br />qu\'image',
 	insert_as_zoom_image_tab: 'Insérer en tant<br />qu\'agrandissement',
 	insert_as_file_tab: 'Insérer en tant<br />que fichier',
 	error: 'Erreur',
 	
  // Settings
 	settings: 'Paramètres',
 	upload_dir: 'Répertoire d\'upload',
 	img_wrap_before: 'Contenu HTML avant l\'image',
 	img_wrap_after: 'Contenu HTML après l\'image',
 	extensions_read_restrict: 'Utiliser aussi pour la lecture',
 	allowed_extensions: 'Extensions autorisées',
 	forbidden_extensions: 'Extensions interdites',
 	forbidden_extensions_permanent: 'Extensions interdites permanentes',
 	default_image_action: 'Action d\'image par défaut',
 	dimensions: 'Dimensions',
 	new_dimension: 'Nouvelle dimension',
 	max_image_dimensions: 'Dimensions maximales',
 	remove: 'Supprimer',
 	add: 'Ajouter',
 	dimension_alias: 'Alias',
 	link_props_pattern: 'Propriétés du fichier',
 	save: 'Sauvegarder',
 	img_rel: 'Lien "rel" de l\'image',
 	default_image_action_file: 'Insérer en tant que fichier',
 	default_image_action_thumb: 'Insérer en tant qu\'image',
 	default_image_action_thumb_zoom: 'Insérer en tant qu\'agrandissement',
 	timezone: 'Fuseau horaire',
  	
  // Upload
 	add_file: 'Ajouter un fichier',
 	upload_parent: 'Choisissez un répertoire de destination',
 	upload: 'Uploader',
 	upload_files: 'Files',
 	remove_all: 'Vider la file d\'attente',
 	upload_error: 'Erreur',
  	
  // New Folder
 	new_folder: 'Nouveau dossier',
 	new_dir_name: 'Nom du dossier',
 	new_dir_parent: 'Dossier parent',
 	create: 'Créer',
 	cant_create_dir: 'Impossible de créer le dossier'
};

if (CKEDITOR['version'][0] == 3) strings = { plugobrowser: strings };
CKEDITOR.plugins.setLang('plugobrowser', 'fr', strings);