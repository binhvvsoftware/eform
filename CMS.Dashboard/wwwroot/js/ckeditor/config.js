/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbar = null;
    config.toolbarGroups = [
		{ name: 'links' },
		{ name: 'insert', groups: ['PlugoBrowser'] },
		{ name: 'others' },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'colors' },
        { name: 'document', groups: ['mode'] },
		{ name: 'tools' },
		'/',
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
		{ name: 'styles' }
    ];
    
    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Strike,Subscript,Superscript,Save,NewPage,Print,Scayt,Language,Cut,Copy,Image';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    config.extraPlugins = 'plugobrowser';

    config.allowedContent = true;
    
    config.protectedSource.push(/<i[^>]*><\/i>/g);
};

CKEDITOR.dtd.$removeEmpty['span'] = false;
CKEDITOR.dtd.$removeEmpty['i'] = false;

function insertToEditor(id, content) {
    var instance = CKEDITOR.instances[id];
    var element = CKEDITOR.dom.element.createFromHtml("<span>" + content + "</span>");
    instance.insertElement(element);
}