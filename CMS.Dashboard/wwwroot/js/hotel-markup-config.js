$(document).ready(function () {

    $("#StarRatings").hide();

    $("#StarRatings").multiselect({
        includeSelectAllOption: true
    });

    displayField();

    $("#IsPercentageBased").change(function () {
        displayField();

        if ($("#IsPercentageBased").is(":checked") === true) {
            $("#PercentageAmount").prop("disabled", false);
        } else {
            $("#PercentageAmount").prop("disabled", true);
        }
    });

    $("#IsValueBased").change(function () {
        displayField();

        if ($("#IsValueBased").is(":checked") === true) {
            $("#ValueAmount").prop("disabled", false);
            $("#ChargeOn").prop("disabled", false);
        } else {
            $("#ValueAmount").prop("disabled", true);
            $("#ChargeOn").prop("disabled", true);
        }
    });

    function displayField() {
        $(".minmax-group-config").hide();

        if ($("#IsPercentageBased").is(":checked") === true && $("#IsValueBased").is(":checked") === true) {

            $(".minmax-group-config").show();
        } else {
            $(".minmax-group-config").hide();
        }

        if ($("#IsPercentageBased").is(":checked") !== true) {
            $("#PercentageAmount").prop("disabled", true);
        } else {
            $("#PercentageAmount").prop("disabled", false);
        }
        if ($("#IsValueBased").is(":checked") !== true) {
            $("#ValueAmount").prop("disabled", true);
            $("#ChargeOn").prop("disabled", true);
        } else {
            $("#ValueAmount").prop("disabled", false);
            $("#ChargeOn").prop("disabled", false);
        }
    }

    $("#frmHotelMarkup").on("submit", function (event) {
        event.preventDefault();

        var formData = new FormData();
        var inputs = $(this).serializeArray();

        $.each(inputs, function (i, input) {
            formData[input.name] = input.value;
        });

        if (formData["StarRatings"]) {
            formData["StarRatings"] = $("#StarRatings").val();
        }

        $(this).formData = formData;
    });
})
