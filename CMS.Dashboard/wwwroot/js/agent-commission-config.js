﻿$(document).ready(function () {
    isHiddenClassContainer();
    $("#DiscountType").change(function () {
        if ($(this).val() === "PercentageBase") {
            isHiddenClassContainer();
        } else {
            isHiddenClassContainer();
        }
    });

    $("#DiscountOn").change(function () {
        isHiddenClassContainer();
    });

    function isHiddenClassContainer() {
        var value = $("#DiscountOn").val();
        var discount = $("#DiscountType").val();

        $("#DiscountOn").removeAttr("readonly");
        $("#DiscountOn").attr("style", "pointer-events: auto;");

        if (discount === "PercentageBase") {
            $(".container-discount-amount").hide();
            $("#tblProductValues").show();
            //$('#DiscountOn').parent().hide();
            $("#DiscountOn").val("PerBooking");
            $("#DiscountOn").attr("readonly", "readonly");
            $("#DiscountOn").attr("style", "pointer-events: none;");
        } else if (discount !== "PercentageBase" && value !== "PerProduct") {
            $("#tblProductValues").hide();
            $(".container-discount-amount").show();
            //$('#DiscountOn').parent().show();
           

        } else {
            $(".container-discount-amount").hide();
            $("#tblProductValues").show();
            //$('#DiscountOn').parent().show();
        }
    }
});
