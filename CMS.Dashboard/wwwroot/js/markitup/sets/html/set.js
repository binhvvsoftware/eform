// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
mySettings = {
    onShiftEnter: { keepDefault: false, replaceWith: '<br />\n' },
    onCtrlEnter: { keepDefault: false, openWith: '\n<p>', closeWith: '</p>\n' },
    onTab: { keepDefault: false, openWith: '	 ' },
    resizeHandle: true,
    markupSet: [
        { name: 'Heading 1', key: '1', openWith: '<h1(!( class="[![Class]!]")!)>', closeWith: '</h1>', placeHolder: 'Your title here...', className: 'icon-h1' },
		{ name: 'Heading 2', key: '2', openWith: '<h2(!( class="[![Class]!]")!)>', closeWith: '</h2>', placeHolder: 'Your title here...', className: 'icon-h2' },
		{ name: 'Heading 3', key: '3', openWith: '<h3(!( class="[![Class]!]")!)>', closeWith: '</h3>', placeHolder: 'Your title here...', className: 'icon-h3' },
		{ name: 'Heading 4', key: '4', openWith: '<h4(!( class="[![Class]!]")!)>', closeWith: '</h4>', placeHolder: 'Your title here...', className: 'icon-h4' },
		{ name: 'Heading 5', key: '5', openWith: '<h5(!( class="[![Class]!]")!)>', closeWith: '</h5>', placeHolder: 'Your title here...', className: 'icon-h5' },
		{ name: 'Heading 6', key: '6', openWith: '<h6(!( class="[![Class]!]")!)>', closeWith: '</h6>', placeHolder: 'Your title here...', className: 'icon-h6' },
		{ name: 'Paragraph', openWith: '<p(!( class="[![Class]!]")!)>', closeWith: '</p>', className: 'icon-paragraph' },
		{ separator: '---------------' },
		{ name: 'Bold', key: 'B', openWith: '(!(<strong>|!|<b>)!)', closeWith: '(!(</strong>|!|</b>)!)', className: 'icon-bold' },
		{ name: 'Italic', key: 'I', openWith: '(!(<em>|!|<i>)!)', closeWith: '(!(</em>|!|</i>)!)', className: 'icon-italic' },
		{ name: 'Stroke through', key: 'S', openWith: '<del>', closeWith: '</del>', className: 'icon-stroke' },
		{ name: 'New Line', openWith: '<br/>', className: 'icon-new-line' },
		{ separator: '---------------' },
		{ name: 'Ul', openWith: '<ul>\n', closeWith: '</ul>\n', className: 'icon-list-bullet' },
		{ name: 'Ol', openWith: '<ol>\n', closeWith: '</ol>\n', className: 'icon-list-numeric' },
		{ name: 'Li', openWith: '<li>', closeWith: '</li>', className: 'icon-list-item' },
		{ separator: '---------------' },
		{ name: 'Picture', key: 'P', replaceWith: '<img src="[![Source:!:http://]!]" alt="[![Alternative text]!]" />', className: 'icon-picture' },
		{ name: 'Link', key: 'L', openWith: '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', closeWith: '</a>', placeHolder: 'Your text to link...', className: 'icon-link' },
		{ separator: '---------------' },
		{ name: 'Clean', className: 'icon-clean', replaceWith: function (markitup) { return markitup.selection.replace(/<(.*?)>/g, ""); } },
		{ name: 'Preview', className: 'icon-preview', call: 'preview' }
    ]
}