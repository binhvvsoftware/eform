$(document).ready(function () {

    isHiddenClassContainer();
    disableControl($('#PercentageBased').is(':checked'));
    $("#PercentageBased").change(function () {
        if ($(this).is(':checked')) {
            isHiddenClassContainer();
            disableControl(true);
        } else {
            disableControl(false);
            isHiddenClassContainer();
        }
    });

    $("#ChargeOn").change(function () {
        isHiddenClassContainer();
    });

    function disableControl(isDisalbe) {
        $('#ChargeOn').prop('disabled', isDisalbe);
        $('#CurrencyCode').prop('disabled', isDisalbe);
    }
    function isHiddenClassContainer() {
        var value = $('#ChargeOn').val();
        if (value !== "PerPassenger" || $('#PercentageBased').is(':checked')) {
            $('input[name="PaxType"]').closest(".deposit-pax-type-container").hide();
        } else {
            $('input[name="PaxType"]').closest(".deposit-pax-type-container").show();
        }
    }
})
