$(document).ready(function () {
    $(document).on("click", ".hover-tooltip", function () {
        let target = $(this).attr('data-target');
        $(this).popover({
            content: $(target).html(),
            html: true,
            trigger: "hover",
        }).click(function () {
            $(this).popover('show');
        });
    });
});
