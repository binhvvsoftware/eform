﻿// Initializes StringBuilder class
function StringBuilder(value) {
    this.strings = new Array("");
    if (value) this.append(value);
}

StringBuilder.prototype.append = function (value) {
    if (value) this.strings.push(value);
};

StringBuilder.prototype.clear = function () {
    this.strings.length = 1;
};

StringBuilder.prototype.toString = function () {
    return this.strings.join("");
};

// =====*** Edit table ** =====
const $TABLEID = $('#table');
const $BTN = $('#export-btn');
const $EXPORT = $('#export');
const $defineTable = $('.define-table');

const tbody_remove = `
<td>
    <span class="table-remove">
        <button type="button"
        class="btn btn-danger btn-rounded btn-sm my-0" style="min-width: unset">
        <i class="fa fa-trash"></i>
        </button>
    </span>
</td>`;

const thead_tr = $defineTable.find('thead tr');
const tbody = $defineTable.find('tbody');
const tfoot_tr = $defineTable.find('tfoot tr');

$('.column-add').on('click', 'button', () => {
    const columnKey = $('#columnKey');
    const columnName = $('#columnName');
    const sumColumn = $('#SumColumn');
    let isSum = sumColumn.prop('checked');

    if (columnKey.val() === "" || columnName.val() === "") {
        alert("Vui lòng nhập Key và Column name cho bảng muốn định nghĩa");
        return false;
    }

    const newColumn = `
        <th class="text-center title-columns">
            <input type="hidden" class="col-md-12 key-name" data-sum="${isSum}" value="${columnKey.val()}"/>
            <input type="text" class="col-md-12 real-name" placeholder="Column name" readonly value="${columnName.val()}" />
            <i class="fa fa-remove remove-column"></i>
        </th>`;

    const newColumnFooter = `
        <td>
            <span></span>
            <input type="hidden" name="Sum${columnKey.val()}"/>
        </td>`;
    thead_tr.find('.delete-col').before(newColumn);
    tfoot_tr.find('.tb-sum').before(newColumnFooter);


    $('.remove-column').on('click', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    columnKey.val("");
    columnName.val("");
    sumColumn.prop('checked', false);
});

$('.table-add').on('click', 'button', () => {
    let sb = new StringBuilder();
    sb.append('<tr>');
    thead_tr.find('th').each((i, item) => {
        let needSum = $(item).find('.key-name').data('sum');
        let innerText = $(item).find('.real-name').val();
        let keyname = $(item).find('.key-name').val();

        if (thead_tr.find('th').length === i + 1) {
            sb.append(tbody_remove);
        }
        if (innerText) {
            if (innerText.toLowerCase().includes('remove') || innerText.toLowerCase().includes('xóa')) {
                sb.append(tbody_remove);
            } else {
                if (needSum) {
                    sb.append(`<td class="pt-3-half ${keyname} sum-now" title="${innerText}" onkeyup="blurFunction('${keyname}')" contenteditable="true">Example</td>`);
                } else {
                    sb.append(`<td class="pt-3-half ${keyname}" title="${innerText}" contenteditable="true">Example</td>`);
                }

            }
        }
    });
    sb.append('</td>');
    tbody.append(sb.toString());
});

$('.table-reset').on('click', 'button', () => {
    thead_tr.find('th:not(.delete-col)').remove();
    tbody.find('tr').remove();
    tfoot_tr.find('td:not(.tb-sum)').remove();
    $('#doneDefineColumn').prop('checked', false);
});

$TABLEID.on('click', '.table-remove', function () {

    $(this).parents('tr').detach();
});

$TABLEID.on('click', '.table-up', function () {

    const $row = $(this).parents('tr');

    if ($row.index() === 1) {
        return;
    }

    $row.prev().before($row.get(0));
});

$TABLEID.on('click', '.table-down', function () {
    const $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on('click', () => {
    const $rows = $TABLEID.find('tr:not(:hidden)');
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {

        headers.push($(this).text());
    });

    // Turn all existing rows into a loopable array
    $rows.each(function () {
        const $td = $(this).find('td');
        const h = {};
        headers.forEach((header, i) => {
            h.name = header.toLowerCase().replace("", "_");
            h.realName = header;
            h.value = $td.eq(i).text();
        });
        data.push(h);
    });

    // Output the result
    $EXPORT.text(JSON.stringify(data));
});

// =====*** End Edit table ** =====

function fillDataToTable(table) {
    let flag = [];
    //Fill rows
    let i = 1;
    table.rows.forEach(row => {
        let sb = new StringBuilder();
        sb.append('<tr>');
        let j = 0;
        for (let key in row) {
            if(table.name ? table.name.toLowerCase().includes("đơn xin nghỉ phép") : false){
                if(j ===0){
                    $(".responsive-form").removeClass("tb-special");
                    $("#addRow").hide();
                    j++;
                    continue;
                }
            }
            let isSum = table.columns.find(x => x.key === key).isSum;
            let value = row[key];

            let column = table.columns.find(x => x.key === key).item;

            sb.append(`<td id="${key}${i}" class="pt-3-half ${key}${i}" title="${column}" contenteditable="true">${value}</td>`);
            sb.append(`<input type="hidden" name="${key}${i}" value="${value}"/>`);
            if (isSum) {
                flag.push(key);
            }
            j++;
        }
        if(table.name ? !table.name.toLowerCase().includes("đơn xin nghỉ phép") : true){
            sb.append(tbody_remove);
        }

        sb.append('</td>');
        tbody.append(sb.toString());
        i++;
    });

    table.columns.forEach((item, i) => {
        const newTh = `
        <th class="text-center title-columns">
            <input type="hidden" class="col-md-12 key-name" data-sum="${item.isSum}" value="${item.key}"/>
            <input type="text" class="col-md-12 real-name" placeholder="Column name" readonly value="${item.item}" />
        </th>`;

        let newColFooter = `
        <td>
            <span></span>
            <input type="hidden" name="Sum${item.key}"/>
        </td>`;

        if(table.name ? table.name.toLowerCase().includes("đơn xin nghỉ phép") : false){
            thead_tr.find('.delete-col').remove();
            //ignore STT
            if(i > 0){
                thead_tr.append(newTh);
            }
        }else{
            thead_tr.find('.delete-col').before(newTh);
        }


        //Fill footer

        if (table.columns.some(x => x.isSum === true)) {
            if (i === 0) {
                tfoot_tr.append('<td class="tb-sum">Tổng</td>');
            } else {
                tfoot_tr.append(newColFooter);
            }
            if (i === table.columns.length - 1 && table.rows.length === 0) {
                tfoot_tr.append('<td></td>');
            }
        }
    });

    if (flag.length > 0) {
        flag.forEach((item, i) => {
            blurFunction(item);
        })
    }
}

function blurFunction(keyName) {
    let currentCol = $(`.${keyName}`);
    let total = 0;
    currentCol.each(function () {
        total += tryParseInt($(this).text(), 0);
    });

    let sumCol = $(`input[name=Sum${keyName}]`);
    sumCol.val(total);
    sumCol.parent().find('span').text(total);
}

// Signature pad
const canvas = document.getElementById('signature-pad');

function resizeCanvas() {
    let ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

if (canvas) {
    window.onresize = resizeCanvas;
    resizeCanvas();

    let signaturePad = new SignaturePad(canvas, {
        backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
    });

    document.getElementById('clear').addEventListener('click', function () {
        signaturePad.clear();
    });

    document.getElementById('undo').addEventListener('click', function () {
        let data = signaturePad.toData();
        if (data) {
            data.pop(); // remove the last dot or line
            signaturePad.fromData(data);
        }
    });
}

function tryParseInt(str, defaultValue) {
    let value = defaultValue;
    if (str !== null) {
        if (str.length > 0) {
            if (!isNaN(str)) {
                value = parseInt(str);
            }
        }
    }
    return value;
}

function fillUserData(formDataJson) {
    jQuery.each(formDataJson, function(key, value) {
        console.log(`item: ${key}. value =${value}`);
        console.log(`key = ${key}`);
        if (key !== "CaptchaCode" && key !== "VerifyCaptchaCode"){
            $(`#${key}`).val(value);
            console.log(`value after = ` + $(`#${key}`).val());
        }
    });
}
// Fill data
function fillData(newTemplate) {
    newTemplate.forEach(function (item) {
        if(item.type === "table"){
            item.columns.forEach(function(col){
                if(col.sumType === "time"){
                    let sumParts = col.sum.split('&');
                    let total = 0;
                    sumParts.forEach(x => {
                        let number = $(`.${x}`).text().trim() || "0";
                        number = parseFloat(number);
                        $(`input[name=${x}]`).val(number);
                        total += number;
                    });
                    let $el = $(`input[name=Sum${col.sumType}]`);
                    $el.val(total);
                    $el.parent().find('span').text(total);
                }
            })
        }
        if (item.sum) {
            let sumParts = item.sum.split('&');
            // if (item.sumType === "date") {
            //     let dateFromParts = $(`input[name=${sumParts[0]}]`).val() ? $(`input[name=${sumParts[0]}]`).val().split('/') : null,
            //         dateToParts = $(`input[name=${sumParts[1]}]`).val() ? $(`input[name=${sumParts[1]}]`).val().split('/') : null;
            //     if (dateFromParts && dateToParts) {
            //         let dateFrom = moment(`${dateFromParts[1]}/${dateFromParts[0]}/${dateFromParts[2]}`),
            //             dateTo = moment(`${dateToParts[1]}/${dateToParts[0]}/${dateToParts[2]}`);
            //         let totalDays;
            //         // const oneDay = 1000 * 60 * 60 * 24;
            //         let fromDate = new Date(dateFromParts[1] + "/" + dateFromParts[0] + "/" + dateFromParts[2]);
            //         let toDate = new Date(dateToParts[1] + "/" + dateToParts[0] + "/" + dateToParts[2]);
            //         totalDays = getBusinessDatesCount(fromDate, toDate);
            //         let isSameDay = dateFrom.isSame(dateTo);
            //
            //         if (item.name === 'total_absent') {
            //             if (isSameDay && (($('input[name=from-absent-session]:checked').val() == 'morning' && $('input[name=to-absent-session]:checked').val() == 'morning') ||
            //                     ($('input[name=from-absent-session]:checked').val() === 'afternoon' && $('input[name=to-absent-session]:checked').val() == 'afternoon'))) {
            //                 totalDays -= 0.5;
            //             } else if (!isSameDay && ($('input[name=from-absent-session]:checked').val() === 'afternoon' && $('input[name=to-absent-session]:checked').val() === 'morning')) {
            //                 totalDays -= 1;
            //             } else if ((dateFrom.day() === 6 && $('input[name=from-absent-session]:checked').val() === 'morning') &&
            //                 (dateTo.day() === 6 && $('input[name=to-absent-session]:checked').val() === 'morning')) {
            //                 totalDays += 0.5;
            //             } else if ((dateFrom.day() === 6 && $('input[name=from-absent-session]:checked').val() === 'afternoon') &&
            //                 (dateTo.day() === 6 && $('input[name=to-absent-session]:checked').val() === 'afternoon')) {
            //                 totalDays += 0.5;
            //             }
            //         }
            //
            //         if (item.name === 'total_absent_no_salary') {
            //             if (isSameDay && (($('input[name=from-nosalary-session]:checked').val() === 'morning' && $('input[name=to-nosalary-session]:checked').val() == 'morning') ||
            //                 ($('input[name=from-nosalary-session]:checked').val() == 'afternoon' && $('input[name=to-nosalary-session]:checked').val() == 'afternoon'))) {
            //                 totalDays -= 0.5;
            //             } else if (!isSameDay && ($('input[name=from-nosalary-session]:checked').val() === 'afternoon' && $('input[name=to-nosalary-session]:checked').val() === 'morning')) {
            //                 totalDays -= 1;
            //             } else if ((dateFrom.day() === 6 && $('input[name=from-absent-session]:checked').val() === 'morning') &&
            //                 (dateTo.day() === 6 && $('input[name=to-absent-session]:checked').val() === 'morning')) {
            //                 totalDays += 0.5;
            //             } else if ((dateFrom.day() === 6 && $('input[name=from-absent-session]:checked').val() === 'afternoon') &&
            //                 (dateTo.day() === 6 && $('input[name=to-absent-session]:checked').val() === 'afternoon')) {
            //                 totalDays += 0.5;
            //             }
            //         }
            //
            //         if (dateFrom.isAfter(dateTo)) {
            //             totalDays = 0;
            //         }
            //
            //         $(`input[name=${item.name}]`).val(totalDays.toFixed(1));
            //
            //     } else {
            //         $(`input[name=${item.name}]`).val(0);
            //     }
            //
            // }
            if (item.sumType === "money") {
                let total = 0;
                sumParts.forEach(x => {
                    let number = $(`input[name=${x}]`).val() || "0";
                    number = number.replace(/\./g, '');
                    total += parseInt(number);
                });
                let $el = $(`input[name=${item.name}]`);
                formatPrice($el, total);
            }
        }
    });
}

function getBusinessDatesCount(startDate, endDate) {
    let count = 0;
    let curDate = startDate;
    while (curDate <= endDate) {
        let dayOfWeek = curDate.getDay();

        if (dayOfWeek !== 0) {
            if (dayOfWeek === 6) {
                count += 0.5;
            } else {
                count++;
            }
        }
        curDate.setDate(curDate.getDate() + 1);
    }
    return count;
}

function getWorkingDays(fromTime, fromDate, toTime, toDate) {
    let count = 0;
    let curDate = fromDate;
    let isSameDate = fromDate.isSame(toDate);
    while (curDate <= toDate) {
        let dayOfWeek = curDate.getDay();

        if (dayOfWeek !== 0) {
            if (dayOfWeek === 6) {
                count += 0.5;
            } else {
                count++;
            }
        }
        curDate.setDate(curDate.getDate() + 1);
    }
    return count;
}

function validate() {
    let valid = true;

    let fields = $("#SaveWorkTemplate input[required], select[required], input[must-validate], input[d]");
    let elements = [];
    for (let i = 0; i < fields.length; i++) {
        let field = fields[i];
        if (!$(field).valid()) {
            elements.push(field);
        }
    }

    if (elements.length > 0) {
        valid = false;
        let position = $(elements[0]).offset();
        $("html, body").animate({
            scrollTop: position.top - 100
        }, "500", "swing");
        $(elements[0]).focus();
    }
    // Tổng dự trù chi
    let totalmoney = $('input[name=totalmoney]');
    // Tổng phân bổ
    let allmoney = $('input[name=allmoney]');

    if (totalmoney.length > 0 && allmoney.length > 0) {
        if (totalmoney.val() != allmoney.val()) {
            valid = false;
            alert("Tổng dự trù chi phải bằng tổng phân bổ");
        }
    }

    return valid;
}

function formatPrice($element, value) {
    value = value.toString().replace(/\./g, '');
    if (value) {
        $element.val(parseInt(value.replace(/[a-zA-Z]/g, '')).toLocaleString("it-IT"));
    }
}

function getRandomCode() {
    var random = (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)).substring(0, 5);
    return random;
}