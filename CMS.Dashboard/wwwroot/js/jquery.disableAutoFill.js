/**
 * Jquery - DisableAutoFill plugin
 * The easiest solution for disabling Google Chrome auto-fill, auto-complete functions.
 *
 * @license MIT
 * @version 1.2.4
 * @author  Terry, https://github.com/terrylinooo/
 * @updated 2018-08-01
 * @link    https://github.com/terrylinooo/jquery.disableAutoFill
 */

(function($) {

    'use strict';

    var realPassword = [];
    var realFields = [];

    // An Object for Helper functions.
    var _helper = {};

    // Extend the Array: add "insert" function.
    Array.prototype.insert = function (index, item) {
        this.splice(index, 0, item);
    };

    /**
     * Helper function - passwordListener
     * - Hide the original password string.
     *
     * @param {object} obj      jQuery DOM object (form)
     * @param {object} settings plugin settings.
     */
    _helper.passwordListener = function(obj, settings) {
        var passObj = (settings.passwordField == '') ? '.disabledAutoFillPassword' : settings.passwordField;
 
        if (obj.find('[type=password]').length > 0) {
            obj.find('[type=password]').attr('type', 'text').addClass('disabledAutoFillPassword');
        }

        obj.on('keyup', passObj, function() {
            var tmpPassword = $(this).val();
            var passwordLen = tmpPassword.length;

            // Get current keyup character position.
            var currKeyupPos = this.selectionStart;

            for (var i = 0; i < passwordLen; i++) {
                if (tmpPassword[i] != '*') {
                    if (typeof realPassword[i] == 'undefined') {
                        realPassword[i] = tmpPassword[i];
                    } else {
                        if (currKeyupPos != passwordLen) {
                            realPassword.insert(currKeyupPos - 1, tmpPassword[i]);
                        }
                    }
                }
            }

            $(this).val(tmpPassword.replace(/./g, '*'));

            if (settings.debugMode) {
                console.log('Current keyup position: ' + currKeyupPos);
                console.log('Password length: ' + passwordLen);
                console.log('Real password:');
                console.log(realPassword);
            }
        });
    }

    /**
     * Helper function - formSubmitListener
     * - Replace submit button to normal button to make sure everything works fine.
     * 
     * @param {object} obj      jQuery DOM object (form)
     * @param {object} settings plugin settings.
     */
    _helper.formSubmitListener = function(obj, settings) {
        var btnObj = (settings.submitButton == '') ? '.disableAutoFillSubmit' : settings.submitButton;

        obj.on('click', btnObj, function(evt) {

            if (settings.callback.call()) {
                if (settings.debugMode) {
                    console.log(obj.serialize())
                } else {
                    // Native HTML form validation requires "type=submit" to work with.
                    if (settings.html5FormValidate) {
                        $(btnObj).attr('type', 'submit').trigger('submit');
                        // Change "type=submit" back to "type=button".
                        setTimeout(function() {
                            $(btnObj).attr('type', 'button');
                        }, 1000);
                        
                    } else {
						var name = evt.currentTarget.name,
						target = $(evt.target),
						form = $(target.parents("form")[0]);

						form.data('unobtrusiveAjaxClick', name ? [{ name: name, value: evt.currentTarget.value }] : []);
						form.data('unobtrusiveAjaxClickTarget', target);
						setTimeout(function () {
							form.removeData('unobtrusiveAjaxClick');
							form.removeData('unobtrusiveAjaxClickTarget');
						}, 10);
		
                        obj.submit();
                    }
                }
            }
        });
    };

    /**
     * Core function
     */
    $.fn.disableAutoFill = function(options) {
        var settings = $.extend(
            {}, 
            $.fn.disableAutoFill.defaults, 
            options
        );

        // Add autocomplete attribute to form, and set it to 'off'
        this.attr('autocomplete', 'off');

        if (this.find('[type=submit]').length > 0) {
            this.find('[type=submit]').addClass('disableAutoFillSubmit').attr('type', 'button');
        }

        if (settings.submitButton != '') {
            this.find(settings.submitButton).addClass('disableAutoFillSubmit').attr('type', 'button');
        }

        if (settings.randomizeInputName) {
            _helper.randomizeInput(this, settings);
        }
        _helper.passwordListener(this, settings);
        _helper.formSubmitListener(this, settings);
        
    };

    $.fn.disableAutoFill.defaults = {
        debugMode: false,
        textToPassword: true,
        randomizeInputName: false,
        passwordField: '',
        html5FormValidate: false,
        submitButton: '',
        callback: function() {
            return true;
        },
    };

})(jQuery);
