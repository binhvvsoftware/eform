﻿var travelEngine = angular.module("travelEngine", ["initialValue"]);

travelEngine.directive('ngDatePicker', function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var $this = $(element);
            
            var dateformat = $this.data("date-format") || 'dd/mm/yyyy';
            
            var options = {
                todayHighlight: true,
                format: dateformat,
                autoclose: true
            };

            var datepicker = $this.datepicker(options);

            if (attrs.ngDatePicker) {
                var dt = new Date(attrs.ngDatePicker);
                datepicker.datepicker('setDate', dt);
            }

            if ($this.is("[data-val-required]") && $this[0].form) {
                $($this[0].form).submit(function (event) {
                    var value = $this.val();
                    if (value == null || value === "") {
                        $this.addClass("input-validation-error");
                        event.preventDefault();
                    } else {
                        $this.removeClass("input-validation-error");
                    }
                });
            }
        }
    };
});

travelEngine.controller('FlightMarkupByAirlineController', ["$scope", function ($scope) {

}]);

travelEngine.controller('FlightMarkupByPriceController', ["$scope", function ($scope) {

}]);

travelEngine.controller('OtherChargeMarkupController', ["$scope", function ($scope) {

}]);

travelEngine.controller('PromotionCodeController', ["$scope", "options", function ($scope, options) {
    if (options != null && options.extraPromos != null) {
        $scope.extraPromos = JSON.parse(options.extraPromos);
    }
    else {
        $scope.extraPromos = [];
    }

    $scope.loadCurrency = function () {

        var formData = new FormData();
        $.ajax({
            url: "../get-currency-code",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $scope.currencies = data;
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            },
            async: true
        });
    };

    $scope.removePromoSaleRate = function (promoSaleRates, promoSaleRate) {
        var index = promoSaleRates.indexOf(promoSaleRate);
        promoSaleRates.splice(index, 1);
    };

    $scope.addPromoSaleRate = function (currencySettings, extraPromos) {
        var amount = $('#PromotionCodeExtras_' + (extraPromos.length - 1) + '__CurrencySettings_' + (currencySettings.length - 1) + '__Amount').val();

        currencySettings.push({
            Amount: amount,
            Currency: $scope.currencies[0].code,
            Applied: "0"
        });
    };

    $scope.removeExtraPromo = function (extraPromo) {
        var index = $scope.extraPromos.indexOf(extraPromo);
        $scope.extraPromos.splice(index, 1);
    };

    $scope.addExtraPromo = function () {
        $scope.extraPromos.push({
            Id: "00000000-0000-0000-0000-000000000000",
            PeriodType: "0",
            StartDate: "",
            EndDate: "",
            Bookings: "1",
            CurrencySettings: [{
                Amount: "",
                Currency: $scope.currencies[0].code,
                Applied: "0"
            }]
        });
    }
}]);