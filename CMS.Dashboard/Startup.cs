﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using CMS.Core;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.Identity;
using CMS.Dashboard.Web.UI.Notify;
using CMS.Services.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using CMS.Core.Repository;
using IdentityRole = CMS.Shared.Data.Domain.IdentityRole;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;
using CMS.Services.Tracking;
using CMS.Shared;
using CMS.Shared.Web;

namespace CMS.Dashboard
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        private IContainer ApplicationContainer { get; set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    // User settings
                    options.User.RequireUniqueEmail = true;
                })
                .AddSignInManager()
                .AddIdentityEntityFrameworkStores<EformDbContext>()
                .AddDefaultTokenProviders();
            services.Configure<SecurityStampValidatorOptions>(options =>
            {
                options.ValidationInterval = TimeSpan.FromMinutes(30);
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.Cookie.SameSite = SameSiteMode.Strict;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.SlidingExpiration = true;
                options.LoginPath = "/account/login";
                options.LogoutPath = "/account/logoff";
                options.AccessDeniedPath = "/account/access-denied";

            });

            services.AddAuthentication(o =>
                {
                    o.DefaultScheme = "Application";
                    o.DefaultSignInScheme = "External";
                })
                .AddCookie("Application")
                .AddCookie("External")
                .AddGoogle(o =>
                {
                    o.ClientId = Configuration["Authentication:Google:ClientId"];
                    o.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                });

            services.Configure<GzipCompressionProviderOptions>(options =>
                options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression();
            services.AddMemoryCache();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.SameSite = SameSiteMode.Strict;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.Cookie.IsEssential = true;
            });
            services.AddSingleton(_ => Configuration);
            services.AddScoped<INotifier, Notifier>();
            services.AddSingleton<IStaticCacheManager, StaticCacheManager>();
            services.AddTransient<ITrackingEntityService, TrackingEntityService>();
            services.AddScoped<WorkContext>();
            services.AddOptions();
            services.AddMvc(opt => { opt.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider()); })
                .AddSessionStateTempDataProvider();

            var containerBuilder = new ContainerBuilder();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            containerBuilder
                .RegisterModule(new CoreModule())
                .RegisterModule(new DashboardModule())
                .RegisterModule(new EntityModule<EformDbContext>(connectionString));

            containerBuilder.Register(s => s.Resolve<ILoggerFactory>().CreateLogger(typeof(LogModule))).As<ILogger>()
                .SingleInstance();
            containerBuilder.Populate(services);
            ApplicationContainer = containerBuilder.Build();

            // Create the IServiceProvider based on the container.
            var provider = new AutofacServiceProvider(ApplicationContainer);

            return provider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IApplicationLifetime appLifetime)
        {
            app.UseResponseCompression();

            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMiddleware<ErrorHandlingMiddleware>();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseMiddleware<ErrorHandlingMiddleware>();
            }

            app.UseSession();

            app.UseAuthentication();

            app.Use(async (context, next) =>
            {
                // Prevent Clickjacking attacks
                context.Response.Headers["X-Frame-Options"] = "SAMEORIGIN";

                context.Response.GetTypedHeaders().CacheControl =
                    new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                    {
                        NoStore = true,
                        NoCache = true,
                        MustRevalidate = true
                    };

                await next();
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}");
            });

            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
        }
    }
}