﻿using System;
using CMS.Dashboard.Web.UI.Notify;
using Microsoft.AspNetCore.Html;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using CMS.Shared.Data.Domain;
using CMS.Services.Services;
using CMS.Shared.Extensions;
using CMS.Core.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;

namespace CMS.Dashboard
{
    public class WorkContext
    {private readonly IHttpContextAccessor _context;
        private readonly IComponentContext _componentContext;
        private readonly ILogger<WorkContext> _logger;
        private readonly UserManager<IdentityUser> _userManager;

        public WorkContext(INotifier notifier, IHttpContextAccessor context, IComponentContext componentContext,
            ILogger<WorkContext> logger, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _componentContext = componentContext;
            _logger = logger;
            _userManager = userManager;
            Breadcrumbs = new List<Breadcrumb>();
            ScriptResources = new List<ScriptResource>();
            StyleResources = new List<StyleResource>();
            Notifier = notifier;
        }

        public ICollection<Breadcrumb> Breadcrumbs { get; }
        public ICollection<ScriptResource> ScriptResources { get; }
        public ICollection<StyleResource> StyleResources { get; }
        public INotifier Notifier { get; }

        public async Task<bool> CheckPermission(MenuAction menuAction)
        {
            if (_context.HttpContext.User == null || !_context.HttpContext.User.Identity.IsAuthenticated)
            {
                return false;
            }

            try
            {
                var values = await GetPermissionMenus().Continue(x => x.ToList());

                if (!values.IsNullOrEmpty() && !values.Where(s =>
                        s.Url.Contains(_context.HttpContext.Request.Path.Value.SafeSplit(new string[] {"/"})[0]) &&
                        (s.MenuAction == MenuAction.IsManage || s.MenuAction == menuAction)).IsNullOrEmpty())
                {
                    return true;
                }

                Notifier.Add(NotifyType.Error,
                    $"{_context.HttpContext.User.Identity.Name}. Anonymous users do not have {menuAction.GetDisplayName()} permission.");
                return false;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new AuthenticationException("Authorized Error", e);
            }
        }

        public async Task<IEnumerable<Menu>> GetPermissionMenus()
        {
            if (_context.HttpContext.User == null || !_context.HttpContext.User.Identity.IsAuthenticated)
                return null;

            try
            {
                var user = await _userManager.GetUserAsync(_context.HttpContext.User);
                var userRoleService = _componentContext.Resolve<IUserRoleService>();
                var usRoles = await userRoleService.GetRecordsAsync(s => s.UserId == user.Id);
                var roleService = _componentContext.Resolve<IRoleService>();
                var roles = await roleService.GetRecordsAsync(x => usRoles.Select(s => s.RoleId).Contains(x.Id));
                var menuService = _componentContext.Resolve<IMenuService>();
                var menuIds = roles.SelectMany(x => x.MenuIds.SafeSplit()).Distinct();
                return await menuService.GetRecordsAsync(s => menuIds.Select(int.Parse).Contains(s.Id));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
                return null;
            }
        }

        public async Task<IEnumerable<Workflow>> GetWorkFlow()
        {
            if (_context.HttpContext.User == null || !_context.HttpContext.User.Identity.IsAuthenticated)
                return null;

            try
            {
                var user = await GetCurrentUser();
                return await _componentContext.Resolve<IWorkflowService>()
                    .GetRecordsAsync(s => s.CampusId == user.CampusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
                return null;
            }
        }

        public async Task<IdentityUser> GetCurrentUser()
        {
            if (_context.HttpContext.User == null || !_context.HttpContext.User.Identity.IsAuthenticated)
            {
                throw new AuthenticationException("User not found.");
            }

            var user = await _userManager.GetUserAsync(_context.HttpContext.User);

            if (user == null)
            {
                throw new AuthenticationException("User not found.");
            }

            return user;
        }

        public void AddScript(string script)
        {
            ScriptResources.Add(new ScriptResource
            {
                InlineMode = false,
                Source = script
            });
        }

        public void AddInlineScript(string script)
        {
            ScriptResources.Add(new ScriptResource
            {
                InlineMode = true,
                Source = script
            });
        }

        public void AddStyle(string href)
        {
            StyleResources.Add(new StyleResource
            {
                InlineMode = false,
                Content = href
            });
        }

        public void AddInlineStyle(string content)
        {
            StyleResources.Add(new StyleResource
            {
                InlineMode = true,
                Content = content
            });
        }

        public void AddBreadcrumb(string text)
        {
            Breadcrumbs.Add(new Breadcrumb(text));
        }

        public void AddBreadcrumb(string text, string url)
        {
            Breadcrumbs.Add(new Breadcrumb(text, url));
        }

        public ICollection<Breadcrumb> GetBreadcrumb()
        {
            return Breadcrumbs;
        }

        public IHtmlContent RenderScripts()
        {
            if (ScriptResources.Count == 0)
            {
                return null;
            }

            var sb = new StringBuilder();

            foreach (var scriptResource in ScriptResources.Where(x => x.InlineMode == false))
            {
                sb.AppendFormat("<script type=\"text/javascript\" src=\"{0}\"></script>", scriptResource.Source);
            }

            foreach (var scriptResource in ScriptResources.Where(x => x.InlineMode))
            {
                sb.AppendFormat("<script type=\"text/javascript\">{0}</script>", scriptResource.Source);
            }

            return new HtmlString(sb.ToString());
        }

        public IHtmlContent RenderStyles()
        {
            if (StyleResources.Count == 0)
            {
                return null;
            }

            var sb = new StringBuilder();

            foreach (var styleResource in StyleResources)
            {
                if (styleResource.InlineMode)
                {
                    sb.AppendFormat("<link type=\"text/css\" rel=\"stylesheet\">{0}</link>", styleResource.Content);
                }
                else
                {
                    sb.AppendFormat("<link type=\"text/css\" rel=\"stylesheet\" href=\"{0}\"></link>",
                        styleResource.Content);
                }
            }

            return new HtmlString(sb.ToString());
        }
    }

    public class Breadcrumb
    {
        public Breadcrumb()
        {
        }

        public Breadcrumb(string text)
        {
            Text = text;
        }

        public Breadcrumb(string text, string url)
        {
            Text = text;
            Url = url;
        }

        public string Text { get; set; }

        public string Url { get; set; }

        public string IconCssClass { get; set; }
    }

    public class ScriptResource
    {
        public string Source { get; set; }

        public bool InlineMode { get; set; }
    }

    public class StyleResource
    {
        public string Content { get; set; }

        public bool InlineMode { get; set; }
    }
}