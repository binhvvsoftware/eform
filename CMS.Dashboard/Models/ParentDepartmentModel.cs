﻿using System;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class ParentDepartmentModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 255, ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 0, LabelText = "Tên cơ sở")]
        public string Name { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 1, LabelText = "Mô tả")]
        public string Description { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, HasLabelControl = false, ContainerCssClass = "col-xs-12", ContainerRowIndex = 2, LabelText = "Trạng thái")]
        public bool Status { get; set; }

        public static implicit operator ParentDepartmentModel(ParentDepartment entity)
        {
            return new ParentDepartmentModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Status = entity.Status
            };
        }
    }
}
