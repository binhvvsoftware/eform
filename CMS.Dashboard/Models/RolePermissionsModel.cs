﻿using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;
using System;
using System.Collections.Generic;

namespace CMS.Dashboard.Models
{
    public class RolePermissionsModel
    {
        public IEnumerable<Menu> Menus { get; set; }

        public IEnumerable<Menu> MenuSelecteds { get; set; }

        public IEnumerable<Menu> MenuChilds { get; set; }

        public int Id { get; set; }

        public Dictionary<string, string> Permissions { get; set; }

        public string Name { get; set; }
    }
}