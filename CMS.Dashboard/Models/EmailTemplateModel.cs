﻿using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class EmailTemplateModel : BaseModel<int>
    {
        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Tên", ContainerCssClass = "col-xs-12 col-md-12", ContainerRowIndex = 0)]
        public string Name { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, HasLabelControl = false, LabelText = "Enabled", ContainerCssClass = "col-md-3", ContainerRowIndex = 1)]
        public bool Enabled { get; set; }

        [RoboText(MaxLength = 255, LabelText = "To Email Address", HelpText = "Multiple email address separate by ;", ContainerCssClass = "col-md-6", ContainerRowIndex = 2)]
        public string EmailAddress { get; set; }

        [RoboText(MaxLength = 255, LabelText = "To Cc Email Address", HelpText = "Multiple email address separate by ;", ContainerCssClass = "col-md-6", ContainerRowIndex = 3)]
        public string CcEmailAddress { get; set; }

        [RoboText(MaxLength = 255, LabelText = "To Bcc Email Address", HelpText = "Multiple email address separate by ;", ContainerCssClass = "col-md-6", ContainerRowIndex = 4)]
        public string BccEmailAddress { get; set; }

        [RoboText(IsRequired = true, LabelText = "Subject", ContainerCssClass = "col-md-6", ContainerRowIndex = 5)]
        public string Subject { get; set; }

        [RoboText(RoboTextType.MultiText, LabelText = "Body", ContainerCssClass = "col-md-12", ContainerRowIndex = 6)]
        [RoboHtmlAttribute("class", "markItUp")]
        public string Body { get; set; }

        public static implicit operator EmailTemplateModel(EmailTemplate emailTemplate)
        {
            return new EmailTemplateModel
            {
                Id = emailTemplate.Id,
                Name = emailTemplate.Name,
                Subject = emailTemplate.Subject,
                Body = emailTemplate.Body,
                EmailAddress = emailTemplate.EmailAddress,
                CcEmailAddress = emailTemplate.CcEmailAddress,
                BccEmailAddress = emailTemplate.BccEmailAddress,
                Enabled = emailTemplate.Enabled
            };
        }
    }
}
