﻿using CMS.Dashboard.Web.UI.RoboUI;

namespace CMS.Dashboard.Models
{
    public abstract class BaseModel<TKey>
    {
        [RoboHidden]
        public TKey Id { get; set; }
    }
}
