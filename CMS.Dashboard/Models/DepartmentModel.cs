﻿using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class DepartmentModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Tên", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 0)]
        public string Name { get; set; }

        [RoboText(MaxLength = 255, LabelText = "Mô tả", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 1)]
        public string Description { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, LabelText = "Trạng thái", HasLabelControl = false, ContainerCssClass = "col-xs-12", ContainerRowIndex = 3)]
        public bool Status { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Campuses", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 2)]
        public int CampusId { get; set; }

        [RoboText(MaxLength = 255, LabelText = "Số thứ tự", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 4)]
        public int Order { get; set; }

        public static implicit operator DepartmentModel(Department entity)
        {
            return new DepartmentModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                CampusId = entity.CampusId,
                Status = entity.Status,
                Order = entity.Order
            };
        }
    }
}
