﻿using System;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class WorkingTimesheetModel : BaseModel<long>
    {
        public WorkingTimesheet WorkingTime { get; set; }

        public static implicit operator WorkingTimesheetModel(WorkingTimesheet entity)
        {
            return new WorkingTimesheetModel
            {
                WorkingTime = entity
            };
        }
    }
}
