﻿using CMS.Dashboard.Web.UI.RoboUI;
using System;

namespace CMS.Dashboard.Models
{
    public class UpdateRolesModel
    {
        [RoboHidden]
        public int UserId { get; set; }

        [RoboChoice(RoboChoiceType.CheckBoxList)]
        public string[] Roles { get; set; }
    }
}