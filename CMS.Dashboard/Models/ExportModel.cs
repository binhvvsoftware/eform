using System.Collections.Generic;
using Newtonsoft.Json;

namespace CMS.Dashboard.Models
{
    public class ExportModel
    {
        public Dictionary<string, string> Bodys { get; set; }
        public Dictionary<string, string> Signatures { get; set; }
        public IList<ExportTable> ExportTables { get; set; }
        public string AllComments { get; set; }
    }
    
    public class ExportTable
    {
        [JsonProperty("itemNo")]
        public int ItemNumber { get; set; }
        
        [JsonProperty("itemName")]
        public string ItemName { get; set; }
        
        [JsonProperty("unitOfItem")]
        public string ItemUnit { get; set; }
        
        [JsonProperty("note")]
        public string Description { get; set; }
        
        [JsonProperty("quantity")]
        public int Total { get; set; }
    }
}