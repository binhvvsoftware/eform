﻿using System;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class WorkingLateRequestModel : BaseModel<long>
    {
        public long TimeSheetId { get; set; }
        public int RequesterId { get; set; }
        public IdentityUser Requester { get; set; }
        public ConfirmStatus CurrentStatus { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int[] ManagerIds { get; set; }

        [RoboText(IsReadOnly = true, LabelText = "Họ và tên cán bộ đi muộn", ContainerCssClass = "col-xs-12 col-md-12", ContainerRowIndex = 0)]
        public string UserFullName { get; set; }

        [RoboText(IsReadOnly = true, LabelText = "Ngày làm việc", ContainerCssClass = "col-xs-12 col-md-12", ContainerRowIndex = 1)]
        public DateTime WorkingDate { get; set; }

        [RoboText(IsReadOnly = true, LabelText = "Giờ đến FSB", ContainerCssClass = "col-xs-12 col-md-12", ContainerRowIndex = 2)]
        public string ActualWorkingTime { get; set; }

        public static implicit operator WorkingLateRequestModel(WorkingLateRequest entity)
        {
            return new WorkingLateRequestModel
            {
                Id = entity.Id,
                TimeSheetId = entity.TimeSheetId,
                RequesterId = entity.RequesterId,
                Requester = entity.Requester,
                ManagerIds = entity.ManagerIds,
                CurrentStatus = entity.ConfirmedStatus,
                CreateDate = entity.CreateDate,
                UpdateDate = entity.UpdateDate,
                UserFullName = entity.Requester?.FullName,
                ActualWorkingTime = entity.ActualWorkTime.ToLongDateString(),
                WorkingDate = entity.ActualWorkTime.Date
            };
        }
    }
}
