﻿using CMS.Dashboard.Web.UI.RoboUI;
using System;

namespace CMS.Dashboard.Models
{
    public class ChangeLockoutDateModel
    {
        [RoboHidden]
        public int UserId { get; set; }

        [RoboDatePicker(LabelText = "Lockout Date", ContainerRowIndex = 0, ContainerCssClass = "col-xs-4")]
        public DateTimeOffset? LockoutDate { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Hour", ContainerRowIndex = 1, ContainerCssClass = "col-xs-2")]
        public int LockoutHour { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Minute", ContainerRowIndex = 1, ContainerCssClass = "col-xs-2")]
        public int LockoutMinute { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, LabelText = "Remove Lockout Date", HasLabelControl = false, ContainerRowIndex = 2, ContainerCssClass = "col-xs-6")]
        public bool RemoveLockoutDate { get; set; }
    }
}
