﻿using CMS.Dashboard.Web.UI.RoboUI;
using System;

namespace CMS.Dashboard.Models
{
    public class UpdatePermissionsModel
    {
        [RoboHidden]
        public int RoleId { get; set; }

        [RoboHidden]
        public int RoleMenuId { get; set; }

        [RoboChoice(RoboChoiceType.CheckBoxList, IsRequired = true, HasLabelControl = false, Columns = 2, ControlContainerCssClass = "col-md-6", GroupedByCategory = true, RequiredIfHaveItemsOnly = true)]
        public string[] Permissions { get; set; }
    }
}