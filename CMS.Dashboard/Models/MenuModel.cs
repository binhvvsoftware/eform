﻿using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class MenuModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Tên", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 0)]
        public string Name { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Url", Value = "/", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 1)]
        public string Url { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Icon", Value = "", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 2)]
        public string Icon { get; set; }

        [RoboChoice(RoboChoiceType.RadioButtonList, IsRequired = true, LabelText = "Chức năng", InlineControls = true, HasLabelControl = false, ContainerCssClass = "col-xs-12", ContainerRowIndex = 3)]
        public MenuAction IsViewManage { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, OptionLabel = "Menu Gốc", Value = 0, LabelText = "Parent", ContainerCssClass = "col-md-12", ContainerRowIndex = 4)]
        public int Parent { get; set; }

        [RoboNumeric(Name = "Vị trí", MinimumValue = "0", MaximumValue = "999", ContainerCssClass = "col-xs-12 col-md-4", ContainerRowIndex = 5)]
        public int Position { get; set; }

        //[RoboChoice(RoboChoiceType.CheckBox, Name = "Trạng thái", HasLabelControl = false, ContainerCssClass = "col-xs-12", ContainerRowIndex = 3)]
        //public bool IsActive { get; set; }

        public static implicit operator MenuModel(Menu entity)
        {
            return new MenuModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Url = entity.Url,
                Icon = entity.Icon,
                Position = entity.Position,
                //IsActive = entity.IsActive,
                Parent = entity.Parent,
                IsViewManage = entity.MenuAction
            };
        }
    }
}
