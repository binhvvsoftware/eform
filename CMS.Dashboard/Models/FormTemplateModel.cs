﻿using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class FormTemplateModel : BaseModel<int>
    {
        public string TemplateName { get; set; }

        public string TemplateContent { get; set; }

        public string TemplatePath { get; set; }

        public bool IsActive { get; set; }
//        public FormType FormType { get; set; }

        public static implicit operator FormTemplateModel(FormTemplate entity)
        {
            return new FormTemplateModel
            {
                Id = entity.Id,
                TemplateName = entity.TemplateName,
                IsActive = entity.IsActive,
                TemplatePath = entity.TemplatePath,
                TemplateContent = entity.TemplateContents,
//                FormType = entity.FormType
            };
        }
    }
}
