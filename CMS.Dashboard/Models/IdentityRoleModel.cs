﻿using CMS.Dashboard.Web.UI.RoboUI;
using System;

namespace CMS.Dashboard.Models
{
    public class IdentityRoleModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 200)]
        public string Name { get; set; }
    }
}
