﻿using CMS.Shared.Data.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.Models
{
    public class RequestDocumentModel : BaseModel<int>
    {
        public int WorkflowId { get; set; }

        public int FormTemplateId { get; set; }

        public int CurrentStepId { get; set; }

        public string WorkflowName { get; set; }

        public string HtmlContent { get; set; }
        public string JsonContent { get; set; }

        public string SignatureImage { get; set; }

        public string CaptchaCode { get; set; }

        public string Comment { get; set; }

        public FormTemplate FormTemplate { get; set; }

        public RequestDocument RequestDocument { get; set; }

        public IdentityUser User { get; set; }

        public IEnumerable<RequestDocumentAssignee> RequestDocumentAssignees { get; set; }

        public RequestDocumentAssignee CurrentStepRequestDocumentAssignee { get; set; }

        public string Status { get; set; }

        public Workflow Workflow { get; set; }
        public bool CanApprove { get; set; }

        public bool Enabled { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }

    public enum FilterDocumentStatus : byte
    {
        [Display(Name = "Lưu tạm")]
        Saved = 0,
        [Display(Name = "Đang chờ duyệt")]
        Inprogress = 2,
        [Display(Name = "Văn bản từ chối duyệt")]
        Rejected = 3,
        [Display(Name = "Văn bản đã duyệt")]
        Approved = 4,
        [Display(Name = "Tôi đã duyệt")]
        MyApproved = 5,
        [Display(Name = "Tôi từ chối duyệt")]
        MyRejected = 6
    }
}
