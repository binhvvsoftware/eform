﻿using CMS.Shared.Data.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.Models
{
    public class WorkflowModel : BaseModel<int>
    {
        public string Name { get; set; }

        public int CampusId { get; set
            ; }

        public int FormTemplateId { get; set; }

        public int Step { get; set; }

        public Dictionary<int, int[]> GroupManage { get; set; }

        public Dictionary<int, int[]> UserIds { get; set; }

        public IEnumerable<IdentityUser> Users { get; set; }

        public GroupManagement? GroupManagement { get; set; }

        public IEnumerable<FormTemplate> FormTemplates { get; set; }

        public IEnumerable<Campus> Campuses { get; set; }

        public IEnumerable<WorkflowStep> WorkflowSteps { get; set; }

        public Workflow Workflow { get; set; }

        public UserType WorkflowType { get; set; }
        public IDictionary<int, KeyValuePair<object,object>> SelectedUsers { get; set; }
    }


}
