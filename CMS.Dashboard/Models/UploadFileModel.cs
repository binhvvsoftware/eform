﻿using CMS.Dashboard.Web.UI.RoboUI;

namespace CMS.Dashboard.Models
{
    public class UploadFileModel
    {
        [RoboFileUpload(LabelText = "Import file", IsRequired = true)]
        public string ImportFile { get; set; }
    }
}