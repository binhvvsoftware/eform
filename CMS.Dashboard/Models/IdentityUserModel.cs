﻿using System;
using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.Models
{
    public class IdentityUserModel : BaseModel<int>
    {
        [Required]
        [RoboText(IsRequired = true, MaxLength = 255, Order = -5, LabelText = "Tên đầy đủ", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 0)]
        public string FullName { get; set; }

        [Required]
        [RoboText(IsRequired = true, MaxLength = 255, Order = -5, LabelText = "Tên tài khoản", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 0)]
        public string UserName { get; set; }

        [Required]
        [RoboText(Type = RoboTextType.Email, MaxLength = 255, IsRequired = true, Order = -4, ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 1)]
        public string Email { get; set; }

        [RoboText(Type = RoboTextType.TextBox, MaxLength = 255, IsRequired = false, Order = -3, LabelText = "Số điện thoại", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 1)]
        public string PhoneNumber { get; set; }

        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [RoboText(Type = RoboTextType.TextBox, MaxLength = 128, IsRequired = true, LabelText = "Mật khẩu", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 2)]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("autocomplete", "off")]
        [RoboHtmlAttribute("class", "password")]
        public string Password { get; set; }

        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [RoboText(Type = RoboTextType.TextBox, MaxLength = 128, IsRequired = true, LabelText = "Xác nhận mật khẩu", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 2, EqualTo = "Password")]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("autocomplete", "off")]
        [RoboHtmlAttribute("class", "password")]
        public string ConfirmPassword { get; set; }

        [RoboText(Type = RoboTextType.TextBox, MaxLength = 255, LabelText = "Mã nhân viên", IsRequired = false, ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public string EmployeeCode { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, AllowMultiple = true, EnableChosen = true, OptionLabel = "", IsRequired = true, LabelText = "Quản lý", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public int[] ManagerIds { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Loại hợp đồng", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public ContractType ContractType { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Giới tính", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public Gender Gender { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Campus", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public int CampusId { get; set; }

        [RoboCascadingDropDown(LabelText = "Bộ phận", ParentControl = "CampusId", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3)]
        public int DepartmentId { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, HasLabelControl = false, LabelText = "Cấp CBQL FSB", ContainerCssClass = "col-md-6 col-xs-6", ContainerRowIndex = 6)]
        public bool SeniorLeader { get; set; } 
        
        [RoboChoice(RoboChoiceType.CheckBox, HasLabelControl = false, LabelText = "Không tính rule", ContainerCssClass = "col-md-6 col-xs-6", ContainerRowIndex = 6)]
        public bool IgnoreWorkingRule { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, HasLabelControl = false, LabelText = "Quản lý đơn hệ thống", ContainerCssClass = "col-md-6 col-xs-6", ContainerRowIndex = 6)]
        public bool IsManageRequestDocument { get; set; }

        [RoboText(Type = RoboTextType.TextBox, MaxLength = 255, LabelText = "Địa chỉ", IsRequired = false, ContainerCssClass = "col-xs-8 col-md-8", ContainerRowIndex = 7)]
        public string Address { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Đối tượng", ContainerCssClass = "col-xs-4 col-md-4", ContainerRowIndex = 7)]
        public UserType UserType { get; set; }

        public DateTime CreateDate { get; set; }
        public bool Disabled { get; set; }

        public static implicit operator IdentityUserModel(IdentityUser entity)
        {
            return new IdentityUserModel
            {
                Email = entity.Email,
                Id = entity.Id,
                PhoneNumber = entity.PhoneNumber,
                UserName = entity.UserName,
                FullName = entity.FullName,
                DepartmentId = entity.DepartmentId,
                EmployeeCode = entity.EmployeeCode,
                CampusId = entity.CampusId,
                ContractType = entity.ContractType,
                Gender = entity.Gender,
                Address = entity.Address,
                SeniorLeader = entity.SeniorLeader,
                IsManageRequestDocument = entity.IsManageRequestDocument,
                UserType = entity.UserType,
                CreateDate = entity.CreateDate,
                Disabled = entity.Disabled,
                IgnoreWorkingRule = entity.IgnoreWorkingRule
            };
        }
    }
}
