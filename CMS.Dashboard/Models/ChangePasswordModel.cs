﻿using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI.RoboUI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.Models
{
    public class ChangePasswordModel
    {
        [RoboHidden]
        public int UserId { get; set; }

        [RoboText(IsReadOnly = true, LabelText = "Tên đăng nhập")]
        public string UserName { get; set; }

        [RoboText(IsReadOnly = true)]
        public string Email { get; set; }

        [Required]
        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [Display(Name = "Mật khẩu cũ")]
        [RoboText(Type = RoboTextType.TextBox, IsRequired = true, LabelText = "Mật khẩu cũ")]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("class", "password")]
        [RoboHtmlAttribute("autocomplete", "off")]
        public string OldPassword { get; set; }

        [Required]
        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [Display(Name = "Mật khẩu mới")]
        [RoboText(Type = RoboTextType.TextBox, IsRequired = true, LabelText = "Mật khẩu mới")]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("class", "password")]
        [RoboHtmlAttribute("autocomplete", "off")]
        public string Password { get; set; }

        [Required]
        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [Display(Name = "Xác nhận mật khẩu mới")]
        [RoboText(Type = RoboTextType.TextBox, IsRequired = true, EqualTo = "Password", LabelText = "Xác nhận mật khẩu mới")]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("class", "password")]
        [RoboHtmlAttribute("autocomplete", "off")]
        public string ConfirmPassword { get; set; }
    }
}