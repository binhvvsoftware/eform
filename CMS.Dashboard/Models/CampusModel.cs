﻿using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;

namespace CMS.Dashboard.Models
{
    public class CampusModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Tên", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 0)]
        public string Name { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Địa chỉ", ContainerCssClass = "col-xs-12 col-md-3", ContainerRowIndex = 1)]
        public string Address { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Điện thoại", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 2)]
        public string Phone { get; set; }

        [RoboChoice(RoboChoiceType.DropDownList, IsRequired = true, LabelText = "Cơ sở", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 3)]
        public int RootDepartment { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, LabelText = "Trạng thái", HasLabelControl = false, ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 4)]
        public bool Status { get; set; }

        public static implicit operator CampusModel(Campus entity)
        {
            return new CampusModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Address = entity.Address,
                Phone = entity.Phone,
                RootDepartment = entity.ParentDepartmentId,
                Status = entity.Status
            };
        }
    }
}
