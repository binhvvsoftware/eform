﻿using CMS.Dashboard.Web.UI.RoboUI;

namespace CMS.Dashboard.Models
{
    public class UploadExcelFile
    {
        [RoboFileUpload(LabelText = "Import excel file")]
        public string ImportFile { get; set; }
    }
}
