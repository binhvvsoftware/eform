﻿using CMS.Dashboard.Web;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Dashboard.Models
{
    public class SmtpConfigModel : BaseModel<int>
    {
        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Name", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 0)]
        public string Name { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Email", ContainerCssClass = "col-xs-12 col-md-3", ContainerRowIndex = 0)]
        public string Email { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Host", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 1)]
        public string Host { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Host", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 1)]
        public int Port { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, LabelText = "EnableSsl", HasLabelControl = false, ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 2)]
        public bool EnableSsl { get; set; }

        [RoboChoice(RoboChoiceType.CheckBox, LabelText = "UseDefaultCredentials",  HasLabelControl = false, ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 2)]
        public bool UseDefaultCredentials { get; set; }

        [RoboText(IsRequired = true, MaxLength = 255, LabelText = "Username", ContainerCssClass = "col-xs-12 col-md-2", ContainerRowIndex = 3)]
        public string Username { get; set; }

        [ModelBinder(BinderType = typeof(EncryptedModelBinder))]
        [RoboText(Type = RoboTextType.TextBox, MaxLength = 128, IsRequired = true, LabelText = "Confirm Password", ContainerCssClass = "col-xs-6 col-md-6", ContainerRowIndex = 3, EqualTo = "Password")]
        [RoboHtmlAttribute("oncopy", "return false")]
        [RoboHtmlAttribute("oncut", "return false")]
        [RoboHtmlAttribute("oncontextmenu", "return false")]
        [RoboHtmlAttribute("autocomplete", "off")]
        [RoboHtmlAttribute("class", "password")]
        public string Password { get; set; }

        public static implicit operator SmtpConfigModel(SmtpConfig entity)
        {
            return new SmtpConfigModel
            {
               Id = entity.Id,
               Email = entity.Email,
               EnableSsl = entity.EnableSsl,
               Host = entity.Host,
               Name = entity.Name,
               Password = entity.Password,
               UseDefaultCredentials = entity.UseDefaultCredentials,
               Username = entity.Username,
               Port = entity.Port
            };
        }
    }
}
