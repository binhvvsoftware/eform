using CMS.Dashboard.Web.UI.RoboUI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace CMS.Dashboard.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static RoboUIGridResult<TModel> RoboGrid<TModel>(this HtmlHelper htmlHelper, TModel model) where TModel : class
        {
            var controllerCtx = new ControllerContext(htmlHelper.ViewContext)
            {
                HttpContext = htmlHelper.ViewContext.HttpContext
            };

            return new RoboUIGridResult<TModel>(controllerCtx);
        }
    }
}