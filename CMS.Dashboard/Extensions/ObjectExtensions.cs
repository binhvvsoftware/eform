﻿//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Dynamic;
//using System.IO;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Security.Cryptography;
//using System.Text;
//using System.Web.Mvc;
//using System.Xml;
//using System.Xml.Serialization;
//using MvcCornerstone.IO;
//using MvcCornerstone.Serialization;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

//namespace MvcCornerstone.Extensions
//{
//    public static class ObjectExtensions
//    {
//        private static Func<object, Dictionary<object, object>, object> GetTypeCloner(Type type)
//        {
//            return TypeCloners.GetOrAdd(type, t => new CloneExpressionBuilder(t).CreateTypeCloner());
//        }

//        private static readonly ConcurrentDictionary<Type, Func<object, Dictionary<object, object>, object>> TypeCloners = new ConcurrentDictionary<Type, Func<object, Dictionary<object, object>, object>>();

//        /// <summary>
//        /// Serializes the specified System.Object and returns the data.
//        /// </summary>
//        /// <typeparam name="T">This item's type</typeparam>
//        /// <param name="item">This item</param>
//        /// <returns>Serialized data of specified System.Object as a Base64 encoded String</returns>
//        public static string Base64Serialize<T>(this T item)
//        {
//            using (var memoryStream = new MemoryStream())
//            {
//                var binaryFormatter = new BinaryFormatter();
//                binaryFormatter.Serialize(memoryStream, item);
//                var bytes = memoryStream.GetBuffer();
//                return string.Concat(bytes.Length, ":", Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None));
//            }
//        }

//        /// <summary>
//        /// Serializes the specified System.Object and returns the data.
//        /// </summary>
//        /// <typeparam name="T">This item's type</typeparam>
//        /// <param name="item">This item</param>
//        /// <returns>Serialized data of specified System.Object as System.Byte[]</returns>
//        public static byte[] BinarySerialize<T>(this T item)
//        {
//            using (var memoryStream = new MemoryStream())
//            {
//                var binaryFormatter = new BinaryFormatter();
//                binaryFormatter.Serialize(memoryStream, item);
//                return memoryStream.ToArray();
//            }
//        }

//        /// <summary>
//        /// Serializes the specified System.Object and writes the data to the specified file.
//        /// </summary>
//        /// <typeparam name="T">This item's type</typeparam>
//        /// <param name="item">This item</param>
//        /// <param name="fileName">The name of the file to save the serialized data to.</param>
//        public static void BinarySerialize<T>(this T item, string fileName)
//        {
//            using (var stream = File.Open(fileName, FileMode.Create))
//            {
//                var binaryFormatter = new BinaryFormatter();
//                binaryFormatter.Serialize(stream, item);
//            }
//        }

//        public static T ConvertTo<T>(this object source)
//        {
//            return (T)Convert.ChangeType(source, typeof(T));
//        }

//        public static bool IsPrimitive(this Type type)
//        {
//            if (type == typeof(string))
//                return true;
//            return type.IsValueType & type.IsPrimitive;
//        }

//        public static object Copy(this object original)
//        {
//            if (original == null)
//                return null;

//            var typeToReflect = original.GetType();
//            if (IsPrimitive(typeToReflect))
//                return original;

//            Func<object, Dictionary<object, object>, object> creator = GetTypeCloner(typeToReflect);
//            return creator(original, new Dictionary<object, object>());
//        }

//        public static T Clone<T>(this T item) where T : class
//        {
//            if (item == null)
//            {
//                return null;
//            }

//            var json = JsonConvert.SerializeObject(item);
//            return JsonConvert.DeserializeObject<T>(json);
//        }

//        /// <summary>
//        /// Creates a deep clone of the current System.Object.
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="item">The original object.</param>
//        /// <returns>A clone of the original object</returns>
//        public static T DeepClone<T>(this T item) where T : ISerializable
//        {
//            using (var memoryStream = new MemoryStream())
//            {
//                var binaryFormatter = new BinaryFormatter();
//                binaryFormatter.Serialize(memoryStream, item);
//                memoryStream.Seek(0, SeekOrigin.Begin);
//                return (T)binaryFormatter.Deserialize(memoryStream);
//            }
//        }

//        /// <summary>
//        /// Determines whether this T is contained in the specified 'IEnumerable of T'
//        /// </summary>
//        /// <typeparam name="T">This System.Object's type</typeparam>
//        /// <param name="t">This item</param>
//        /// <param name="enumerable">The 'IEnumerable of T' to check</param>
//        /// <returns>true if enumerable contains this item, otherwise false.</returns>
//        public static bool In<T>(this T t, IEnumerable<T> enumerable)
//        {
//            foreach (T item in enumerable)
//            {
//                if (item.Equals(t))
//                { return true; }
//            }
//            return false;
//        }

//        /// <summary>
//        /// Determines whether this T is contained in the specified values
//        /// </summary>
//        /// <typeparam name="T">This System.Object's type</typeparam>
//        /// <param name="t">This item</param>
//        /// <param name="items">The values to compare</param>
//        /// <returns>true if values contains this item, otherwise false.</returns>
//        public static bool In<T>(this T t, params T[] items)
//        {
//            foreach (T item in items)
//            {
//                if (item.Equals(t))
//                { return true; }
//            }
//            return false;
//        }

//        public static bool IsDefault<T>(this T item)
//        {
//            return EqualityComparer<T>.Default.Equals(item, default(T));
//        }

//        public static bool GenericEquals<T>(this T item, T other)
//        {
//            return EqualityComparer<T>.Default.Equals(item, other);
//        }

//        public static string SharpSerialize<T>(this T item)
//        {
//            var sharpSettings = new SharpSerializerXmlSettings
//            {
//                IncludeAssemblyVersionInTypeName = false,
//                IncludeCultureInTypeName = false,
//                IncludePublicKeyTokenInTypeName = false
//            };
//            return new SharpSerializer(sharpSettings).Serialize(item);
//        }

//        public static IDictionary<string, object> ToDictionary(this object obj)
//        {
//            return HtmlHelper.AnonymousObjectToHtmlAttributes(obj);
//        }

//        public static ExpandoObject ToExpando(this object anonymousObject)
//        {
//            IDictionary<string, object> anonymousDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(anonymousObject);
//            IDictionary<string, object> expando = new ExpandoObject();
//            foreach (var item in anonymousDictionary)
//            {
//                expando.Add(item);
//            }
//            return (ExpandoObject)expando;
//        }

//        public static string ToJson<T>(this T item)
//        {
//            var settings = new JsonSerializerSettings
//            {
//                NullValueHandling = NullValueHandling.Ignore
//            };

//            return JsonConvert.SerializeObject(item, settings);
//        }

//        /// <summary>
//        /// <para>Serializes the specified System.Object and writes the XML document</para>
//        /// <para>to the specified file.</para>
//        /// </summary>
//        /// <typeparam name="T">This item's type</typeparam>
//        /// <param name="item">This item</param>
//        /// <param name="fileName">The file to which you want to write.</param>
//        /// <param name="removeNamespaces">
//        ///     <para>Specify whether to remove xml namespaces.</para>para>
//        ///     <para>If your object has any XmlInclude attributes, then set this to false</para>
//        /// </param>
//        /// <param name="omitXmlDeclaration"></param>
//        /// <returns>true if successful, otherwise false.</returns>
//        public static bool XmlSerialize<T>(this T item, string fileName, bool removeNamespaces = true, bool omitXmlDeclaration = true)
//        {
//            var locker = new object();

//            var xmlns = new XmlSerializerNamespaces();
//            xmlns.Add(string.Empty, string.Empty);

//            var xmlSerializer = new XmlSerializer(item.GetType());

//            var settings = new XmlWriterSettings
//            {
//                Indent = true,
//                OmitXmlDeclaration = omitXmlDeclaration
//            };

//            lock (locker)
//            {
//                using (var writer = XmlWriter.Create(fileName, settings))
//                {
//                    if (removeNamespaces)
//                    {
//                        xmlSerializer.Serialize(writer, item, xmlns);
//                    }
//                    else { xmlSerializer.Serialize(writer, item); }
//                }
//            }

//            return true;
//        }

//        /// <summary>
//        /// Serializes the specified System.Object and returns the serialized XML
//        /// </summary>
//        /// <typeparam name="T">This item's type</typeparam>
//        /// <param name="item">This item</param>
//        /// <param name="removeNamespaces">
//        ///     <para>Specify whether to remove xml namespaces.</para>para>
//        ///     <para>If your object has any XmlInclude attributes, then set this to false</para>
//        /// </param>
//        /// <param name="omitXmlDeclaration"></param>
//        /// <param name="encoding"></param>
//        /// <returns>Serialized XML for specified System.Object</returns>
//        public static string XmlSerialize<T>(this T item, bool removeNamespaces = true, bool omitXmlDeclaration = true, Encoding encoding = null)
//        {
//            var locker = new object();

//            var xmlns = new XmlSerializerNamespaces();
//            xmlns.Add(string.Empty, string.Empty);

//            var xmlSerializer = new XmlSerializer(item.GetType());

//            var settings = new XmlWriterSettings
//            {
//                Indent = true,
//                OmitXmlDeclaration = omitXmlDeclaration
//            };

//            lock (locker)
//            {
//                var stringBuilder = new StringBuilder();
//                using (var stringWriter = new CustomEncodingStringWriter(encoding, stringBuilder))
//                using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
//                {
//                    if (removeNamespaces)
//                    {
//                        xmlSerializer.Serialize(xmlWriter, item, xmlns);
//                    }
//                    else { xmlSerializer.Serialize(xmlWriter, item); }

//                    return stringBuilder.ToString();
//                }

//            }
//        }

//        #region Compute Hash

//        public static string ComputeMD5Hash(this object instance)
//        {
//            return ComputeHash(instance, new MD5CryptoServiceProvider());
//        }

//        public static string ComputeSHA1Hash(this object instance)
//        {
//            return ComputeHash(instance, new SHA1CryptoServiceProvider());
//        }

//        private static string ComputeHash<T>(object instance, T cryptoServiceProvider) where T : HashAlgorithm, new()
//        {
//            var serializer = new DataContractSerializer(instance.GetType());
//            using (var memoryStream = new MemoryStream())
//            {
//                serializer.WriteObject(memoryStream, instance);
//                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
//                return Convert.ToBase64String(cryptoServiceProvider.Hash);
//            }
//        }

//        #endregion Compute Hash

//        #region JSON

//        public static MvcHtmlString ToHtmlJson<T>(this IEnumerable<T> enumerable, Func<T, JToken> func)
//        {
//            var array = new JArray();
//            foreach (var item in enumerable)
//            {
//                array.Add(func(item));
//            }
//            return new MvcHtmlString(array.ToString(Newtonsoft.Json.Formatting.None));
//        }

//        #endregion JSON
//    }
//}