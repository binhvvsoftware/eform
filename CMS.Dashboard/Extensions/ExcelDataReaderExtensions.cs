﻿using System;
using ExcelDataReader;
using CMS.Dashboard.Common;

namespace CMS.Dashboard.Extensions
{
    public static class ExcelDataReaderExtensions
    {
        public class ExcelDataTableConfiguration
        {
            /// <summary>
            /// Gets or sets a value indicating the prefix of generated column names.
            /// </summary>
            public string EmptyColumnNamePrefix { get; set; } = "Column";

            /// <summary>
            /// Gets or sets a value indicating whether to use a row from the data as column names.
            /// </summary>
            public bool UseHeaderRow { get; set; } = true;

            /// <summary>
            /// Gets or sets a callback to determine which row is the header row. Only called when UseHeaderRow = true.
            /// </summary>
            public Action<IExcelDataReader> ReadHeaderRow { get; set; }
        }

        public static DataTable AsDataTable(this IExcelDataReader self)
        {
            var result = new DataTable();
            var configuration = new ExcelDataTableConfiguration();
            bool first = true;
            while (self.Read())
            {
                if (first)
                {
                    if (configuration.UseHeaderRow && configuration.ReadHeaderRow != null)
                    {
                        configuration.ReadHeaderRow(self);
                    }

                    for (var i = 0; i < self.FieldCount; i++)
                    {
                        var name = configuration.UseHeaderRow
                            ? Convert.ToString(self.GetValue(i))
                            : null;

                        if (string.IsNullOrEmpty(name))
                        {
                            name = configuration.EmptyColumnNamePrefix + i;
                        }

                        // if a column already exists with the name append _i to the duplicates
                        var columnName = GetUniqueColumnName(result, name);
                        var column = new DataColumn(columnName, typeof(object)) { Caption = name };
                        result.Columns.Add(column);
                    }

                    first = false;

                    if (configuration.UseHeaderRow)
                    {
                        continue;
                    }
                }

                var row = result.NewRow();

                for (var i = 0; i < self.FieldCount; i++)
                {
                    var value = self.GetValue(i);
                    row[i] = value;
                }

                result.Rows.Add(row);
            }

            return result;
        }

        private static string GetUniqueColumnName(DataTable table, string name)
        {
            var columnName = name;
            var i = 1;
            while (table.Columns[columnName] != null)
            {
                columnName = string.Format("{0}_{1}", name, i);
                i++;
            }

            return columnName;
        }
    }
}
