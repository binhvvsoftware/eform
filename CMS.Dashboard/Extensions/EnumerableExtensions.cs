﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Services;
using CMS.Services.Services;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMS.Dashboard.Extensions
{
    public static class EnumerableExtensions
    {
        public static SelectList ToSelectList(this IEnumerable<string> enumerable)
        {
            return enumerable.ToSelectList(x => x, x => x);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, object> valueFieldSelector, Func<T, string> textFieldSelector)
        {
            var values = from T item in enumerable
                select new
                {
                    ValueField = Convert.ToString(valueFieldSelector(item)),
                    TextField = textFieldSelector(item)
                };
            return new SelectList(values, "ValueField", "TextField");
        }

        public static async Task<SelectList> ToSelectListAsync<T>(this Task<IEnumerable<T>> enumerable, Func<T, object> valueFieldSelector, Func<T, string> textFieldSelector)
        {
            var values = from T item in await enumerable
                select new
                {
                    ValueField = Convert.ToString(valueFieldSelector(item)),
                    TextField = textFieldSelector(item)
                };
            return new SelectList(values, "ValueField", "TextField");
        }

        public static async Task<SelectList> ToSelectListAsync<T>(this Task<IEnumerable<T>> enumerable, Func<T, object> valueFieldSelector, Func<T, string> textFieldSelector, Func<T, string> groupFieldSelector)
        {
            var values = from T item in await enumerable
                select new
                {
                    ValueField = Convert.ToString(valueFieldSelector(item)),
                    TextField = textFieldSelector(item),
                    GroupField = groupFieldSelector(item)
                };
            return new SelectList(values, "ValueField", "TextField", "GroupField");
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> valueFieldSelector, Func<T, string> textFieldSelector, string emptyText)
        {
            var values = (from T item in enumerable
                select new
                {
                    ValueField = valueFieldSelector(item),
                    TextField = textFieldSelector(item)
                }).ToList();

            if (emptyText != null) // we don't check for empty, because empty string can be valid for emptyText value.
            {
                values.Insert(0, new { ValueField = string.Empty, TextField = emptyText });
            }

            return new SelectList(values, "ValueField", "TextField");
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> valueFieldSelector, Func<T, string> textFieldSelector, object selectedValue)
        {
            var values = from T item in enumerable
                select new
                {
                    ValueField = valueFieldSelector(item),
                    TextField = textFieldSelector(item)
                };
            return new SelectList(values, "ValueField", "TextField", selectedValue);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> valueFieldSelector, Func<T, string> textFieldSelector, object selectedValue, string emptyText)
        {
            var values = (from T item in enumerable
                select new
                {
                    ValueField = valueFieldSelector(item),
                    TextField = textFieldSelector(item)
                }).ToList();

            if (emptyText != null) // we don't check for empty, because empty string can be valid for emptyText value.
            {
                values.Insert(0, new { ValueField = string.Empty, TextField = emptyText });
            }
            return new SelectList(values, "ValueField", "TextField", selectedValue);
        }

        public static List<T> ToListOf<T>(this IEnumerable enumerable)
        {
            return (from object item in enumerable select item.ConvertTo<T>()).ToList();
        }

        public static T ConvertTo<T>(this object source)
        {
            return (T)Convert.ChangeType(source, typeof(T));
        }

        public static async Task<IEnumerable<TEntity>> GetRecords<TEntity, TKey>(this IGenericService<TEntity, TKey> service, int pageIndex, int pageSize)
        {
            return await service.GetRecordsAsync(null, new FindOptions<TEntity>
            {
                Skip = (pageIndex - 1) * pageSize,
                Limit = pageSize
            });
        }
    }
}
