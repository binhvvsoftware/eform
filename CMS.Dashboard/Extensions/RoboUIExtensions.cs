﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Linq;
using CMS.Dashboard.Web.UI.RoboUI;
using CMS.Dashboard.Web.UI.RoboUI.Filters;
using CMS.Services;
using CMS.Services.Services;
using CMS.Shared.Collections;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMS.Dashboard.Extensions
{
    public static class RoboUIExtensions
    {
        public static string ToHtmlString(this TagBuilder tagBuilder)
        {
            var stringWriter = new System.IO.StringWriter();
            tagBuilder.WriteTo(stringWriter, HtmlEncoder.Default);
            return stringWriter.ToString();
        }

        public static string ToHtmlString(this IHtmlContent htmlContent)
        {
            var stringWriter = new System.IO.StringWriter();
            htmlContent.WriteTo(stringWriter, HtmlEncoder.Default);
            return stringWriter.ToString();
        }

        public static async Task<IPagedList<TRecord>> GetRecords<TRecord, TKey>(this IGenericService<TRecord, TKey> service, RoboUIGridRequest request, Expression<Func<TRecord, bool>> predicate = null)
        {
            var findOptions = new FindOptions<TRecord>
            {
                Skip = (request.PageIndex - 1) * request.PageSize,
                Limit = request.PageSize
            };

            if (!request.Sorts.IsNullOrEmpty())
            {
                TranslateSortOptions(request.Sorts, findOptions);
            }
            else
            {
                service.ApplyDefaultSort(findOptions);
            }

            // Filtering
            var filterExpression = PredicateBuilder.True<TRecord>();

            if (!request.Filters.IsNullOrEmpty())
            {
                filterExpression = TranslateFilters<TRecord>(request.Filters);
            }

            if (predicate != null)
            {
                filterExpression = filterExpression.And(predicate);
            }

            filterExpression = filterExpression.Expand();

            var items = await service.GetRecordsAsync(filterExpression, findOptions);
            var count = await service.CountAsync(filterExpression);

            return new PagedList<TRecord>(items, request.PageIndex, request.PageSize, (int)count);
        }

        public static async Task<IPagedList<TRecord>> GetRecordsInclude<TRecord, TKey>(this IGenericService<TRecord, TKey> service, RoboUIGridRequest request, Expression<Func<TRecord, bool>> predicate = null, params Expression<Func<TRecord, dynamic>>[] includePaths)
        {
            var findOptions = new FindOptions<TRecord>
            {
                Skip = (request.PageIndex - 1) * request.PageSize,
                Limit = request.PageSize
            };

            if (!request.Sorts.IsNullOrEmpty())
            {
                TranslateSortOptions(request.Sorts, findOptions);
            }
            else
            {
                service.ApplyDefaultSort(findOptions);
            }

            // Filtering
            var filterExpression = PredicateBuilder.True<TRecord>();

            if (!request.Filters.IsNullOrEmpty())
            {
                filterExpression = TranslateFilters<TRecord>(request.Filters);
            }

            if (predicate != null)
            {
                filterExpression = filterExpression.And(predicate);
            }

            filterExpression = filterExpression.Expand();

            var items = new List<TRecord>();
            var count = await service.CountAsync(filterExpression);
            if (count > 0)
                items.AddRange(await service.GetRecordsAsync(filterExpression, findOptions, includePaths));

            return new PagedList<TRecord>(items, request.PageIndex, request.PageSize, (int)count);
        }

        public static async Task<IEnumerable<TRecord>> GetRecords<TRecord, TKey>(this IGenericService<TRecord, TKey> service, RoboUIGridRequest request, Expression<Func<TRecord, bool>> predicate = null, params Expression<Func<TRecord, dynamic>>[] includePaths)
        {
            var findOptions = new FindOptions<TRecord>
            {
                Skip = (request.PageIndex - 1) * request.PageSize,
                Limit = request.PageSize
            };

            if (!request.Sorts.IsNullOrEmpty())
            {
                TranslateSortOptions(request.Sorts, findOptions);
            }
            else
            {
                service.ApplyDefaultSort(findOptions);
            }

            // Filtering
            var filterExpression = PredicateBuilder.True<TRecord>();

            if (!request.Filters.IsNullOrEmpty())
            {
                filterExpression = TranslateFilters<TRecord>(request.Filters);
            }

            if (predicate != null)
            {
                filterExpression = filterExpression.And(predicate);
            }

            filterExpression = filterExpression.Expand();

            return await service.GetRecordsAsync(filterExpression, findOptions, includePaths);
        }

        private static Expression<Func<TRecord, bool>> TranslateFilters<TRecord>(IList<IFilterDescriptor> filters)
        {
            var expressionBuilder = new FilterDescriptorCollectionExpressionBuilder(Expression.Parameter(typeof(TRecord), "item"), filters);
            expressionBuilder.Options.LiftMemberAccessToNull = false;
            var filterExpression = expressionBuilder.CreateFilterExpression<Func<TRecord, bool>>();
            return filterExpression;
        }

        private static void TranslateSortOptions<TRecord>(IList<SortDescriptor> sorts, FindOptions<TRecord> findOptions)
        {
            foreach (var sortDescriptor in sorts)
            {
                var parameter = Expression.Parameter(typeof(TRecord));
                var memberExpression = Expression.Property(parameter, typeof(TRecord).GetProperty(sortDescriptor.Member));

                if (memberExpression.Type.GetTypeInfo().IsEnum)
                {
                    var delegateType = typeof(Func<,>).MakeGenericType(typeof(TRecord), typeof(int));
                    var body = Expression.Convert(memberExpression, typeof(int));

                    findOptions.Sorts.Add(Expression.Lambda(delegateType, body, parameter), sortDescriptor.SortDirection);
                }
                else
                {
                    Type type = memberExpression.Type;
                    bool isNullable = type.IsNullable();

                    if (isNullable)
                    {
                        type = Nullable.GetUnderlyingType(type);
                    }

                    switch (Type.GetTypeCode(type))
                    {
                        case TypeCode.String:
                            findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, string>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            break;

                        case TypeCode.Boolean:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, bool?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, bool>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Byte:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, byte?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, byte>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Int16:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, short?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, short>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Int32:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, int?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, int>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Int64:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, long?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, long>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Single:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, float?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, float>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Decimal:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, decimal?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, decimal>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        case TypeCode.Double:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, double?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, double>>(memberExpression, parameter), sortDescriptor.SortDirection);

                            }
                            break;

                        case TypeCode.DateTime:
                            if (isNullable)
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, DateTime?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, DateTime>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            break;

                        default:
                            if (memberExpression.Type == typeof(Guid))
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, Guid>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else if (type == typeof(Guid))
                            {
                                findOptions.Sorts.Add(Expression.Lambda<Func<TRecord, Guid?>>(memberExpression, parameter), sortDescriptor.SortDirection);
                            }
                            else
                            {
                                var defaultLambdaExpression = Expression.Lambda<Func<TRecord, object>>(memberExpression, parameter);
                                findOptions.Sorts.Add(defaultLambdaExpression, sortDescriptor.SortDirection);
                            }
                            break;
                    }
                }
            }
        }
    }
}
