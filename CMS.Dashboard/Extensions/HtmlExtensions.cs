﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CMS.Dashboard.Web.UI.RoboUI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace CMS.Dashboard.Extensions
{
    public static class HtmlExtensions
    {
        public static WorkContext GetWorkContext(this ControllerContext controllerContext)
        {
            return controllerContext.HttpContext.RequestServices.GetService<WorkContext>();
        }

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            if (request.Headers != null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }

        public static RouteValueDictionary Merge(this RouteValueDictionary obj, object values, params string[] removeKeys)
        {
            var mergeValues = new RouteValueDictionary(values);

            var result = new RouteValueDictionary(obj);
            foreach (var value in mergeValues)
            {
                result[value.Key.Replace("_", "-")] = value.Value;
            }

            if (removeKeys != null && removeKeys.Length > 0)
            {
                foreach (var key in removeKeys.Where(result.ContainsKey))
                {
                    result.Remove(key);
                }
            }

            return result;
        }

        public static HelperResult AddInlineScript<TModel>(this IHtmlHelper<TModel> html, Func<object, HelperResult> template)
        {
            return new HelperResult(async writer =>
            {
                var sb = new StringBuilder();
                using (var stringWriter = new StringWriter(sb))
                {
                    template(null).WriteTo(stringWriter, HtmlEncoder.Default);
                    if (sb.Length > 0)
                    {
                        var workContext = (WorkContext)html.ViewContext.HttpContext.RequestServices.GetService(typeof(WorkContext));
                        sb.Replace("<script>", string.Empty);
                        sb.Replace("</script>", string.Empty);
                        workContext.AddInlineScript(sb.ToString());
                    }
                }

                await Task.CompletedTask;
            });
        }

        public static RoboUIGridResult<TModel> RoboGrid<TModel>(this IHtmlHelper htmlHelper, TModel model) where TModel : class
        {
            var controllerContext = new ControllerContext
            {
                HttpContext = htmlHelper.ViewContext.HttpContext
            };

            return new RoboUIGridResult<TModel>(controllerContext);
        }
    }
}
