﻿using System;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using Microsoft.Extensions.Primitives;

namespace CMS.Dashboard.Web
{
    public class FormButtonAttribute : ActionMethodSelectorAttribute
    {
        public FormButtonAttribute(string buttonName)
        {
            ButtonName = buttonName;
            BindValue = "id";
        }

        public string ButtonName { get; set; }

        public string BindValue { get; set; }

        public override bool IsValidForRequest(RouteContext routeContext, ActionDescriptor action)
        {
            if (routeContext.HttpContext.Request.Method != "POST")
            {
                return false;
            }

            StringValues valueProviderResult;
            try
            {
                valueProviderResult = routeContext.HttpContext.Request.Form[ButtonName];
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("Button name: " + ButtonName);
            }

            bool hasValue = valueProviderResult.Count > 0;

            if (hasValue && !string.IsNullOrWhiteSpace(BindValue))
            {
                var routeValues = new Dictionary<string, string>(action.RouteValues);
                routeValues[BindValue] = valueProviderResult[0];
                action.RouteValues = routeValues;
            }

            return hasValue;
        }
    }
}
