﻿using System;

namespace CMS.Dashboard.Web.UI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ThemedAttribute : Attribute
    {
        public bool Minimal { get; set; }

        public ThemedAttribute(bool isFullLayout)
        {
            Minimal = !isFullLayout;
        }
    }
}
