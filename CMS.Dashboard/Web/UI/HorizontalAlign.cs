﻿namespace CMS.Dashboard.Web.UI
{
    public enum HorizontalAlign
    {
        NotSet,
        Left,
        Center,
        Right
    }
}
