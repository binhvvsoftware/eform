﻿namespace CMS.Dashboard.Web.UI.Notify
{
    public enum NotifyType
    {
        Info,
        Warning,
        Error
    }
}
