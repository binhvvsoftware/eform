﻿using System.Collections.Generic;

namespace CMS.Dashboard.Web.UI.Notify
{
    public class Notifier : INotifier
    {
        private readonly IList<NotifyEntry> entries;

        public Notifier()
        {
            entries = new List<NotifyEntry>();
        }

        public void Add(NotifyType type, string message)
        {
            entries.Add(new NotifyEntry { Type = type, Message = message });
        }

        public IEnumerable<NotifyEntry> List()
        {
            return entries;
        }
    }
}
