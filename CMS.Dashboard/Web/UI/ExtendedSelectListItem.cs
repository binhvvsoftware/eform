﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMS.Dashboard.Web.UI
{
    public class ExtendedSelectListItem : SelectListItem
    {
        public object HtmlAttributes { get; set; }

        public string GroupKey { get; set; }

        public string GroupName { get; set; }
    }
}
