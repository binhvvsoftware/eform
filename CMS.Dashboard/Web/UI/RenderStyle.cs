﻿namespace CMS.Dashboard.Web.UI
{
    internal struct RenderStyle
    {
        public string name;

        public string @value;

        public HtmlTextWriterStyle key;
    }
}
