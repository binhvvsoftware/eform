﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CMS.Dashboard.Web.UI.RoboUI.Filters;

namespace CMS.Dashboard.Web.UI.RoboUI.Expressions
{
    internal class IndexerToken : IMemberAccessToken
    {
        private readonly ReadOnlyCollection<object> arguments;

        public IndexerToken(IEnumerable<object> arguments)
        {
            this.arguments = new ReadOnlyCollection<object>(arguments.ToArray());
        }

        public IndexerToken(params object[] arguments)
            : this((IEnumerable<object>)arguments)
        {
        }

        public ReadOnlyCollection<object> Arguments => arguments;
    }
}
