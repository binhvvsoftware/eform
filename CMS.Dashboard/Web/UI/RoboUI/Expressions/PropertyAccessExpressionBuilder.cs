﻿using System;
using System.Linq.Expressions;
using CMS.Dashboard.Web.UI.RoboUI.Filters;

namespace CMS.Dashboard.Web.UI.RoboUI.Expressions
{
    internal class PropertyAccessExpressionBuilder : MemberAccessExpressionBuilderBase
    {
        public PropertyAccessExpressionBuilder(Type itemType, string memberName)
            : base(itemType, memberName)
        {
        }

        public override Expression CreateMemberAccessExpression()
        {
            if (string.IsNullOrEmpty(MemberName))
                return ParameterExpression;
            return ExpressionFactory.MakeMemberAccess(ParameterExpression, MemberName, Options.LiftMemberAccessToNull);
        }
    }
}
