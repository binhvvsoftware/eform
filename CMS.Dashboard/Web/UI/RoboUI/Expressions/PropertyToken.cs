﻿using CMS.Dashboard.Web.UI.RoboUI.Filters;

namespace CMS.Dashboard.Web.UI.RoboUI.Expressions
{
    internal class PropertyToken : IMemberAccessToken
    {
        private readonly string propertyName;

        public PropertyToken(string propertyName)
        {
            this.propertyName = propertyName;
        }

        public string PropertyName => propertyName;
    }
}
