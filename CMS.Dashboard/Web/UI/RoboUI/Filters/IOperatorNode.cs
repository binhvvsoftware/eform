﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public interface IOperatorNode
    {
        FilterOperator FilterOperator { get; }
    }
}
