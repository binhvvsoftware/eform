﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public class PropertyNode : IFilterNode
    {
        public string Name { get; set; }

        public void Accept(IFilterNodeVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
