﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public class FilterToken
    {
        public FilterTokenType TokenType { get; set; }

        public string Value { get; set; }
    }
}
