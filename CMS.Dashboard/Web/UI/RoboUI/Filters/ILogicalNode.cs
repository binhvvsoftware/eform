﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public interface ILogicalNode
    {
        FilterCompositionLogicalOperator LogicalOperator { get; }
    }
}
