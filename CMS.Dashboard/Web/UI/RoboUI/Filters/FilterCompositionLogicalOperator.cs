﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    /// <summary>
    /// Logical operator used for filter descriptor composition.
    ///
    /// </summary>
    public enum FilterCompositionLogicalOperator
    {
        And,
        Or,
    }
}
