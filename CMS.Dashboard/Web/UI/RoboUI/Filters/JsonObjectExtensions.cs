﻿using System.Collections.Generic;
using System.Linq;

namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public static class JsonObjectExtensions
    {
        public static IEnumerable<IDictionary<string, object>> ToJson(IEnumerable<JsonObject> items)
        {
            return items.Select(i => i.ToJson());
        }
    }
}
