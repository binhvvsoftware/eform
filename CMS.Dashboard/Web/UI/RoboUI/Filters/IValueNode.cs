﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public interface IValueNode
    {
        object Value { get; }
    }
}
