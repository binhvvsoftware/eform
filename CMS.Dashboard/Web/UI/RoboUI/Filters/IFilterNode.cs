﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    public interface IFilterNode
    {
        void Accept(IFilterNodeVisitor visitor);
    }
}
