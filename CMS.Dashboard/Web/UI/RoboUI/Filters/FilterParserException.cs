﻿using System;

namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    [Serializable]
    public class FilterParserException : Exception
    {
        public FilterParserException()
        {
        }

        public FilterParserException(string message)
            : base(message)
        {
        }

        public FilterParserException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
