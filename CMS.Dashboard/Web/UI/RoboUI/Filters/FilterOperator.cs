﻿namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    /// <summary>
    /// Operator used in <see cref="T:CMS.Dashboard.Web.UI.RoboUI.Filters.FilterDescriptor"/>
    /// </summary>
    public enum FilterOperator
    {
        IsLessThan,
        IsLessThanOrEqualTo,
        IsEqualTo,
        IsNotEqualTo,
        IsGreaterThanOrEqualTo,
        IsGreaterThan,
        StartsWith,
        EndsWith,
        Contains,
        IsContainedIn,
        IsContainedInList,
        DoesNotContain
    }
}
