﻿using System.Collections.ObjectModel;

namespace CMS.Dashboard.Web.UI.RoboUI.Filters
{
    /// <summary>
    ///     Represents collection of <see cref="T:CMS.Dashboard.Web.UI.RoboUI.Filters.IFilterDescriptor" />.
    /// </summary>
    public class FilterDescriptorCollection : Collection<IFilterDescriptor>
    {
    }
}
