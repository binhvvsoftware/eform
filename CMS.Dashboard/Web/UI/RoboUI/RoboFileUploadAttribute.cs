﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboFileUploadAttribute : RoboControlAttribute
    {
        public bool EnableUploader { get; set; }

        public string AllowedExtensions { get; set; }

        public bool ShowThumbnail { get; set; }

        public string UploadFolder { get; set; }

        public bool EnableBrowse { get; set; }
    }

    public class RoboFileUploadOptions
    {
        public string UploadUrl { get; set; }

        public string AllowedExtensions { get; set; }

        public int SizeLimit { get; set; }

        public string UploadFolder { get; set; }
    }
}
