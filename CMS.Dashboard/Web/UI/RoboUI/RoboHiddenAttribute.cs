﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboHiddenAttribute : RoboControlAttribute
    {
        public override bool HasLabelControl => false;
    }
}
