﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboColorPickerAttribute : RoboChoiceAttribute
    {
        public RoboColorPickerAttribute()
            : base(RoboChoiceType.DropDownList)
        {
        }

        /// <summary>
        /// Show the colors inside a picker instead of inline, default: False
        /// </summary>
        public bool Picker { get; set; }

        /// <summary>
        /// Font to use for the ok/check mark, default: '', available themes: regularfont, fontawesome, glyphicons
        /// </summary>
        public string Theme { get; set; }
    }
}
