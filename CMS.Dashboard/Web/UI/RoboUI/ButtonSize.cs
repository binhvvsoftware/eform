﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public enum ButtonSize : byte
    {
        Default,
        Large,
        Small,
        ExtraSmall,
    }
}
