﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboCascadingCheckBoxListAttribute : RoboControlAttribute
    {
        public string ParentControl { get; set; }
    }
}
