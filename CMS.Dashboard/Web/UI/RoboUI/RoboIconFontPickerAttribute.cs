﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboIconFontPickerAttribute : RoboControlAttribute
    {
        public RoboIconFontPickerAttribute()
        {
            IconSet = "glyphicon";
            Placement = "right";
            Rows = 3;
            Columns = 6;
        }

        /// <summary>
        /// The icon set, glyphicon or fontawesome
        /// </summary>
        public string IconSet { get; set; }

        public string Placement { get; set; }

        public int Rows { get; set; }

        public int Columns { get; set; }
    }
}
