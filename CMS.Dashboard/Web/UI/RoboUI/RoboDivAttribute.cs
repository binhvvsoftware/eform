﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboDivAttribute : RoboControlAttribute
    {
        public bool RenderContentOnly { get; set; }
    }
}
