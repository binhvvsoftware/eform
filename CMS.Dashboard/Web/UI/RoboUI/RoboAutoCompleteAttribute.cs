﻿using System;

namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboAutoCompleteAttribute : RoboControlAttribute
    {
        public RoboAutoCompleteAttribute()
        {
            MinLength = 2;
        }

        public int MinLength { get; set; }

        public bool MustMatch { get; set; }

        public string OnSuccess { get; set; }

        public RoboAutoCompleteOptions Options { get; set; }
    }

    public class RoboAutoCompleteOptions
    {
        public string SourceUrl { get; set; }

        public virtual bool HasTextSelector => false;

        public virtual string GetText(object model)
        {
            return null;
        }
    }

    public class RoboAutoCompleteOptions<TModel> : RoboAutoCompleteOptions
    {
        public override bool HasTextSelector => TextSelector != null;

        public Func<TModel, string> TextSelector { get; set; }

        public override string GetText(object model)
        {
            return TextSelector != null ? TextSelector((TModel)model) : null;
        }
    }
}
