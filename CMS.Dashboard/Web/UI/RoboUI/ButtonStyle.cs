﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public enum ButtonStyle : byte
    {
        Default,
        Primary,
        Info,
        Success,
        Warning,
        Danger,
        Inverse,
        Link,
    }
}
