﻿namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboSlugAttribute : RoboControlAttribute
    {
        public int MaxLength { get; set; }
    }
}
