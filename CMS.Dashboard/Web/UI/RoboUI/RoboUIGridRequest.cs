﻿using System.Collections.Generic;
using CMS.Core.Data;
using CMS.Dashboard.Web.UI.RoboUI.Filters;

namespace CMS.Dashboard.Web.UI.RoboUI
{
    public class RoboUIGridRequest
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public IList<SortDescriptor> Sorts { get; set; }

        public IList<IFilterDescriptor> Filters { get; set; }
    }
}
