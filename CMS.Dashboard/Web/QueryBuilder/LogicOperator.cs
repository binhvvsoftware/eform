﻿namespace CMS.Dashboard.Web.QueryBuilder
{
    /// <summary>
    /// Represents logic operators for chaining WHERE and HAVING clauses together in a statement
    /// </summary>
    public enum LogicOperator : byte
    {
        And,
        Or
    }
}
