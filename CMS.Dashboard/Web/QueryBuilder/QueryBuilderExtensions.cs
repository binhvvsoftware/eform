﻿namespace CMS.Dashboard.Web.QueryBuilder
{
    public static class QueryBuilderExtensions
    {
        public static ISelectQueryBuilder JoinIf(this ISelectQueryBuilder queryBuilder, bool condition, JoinType joinType, string toTableName, string toColumnName, ComparisonOperator comparisonOperator, string fromTableName, string fromColumnName)
        {
            if (condition)
            {
                return queryBuilder.Join(joinType, toTableName, toColumnName, comparisonOperator, fromTableName, fromColumnName);
            }

            return queryBuilder;
        }
    }
}
