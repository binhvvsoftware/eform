﻿using System.ComponentModel.DataAnnotations;

namespace CMS.Dashboard.Web.QueryBuilder
{
    /// <summary>
    /// Represents comparison operators for WHERE, HAVING and JOIN clauses
    /// </summary>
    public enum ComparisonOperator : byte
    {
        [Display(Name = "=")]
        EqualTo = 0,

        [Display(Name = "<>")]
        NotEqualTo = 1,

        [Display(Name = "Like")]
        Like = 2,

        [Display(Name = "Not Like")]
        NotLike = 3,

        [Display(Name = ">")]
        GreaterThan = 4,

        [Display(Name = ">=")]
        GreaterThanOrEqualTo = 5,

        [Display(Name = "<")]
        LessThan = 6,

        [Display(Name = "<=")]
        LessThanOrEqualTo = 7,

        [Display(Name = "In")]
        In = 8,

        [Display(Name = "Contains")]
        Contains = 9,

        [Display(Name = "Not Contains")]
        NotContains = 10,

        [Display(Name = "Starts With")]
        StartsWith = 11,

        [Display(Name = "Ends With")]
        EndsWith = 12,
    }
}
