﻿using CMS.Core.Data;

namespace CMS.Dashboard.Web.QueryBuilder
{
    /// <summary>
    /// Represents a ORDER BY clause to be used with SELECT statements
    /// </summary>
    public struct OrderByClause
    {
        public string FieldName;
        public SortDirection SortDirection;

        public OrderByClause(string field)
        {
            FieldName = field;
            SortDirection = SortDirection.Ascending;
        }

        public OrderByClause(string field, SortDirection order)
        {
            FieldName = field;
            SortDirection = order;
        }
    }
}
