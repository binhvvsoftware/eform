﻿using System.Collections.Generic;

namespace CMS.Dashboard.Web.QueryBuilder
{
    public class WhereStatement
    {
        public WhereStatement()
        {
            this.Clauses = new List<WhereClause>();
        }

        public ICollection<WhereClause> Clauses { get; private set; }

        public WhereStatement AddClause(WhereClause clause)
        {
            Clauses.Add(clause);
            return this;
        }
    }
}
