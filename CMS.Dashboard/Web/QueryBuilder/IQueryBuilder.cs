﻿namespace CMS.Dashboard.Web.QueryBuilder
{
    public interface IQueryBuilder
    {
        string BuildQuery();
    }
}
