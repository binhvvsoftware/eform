﻿namespace CMS.Dashboard.Web.QueryBuilder
{
    /// <summary>
    /// Represents operators for JOIN clauses
    /// </summary>
    public enum JoinType : byte
    {
        InnerJoin,
        OuterJoin,
        LeftJoin,
        RightJoin
    }
}
