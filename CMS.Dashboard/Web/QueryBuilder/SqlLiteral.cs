﻿namespace CMS.Dashboard.Web.QueryBuilder
{
    public class SqlLiteral
    {
        public static string StatementRowsAffected = "SELECT @@ROWCOUNT";

        private string _value;

        public string Value
        {
            get => _value;
            set => _value = value;
        }

        public SqlLiteral(string value)
        {
            _value = value;
        }
    }
}
