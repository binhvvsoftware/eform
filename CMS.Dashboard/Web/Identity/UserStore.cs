﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CMS.Dashboard.Web.Identity
{
    public class UserStore<TUser, TRole, TContext, TKey> : Microsoft.AspNetCore.Identity.EntityFrameworkCore.UserStore<TUser, TRole, TContext, TKey> where TUser : IdentityUser<TKey> where TRole : IdentityRole<TKey> where TContext : DbContext where TKey : IEquatable<TKey>
    {
        public UserStore(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        public override async Task<IList<Claim>> GetClaimsAsync(TUser user, CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.GetClaimsAsync(user, cancellationToken);
        }
    }
}
