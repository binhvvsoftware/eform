﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CMS.Dashboard.Web.Identity
{
    public class RoleStore<TRole, TContext, TKey> : Microsoft.AspNetCore.Identity.EntityFrameworkCore.RoleStore<TRole, TContext, TKey> where TRole : IdentityRole<TKey> where TContext : DbContext where TKey : IEquatable<TKey>
    {
        public RoleStore(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
