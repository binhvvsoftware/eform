﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CMS.Dashboard.Web
{
    public class DateTimeModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.UnderlyingOrModelType == typeof(DateTime))
            {
                return new DateTimeModelBinder();
            }

            return null;
        }

        public class DateTimeModelBinder : IModelBinder
        {
            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                if (bindingContext == null)
                {
                    throw new ArgumentNullException(nameof(bindingContext));
                }

                var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                
                if (!string.IsNullOrEmpty(valueProviderResult.FirstValue))
                {
                    var dateFormat = bindingContext.ValueProvider.GetValue(".DateFormat");
                    var altDateFormatValueProvider = bindingContext.ValueProvider.GetValue(".AltDateFormat");
                    if (!string.IsNullOrEmpty(dateFormat.FirstValue))
                    {
                        var altDateFormat = string.IsNullOrEmpty(altDateFormatValueProvider.FirstValue) ? dateFormat.FirstValue : altDateFormatValueProvider.FirstValue;
                        if (DateTime.TryParseExact(valueProviderResult.FirstValue, new[] { dateFormat.FirstValue, altDateFormat }, null, DateTimeStyles.None, out var dt))
                        {
                            bindingContext.Result = ModelBindingResult.Success(dt);
                        }
                    }
                    else
                    {
                        var dt = DateTime.Parse(valueProviderResult.FirstValue);
                        bindingContext.Result = ModelBindingResult.Success(dt);
                    }
                }

                return Task.CompletedTask;
            }
        }
    }
}
