﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Authorization;

namespace CMS.Dashboard.Web.TagHelpers
{
    [HtmlTargetElement(Attributes = "asp-permission")]
    public class PermissionTagHelper : TagHelper
    {
        private readonly IAuthorizationService authorizationService;
        private readonly IHttpContextAccessor httpContextAccessor;

        public PermissionTagHelper(IAuthorizationService authorizationService, IHttpContextAccessor httpContextAccessor)
        {
            this.authorizationService = authorizationService;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HtmlAttributeName("asp-permission")]
        public string Permission { get; set; }

        [HtmlAttributeName("asp-permission-disable-mode")]
        public bool DisableModel { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.RemoveAll("asp-permission");
            output.Attributes.RemoveAll("asp-permission-disable-mode");

            if (!string.IsNullOrEmpty(Permission))
            {
                var result = await authorizationService.AuthorizeAsync(httpContextAccessor.HttpContext.User, Permission);
                if (!result.Succeeded)
                {
                    if (DisableModel)
                    {
                        switch (output.TagName)
                        {
                            case "a":
                                output.Attributes.RemoveAll("href");
                                output.Attributes.RemoveAll("data-toggle");
                                output.Attributes.Add("href", "javascript:void(0)");
                                break;
                        }
                    }

                    output.SuppressOutput();
                }
            }
        }
    }
}
