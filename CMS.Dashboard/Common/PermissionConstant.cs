﻿namespace CMS.Dashboard.Common
{
    public static class PermissionConstant
    {
        public static readonly string BookingManagement = "Booking Management";
        public static readonly string ManageBooking = "Administrators";
        public static readonly string FullAccess = "Administrators";
        public static readonly string ManageMarkup = "Booking Management";
        public static readonly string ManageLanguage = "Manage Languages";
        public static readonly string ClientAdministration = "Client Administration";
        public static readonly string ImportExportSettings = "Import Export Settings";
        public static readonly string ViewCurrency = "View Currency";
        public static readonly string ViewPayment = "View Payment";
        public static readonly string ViewPromotions = "View Promotions";
        public static readonly string ViewBooking = "View Booking";
        public static readonly string ClientAgent = "Client Agent";
        public static readonly string ClientFlightAdministration = "Client Flight Administration";
        public static readonly string ClientHotelAdministration = "Client Hotel Administration";
        public static readonly string ClientTourAdministration = "Client Tour Administration";
        public static readonly string ClientTransferAdministration = "Client Transfer Administration";
    }
}