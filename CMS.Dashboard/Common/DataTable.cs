﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CMS.Dashboard.Common
{
    public class DataColumn
    {
        public DataColumn(string columnName)
        {
            ColumnName = columnName;
        }

        public DataColumn(string columnName, Type type)
        {
            ColumnName = columnName;
            ColumnType = type;
        }

        public string Caption { get; set; }

        public string ColumnName { get; set; }

        public Type ColumnType { get; }
    }

    public class DataColumnCollection : IEnumerable<DataColumn>
    {
        private readonly List<DataColumn> columns = new List<DataColumn>();

        public DataColumn this[string name]
        {
            get
            {
                var column = columns.FirstOrDefault(x => x.ColumnName == name);
                return column;
            }
        }

        public DataColumn this[int index]
        {
            get
            {
                var column = columns.ElementAt(index);
                return column;
            }
        }

        public int Count => columns.Count;

        public IEnumerator<DataColumn> GetEnumerator()
        {
            return columns.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(DataColumn column)
        {
            columns.Add(column);
        }
    }

    public class DataRow
    {
        private readonly string[] columns;
        private readonly object[] data;

        public DataRow(int size, string[] columns)
        {
            this.columns = columns;
            data = new object[size];
        }

        public object this[int i]
        {
            get => data[i];
            set => data[i] = value;
        }

        public object this[string name]
        {
            get
            {
                var index = Array.IndexOf(columns, name);
                if (index > -1)
                {
                    return data[index];
                }
                throw new ArgumentException("Column " + name + " doesn't exist.");
            }
            set
            {
                var index = Array.IndexOf(columns, name);
                if (index > -1)
                {
                    data[index] = value;
                }
                else
                {
                    throw new ArgumentException("Column " + name + " doesn't exist.");
                }
            }
        }

        public bool AreAllColumnsEmpty()
        {
            return data.All(x => x == null);
        }
    }

    public class DataTable
    {
        public DataTable()
        {
            Columns = new DataColumnCollection();
            Rows = new List<DataRow>();
        }

        public DataColumnCollection Columns { get; set; }

        public List<DataRow> Rows { get; set; }

        public DataRow NewRow()
        {
            var row = new DataRow(Columns.Count, Columns.Select(x => x.ColumnName).ToArray());
            return row;
        }
    }

    public static class ArrayToPivotTable
    {
        public static string DataTableToCsv(this DataTable datatable, char seperator)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < datatable.Columns.Count; i++)
            {
                sb.Append(datatable.Columns[i].ColumnName);
                if (i < datatable.Columns.Count - 1)
                    sb.Append(seperator);
            }
            sb.AppendLine();
            foreach (DataRow dr in datatable.Rows)
            {
                for (int i = 0; i < datatable.Columns.Count; i++)
                {
                    sb.Append(dr[i]);

                    if (i < datatable.Columns.Count - 1)
                        sb.Append(seperator);
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public static DataTable ToPivotTable<T, TColumn, TRow, TData>(
            this IEnumerable<T> source,
            Func<T, TColumn> columnSelector,
            Expression<Func<T, TRow>> rowSelector,
            Func<IEnumerable<T>, TData> dataSelector)
        {
            DataTable table = new DataTable();
            var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
            table.Columns.Add(new DataColumn(rowName));
            var columns = source.Select(columnSelector).Distinct();

            foreach (var column in columns)
                table.Columns.Add(new DataColumn(column.ToString()));
            table.Columns[0].ColumnName = "Key";
            var rows = source.GroupBy(rowSelector.Compile())
                .Select(rowGroup => new
                {
                    rowGroup.Key,
                    Values = columns.GroupJoin(
                        rowGroup,
                        c => c,
                        columnSelector,
                        (c, columnGroup) => dataSelector(columnGroup))
                });

            foreach (var row in rows)
            {
                var dataRow = table.NewRow();
                var items = row.Values.Cast<object>().ToList();
                items.Insert(0, row.Key);
                for (int i = 0; i < items.Count; i++)
                {
                    dataRow[i] = items[i];
                }
                table.Rows.Add(dataRow);
            }

            return table;
        }
    }
}
