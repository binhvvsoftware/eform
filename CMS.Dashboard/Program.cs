﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace CMS.Dashboard
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true)
                .AddJsonFile("/configmaps/appsettings.json", optional: true)
                .Build();

            var loggerConfiguration = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext();

            Log.Logger = loggerConfiguration.CreateLogger();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json", true)
                .Build();

            return WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .ConfigureAppConfiguration((hostBuilder, builder) =>
                {
                    builder.AddJsonFile("appsettings.json", optional: true);
                    builder.AddJsonFile("/configmaps/appsettings.json", optional: true);
                    builder.AddEnvironmentVariables();
                })
                .UseStartup<Startup>()
                .UseSerilog();
        }
    }
}
