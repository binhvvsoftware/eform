﻿using Autofac;
using CMS.Services.Services;
using CMS.Services.Templates;
using Module = Autofac.Module;

namespace CMS.Dashboard
{
    public class DashboardModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CampusService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ParentDepartmentService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<UserService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RoleService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<MenuService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<VersionHistoryService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LogService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DepartmentService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<UserRoleService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<MemberRelationService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SmtpConfigService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<EmailTemplateService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<EmailService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<FormTemplateService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RequestDocumentService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TemplateParser>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorkflowService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorkflowStepService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorkingTimeSheetService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RequestDocumentAssigneeService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LeaveDocumentService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorkingLateService>().AsImplementedInterfaces().SingleInstance();

        }
    }
}
