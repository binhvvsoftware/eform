﻿using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using IdentityRole = CMS.Shared.Data.Domain.IdentityRole;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;

namespace CMS.EformMigrator
{
    public class EformMigrateDbContext : DbContext
    {
        public EformMigrateDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<IdentityRole> IdentityRoles { get; set; }

        public DbSet<IdentityUserLogin> IdentityUserLogins { get; set; }

        public DbSet<IdentityUserRole> IdentityUserRoles { get; set; }

        public DbSet<IdentityUserClaim> IdentityUserClaims { get; set; }

        public DbSet<IdentityRoleClaim> IdentityRoleClaims { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<VersionHistory> VersionHistorys { get; set; }

        public DbSet<EntityVersion> EntityVersions { get; set; }

        public DbSet<SmtpConfig> SmtpConfigs { get; set; }

        public DbSet<EmailTemplate> EmailTemplates { get; set; }

        public DbSet<FormTemplate> FormTemplates { get; set; }

        public DbSet<RequestDocument> RequestDocuments { get; set; }

        public DbSet<Workflow> Workflows { get; set; }

        public DbSet<WorkflowStep> WorkflowSteps { get; set; }

        public DbSet<RequestDocumentAssignee> RequestDocumentAssignees { get; set; }

        public DbSet<WorkingTimesheet> WorkingTimesheets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)

        {
            modelBuilder.Entity<IdentityUser>()
                .ToTable("Cms_IdentityUsers")
                .HasKey(x => new
                {
                    x.Id
                });

            modelBuilder.Entity<Menu>()
                .ToTable("Cms_Menus")
                .HasKey(x => new
                {
                    x.Id
                });

            modelBuilder.Entity<ParentDepartment>()
                .ToTable("Cms_ParentDepartment")
                .HasKey(x => new
                {
                    x.Id
                });

            modelBuilder.Entity<Campus>()
                .ToTable("Cms_Campus")
                .HasKey(x => new
                {
                    x.Id
                });

            modelBuilder.Entity<Department>()
                .ToTable("Cms_Department")
                .HasKey(x => new
                {
                    x.Id
                });

            modelBuilder.Entity<MemberRelation>()
                .ToTable("Cms_MemberRelation")
                .HasKey(x => new
                {
                    x.Id
                });

            //Dump data

            #region Users

            var passwordHasher = new PasswordHasher<IdentityUser>();
            modelBuilder.Entity<IdentityUser>().HasData(
                new IdentityUser
                {
                    Id = 1,
                    Email = "nguyenquangduy0310@gmail.com",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "DuyNQ",
                    NormalizedUserName = "DuyNQ".ToUpper(),
                    NormalizedEmail = "nguyenquangduy0310@gmail.com".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 1,
                    FullName = "Nguyễn Quang Duy",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV01"
                },
                new IdentityUser
                {
                    Id = 2,
                    Email = "vuthanh20286@gmail.com",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "ThanhVX",
                    NormalizedUserName = "ThanhVX".ToUpper(),
                    NormalizedEmail = "vuthanh20286@gmail.com".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Vũ Xuân Thành",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV02"
                },
                new IdentityUser
                {
                    Id = 3,
                    Email = "jymyct@gmail.com",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "DongNP",
                    NormalizedUserName = "DongNP".ToUpper(),
                    NormalizedEmail = "jymyct@gmail.com".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Nguyễn Phương Đông",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV03"
                },
                new IdentityUser
                {
                    Id = 4,
                    Email = "hungh@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "HungH",
                    NormalizedUserName = "HungH".ToUpper(),
                    NormalizedEmail = "hungh@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Hoàng Hưng",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV04"
                },
                new IdentityUser
                {
                    Id = 5,
                    Email = "test_admin_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "Admin",
                    NormalizedUserName = "Admin".ToUpper(),
                    NormalizedEmail = "test_admin_eform@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Admin Eform",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV05"
                },
                new IdentityUser
                {
                    Id = 6,
                    Email = "test_qltt_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "LongNV",
                    NormalizedUserName = "LongNV".ToUpper(),
                    NormalizedEmail = "test_qltt_eform@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Nguyễn Văn Long",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV06"
                },
                new IdentityUser
                {
                    Id = 7,
                    Email = "test_qllv1_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "HoangVB",
                    NormalizedUserName = "HoangVB".ToUpper(),
                    NormalizedEmail = "test_qllv1_eform@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Vũ Bình Hoàng",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV07"
                },
                new IdentityUser
                {
                    Id = 8,
                    Email = "test_hr_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "LucNT",
                    NormalizedUserName = "test_hr_eform@fsb.edu.vn".ToUpper(),
                    NormalizedEmail = "LucNT".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Nguyễn Thị Lục",
                    CampusId = 1,
                    Gender = Gender.Female,
                    EmployeeCode = "NV08"
                },
                new IdentityUser
                {
                    Id = 9,
                    Email = "test_accounting_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "AnhNV",
                    NormalizedUserName = "test_accounting_eform@fsb.edu.vn".ToUpper(),
                    NormalizedEmail = "test_accounting_eform@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Nguyễn Văn Anh",
                    CampusId = 1,
                    Gender = Gender.Male,
                    EmployeeCode = "NV09"
                },
                new IdentityUser
                {
                    Id = 10,
                    Email = "test_dean_eform@fsb.edu.vn",
                    EmailConfirmed = true,
                    PasswordHash = passwordHasher.HashPassword(new IdentityUser(), "Admin@123"),
                    UserName = "EmNV",
                    NormalizedUserName = "EmNV".ToUpper(),
                    NormalizedEmail = "test_dean_eform@fsb.edu.vn".ToUpper(),
                    PhoneNumber = "0987083373",
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    DepartmentId = 2,
                    FullName = "Nguyễn Văn Em",
                    CampusId = 1,
                    Gender = Gender.Female,
                    EmployeeCode = "NV10"
                }
            );

            #endregion

            #region Member Relation

            modelBuilder.Entity<MemberRelation>().HasData(
                new MemberRelation
                {
                    Id = 1,
                    UserId = 2,
                    ManagerId = 1
                },
                new MemberRelation
                {
                    Id = 2,
                    UserId = 3,
                    ManagerId = 2
                },
                new MemberRelation
                {
                    Id = 3,
                    UserId = 1,
                    ManagerId = 3
                }
            );

            #endregion

            #region Parent Department

            modelBuilder.Entity<ParentDepartment>().HasData(
                new ParentDepartment
                {
                    Id = 1,
                    Name = "Viện công nghệ FSB",
                    Description = "Viện công nghệ FSB",
                    Status = true
                }
            );

            #endregion

            #region Campus

            modelBuilder.Entity<Campus>().HasData(
                new Campus
                {
                    Id = 1,
                    Name = "FSB Hà Nội",
                    Status = true,
                    ParentDepartmentId = 1
                },
                new Campus
                {
                    Id = 2,
                    Name = "FSB Hồ Chí Minh",
                    Status = true,
                    ParentDepartmentId = 1
                }
            );

            #endregion

            #region Department

            modelBuilder.Entity<Department>().HasData(
                new Department
                {
                    Id = 1,
                    CampusId = 1,
                    Name = "Ban lãnh đạo",
                    Status = true
                },
                new Department
                {
                    Id = 2,
                    CampusId = 1,
                    Name = "Phòng IT",
                    Status = true
                }
            );

            #endregion

            #region Identity Roles

            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = 1,
                    MenuIds = "1;4;5;2;6;7;3;17;18;8;9;10;11;12;13;14;15;16;19;20;21;22;23;24;25;26;27",
                    Name = "Administrator",
                    NormalizedName = "ADMINISTRATOR"
                },
                new IdentityRole
                {
                    Id = 2,
                    MenuIds = "1",
                    Name = "Staff",
                    NormalizedName = "STAFF"
                }
            );

            #endregion

            #region User Roles

            modelBuilder.Entity<IdentityUserRole>().HasData(
                new IdentityUserRole
                {
                    Id = 1,
                    RoleId = 1,
                    UserId = 1
                },
                new IdentityUserRole
                {
                    Id = 2,
                    RoleId = 1,
                    UserId = 2
                },
                new IdentityUserRole
                {
                    Id = 3,
                    RoleId = 1,
                    UserId = 3
                },
                new IdentityUserRole
                {
                    Id = 4,
                    RoleId = 1,
                    UserId = 4
                },
                new IdentityUserRole
                {
                    Id = 5,
                    RoleId = 1,
                    UserId = 5
                }
            );

            #endregion

            #region Menus

            modelBuilder.Entity<Menu>().HasData(
                new Menu
                {
                    Id = 1,
                    Name = "Quản lý Menu",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "#",
                    IsActive = true,
                    Icon = "fa fa-list",
                    Position = 0
                },
                new Menu
                {
                    Id = 2,
                    Name = "Quản lý vai trò",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/role",
                    IsActive = true,
                    Icon = "fa fa-cogs",
                    Position = 1
                },
                new Menu
                {
                    Id = 3,
                    Name = "Quản lý người dùng",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/user",
                    IsActive = true,
                    Icon = "fa fa-users",
                    Position = 2
                },
                new Menu
                {
                    Id = 4,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 1,
                    Url = "/menu",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 5,
                    Name = "Chỉnh sửa menu",
                    MenuAction = MenuAction.IsManage,
                    Parent = 1,
                    Url = "/menu",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 6,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 2,
                    Url = "/role",
                    IsActive = true,
                    Icon = "fas fa-list-ul"
                },
                new Menu
                {
                    Id = 7,
                    Name = "Chỉnh sửa quyền",
                    MenuAction = MenuAction.IsManage,
                    Parent = 2,
                    Url = "/role",
                    IsActive = true,
                    Icon = "fas fa-edit"
                },
                new Menu
                {
                    Id = 8,
                    Name = "Quản lý campus",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/campus",
                    IsActive = true,
                    Icon = "fas fa-school",
                    Position = 4
                },
                new Menu
                {
                    Id = 9,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 8,
                    Url = "/campus",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 10,
                    Name = "Chỉnh sửa campus",
                    MenuAction = MenuAction.IsManage,
                    Parent = 8,
                    Url = "/campus",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 11,
                    Name = "Quản lý cơ sở",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/parent-department",
                    IsActive = true,
                    Icon = "fas fa-city",
                    Position = 3
                },
                new Menu
                {
                    Id = 12,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 11,
                    Url = "/parent-department",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 13,
                    Name = "Chỉnh sửa cơ sở",
                    MenuAction = MenuAction.IsManage,
                    Parent = 11,
                    Url = "/parent-department",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 14,
                    Name = "Quản lý phòng ban",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/department",
                    IsActive = true,
                    Icon = "fas fa-landmark",
                    Position = 5
                },
                new Menu
                {
                    Id = 15,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 14,
                    Url = "/department",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 16,
                    Name = "Chỉnh sửa phòng ban",
                    MenuAction = MenuAction.IsManage,
                    Parent = 14,
                    Url = "/department",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 17,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 3,
                    Url = "/user",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 18,
                    Name = "Chỉnh sửa người dùng",
                    MenuAction = MenuAction.IsManage,
                    Parent = 3,
                    Url = "/user",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 19,
                    Name = "Quản lý mẫu văn bản",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "#",
                    IsActive = true,
                    Icon = "fa fa-file",
                    Position = 6
                },
                new Menu
                {
                    Id = 20,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 19,
                    Url = "/form-template",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 21,
                    Name = "Chỉnh sửa mẫu văn bản",
                    MenuAction = MenuAction.IsManage,
                    Parent = 19,
                    Url = "/form-template",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 22,
                    Name = "Quản lý quy trình",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/workflow",
                    IsActive = true,
                    Icon = "fa fa-sitemap",
                    Position = 7
                },
                new Menu
                {
                    Id = 23,
                    Name = "Xem danh sách",
                    MenuAction = MenuAction.IsView,
                    Parent = 22,
                    Url = "/workflow",
                    IsActive = true,
                    Icon = "fas fa-list-ul",
                    Position = 0
                },
                new Menu
                {
                    Id = 24,
                    Name = "Chỉnh sửa quy trình",
                    MenuAction = MenuAction.IsManage,
                    Parent = 22,
                    Url = "/workflow",
                    IsActive = true,
                    Icon = "fas fa-edit",
                    Position = 1
                },
                new Menu
                {
                    Id = 25,
                    Name = "Quản lý văn bản",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/request-document",
                    IsActive = true,
                    Icon = "fas fa-file-invoice",
                    Position = 8
                },
                new Menu
                {
                    Id = 26,
                    Name = "Cấu hình",
                    MenuAction = MenuAction.IsManage,
                    Parent = 0,
                    Url = "/config",
                    IsActive = true,
                    Icon = "fa fa-cog",
                    Position = 0
                },
                new Menu
                {
                    Id = 27,
                    Name = "Mẫu Email",
                    MenuAction = MenuAction.IsManage,
                    Parent = 26,
                    Url = "config/email-templates",
                    IsActive = true,
                    Icon = "fas fa-mail-bulk",
                    Position = 0
                }
            );

            #endregion

            #region Email

            modelBuilder.Entity<EmailTemplate>().HasData(
                new EmailTemplate
                {
                    Id = 1,
                    Enabled = true,
                    Body = "Test body",
                    Name = "Test",
                }
            );

            #endregion
        }
    }
}