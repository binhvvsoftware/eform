﻿using CMS.Sharp.Docx.Extensions;
using DocumentFormat.OpenXml.Wordprocessing;

namespace CMS.Sharp.Docx.CodeBlocks
{
    /// <summary>
    ///     ConditionalText is used to conditionally show or hide Word elements.
    /// </summary>
    internal class ConditionalText : CodeBlock
    {
        public string Condition { get; set; }

        public Text EndConditionalPart { get; set; }

        public ConditionalText(string code) : base(code)
        {
            Condition = code.GetExpressionInBrackets();
        }
    }
}