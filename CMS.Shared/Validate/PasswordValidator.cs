﻿using System.Linq;

namespace CMS.Shared.Validate
{
    public static class PasswordValidator
    {
        public const string PasswordSuggest =
            @"Password expects atleast 1 small-case letter, 1 Capital letter, 1 digit, 1 special character and the length should be between 8 - 50 characters.The sequence of the characters is not important. 
Matches: 1A2a$567 | 1234567Tt# | Tsd6779%.
Non-Matches: Tt12234 | 1tdfy34564646T*";

        public const string PasswordAlert =
            @"Please enter your password atleast 1 small-case letter, 1 Capital letter, 1 digit, 1 special character and the length should be between 8 - 50 characters.
The password should not contain the user name.
The sequence of the characters is not important.";

        public static bool IsPasswordValidate(string password, string userName)
        {
            if (string.IsNullOrEmpty(password)) return false;
            if (password.Length < 8) return false;
            if (!password.Any(char.IsUpper)) return false;
            if (!password.Any(char.IsLower)) return false;
            if (!password.Any(char.IsDigit)) return false;
            if (password.All(char.IsLetterOrDigit)) return false;
            if (!string.IsNullOrEmpty(userName) && password.Contains(userName.Trim(), System.StringComparison.InvariantCultureIgnoreCase)) return false;
            return true;
        }
    }
}
