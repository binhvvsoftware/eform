﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace CMS.Shared.Xml
{
    public class NoAttributeXmlWriter : XmlWriter
    {
        private readonly XmlTextWriter writer;

        public NoAttributeXmlWriter(Stream stream, Encoding encoding)
        {
            writer = new XmlTextWriter(stream, encoding);
        }

        public override void WriteStartDocument()
        {
            writer.WriteStartDocument();
        }

        public override void WriteStartDocument(bool standalone)
        {
            writer.WriteStartDocument(standalone);
        }

        public override void WriteEndDocument()
        {
            writer.WriteEndDocument();
        }

        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            writer.WriteDocType(name, pubid, sysid, subset);
        }

        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            writer.WriteStartElement(string.Empty, localName, string.Empty);
        }

        public override void WriteEndElement()
        {
            writer.WriteEndElement();
        }

        public override void WriteFullEndElement()
        {
            writer.WriteFullEndElement();
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            //            writer.WriteStartAttribute(prefix, localName, ns);
        }

        public override void WriteEndAttribute()
        {
            //            writer.WriteEndAttribute();
        }

        public override void WriteCData(string text)
        {
            writer.WriteCData(text);
        }

        public override void WriteComment(string text)
        {
            writer.WriteComment(text);
        }

        public override void WriteProcessingInstruction(string name, string text)
        {
            writer.WriteProcessingInstruction(name, text);
        }

        public override void WriteEntityRef(string name)
        {
            writer.WriteEntityRef(name);
        }

        public override void WriteCharEntity(char ch)
        {
            writer.WriteCharEntity(ch);
        }

        public override void WriteWhitespace(string ws)
        {
            writer.WriteWhitespace(ws);
        }

        public override void WriteString(string text)
        {
            writer.WriteString(text);
        }

        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            writer.WriteSurrogateCharEntity(lowChar, highChar);
        }

        public override void WriteChars(char[] buffer, int index, int count)
        {
            writer.WriteChars(buffer, index, count);
        }

        public override void WriteRaw(char[] buffer, int index, int count)
        {
            writer.WriteRaw(buffer, index, count);
        }

        public override void WriteRaw(string data)
        {
            writer.WriteRaw(data);
        }

        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            writer.WriteBase64(buffer, index, count);
        }

        public override void Flush()
        {
            writer.Flush();
        }

        public override string LookupPrefix(string ns)
        {
            return string.Empty;
        }

        public override WriteState WriteState
        {
            get { return writer.WriteState; }
        }
    }

    public class NoAttributeXmlReader : XmlTextReader
    {
        public NoAttributeXmlReader(Stream input) : base(input)
        {
        }

        public NoAttributeXmlReader(TextReader input) : base(input)
        {
        }

        public override bool MoveToFirstAttribute()
        {
            return false;
        }

        public override bool MoveToNextAttribute()
        {
            return false;
        }

        public override string ReadOuterXml()
        {
            return base.ReadOuterXml();
        }

        public override Task<string> ReadOuterXmlAsync()
        {
            return base.ReadOuterXmlAsync();
        }

        public override string Prefix {get { return string.Empty; }}

        public override IXmlSchemaInfo SchemaInfo { get { return null; } }
        
        public override bool HasAttributes { get { return false; } }

        public override string NamespaceURI { get { return string.Empty; } }

        public override string LookupNamespace(string prefix)
        {
            return string.Empty;
        }
    }
}
