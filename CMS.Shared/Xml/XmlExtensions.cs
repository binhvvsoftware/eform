﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace CMS.Shared.Xml
{
    public class XmlNodeDocument : XmlNodeElement
    {
        public XmlNodeDocument(XmlDocument document) : base(document)
        {
            var child = document.FirstChild;
            while (child != null)
            {
                if (child.Name == "Envelope")
                {
                    Envelope = child;
                    EnvelopeBody = Envelope.Element("Body");
                    break;
                }
                child = child.NextSibling;
            }
        }

        public XmlNodeElement CreateElement(string name)
        {
            return ((XmlDocument)XmlNode).CreateElement(name);
        }

        public XmlNodeElement Envelope { get; private set; }
        public XmlNodeElement EnvelopeBody { get; private set; }
    }

    public class XmlNodeElement
    {
        private readonly XmlNode xmlNode;

        public XmlNodeElement(XmlNode node)
        {
            xmlNode = node;
        }

        public bool IsEmpty
        {
            get
            {
                if (IsTextNode)
                    return string.IsNullOrEmpty(Value);

                return !xmlNode.HasChildNodes;
            }
        }

        public string Name
        {
            get { return xmlNode.Name; }
        }

        public XmlDocument Document
        {
            get { return xmlNode.OwnerDocument; }
        }

        public XmlNode XmlNode { get { return xmlNode; } }

        public IEnumerable<XmlNodeElement> ChildNodes
        {
            get
            {
                foreach (XmlNode child in xmlNode.ChildNodes)
                {
                    yield return child;
                }
            }
        }

        public void Remove()
        {
            if (XmlNode.ParentNode == null)
                throw new InvalidOperationException("Unable to remove a parentless node");

            XmlNode.ParentNode.RemoveChild(XmlNode);
        }

        public void RemoveAll()
        {
            XmlNode.RemoveAll();
        }

        public XmlNodeElement Add(XmlNode node)
        {
            xmlNode.AppendChild(node.CloneNode(true));
            return this;
        }

        public XmlNodeElement Add(XmlNodeElement node)
        {
            return Add(node.XmlNode);
        }

        public XmlNodeElement CloneNode()
        {
            return XmlNode.CloneNode(true);
        }

        public string OuterXml
        {
            get { return xmlNode.OuterXml; }
        }

        public string InnerXml
        {
            get { return xmlNode.InnerXml; }
        }

        public bool HasElements
        {
            get { return !IsTextNode && xmlNode.HasChildNodes; }
        }

        public IEnumerable<XmlNodeElement> Elements()
        {
            foreach (XmlNode c in xmlNode.ChildNodes)
            {
                yield return c;
            }
        }

        public IEnumerable<XmlNodeElement> XPathSelectElements(string xpath)
        {
            var xmlNodeList = XmlNode.SelectNodes(xpath);

            if (xmlNodeList != null)
                foreach (XmlNode node in xmlNodeList)
                {
                    yield return node;
                }
        }

        public XmlNodeElement XPathSelectElement(string xpath)
        {
            var node = XmlNode.SelectSingleNode(xpath);
            return node;
        }

        private bool IsTextNode
        {
            get { return (xmlNode.ChildNodes.Count == 1 && xmlNode.FirstChild.NodeType == XmlNodeType.Text); }
        }

        public string Value
        {
            get
            {
                if (IsTextNode)
                    return xmlNode.FirstChild.Value;

                return null;
            }
        }

        public static implicit operator XmlNodeElement(XmlNode node)
        {
            return new XmlNodeElement(node);
        }

        public static XmlNodeDocument LoadFrom(Stream stream)
        {
            var document = new XmlDocument();
            using (var reader = new NoAttributeXmlReader(stream))
            {
                document.Load(reader);
            }
            return new XmlNodeDocument(document);
        }

        public static XmlNodeDocument LoadFrom(string xmlString)
        {
            using (var stringReader = new StringReader(xmlString))
            {
                return LoadFrom(stringReader);
            }
        }

        public static XmlNodeDocument LoadFrom(TextReader textReader)
        {
            var document = new XmlDocument();
            using (var reader = new NoAttributeXmlReader(textReader))
            {
                document.Load(reader);
            }
            return new XmlNodeDocument(document);
        }
    }

    public static class XmlElementExtensions
    {
        private static Dictionary<Type, System.Xml.Serialization.XmlSerializer> xmlSerializers = new Dictionary<Type, System.Xml.Serialization.XmlSerializer>();
        private static readonly object sync = new object();

        public static XElement CloneNode(this XElement element)
        {
            return new XElement(element);
        }

        public static XElement CreateElement(this XElement element, XName name)
        {
            return new XElement(name);
        }

        public static string ToJson(this XmlNodeElement node)
        {
            return JsonConvert.SerializeXmlNode(node.XmlNode);
        }

        public static string ToJson(this XElement node)
        {
            return JsonConvert.SerializeXNode(node);
        }

        private static System.Xml.Serialization.XmlSerializer GetOrAddSerializer<T>()
        {
            lock (sync)
            {
                System.Xml.Serialization.XmlSerializer serializer;
                if (!xmlSerializers.TryGetValue(typeof (T), out serializer))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof (T));
                    xmlSerializers.Add(typeof(T),serializer);
                }
                return serializer;
            }
        }

        public static T ToObject<T>(this XmlNodeElement node)
        {
            return (T) GetOrAddSerializer<T>().Deserialize(new XmlNodeReader(node.XmlNode));
        }

        public static string XmlStringWithoutAttributes(this XmlNodeElement node)
        {
            return node.OuterXml;
            /* string result;

             using (var memoryStream = new MemoryStream())
             {
                 using (var writer = new NoAttributeXmlWriter(memoryStream, Encoding.UTF8))
                 {
                     node.XmlNode.WriteTo(writer);
                     writer.Flush();

                     memoryStream.Seek(0, SeekOrigin.Begin);

                     using (var reader = new StreamReader(memoryStream))
                     {
                         result = reader.ReadToEnd();
                     }
                 }
             }
             return result;*/
        }

        public static string XmlStringWithoutAttributes(this XElement node)
        {
            string result;

            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new NoAttributeXmlWriter(memoryStream, Encoding.UTF8))
                {
                    node.WriteTo(writer);
                    writer.Flush();

                    memoryStream.Seek(0, SeekOrigin.Begin);

                    using (var reader = new StreamReader(memoryStream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
            return result;
        }

        public static IEnumerable<XmlNodeElement> Elements(this IEnumerable<XmlNodeElement> nodes, string name)
        {
            foreach (var node in nodes)
            {
                foreach (var n in node.Elements(name))
                {
                    yield return n;
                }
            }
        }
        public static XmlNodeElement Element(this IEnumerable<XmlNodeElement> nodes, string name)
        {
            foreach (var node in nodes)
            {
                var n = node.Element(name);
                if (n != null) return n;
            }

            return null;
        }

        public static XmlNodeElement Add(this XmlNodeElement node, XmlNode child)
        {
            node.Add(child);
            return node;
        }

        public static XmlNode Add(this XmlNode node, XmlNode child)
        {
            node.AppendChild(child);
            return node;
        }
        public static IEnumerable<XmlNodeElement> Elements(this XmlNodeElement node, string name)
        {
            var list = node.ChildNodes.Where(x => x.Name.Equals(name,StringComparison.InvariantCultureIgnoreCase)).ToList();
            return list;
        }

        public static XmlNodeElement Element(this XmlNodeElement node, string name)
        {
            foreach (XmlNodeElement child in node.ChildNodes)
            {
                if (child.Name.Equals(name,StringComparison.InvariantCultureIgnoreCase))
                    return child;
            }
            return null;
        }

        public static void ReplaceAll(this XmlNodeElement node, params XmlNodeElement[] replacements)
        {
            node.RemoveAll();
            for (var i = 0; i < replacements.Length; i++)
                node.Add(replacements[i]);
        }

        public static IEnumerable<XmlNodeElement> Descendants(this XmlNodeElement node, string name)
        {
            foreach (XmlNodeElement child in node.ChildNodes)
            {
                if (child.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    yield return child;

                foreach (var x in child.Descendants(name))
                {
                    yield return x;
                }
            }
        }

        public static IEnumerable<XmlNodeElement> Descendants(this IEnumerable<XmlNodeElement> nodes, string name)
        {
            foreach (XmlNodeElement n in nodes)
            {
                foreach (var x in n.Descendants(name))
                {
                    yield return x;
                }
            }
        }
    }
}
