﻿using System.Collections.Generic;
using System.Linq;

namespace CMS.Shared.Web.Permissions
{
    public interface ICachedPermissionProvider
    {
        Permission GetPermission(string name);
    }

    public class CachedPermissionProvider : ICachedPermissionProvider
    {
        private readonly IEnumerable<Permission> permissions;

        public CachedPermissionProvider(IEnumerable<IPermissionProvider> permissionProviders)
        {
            permissions = permissionProviders.Select(x => x.GetPermissions()).SelectMany(x => x).ToList();
        }

        public Permission GetPermission(string name)
        {
            return permissions.FirstOrDefault(x => x.Name == name);
        }
    }
}
