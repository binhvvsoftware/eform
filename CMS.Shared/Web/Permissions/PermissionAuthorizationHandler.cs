﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace CMS.Shared.Web.Permissions
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionAuthorizationRequirement>
    {
        private readonly ICachedPermissionProvider cachedPermissionProvider;

        public PermissionAuthorizationHandler(ICachedPermissionProvider cachedPermissionProvider)
        {
            this.cachedPermissionProvider = cachedPermissionProvider;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement)
        {
            var permission = cachedPermissionProvider.GetPermission(requirement.Permission);
            if (permission == null)
            {
                return;
            }

            var granted = false;

            var permissions = context.User.Claims.Where(x => x.Type == CustomClaimTypes.Permission).Select(x => x.Value).ToList();

            // determine which set of permissions would satisfy the access check
            var grantingNames = PermissionNames(permission, Enumerable.Empty<string>()).Distinct().ToArray();

            foreach (var rolePermission in permissions)
            {
                if (grantingNames.Any(grantingName => string.Equals(rolePermission, grantingName, StringComparison.OrdinalIgnoreCase)))
                {
                    granted = true;
                }

                if (granted)
                    break;
            }

            if (granted)
            {
                context.Succeed(requirement);
            }

            await Task.CompletedTask;
        }

        private static IEnumerable<string> PermissionNames(Permission permission, IEnumerable<string> stack)
        {
            // the given name is tested
            yield return permission.Name;

            // iterate implied permissions to grant, it present
            if (permission.ImpliedBy != null && permission.ImpliedBy.Any())
            {
                foreach (var impliedBy in permission.ImpliedBy)
                {
                    // avoid potential recursion
                    if (stack.Contains(impliedBy.Name))
                        continue;

                    // otherwise accumulate the implied permission names recursively
                    foreach (var impliedName in PermissionNames(impliedBy, stack.Concat(new[] { permission.Name })))
                    {
                        yield return impliedName;
                    }
                }
            }

            yield return StandardPermissions.FullAccessPermission.Name;
        }
    }
}
