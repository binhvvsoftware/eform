﻿using System.Collections.Generic;

namespace CMS.Shared.Web.Permissions
{
    /// <summary>
    /// Implemented by modules to enumerate the types of permissions the which may be granted
    /// </summary>
    public interface IPermissionProvider
    {
        IEnumerable<Permission> GetPermissions();
    }
}
