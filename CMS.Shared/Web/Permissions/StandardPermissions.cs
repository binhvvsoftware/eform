﻿using System.Collections.Generic;

namespace CMS.Shared.Web.Permissions
{
    public class StandardPermissions : IPermissionProvider
    {
        public const string FullAccess = "FullAccess";

        public static readonly Permission FullAccessPermission = new Permission { Name = FullAccess, Category = "System", Description = "Grant full system access" };

        public IEnumerable<Permission> GetPermissions()
        {
            return new[]
            {
                FullAccessPermission
            };
        }
    }
}
