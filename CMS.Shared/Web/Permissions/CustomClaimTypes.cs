﻿namespace CMS.Shared.Web.Permissions
{
    public static class CustomClaimTypes
    {
        /// <summary>
        /// A claim that specifies the permission of an entity
        /// </summary>
        public const string Permission = "permission";

        /// <summary>
        /// A claim that specifies the full name of an entity
        /// </summary>
        public const string FullName = "fullname";
    }
}
