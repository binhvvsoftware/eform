﻿using Microsoft.AspNetCore.Authorization;

namespace CMS.Shared.Web.Permissions
{
    public class PermissionAuthorizationRequirement : IAuthorizationRequirement
    {
        public string Permission { get; }

        public PermissionAuthorizationRequirement(string permission)
        {
            Permission = permission;
        }
    }
}
