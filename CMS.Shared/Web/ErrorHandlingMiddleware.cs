﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CMS.Core.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace CMS.Shared.Web
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, logger);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception, ILogger logger)
        {
            logger.LogError(new EventId(0), exception, $"Error during invoke {context.Request.Path.Value}");

            if (exception is UnauthorizedAccessException)
            {
                context.Response.Redirect($"/error/{403}");
            }
            else
            {
                context.Response.Redirect($"/error/{500}");
            }
            return context.Response.WriteAsync(exception.StackTrace);
        }
    }
}
