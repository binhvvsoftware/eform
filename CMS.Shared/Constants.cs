﻿
namespace CMS.Shared
{
    public static class Constants
    {
        public const string ShortDatePattern = "dd/MM/yyyy";
        public const string FullDatePattern = "dd/MM/yyyy HH:mm:ss";
        public const string HotelChargeDatePattern = "yyyyMMdd";
        public const string ReplyToEmail = "eform@fsb.edu.vn";
        public const string ReplyToDisplayName = "Admin Eform";
        public const string PasswordNoChange = "[No Change]";
        public const string SessionMenu = "CMS_SESSION_MENU";
        public const string GeneratePath = "Uploads";
        public const string TemplatePath = "Templates";
        public const string ImagePath = @"wwwroot\SignatureImages";

        public static class Areas
        {
            public const string Dashboard = "Dashboard";
        }

        public static class LocalizedStrings
        {
            public const string CreateFor = "Tạo {0}";
            public const string EditFor = "Sửa {0}";
            public const string ManageFor = "Quản lý {0}";
            public const string SaveSuccess = "Lưu thành công!";
            public const string ConfirmDeleteRecord = "Bạn có chắc chắn muốn xóa bản ghi này?";
            public const string ConfirmDeactiveRecord = "Bạn có chắc chắn muốn hủy kích hoạt bản ghi này?";
            public const string ConfirmReactiveRecord = "Bạn có chắc chắn muốn kích hoạt lại bản ghi này?";

            public const string ProductNotAvailable = "{0} not available for the specified dates. Please try again with different dates.";
            public const string FlightsNotAvailable = "Flights not available for the specified dates. Please try again with different dates.";
            public const string HotelsNotAvailable = "Hotels not available for the specified dates. Please try again with different dates.";
            public const string ToursNotAvailable = "Tours not available for the specified dates. Please try again with different dates.";
            public const string TransfersNotAvailable = "Transfers not available for the specified dates. Please try again with different dates.";
            public const string CarsNotAvailable = "Car not available for the specified dates. Please try again with different dates.";

            public const string ErrorInputDate = "You must be input from date before to date.";
            public const string PreBookingHotelFailed = "Sorry, this room has been sold out due to popular demand. Please choose a different room instead.";
            public const string PreBookingTourFailed = "Can not book this tour. Please try another or contact us!";
            public const string PreBookingTransferFailed = "Can not book this transfer. Please try another or contact us!";
            public const string PreBookingPickupTransferFailed = "You are suggested to have more time with airport procedures. Please kindly select another Pick-up time.";
            public const string RemoveBookingTransferFailed = "Can not remove booking this transfer. Please try another or contact us!";
            public const string ConfirmTaxFlightFail = "Unable to get flight tax. Please try another or contact us!";
            public const string VerifyBookingFlightFail = "Your flight is sold out due to high demand. Please try with another flight.";
            public const string VerifyBookingHotelFail = "The selected hotel has been sold out, please start a new session!";

            public const string PackageQueryNotFound = "Package Query not found.";
            public const string PaymentGatewayIncorrect = "Payment gateway incorrect.";
            public const string AgentAuthTokenInvalid = "Agent Auth Token is invalid.";
            public const string TransactionOnCreditCardWasDeclined = "Unfortunately, we are unable to process your booking because the transaction on your credit card was declined. Your booking has not been completed. You can try using an alternate credit card to complete your booking. We apologize for any inconvenience caused.";
            public const string AmexCvvInvalid = "Please input the correct CVV for Amex credit card.";
            public const string BaggageNotMatch = "Baggage not match";
            public const string YourCreditCardWasDeclined = "We’re sorry but your credit card was declined. Please use an alternative credit card and try submitting again.";
            public const string ModifySearchWithAdultInsteadOfChildDepartureDate = "On the date of departure, {0} will be {1} years old. Kindly modify your search with Adult instead of Child.";
            public const string ModifySearchWithAdultInsteadOfChildReturnDate = "On the date of return, {0} will be {1} years old. Kindly modify your search with Adult instead of Child.";
            public const string PleaseKeyTheCorrectDateOfBirth = "Child {0} age is not the same as previously selected. Please key the correct date of birth";
            public const string ModifySearchWithChildInsteadOfInfantReturnDate = "On the date of return, {0} will be {1} years old. Kindly modify your search with Child instead of Infant";
            public const string ModifySearchWithChildInsteadOfInfantDepartureDate = "On the date of departure, {0} will be {1} years old. Kindly modify your search with Child instead of Infant.";
            public const string PassportExpiryDate = "Invalid passport expiry date.";
            public const string PassportIsValidLessThanSixMonths = "Your passport is valid less than six months beyond your date of departure.";
            public const string WeDontAcceptDuplicatedName = "Please make sure the passenger name is correct. We don't accept duplicated name";
            public const string PassengerInTourCantDuplicate = "Passenger in tour {0} can't duplicate";
            public const string InsurancePrebookFailed = "Insurance Pre-Book Failed";
            public const string MakeSureThePassengerTitleIsCorrect = "Please make sure the passenger title is correct.Please kindly check again.";
            public const string MakeSureThePassengerFirstNameIsCorrect = "Please make sure the passenger first name is correct. Please kindly check again.";
            public const string FirstNameMoreThan26Characters = "Please make sure the passenger first name is correct. We don't accept first name more than 26 characters.";
            public const string MakeSureThePassengerLastNameIsCorrect = "Please make sure the passenger last name is correct. Please kindly check again.";
            public const string LastNameMoreThan26Characters = "Please make sure the passenger last name is correct. We don't accept last name more than 26 characters.";
            public const string DontAcceptNonEnglishName = "Please make sure that the passenger name is correct. We don't accept non-english name.";
            public const string MakeSureThePassengerNationalityIsCorrect = "Please make sure the passenger nationality is correct. Please kindly check again.";
            public const string MakeSureThePersonalTitleIsCorrect = "Please make sure the personal title is correct. Please kindly check again.";
            public const string MakeSureThePersonalFirstNameIsCorrect = "Please make sure the personal first name is correct. Please kindly check again.";
            public const string MakeSureThatThePersonalNameIsCorrect = "Please make sure that the personal name is correct. We don't accept non-english name.";
            public const string MakeSureThePersonalLastNameIsCorrect = "Please make sure the personal last name is correct. Please kindly check again.";
            public const string MakeSureThePersonalAddressIsCorrect = "Please make sure the personal address is correct. Please kindly check again.";
            public const string MakeSureThePersonalCityIsCorrect = "Please make sure the personal city is correct. Please kindly check again.";
            public const string MakeSureThePersonalEmailIsCorrect = "Please make sure the personal email is correct. Please kindly check again.";
            public const string MakeSureThePersonalCountryIsCorrect = "Please make sure the personal country is correct. Please kindly check again.";
            public const string MakeSureThePersonalMobileNumberIsCorrect = "Please make sure the personal mobile number is correct. Please kindly check again.";
            public const string PassportNoMustBeUnique = "Passport No. must be unique. Please enter another valid passport number to avoid duplication.";
            public const string CannotBookThisFlight = "Cannot book this flight. {0}";
            public const string PaymentNotSuccess = "Payment not success";
            public const string UnableToProcessReviewStatus = "Unable to Process Review status for non UnderReviewing Booking";
            public const string CannotHoldBooking = "Can't hold this booking.";
            public const string BookingNotExist = "Booking is not exist.";
            public const string B2BPaymentRequiredLogin = "This is B2B Booking. Please do log-in before making payment.";
            public const string B2BPaymentRequiredSameAgent = "This booking is not conducted by current user. Please check and try again.";
            public const string B2BPaymentExpired = "We can confirm that your booking is over payment due and your reservation is cancelled automatically for free. Please visit our site and make another booking or contact us!";
            public const string BookingInvalidLastName = "Please enter a valid Last Name in this field";
            public const string SiteNotFound = "Site not found.";
            public const string ChecksumInvalid = "Your package has been changed. Please kindly review your booking and reload this page.";

            public static class Validation
            {
                public const string Date = "Please enter a valid date.";
                public const string Digits = "Please enter only digits.";
                public const string Email = "Please enter a valid email address.";
                public const string EqualTo = "Please enter the same value again.";
                public const string MaxLength = "Please enter no more than {0} characters.";
                public const string MinLength = "Please enter at least {0} characters.";
                public const string Number = "Please enter a valid number.";
                public const string PhoneNumber = "Please enter a valid phone number.";
                public const string Range = "Please enter a value between {0} and {1}.";
                public const string RangeLength = "Please enter a value between {0} and {1} characters long.";
                public const string RangeMax = "Please enter a value less than or equal to {0}.";
                public const string RangeMin = "Please enter a value greater than or equal to {0}.";
                public const string Required = "Please enter a value.";
                public const string Url = "Please enter a valid URL.";
                public const string CreditCard = "Please enter a valid credit card number.";
                public const string LettersOnly = "Please enter letters only.";
            }
        }

        public static class PdfTemplates
        {
            public const string Booking = "Success Booking Template";
            public const string CancelBooking = "Cancel Booking Template";
            public const string TaxInvoice = "Tax Invoice Template";
            public const string DepositBooking = "Deposit PDF Template";
            public const string FailedButCaptured = "Failed But Captured PDF Template";

            public static string[] GetPdfTemplates()
            {
                return new[]
                {
                    Booking,
                    CancelBooking,
                    TaxInvoice,
                    DepositBooking,
                    FailedButCaptured
                };
            }
        }

        public static class EmailTemplates
        {
            public const string RequestDocumentApproved = "Văn bản được duyệt";
            public const string RequestDocumentRejected = "Văn bản bị từ chối";
            public const string RequestDocumentNew = "Yêu cầu duyệt văn bản";
            public const string RequestDocumentCreated = "Văn bản được tạo";
            public const string WorkingLateTemplate = "Đi làm muộn giờ";
            public const string LateRequestToUserOnly = "Thông báo đi làm muộn giờ cho nhân viên";
            public const string LateRequestToUserAndManagers = "Thông báo đi làm muộn giờ cho nhân viên và quản lý";
            public const string LateRequestExpired = "Yêu cầu xác nhận đi muộn hết hạn";
            public const string LateRequestUpdated = "Yêu cầu xác nhận đi muộn đã được thay đổi";

            public static string[] GetEmailTemplates()
            {
                return new[]
                {
                    RequestDocumentApproved,
                    RequestDocumentRejected,
                    RequestDocumentNew,
                    RequestDocumentCreated,
                    WorkingLateTemplate,
                    LateRequestToUserOnly,
                    LateRequestToUserAndManagers,
                    LateRequestExpired,
                    LateRequestUpdated
                };
            }
        }
    }
}