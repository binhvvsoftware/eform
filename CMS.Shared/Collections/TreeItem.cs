﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Shared.Collections
{
    public class TreeItem<T>
    {
        public T Item { get; set; }

        public IEnumerable<TreeItem<T>> Children { get; set; }
    }

    public static class TreeItemExtensions
    {
        public static IEnumerable<TreeItem<T>> GenerateTree<T, TK>(this IEnumerable<T> collection,
            Func<T, TK> idSelector,
            Func<T, TK> parentIdSelector,
            TK rootId = default(TK))
        {
            foreach (var c in collection.Where(c => parentIdSelector(c).Equals(rootId)))
            {
                yield return new TreeItem<T>
                {
                    Item = c,
                    Children = collection.GenerateTree(idSelector, parentIdSelector, idSelector(c))
                };
            }
        }
    }
}
