﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CMS.Shared.Collections
{
    public interface IPagedList : IEnumerable
    {
        int PageIndex { get; set; }

        int PageSize { get; set; }

        int ItemCount { get; set; }

        int PageCount { get; set; }

        bool HasPreviousPage { get; }

        bool HasNextPage { get; }
    }

    public interface IPagedList<T> : IPagedList, IList<T>
    {
    }

    public class PagedList<T> : List<T>, IPagedList<T>
    {
        public PagedList()
        {
            
        }

        public PagedList(IEnumerable<T> source)
        {
            AddRange(source);
            PageIndex = 1;
            PageSize = Count;
            ItemCount = Count;
            PageCount = 1;
        }

        public PagedList(IEnumerable<T> source, int itemCount)
        {
            AddRange(source);
            PageIndex = 1;
            ItemCount = itemCount;
        }

        public PagedList(IEnumerable<T> source, int pageIndex, int pageSize, int itemCount)
        {
            ItemCount = itemCount;
            PageCount = (int)Math.Ceiling((double)itemCount / pageSize);
            PageIndex = pageIndex;
            PageSize = pageSize;

            AddRange(source);
        }

        #region IPagedList<T> Members

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int ItemCount { get; set; }

        public int PageCount { get; set; }

        public bool HasPreviousPage
        {
            get { return PageIndex > 1; }
        }

        public bool HasNextPage
        {
            get { return PageIndex < PageCount; }
        }

        #endregion IPagedList<T> Members
    }

    public class PagedList<TParent, T> : PagedList<T>
    {
        public PagedList(TParent parent, IEnumerable<T> source)
            : base(source)
        {
            Parent = parent;
        }

        public PagedList(TParent parent, IEnumerable<T> source, int pageIndex, int pageSize, int itemCount)
            : base(source, pageIndex, pageSize, itemCount)
        {
            Parent = parent;
        }

        public TParent Parent { get; set; }
    }
}
