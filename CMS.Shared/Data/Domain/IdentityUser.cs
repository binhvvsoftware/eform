﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;
using Microsoft.AspNetCore.Identity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_IdentityUsers")]
    public class IdentityUser : IdentityUser<int>, IBaseEntity<int>
    {
        [Key]
        public override int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string FullName { get; set; }

        [Required]
        [StringLength(255)]
        public override string UserName { get; set; }

        [Required]
        [StringLength(255)]
        public override string Email { get; set; }

        [Required]
        [StringLength(255)]
        public override bool EmailConfirmed { get; set; }

        [Required]
        [StringLength(255)]
        public override string PasswordHash { get; set; }

        public override string PhoneNumber { get; set; }

        public override bool PhoneNumberConfirmed { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        [ForeignKey("CampusId")]
        public virtual Campus Campus { get; set; }

        [NotMapped]
        public ICollection<MemberRelation> MemberRelations { get; set; }

        [NotMapped]
        public ICollection<IdentityUser> Managers { get; set; }

        [NotMapped]
        public string DepartmentName => Department?.Name ?? string.Empty;

        public override bool TwoFactorEnabled { get; set; }

        [Column("LockoutEndDateUtc")]
        public override DateTimeOffset? LockoutEnd { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating if the user could be locked out.
        /// </summary>
        /// <value>True if the user could be locked out, otherwise false.</value>
        public override bool LockoutEnabled { get; set; }

        /// <summary>
        /// Gets or sets the number of failed login attempts for the current user.
        /// </summary>
        public override int AccessFailedCount { get; set; }

        /// <summary>
        /// Gets or sets the number of failed login attempts for the current user.
        /// </summary>
        public DateTime CreateDate { get; set; }
//        public DateTime? UpdateDate { get; set; }
        public string EmployeeCode { get; set; }

        public string Address { get; set; }

        public ContractType ContractType { get; set; }

        public Gender Gender { get; set; }

        public UserType UserType { get; set; }

        public int CampusId { get; set; }

        public bool SeniorLeader { get; set; }

        public bool IsManageRequestDocument { get; set; }
        
        public bool IgnoreWorkingRule { get; set; }

        public bool Disabled { get; set; }

        /// <summary>
        /// Returns the username for this user.
        /// </summary>
        public override string ToString()
        {
            return UserName;
        }

        public object GetIdValue()
        {
            return Id;
        }
    }

    public enum ContractType : short
    {
        [Display(Name = "Học việc")]
        Hv = 1,
        [Display(Name = "Thử việc")]
        Tv = 2,
        [Display(Name = "LĐ-ĐT")]
        Lddt = 3,
        [Display(Name = "Dài hạn")]
        Dh = 4,
        [Display(Name = "KXĐ thời hạn")]
        Kxdth = 5,
        [Display(Name = "Khoán công việc")]
        Kcv = 6,
        [Display(Name = "Cộng tác (thính giảng)")]
        Ctv = 7,
    }

    public enum Gender : short
    {
        [Display(Name = "Nam")]
        Male = 1,
        [Display(Name = "Nữ")]
        Female = 2
    }

    public enum UserType : short
    {
        [Display(Name = "CBNV - GV")]
        Staff = 1,
        [Display(Name = "Học Viên")]
        Student = 2
    }
}
