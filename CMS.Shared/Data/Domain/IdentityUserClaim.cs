﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_IdentityUserClaims")]
    public class IdentityUserClaim : IdentityUserClaim<int>
    {
        [Key]
        public override int Id { get; set; }
    }
}
