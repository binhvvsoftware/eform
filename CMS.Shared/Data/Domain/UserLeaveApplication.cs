using System;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;
using System.ComponentModel.DataAnnotations;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_UserLeaveApplications")]
    public class UserLeaveApplication : BaseEntity<int>
    {
        private UserLeaveApplication userLeave;
        private readonly string[] LeaveTypes = new[]
        {
            "Nghỉ phép",
            "Nghỉ kết hôn",
            "Nghỉ con kết hôn",
            "Nghỉ khám thai",
            "Nghỉ sinh con",
            "Nghỉ sảy thai",
            "Nghỉ ốm",
            "Nghỉ chăm sóc con ốm",
            "Nghỉ tang",
            "Nghỉ khác [3]"
        };

        public int UserId { get; set; }
        public int RequestDocumentId { get; set; }
        public LeaveType LeaveType { get; set; }
        public DateTime? LeaveFrom { get; set; }
        public DateTime? LeaveTo { get; set; }

        public double TotalLeaveTime { get; set; }
        public DateTime? LeaveUnPaidFrom { get; set; }
        public DateTime? LeaveUnPaidTo { get; set; }
        public double TotalLeaveUnPaidTime { get; set; }
        public DateTime CreateAt { get; set; }

        [NotMapped]
        [ForeignKey("UserId")]
        public IdentityUser User { get; set; }

        public string LeaveReason { get; set; }
        
        public string LeaveArrangement { get; set; }

        public ApplyStatus ApplyStatus { get; set; }

        [NotMapped]
        [ForeignKey("RequestDocumentId")]
        public RequestDocument RequestDocument { get; set; }
    }

    public enum LeaveType : byte
    {
        [Display(Name = "Nghỉ phép có lương")]
        Paid = 0,
        [Display(Name = "Nghỉ không lương")]
        UnPaid = 1
    }

    public enum ApplyStatus : byte
    {
        [Display(Name = "Đơn mới")]
        NewApply = 1,
        [Display(Name = "Đã duyệt")]
        Approved = 2
    }
}