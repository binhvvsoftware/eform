﻿using CMS.Core.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_FormTemplates")]
    public class FormTemplate : BaseEntity<int>
    {
        [Required, MaxLength(125)]
        public string TemplateName { get; set; }

        [Required]
        public string TemplateContents { get; set; }

        [Required, MaxLength(512)]
        public string TemplatePath { get; set; }

        public bool IsActive { get; set; }
        
//        public FormType FormType { get; set; }
    }
    
    public enum FormType
    {
        [Display(Name = "Cán bộ nhân viên")]
        StaffForm = 1,
        [Display(Name = "Cán bộ quản lý")]
        ManagerForm = 2,
        [Display(Name = "Học viên")]
        StudentForm = 3
        
    }
}
