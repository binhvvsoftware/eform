﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;
using Microsoft.AspNetCore.Identity;

namespace CMS.Shared.Data.Domain
{
    /// <summary>
    /// Represents a role in the identity system
    /// </summary>
    [Table("Cms_IdentityRoles")]
    public class IdentityRole : IdentityRole<int>, IBaseEntity<int>
    {
        /// <summary>
        /// Gets or sets the name for this role.
        /// </summary>
        [Required]
        [StringLength(50)]
        public override string Name { get; set; }

        [StringLength(50)]
        public override string NormalizedName { get; set; }
        //
        // Summary:
        //     A random value that should change whenever a role is persisted to the store
        [StringLength(50)]
        public override string ConcurrencyStamp { get; set; }
        
        public string MenuIds { get; set; }

        /// <summary>
        /// Returns the name of the role.
        /// </summary>
        /// <returns>The name of the role.</returns>
        public override string ToString()
        {
            return Name;
        }

        public object GetIdValue()
        {
            return Id;
        }
    }
}
