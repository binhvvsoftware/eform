﻿using CMS.Core.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Logs")]
    public class Log : BaseEntity<int>
    {
        /// <summary>
        /// Gets or sets the log DateTime
        /// </summary>
        [Required]
        public DateTime LogTime { get; set; }

        /// <summary>
        /// Gets or sets the log Action Type
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the log Method Type
        /// </summary>
        [Required]
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets the log values
        /// </summary>
        
        [Required]
        public string Values { get; set; }

        /// <summary>
        /// Gets or sets the log User change
        /// </summary>
        [Required]
        public string UserName { get; set; }
    }
}
