using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CMS.Core.Entity;
using CMS.Core.Extensions;
using System.Globalization;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_WorkingTimeSheets")]
    public class WorkingTimesheet : BaseEntity<long>
    {
        public int UserId { get; set; }
        public DateTime WorkingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime CreateAt { get; set; }
        public string CreateBy { get; set; }

        [ForeignKey("UserId")]
        public virtual IdentityUser Requester { get; set; }

        public virtual WorkingLateRequest LateRequest { get; set; }

        [NotMapped]
        public int TotalLateTime
        {
            get
            {
                if (LateRequest == null || LateRequest.TotalLateMinutes <= 0)
                    return 0;

                return LateRequest.TotalLateMinutes;
            }
        }

        [NotMapped]
        public string PenaltyRule
        {
            get
            {
                if (LateRequest == null)
                    return string.Empty;

                return LateRequest.DisplayPenaltyRule;
            }
        }

        [NotMapped]
        public string ConfirmStatusString
        {
            get
            {
                if (LateRequest == null)
                    return "<span class=\"label label-default\">Chờ xác nhận</span>";

                if (LateRequest.TotalLateMinutes <= 0)
                    return "<span class=\"label label-info\">Không Rule</span>";

                return StatusDisplay(LateRequest.ConfirmedStatus);
            }
        }

        [NotMapped]
        public int ConfirmStatusValue
        {
            get
            {
                if (LateRequest == null || LateRequest.TotalLateMinutes <= 0)
                    return 0;

                return (int)LateRequest.ConfirmedStatus;
            }
        }

        [NotMapped]
        public string ActualStartTime
        {
            get
            {
                if (LateRequest == null || LateRequest.ActualWorkTime == DateTime.MinValue)
                    return string.Empty;

                CultureInfo.CurrentCulture = new CultureInfo("vi-VN", false);

                return LateRequest.ActualWorkTime.ToString("dddd, dd MMM yyyy, HH:mm:ss");
            }
        }

        [NotMapped]
        public bool CanApproval
        {
            get
            {
                if (LateRequest == null || LateRequest.TotalLateMinutes <= 0)
                    return false;

                if (LateRequest.ConfirmedStatus != ConfirmStatus.Inprogress)
                    return false;

                return true;
            }
        }

        private static string StatusDisplay(ConfirmStatus status)
        {
            switch (status)
            {
                case ConfirmStatus.Inprogress:
                    return
                        $"<span class=\"label label-default\">{status.GetDisplayName()}</span>";
                case ConfirmStatus.Approved:
                    return $"<span class=\"label label-success\">{status.GetDisplayName()}</span>";
                case ConfirmStatus.Rejected:
                    return $"<span class=\"label label-danger\">{status.GetDisplayName()}</span>";
                case ConfirmStatus.Expired:
                    return $"<span class=\"label label-danger\">{status.GetDisplayName()}</span>";
                case ConfirmStatus.ApproveWithReason:
                    return $"<span class=\"label label-warning\">{status.GetDisplayName()}</span>";
                default:
                    return $"<span class=\"label label-default\">{status.GetDisplayName()}</span>";
            }
        }
    }
}