using System.Reflection;
using System.Threading;
using System.Collections;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;
using CMS.Shared.Data.Annotations;
using System.Collections.Generic;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_WorkingLateRequest")]
    public class WorkingLateRequest : BaseEntity<long>
    {
        private string _penaltyRule;
        public WorkingLateRequest()
        {
            ConfirmedStatus = ConfirmStatus.Inprogress;
            CreateDate = DateTime.Now;
        }
        public long TimeSheetId { get; set; }

        [JsonColumn("ManagerIds")]
        public int[] ManagerIds { get; set; }
        public int RequesterId { get; set; }
        public DateTime ActualWorkTime { get; set; }
        public int TotalLateMinutes { get; set; }

        [Column("LateReviewStatus")]
        public ConfirmStatus ConfirmedStatus { get; private set; }
        [NotMapped]
        public IdentityUser Requester { get; set; }
        [NotMapped]
        public ICollection<IdentityUser> Managers { get; set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public DateTime ExpiredDate { get; private set; }

        public string PenaltyRule
        {
            get
            {
                if (string.IsNullOrEmpty(_penaltyRule))
                {
                    _penaltyRule = CalcPenaltyRule();
                }
                return _penaltyRule;
            }
            private set
            {
                _penaltyRule = value;
            }
        }

        [ForeignKey("TimeSheetId")]
        public virtual WorkingTimesheet WorkTimesheet { get; set; }

        public void UpdateReviewTimeToExpired(DateTime expiredDate)
        {
            ExpiredDate = expiredDate;
            UpdateDate = DateTime.Now;
        }

        public void UpdateConfirmStatus(ConfirmStatus confirmedStatus)
        {
            ConfirmedStatus = confirmedStatus;
            PenaltyRule = CalcPenaltyRule();
            UpdateDate = DateTime.Now;
        }

        private string CalcPenaltyRule()
        {
            var penaltyRule = "0đ";

            if (TotalLateMinutes <= 0)
                return penaltyRule;

            switch (ConfirmedStatus)
            {
                case ConfirmStatus.Inprogress:
                    return string.Empty;
                case ConfirmStatus.Approved:
                    return penaltyRule;
                case ConfirmStatus.ApproveWithReason:
                    return "30,000đ";
                case ConfirmStatus.Expired:
                    return "50,000đ";
                case ConfirmStatus.Rejected:
                    string penaltyRange;
                    if (TotalLateMinutes <= 60)
                        penaltyRange = "20,000đ";
                    else if (TotalLateMinutes <= 120)
                        penaltyRange = "50,000đ";
                    else
                        penaltyRange = "50,000đ + nghỉ làm";
                    return penaltyRange;
                default:
                    return penaltyRule;
            }
        }

        public string DisplayPenaltyRule
        {
            get
            {
                var penaltyRule = string.Empty;

                if (TotalLateMinutes <= 0)
                    penaltyRule = $"<span class=\"label label-success\">0đ</span>";

                switch (ConfirmedStatus)
                {
                    case ConfirmStatus.Inprogress:
                        penaltyRule = $"<span class=\"label label-default\">N/A</span>";
                        break;
                    case ConfirmStatus.Approved:
                        penaltyRule = $"<span class=\"label label-success\">0đ</span>";
                        break;
                    case ConfirmStatus.ApproveWithReason:
                        penaltyRule = $"<span class=\"label label-warning\">30,000đ</span>";
                        break;
                    case ConfirmStatus.Expired:
                        penaltyRule = $"<span class=\"label label-warning\">50,000đ</span>";
                        break;
                    case ConfirmStatus.Rejected:
                        string penaltyRange;

                        if (TotalLateMinutes <= 60)
                            penaltyRange = "20,000đ";
                        else if (TotalLateMinutes <= 120)
                            penaltyRange = "50,000đ";
                        else
                            penaltyRange = "50,000đ + nghỉ làm";

                        penaltyRule = $"<span class=\"label label-danger\">{penaltyRange}</span>";
                        break;
                    default:
                        break;
                }

                return penaltyRule;
            }
        }
    }

    public enum ConfirmStatus : byte
    {
        [Display(Name = "Đang chờ duyệt")]
        Inprogress = 1,
        [Display(Name = "Đã xác nhận")]
        Approved = 2,
        [Display(Name = "Xác nhận quên điểm danh")]
        ApproveWithReason = 3,
        [Display(Name = "Không xác nhận")]
        Rejected = 4,
        [Display(Name = "Đã quá hạn")]
        Expired = 5
    }
}