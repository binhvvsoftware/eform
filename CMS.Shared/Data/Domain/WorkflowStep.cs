﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_WorkflowSteps")]
    public class WorkflowStep : BaseEntity<int>
    {
        public string Name { get; set; }

        public int WorkflowId { get; set; }

        public int Step { get; set; }

        public string UserIds { get; set; }

        public GroupManagement? GroupManagement { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateBy { get; set; }
    }
}
