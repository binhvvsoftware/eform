﻿using CMS.Core.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Menus")]
    public class Menu : BaseEntity<int>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        public string Icon { get; set; }

        public int Position { get; set; }
        [Required]
        public int Parent { get; set; }

        [Required]
        public MenuAction MenuAction { get; set; }

        [ForeignKey("Parent")]
        public virtual Menu MenuParent { get; set; }

        [NotMapped]
        public string MenuParentName => MenuParent?.Name ?? string.Empty;
    }

    public enum MenuAction : byte
    {
        [Display(Name = "Xem")]
        IsView = 0,

        [Display(Name = "Quản lý")]
        IsManage = 1,
    }
}
