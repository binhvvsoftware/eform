﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_RequestDocuments")]
    public class RequestDocument : BaseEntity<int>
    {
        public int WorkflowId { get; set; }

        [ForeignKey("WorkflowId")]
        public virtual Workflow Workflow { get; set; }
        [Required]
        public string JsonContent { get; set; }
        public string HtmlContent { get; set; }
        public string SignatureImage { get; set; }
        public string CaptchaCode { get; set; }
        public RequestStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [ForeignKey("CreateByUserId")]
        public virtual IdentityUser CreateByUser { get; set; }
        public virtual IList<RequestDocumentAssignee> RequestDocumentAssignees { get; set; }
        [NotMapped]
        public virtual IList<RequestDocumentAssignee> CurrentReviewSteps { get; set; }
        [NotMapped]
        public virtual IList<RequestDocumentAssignee> CompleteReviewSteps { get; set; }

        public bool Enabled { get; set; }

        public bool IsValidSignature()
        {
            return true;
        }
    }

    public enum RequestStatus : byte
    {
        [Display(Name = "Lưu tạm")]
        Saved = 0,
        [Display(Name = "Đang chờ duyệt")]
        Inprogress = 2,
        [Display(Name = "Từ chối duyệt")]
        Rejected = 3,
        [Display(Name = "Đã duyệt")]
        Approved = 4,
    }
}