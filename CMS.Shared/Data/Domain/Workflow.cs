﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Workflows")]
    public class Workflow : BaseEntity<int>
    {
        public string Name { get; set; }

        public int CampusId { get; set; }

        public int FormTemplateId { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateBy { get; set; }

        public UserType WorkflowType { get; set; }
    }

    public enum GroupManagement : byte
    {
        [Display(Name = "Quản lý cấp trên")]
        SuperiorManagement = 1,

        [Display(Name = "Quản lý trên 1 cấp")]
        UpSuperiorManagement = 2,
    }
}