﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_ReqDocumentAssignee")]
    public class RequestDocumentAssignee : BaseEntity<int>
    {
        public int RequestDocumentId { get; set; }

        public int UserId { get; set; }

        public RequestStatus Status { get; set; }

        public int Step { get; set; }

        public string SignatureImage { get; set; }
        public string CaptchaCode { get; set; }

        public string Comment { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        [ForeignKey("UserId")]
        public IdentityUser User { get; set; }

        [ForeignKey("RequestDocumentId")]
        public virtual RequestDocument ReviewDocument {get; set;}

        public string GetReviewerComment()
        {
            return $"{User?.FullName} - {UpdateDate:D}: {Comment}{Environment.NewLine}";
        }
    }
}