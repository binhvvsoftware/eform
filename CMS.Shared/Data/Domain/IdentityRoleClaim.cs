﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_IdentityRoleClaims")]
    public class IdentityRoleClaim : IdentityRoleClaim<int>
    {
        [Key]
        public override int Id { get; set; }
    }
}
