﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_ParentDepartment")]
    public class ParentDepartment : BaseEntity<int>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public bool Status { get; set; }

        public string Description { get; set; }

        [NotMapped]
        public ICollection<Campus> Campuses { get; set; }

    }
}
