﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_MemberRelation")]
    public class MemberRelation : BaseEntity<int>
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int ManagerId { get; set; }

        [ForeignKey("UserId")]
        [NotMapped]
        public virtual IdentityUser User { get; set; }

        [ForeignKey("ManagerId")]
        [NotMapped]
        public virtual IdentityUser Manager { get; set; }
    }


}
