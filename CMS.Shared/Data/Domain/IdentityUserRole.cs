﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;
using Microsoft.AspNetCore.Identity;

namespace CMS.Shared.Data.Domain
{
    /// <summary>
    /// Represents a role in the identity system
    /// </summary>
    [Table("Cms_IdentityUserRoles")]
    public class IdentityUserRole : BaseEntity<int>
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }
    }
}
