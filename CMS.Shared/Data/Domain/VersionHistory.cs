﻿using CMS.Core.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Versions_Histories")]
    public class VersionHistory : BaseEntity<int>
    {
        [Required]
        public int TypeId { get; set; }

        [Required]
        public string EntityId { get; set; }

        [Required]
        public string Values { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }
    }

    [Table("Cms_Versions_Entities")]
    public class EntityVersion : BaseEntity<int>
    {
        [Required]
        public string Type { get; set; }
    }
}
