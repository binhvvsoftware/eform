﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Department")]
    public class Department : BaseEntity<int>
    {
        [Required]
        public int Order { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public bool Status { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [Required]
        public int CampusId { get; set; }

        [ForeignKey("CampusId")]
        public virtual Campus RootCampus { get; set; }

        [NotMapped]
        public string CampusName => RootCampus?.Name ?? string.Empty;

        [NotMapped]
        public ICollection<IdentityUser> Users { get; set; }
    }
}
