﻿using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_IdentityUserLogins")]
    public class IdentityUserLogin : BaseEntity<int>
    {
        public int UserId { get; set; }
    }
}
