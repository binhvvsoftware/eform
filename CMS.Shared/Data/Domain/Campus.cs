﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_Campus")]
    public class Campus : BaseEntity<int>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [Required]
        public bool Status { get; set; }

        [Required]
        public int ParentDepartmentId { get; set; }

        [ForeignKey("ParentDepartmentId")]
        public virtual ParentDepartment RootDepartment { get; set; }

        [NotMapped]
        public string RootDepartmentName => RootDepartment?.Name ?? string.Empty;

        [NotMapped]
        public ICollection<Department> Departments { get; set; }
    }
}
