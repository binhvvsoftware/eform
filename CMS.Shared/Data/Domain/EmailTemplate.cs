﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Core.Entity;

namespace CMS.Shared.Data.Domain
{
    [Table("Cms_EmailTemplates")]
    public class EmailTemplate : BaseEntity<int>
    {
        [Required, MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Subject { get; set; }

        public string Body { get; set; }

        [MaxLength(255)]
        public string EmailAddress { get; set; }

        [MaxLength(255)]
        public string CcEmailAddress { get; set; }

        [MaxLength(255)]
        public string BccEmailAddress { get; set; }

        public bool Enabled { get; set; }
    }
}
