﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Shared.Data.Annotations
{
    [AttributeUsage(AttributeTargets.Field|AttributeTargets.Property)]
    public class JsonColumnAttribute:NotMappedAttribute
    {
        public string Name { get; }
        public string TypeName { get; }

        public JsonColumnAttribute()
        {
        }

        public JsonColumnAttribute(string name, string typeName = null)
        {
            Name = name;
            TypeName = typeName;
        }
    }
}
