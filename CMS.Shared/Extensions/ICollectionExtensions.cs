﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Shared.Extensions
{
    public static class ICollectionExtensions
    {
        private static readonly string[] KeyIgnore = new[] { "__RequestVerificationToken", "HtmlContent" };

        public static IEnumerable<T> Trim<T>(this IList<T> list, Func<T, bool> predicate)
        {
            int size = list.Count;
            for (; size > 0; size--)
            {
                if (!predicate(list[size - 1])) break;
            }

            return list.Take(size);
        }

        public static IDictionary<string, string> ToDictionary(this IFormCollection collection)
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var item in collection)
            {
                if (!KeyIgnore.Contains(item.Key))
                {
                    dictionary[item.Key] = item.Value;
                }
            }

            //collection.Keys.ToDictionary(k => k, v => collection[v].ToString());

            return dictionary;
        }

        public static bool TryGetValue<T>(this IFormCollection collection, string key, out T value)
        {
            value = default(T);

            try
            {
                if (collection.TryGetValue(key, out var val) && !string.IsNullOrEmpty(val))
                {
                    value = ConvertValue<T>(val);

                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static T ConvertValue<T>(string value)
        {
            if (typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), value);

            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}
