﻿using System.Collections.Generic;

namespace CMS.Shared.Extensions
{
    public static class LinqExtensions
    {
        public static LinkedList<T> ToLinkList<T>(this IEnumerable<T> collection)
        {
            return new LinkedList<T>(collection);
        }

        /// <summary>
        /// join strings into single string, default sperator is semi colon ;
        /// </summary>
        /// <param name="items"></param>
        /// <param name="separator">seperator</param>
        /// <returns>joined string</returns>
        public static string SafeJoin(this IEnumerable<string> items, char separator= ';')
        {
            return items == null ? string.Empty : string.Join(separator, items);
        }
    }
}
