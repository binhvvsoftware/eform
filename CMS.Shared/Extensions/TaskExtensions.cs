﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Shared.Extensions
{
    public static class TaskExtensions
    {
        public static Task<T> AsTask<T>(this T result)
        {
            return Task.FromResult(result);
        }

        public static async Task<T> Unboxing<T>(this Task<object> t)
        {
            var obj = await t;
            return (T) obj;
        }

        public static async Task<object> Boxing<T>(this Task<T> t)
        {
            var obj = await t;
            return obj;
        }

        public static async Task<T> CancelAfter<T>(this TaskCompletionSource<T> t, TimeSpan due, Func<T> @default = null)
        {
            var d = Task.Delay(due);
            Exception ex = null;

            try
            {
                await Task.WhenAny(d, t.Task);
            }
            catch (Exception e)
            {
                ex = e;
            }

            if (t.Task.Status == TaskStatus.RanToCompletion)
                return t.Task.Result;

            if (@default != null)
                return @default();

            throw ex ?? new TimeoutException();
        }

        public static async Task<T[]> ToArrayAsync<T>(this Task<IEnumerable<T>> source)
        {
            var items = await source;
            return items.ToArray();
        }
    }
}
