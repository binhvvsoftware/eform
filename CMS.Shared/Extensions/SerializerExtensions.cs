﻿using System.IO;
using CMS.Core.Serialization;
using Newtonsoft.Json;

namespace CMS.Shared.Extensions
{
    public static class SerializerExtensions
    {
        public static readonly JsonSerializerSettings NoTypeJsonSetting = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.None,
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore
        };

        public static byte[] Serialize<T>(this ISerializer s, T data)
        {
            using (var ms = new MemoryStream())
            {
                s.Serialize(data, ms);
                return ms.ToArray();
            }
        }

        public static T Deserialize<T>(this ISerializer s, byte[] bytes)
        {
            if (bytes == null)
            {
                return default(T);
            }

            using (var ms = new MemoryStream(bytes))
            {
                return s.Deserialize<T>(ms);
            }
        }
    }
}
