﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using CMS.Core.Extensions;

namespace CMS.Shared.Extensions
{
    public static class DateTimeExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsOverlap(this DateTime[][] ranges, DateTime[] range)
        {
            if (ranges.IsNullOrEmpty()) return false;
            
            return ranges.Any(r => IsOverlap(r, range));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsNotOverlap(this DateTime[][] ranges, DateTime[] range)
        {
            if (ranges.IsNullOrEmpty()) return true;
            
            return ranges.All(r => !IsOverlap(r, range));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsOverlap(this DateTime[] ra, DateTime[] rb)
        {
            if (rb[0] > ra[1]) return false; 
            if( rb[1] < ra[0]) return false; 

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static DateTime EndOfWeek(this DateTime dt, DayOfWeek endOfWeek)
        {
            var diff = endOfWeek - dt.DayOfWeek;
            return dt.AddDays(diff).Date;
        }
    }
}
