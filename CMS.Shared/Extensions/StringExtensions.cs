﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using CMS.Core.Extensions;

namespace CMS.Shared.Extensions
{
    public static class StringExtensions
    {
        private static readonly char[] DefaultSeprators = { ';', ',' };
        private static readonly string DefaultLastIndexOf = @"\";

        /// <summary>
        /// Converts a string that has been HTML-encoded for HTTP transmission into a decoded string.
        /// </summary>
        /// <param name="s">The string to decode.</param>
        /// <returns>A decoded string</returns>
        public static string HtmlDecode(this string s)
        {
            return WebUtility.HtmlDecode(s);
        }

        /// <summary>
        /// Converts a string to an HTML-encoded string.
        /// </summary>
        /// <param name="s">The string to encode.</param>
        /// <returns>An encoded string.</returns>
        public static string HtmlEncode(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            return WebUtility.HtmlEncode(s);
        }

        public static string RemoveBetween(this string str, char begin, char end)
        {
            var regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(str, string.Empty);
        }

        /// <summary>
        /// split string into array, default seperators are semicolon ; and colon ,
        /// </summary>
        /// <param name="str"></param>
        /// <param name="seperators">seprators</param>
        /// <returns></returns>
        public static string[] SafeSplit(this string str, char[] seperators = null)
        {
            if (seperators.IsNullOrEmpty())
                seperators = DefaultSeprators;

            if (str.IsNullOrEmpty()) return Array.Empty<string>();

            return str.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SafeSplit(this string str, string[] seperators)
        {
            if (str.IsNullOrEmpty() || seperators.IsNullOrEmpty()) return Array.Empty<string>();

            return str.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string JsEncode(this string value, bool appendQuotes = true)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            if (appendQuotes)
            {
                sb.Append("\"");
            }

            foreach (char c in value)
            {
                switch (c)
                {
                    case '\"': sb.Append("\\\""); break;
                    case '\\': sb.Append("\\\\"); break;
                    case '\b': sb.Append("\\b"); break;
                    case '\f': sb.Append("\\f"); break;
                    case '\n': sb.Append("\\n"); break;
                    case '\r': sb.Append("\\r"); break;
                    case '\t': sb.Append("\\t"); break;
                    default:
                        var i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else { sb.Append(c); }
                        break;
                }
            }

            if (appendQuotes)
            {
                sb.Append("\"");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Adds a pair of single quotes to the specified System.String.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string AddSingleQuotes(this string s)
        {
            return string.Concat("'", s, "'");
        }

        public static string ToFixedString(this decimal value)
        {
            return value.ToString("G29", CultureInfo.InvariantCulture);
        }

        public static string ToFixedStringWithSeparator(this decimal value)
        {
            return value.ToString("#,###0.############################", CultureInfo.InvariantCulture);
        }

        public static string ToShortDateFormat(this DateTime dt)
        {
            if (dt == DateTime.MinValue)
            {
                return string.Empty;
            }
            return dt.ToString(Constants.ShortDatePattern);
        }

        public static string ToShortDateFormat(this DateTime? dt)
        {
            if (!dt.HasValue || dt.Value == DateTime.MinValue)
            {
                return string.Empty;
            }
            return dt.Value.ToString(Constants.ShortDatePattern);
        }

        public static string ToFullDateFormat(this DateTime dt)
        {
            if (dt == DateTime.MinValue)
            {
                return string.Empty;
            }
            return dt.ToString(Constants.FullDatePattern);
        }

        public static string ToFullDateFormat(this DateTime? dt)
        {
            if (!dt.HasValue || dt.Value == DateTime.MinValue)
            {
                return string.Empty;
            }
            return dt.Value.ToString(Constants.FullDatePattern);
        }

        const string RegexArabicAndHebrew = @"[\u0600-\u06FF,\u0590-\u05FF]+";
        private static readonly Regex regexValidEmail = new Regex(@"[\w-]+@([\w-]+\.)+[\w-]+");
        internal static char[] LatinChars = new[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        internal static Random Random = new Random();

        public static string RandomString(int length)
        {
            var arr = new char[length];
            for (var i = 0; i < length; i++)
            {
                arr[i] = LatinChars[Random.Next(LatinChars.Length)];
            }
            return new string(arr);
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool Contains(this string source, string text, StringComparison comparison)
        {
            if (string.IsNullOrEmpty(source)) return false;
            return source.IndexOf(text, comparison) >= 0;
        }

        public static bool ContainsAny(this string source, IEnumerable<string> texts)
        {
            foreach (var t in texts)
            {
                if (source.Contains(t)) return true;
            }
            return false;
        }

        public static bool ContainsAny(this string source, params string[] texts)
        {
            foreach (var t in texts)
            {
                if (source.Contains(t)) return true;
            }
            return false;
        }

        public static TValue As<TValue>(this string value)
        {
            return value.As(default(TValue));
        }

        public static TValue As<TValue>(this string value, TValue defaultValue)
        {
            try
            {
                return (TValue)Convert.ChangeType(value, typeof(TValue));
            }
            catch
            {
                return defaultValue;
            }
        }

        public static bool AsBool(this string value)
        {
            return value.AsBool(false);
        }

        public static bool AsBool(this string value, bool defaultValue)
        {
            bool flag;
            if (!bool.TryParse(value, out flag))
            {
                return defaultValue;
            }
            return flag;
        }

        public static DateTime AsDateTime(this string value)
        {
            return value.AsDateTime(new DateTime());
        }

        public static DateTime AsDateTime(this string value, DateTime defaultValue)
        {
            DateTime time;
            if (!DateTime.TryParse(value, out time))
            {
                return defaultValue;
            }
            return time;
        }

        public static decimal AsDecimal(this string value)
        {
            return value.As<decimal>();
        }

        public static decimal AsDecimal(this string value, decimal defaultValue)
        {
            return value.As(defaultValue);
        }

        public static float AsFloat(this string value)
        {
            return value.AsFloat(0f);
        }

        public static float AsFloat(this string value, float defaultValue)
        {
            float num;
            if (!float.TryParse(value, out num))
            {
                return defaultValue;
            }
            return num;
        }

        public static Guid AsGuid(this string value)
        {
            return string.IsNullOrEmpty(value) ? Guid.Empty : new Guid(value);
        }

        public static int AsInt(this string value)
        {
            return value.AsInt(0);
        }

        public static int AsInt(this string value, int defaultValue)
        {
            int num;
            if (!int.TryParse(value, out num))
            {
                return defaultValue;
            }
            return num;
        }

        public static bool Is<TValue>(this string value)
        {
            try
            {
                // ReSharper disable once UnusedVariable
                var obj = Convert.ChangeType(value, typeof(TValue));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsBool(this string value)
        {
            bool flag;
            return bool.TryParse(value, out flag);
        }

        public static bool IsDateTime(this string value)
        {
            DateTime time;
            return DateTime.TryParse(value, out time);
        }

        public static bool IsDecimal(this string value)
        {
            return value.Is<decimal>();
        }

        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsFloat(this string value)
        {
            float num;
            return float.TryParse(value, out num);
        }

        public static bool IsInt(this string value)
        {
            int num;
            return int.TryParse(value, out num);
        }

        public static string SafeTrim(this string str, params char[] trimChars)
        {
            return string.IsNullOrEmpty(str) ? str : str.Trim(trimChars);
        }

        public static string ToSlugUrl(this string value)
        {
            string stringFormKd = value.Normalize(NormalizationForm.FormKD);
            var stringBuilder = new StringBuilder();

            foreach (char character in stringFormKd)
            {
                UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(character);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(character);
                }
            }

            // Replace some characters
            stringBuilder.Replace(",", "-").Replace(".", "-").Replace("&", "-").Replace("?", "-");

            var slug = stringBuilder.ToString().Normalize(NormalizationForm.FormKC);

            //First to lower case
            slug = slug.ToLowerInvariant();

            if (!IsRightToLeft(slug))
            {
                //Remove all accents
                //                var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(slug);
                //                slug = Encoding.ASCII.GetString(bytes);

                //Remove invalid chars
                slug = Regex.Replace(slug, @"[^a-z0-9\s-_]", string.Empty, RegexOptions.Compiled);
            }

            //Replace spaces
            slug = Regex.Replace(slug, @"\s", "-", RegexOptions.Compiled);

            //Trim dashes from end
            slug = slug.Trim('-', '_');

            //Replace double occurences of - or _
            slug = Regex.Replace(slug, @"([-_]){2,}", "$1", RegexOptions.Compiled);

            return slug;
        }

        public static bool IsRightToLeft(this string value)
        {
            if (Regex.IsMatch(value, RegexArabicAndHebrew, RegexOptions.IgnoreCase))
            {
                return true;
            }
            return false;
        }

        public static bool IsValidEmailAddress(this string mailAddress)
        {
            return !string.IsNullOrEmpty(mailAddress) && regexValidEmail.IsMatch(mailAddress);
        }

        /// <summary>
        /// converts from a string Base64 representation to an array of bytes
        /// </summary>
        public static byte[] FromBase64(this string base64Encoded)
        {
            if (string.IsNullOrEmpty(base64Encoded))
            {
                return null;
            }
            try
            {
                return Convert.FromBase64String(base64Encoded);
            }
            catch (FormatException ex)
            {
                throw new FormatException("The provided string does not appear to be Base64 encoded:" + Environment.NewLine + base64Encoded + System.Environment.NewLine, ex);
            }
        }

        /// <summary>
        /// converts from an array of bytes to a string Base64 representation
        /// </summary>
        public static string ToBase64(this byte[] b)
        {
            if (b == null || b.Length == 0)
            {
                return "";
            }
            return Convert.ToBase64String(b);
        }

        public static string SpacePascal(this string pascalText)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < pascalText.Length; i++)
            {
                char a = pascalText[i];
                if (char.IsUpper(a) && i + 1 < pascalText.Length && !char.IsUpper(pascalText[i + 1]))
                {
                    if (sb.Length > 0)
                    { sb.Append(' '); }
                    sb.Append(a);
                }
                else { sb.Append(a); }
            }

            return sb.ToString();
        }

        public static string Sub(this string txt, int skip, int take)
        {
            take = Math.Min(take, txt.Length - skip);
            return txt.Substring(skip, take);
        }

        public static string Or(this string s1, string s2)
        {
            return s1.IsNullOrEmpty() ? s2 : s1;
        }

        /// <summary>
        /// WildCard like pattern
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="pattern">wildcard pattern</param>
        /// <param name="ignoreCase">ignoreCase</param>
        /// <returns>true if text is match with wildcard pattern</returns>
        public static bool IsLike(this string text, string pattern, bool ignoreCase = false)
        {
            pattern = pattern.Replace(".", @"\.")
                .Replace("?", ".")
                .Replace("*", ".*?")
                .Replace(@"\", @"\\")
                .Replace(" ", @"\s");

            return new Regex(pattern, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None).IsMatch(text);
        }

        public static string GeLastDisplay(this string str, string lastIndexOf = null)
        {
            try
            {
                if (string.IsNullOrEmpty(lastIndexOf))
                    return str.Substring(str.LastIndexOf(DefaultLastIndexOf) + 1);
                else return str.Substring(str.LastIndexOf(lastIndexOf) + 1);
            }
            catch
            {
                return str;
            }
        }
    }
}