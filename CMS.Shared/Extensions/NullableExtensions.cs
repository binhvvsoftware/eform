﻿namespace CMS.Shared.Extensions
{
    public static class NullableExtensions
    {
        public static bool IsNullOrDefault<T>(this T? value) where T : struct
        {
            return value == null || value.Value.Equals(default(T));
        }
    }
}
