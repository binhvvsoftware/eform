﻿using CMS.Core.Extensions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace CMS.Shared.Extensions
{
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, value.JsonSerialize());
        }

        public static Task<bool> TryGet<T>(this ISession session, string key, out T value)
        {
            value = default(T);

            var sessionValue = session.GetString(key);

            if (string.IsNullOrEmpty(sessionValue))
            {
                return Task.FromResult(false);
            }
            else
            {
                value = sessionValue.JsonDeserialize<T>();

                return Task.FromResult(true);
            }
        }
    }
}
