﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GoQuoEngine.Collections;

namespace GoQuoEngine.Extensions
{
    public static class RepositoryExtensions
    {
        public static async Task<IPagedList<TR>> FindManyAsync<TR, TK>(this IRepository<TR, TK> repo, Expression<Func<TR, bool>> expression = null, FindOptions<TR> findOptions = null, int batchSize = 500) where TR : BaseEntity<TK>
        {

            var count = await repo.CountAsync(expression);
            var records = new List<TR>();

            var limit = batchSize;

            if (findOptions == null)
            {
                findOptions = new FindOptions<TR> { Skip = 0, Limit = null };
                findOptions.SortAscending(x => true);
            }

            if (findOptions.Skip == null)
                findOptions.Skip = 0;

            var take = findOptions.Limit ?? int.MaxValue;

            if (take < 0)
                take = int.MaxValue;

            findOptions.Limit = limit;

            for (var i = 0; i < count; i += limit)
            {
                var founds = (await repo.FindAsync(expression, findOptions)).ToList();

                records.AddRange(founds);

                findOptions.Skip += founds.Count;
                findOptions.Limit = Math.Min(limit, take - records.Count);

                if (founds.Count < limit || records.Count >= take) break;
            }
            return new PagedList<TR>(records, count);
        }

        public static async Task<IPagedList<TX>> FindManyAsync<TR, TK, TX>(this IRepository<TR, TK> repo, Expression<Func<TR, TX>> projection, Expression<Func<TR, bool>> expression, FindOptions<TR> findOptions = null, int batchSize = 500) where TR : BaseEntity<TK>
        {
            var count = await repo.CountAsync(expression);
            var records = new List<TX>();

            var limit = batchSize;

            if (findOptions == null)
            {
                findOptions = new FindOptions<TR> { Skip = 0, Limit = null };
                findOptions.SortAscending(x => true);
            }

            if (findOptions.Skip == null)
                findOptions.Skip = 0;

            var take = findOptions.Limit ?? int.MaxValue;

            if (take < 0)
                take = int.MaxValue;

            findOptions.Limit = limit;

            for (var i = 0; i < count; i += limit)
            {
                var founds = (await repo.FindAsync(expression, projection, findOptions)).ToList();

                records.AddRange(founds);

                findOptions.Skip += founds.Count;
                findOptions.Limit = Math.Min(limit, take - records.Count);

                if (founds.Count < limit || records.Count >= take) break;
            }
            return new PagedList<TX>(records, count);
        }

        public static IPagedList<TR> FindMany<TR, TK>(this IRepository<TR, TK> repo, Expression<Func<TR, bool>> expression = null, FindOptions<TR> findOptions = null, int batchSize = 500) where TR : BaseEntity<TK>
        {
            var count = repo.Count(expression);
            var records = new List<TR>();

            var limit = batchSize;

            if (findOptions == null)
            {
                findOptions = new FindOptions<TR> { Skip = 0, Limit = null };
                findOptions.SortAscending(x => true);
            }

            if (findOptions.Skip == null)
                findOptions.Skip = 0;

            var take = findOptions.Limit ?? int.MaxValue;

            if (take < 0)
                take = int.MaxValue;

            findOptions.Limit = limit;

            for (var i = 0; i < count; i += limit)
            {
                var founds = repo.Find(expression, findOptions).ToList();

                records.AddRange(founds);

                findOptions.Skip += founds.Count;
                findOptions.Limit = Math.Min(limit, take - records.Count);

                if (founds.Count < limit || records.Count >= take) break;
            }
            return new PagedList<TR>(records, count);
        }
    }
}
