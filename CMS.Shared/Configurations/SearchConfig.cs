﻿using System;

namespace CMS.Shared.Configurations
{
    public class SearchConfig
    {
        private static readonly TimeSpan DefaultExpiry = TimeSpan.FromMinutes(30);

        private TimeSpan expiryTime;
        public TimeSpan SearchTimeOut { get; set; }

        public TimeSpan ExpiryTime
        {
            get { return (expiryTime <= TimeSpan.Zero) ? DefaultExpiry : expiryTime; }
            set { expiryTime = value; }
        }
    }
}
