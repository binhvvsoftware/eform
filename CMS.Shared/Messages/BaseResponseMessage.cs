﻿using System;

namespace CMS.Shared.Messages
{
    [Serializable]
    public class BaseResponseMessage
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
