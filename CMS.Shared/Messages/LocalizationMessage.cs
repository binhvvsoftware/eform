﻿namespace CMS.Shared.Messages
{
    public class LocalizationMessage
    {
        public string Key { get; set; }

        public object[] Args { get; set; }
    }
}
