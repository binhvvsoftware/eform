﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Shared.Compare
{
    /// <summary>
    /// Class that allows comparison of two objects of the same type to each other.  Supports classes, lists, arrays, dictionaries, child comparison and more.
    /// </summary>
    /// <example>
    /// CompareLogic compareLogic = new CompareLogic();
    ///
    /// Person person1 = new Person();
    /// person1.DateCreated = DateTime.Now;
    /// person1.Name = "Greg";
    ///
    /// Person person2 = new Person();
    /// person2.Name = "John";
    /// person2.DateCreated = person1.DateCreated;
    ///
    /// ComparisonResult result = compareLogic.Compare(person1, person2);
    ///
    /// if (!result.AreEqual)
    ///    Console.WriteLine(result.DifferencesString);
    ///
    /// </example>
    public class CompareLogic : ICompareLogic
    {
        #region Properties

        /// <summary>
        /// The default configuration
        /// </summary>
        public ComparisonConfig Config { get; set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Set up defaults for the comparison
        /// </summary>
        public CompareLogic()
        {
            Config = new ComparisonConfig();
        }

        /// <summary>
        /// Pass in the configuration
        /// </summary>
        /// <param name="config"></param>
        public CompareLogic(ComparisonConfig config)
        {
            Config = config;
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Compare two objects of the same type to each other.
        /// </summary>
        /// <remarks>
        /// Check the Differences or DifferencesString Properties for the differences.
        /// Default MaxDifferences is 1 for performance
        /// </remarks>
        /// <param name="object1"></param>
        /// <param name="object2"></param>
        /// <returns>True if they are equal</returns>
        public ComparisonResult Compare(object object1, object object2)
        {
            ComparisonResult result = new ComparisonResult(Config);

#if !PORTABLE
            result.Watch.Start();
#endif

            RootComparer rootComparer = RootComparerFactory.GetRootComparer();

            CompareParms parms = new CompareParms
            {
                Config = Config,
                Result = result,
                Object1 = object1,
                Object2 = object2,
                BreadCrumb = string.Empty
            };

            rootComparer.Compare(parms);

            if (Config.AutoClearCache)
                ClearCache();

#if !PORTABLE
            result.Watch.Stop();
#endif

            return result;
        }

        /// <summary>
        /// Reflection properties and fields are cached. By default this cache is cleared automatically after each compare.
        /// </summary>
        public void ClearCache()
        {
            Cache.ClearCache();
        }

        #endregion Public Methods
    }
}
