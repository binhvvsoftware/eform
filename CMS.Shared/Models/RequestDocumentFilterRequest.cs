using System.Linq.Expressions;
using System;
using CMS.Shared.Data.Domain;

namespace CMS.Shared.Models
{
    public class RequestDocumentFilterRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public RequestStatus? FormFilterStatus { get; set; }
        public RequestStatus? CustomFilterStatus { get; set; }
        public int? WorkflowId { get; set; }
        public int UserId { get; set; }
    }
}