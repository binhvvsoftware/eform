using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CMS.Shared.Models
{
    public partial class HtmlContent
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("required", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HtmlContentRequired { get; set; }

        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }

        [JsonProperty("className", NullValueHandling = NullValueHandling.Ignore)]
        public string ClassName { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("subtype", NullValueHandling = NullValueHandling.Ignore)]
        public string Subtype { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        [JsonProperty("userData", NullValueHandling = NullValueHandling.Ignore)]
        public string[] UserData { get; set; }

        [JsonProperty("undefined", NullValueHandling = NullValueHandling.Ignore)]
        public Undefined[] Undefined { get; set; }

        [JsonProperty("maxlength", NullValueHandling = NullValueHandling.Ignore)]
        public long? Maxlength { get; set; }

        [JsonProperty("values", NullValueHandling = NullValueHandling.Ignore)]
        public Value[] Values { get; set; }

        [JsonProperty("rows", NullValueHandling = NullValueHandling.Ignore)]
        public Rows? Rows { get; set; }

        [JsonProperty("inline", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Inline { get; set; }

        [JsonProperty("columns", NullValueHandling = NullValueHandling.Ignore)]
        public object[] Columns { get; set; }

        [JsonProperty("enabled", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Enabled { get; set; }
    }

    public partial class Undefined
    {
    }

    public partial class Value
    {
        [JsonProperty("selected")]
        public bool Selected { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string ValueValue { get; set; }
    }

    public enum Subtype { Text, Textarea };

    public partial struct Rows
    {
        public object[] AnythingArray;
        public long? Integer;

        public static implicit operator Rows(object[] AnythingArray) => new Rows { AnythingArray = AnythingArray };
        public static implicit operator Rows(long Integer) => new Rows { Integer = Integer };
    }

    public partial class HtmlContent
    {
        public static HtmlContent[] FromJson(string json) => JsonConvert.DeserializeObject<HtmlContent[]>(json, Converter.Settings);
        public static bool TryParse(string json)
        {
            try
            {
                FromJson(json);
            }
            catch (System.Exception e)
            {
                return false;
            }
            return true;
        }
    }

    public static class HtmlSerialize
    {
        public static string ToJson(this HtmlContent[] self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class HtmlConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                RowsConverter.Singleton,
                SubtypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class RowsConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Rows) || t == typeof(Rows?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Integer:
                    var integerValue = serializer.Deserialize<long>(reader);
                    return new Rows { Integer = integerValue };
                case JsonToken.StartArray:
                    var arrayValue = serializer.Deserialize<object[]>(reader);
                    return new Rows { AnythingArray = arrayValue };
            }
            throw new Exception("Cannot unmarshal type Rows");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (Rows)untypedValue;
            if (value.Integer != null)
            {
                serializer.Serialize(writer, value.Integer.Value);
                return;
            }
            if (value.AnythingArray != null)
            {
                serializer.Serialize(writer, value.AnythingArray);
                return;
            }
            throw new Exception("Cannot marshal type Rows");
        }

        public static readonly RowsConverter Singleton = new RowsConverter();
    }

    internal class SubtypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Subtype) || t == typeof(Subtype?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "text":
                    return Subtype.Text;
                case "textarea":
                    return Subtype.Textarea;
            }
            throw new Exception("Cannot unmarshal type Subtype");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Subtype)untypedValue;
            switch (value)
            {
                case Subtype.Text:
                    serializer.Serialize(writer, "text");
                    return;
                case Subtype.Textarea:
                    serializer.Serialize(writer, "textarea");
                    return;
            }
            throw new Exception("Cannot marshal type Subtype");
        }

        public static readonly SubtypeConverter Singleton = new SubtypeConverter();
    }
}