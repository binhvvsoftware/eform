﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using CMS.Core.Extensions;
using CMS.Shared.Data.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CMS.Shared.Models
{
    public class AbsentModel
    {
        [JsonProperty("user_name")] public string UserName { get; set; }
        [JsonProperty("user_code")] public string UserCode { get; set; }
        [JsonProperty("position")] public string Position { get; set; }
        [JsonProperty("contract_type")] public string ContractType { get; set; }
        [JsonProperty("room")] public string Room { get; set; }

        [JsonProperty("parent_room")] public string ParentRoom { get; set; }

        [JsonProperty("parent_user")] public string ParentUser { get; set; }

        [JsonProperty("create_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime CreateDate { get; set; }

        [JsonProperty("from_absent_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? FromAbsentDate { get; set; }

        [JsonProperty("to_absent_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? ToAbsentDate { get; set; }

        [JsonProperty("total_absent")] public double TotalAbsent { get; set; }

        [JsonProperty("from_absent_no_salary")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? FromAbsentNoSalary { get; set; }

        [JsonProperty("to_absent_no_salary")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? ToAbsentNoSalary { get; set; }

        [JsonProperty("total_absent_no_salary")]
        public double TotalAbsentNoSalary { get; set; }

        [JsonProperty("reason_absent")] public string ReasonAbsent { get; set; }

        [JsonProperty("orther_nogotiation")] public string OrtherNogotiation { get; set; }

        [JsonProperty("tableContent")] public string TableContent { get; set; }

        public IList<AbsentTable> AbsentContents
        {
            get => TableContent?.JsonDeserialize<List<AbsentTable>>();
        }

        public int UserId { get; set; }
        public int RequestDocumentId { get; set; }
        public bool IsNew { get; set; }

        public bool IsValidateModel()
        {
            if (UserName.IsNullOrEmpty()) return false;
            if (Position.IsNullOrEmpty()) return false;
            if (ContractType.IsNullOrEmpty()) return false;
            if (Room.IsNullOrEmpty()) return false;
            if (ParentRoom.IsNullOrEmpty()) return false;
            if (ParentUser.IsNullOrEmpty()) return false;
            if (!FromAbsentDate.HasValue && !ToAbsentDate.HasValue && !FromAbsentNoSalary.HasValue &&
                !ToAbsentNoSalary.HasValue) return false;
            if (TotalAbsent.Equals(0) && TotalAbsentNoSalary.Equals(0)) return false;

            return true;
        }

        public UserLeaveApplication ToEntity()
        {
            var leaveType = TotalAbsent > 0 ? LeaveType.Paid : LeaveType.UnPaid;
            var entity = new UserLeaveApplication()
            {
                LeaveFrom = FromAbsentDate,
                LeaveTo = ToAbsentDate,
                TotalLeaveTime = TotalAbsent,
                LeaveUnPaidFrom = FromAbsentNoSalary,
                LeaveUnPaidTo = ToAbsentNoSalary,
                TotalLeaveUnPaidTime = TotalAbsentNoSalary,
                UserId = UserId,
                RequestDocumentId = RequestDocumentId,
                CreateAt = CreateDate,
                ApplyStatus = ApplyStatus.NewApply,
                LeaveType = leaveType
            };

            return entity;
        }
    }

    public partial class AbsentTable
    {
        [JsonProperty("itemNumber")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ItemNumber { get; set; }

        [JsonProperty("absent_type")] public string AbsentType { get; set; }

        [JsonProperty("time")] public string Time { get; set; }

        [JsonProperty("accumulated")] public string Accumulated { get; set; }
    }

    public partial class AbsentTable
    {
        public static AbsentTable[] FromJson(string json) =>
            JsonConvert.DeserializeObject<AbsentTable[]>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this AbsentTable[] self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            },
        };
    }

    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            DateTimeFormat = "dd/MM/yyyy";
        }
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }

            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (long) untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}