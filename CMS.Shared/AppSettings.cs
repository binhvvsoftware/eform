﻿using System;

namespace CMS.Shared
{
    public class AppSettings
    {
        public TimeSpan WorkingTime { get; set; }
    }
    public class EmailSettings
    {
        public string Host { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
        public bool IsDebug { get; set; }
        public int Port { get; set; }
    }
}