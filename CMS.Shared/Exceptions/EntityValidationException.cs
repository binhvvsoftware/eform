﻿using System;

namespace CMS.Shared.Exceptions
{
    public class EntityValidationException : Exception
    {
        public EntityValidationException(string message) : base(message)
        {
        }
    }
}
