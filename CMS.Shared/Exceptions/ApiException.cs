﻿using System;

namespace CMS.Shared.Exceptions
{
    public class ApiException : Exception
    {
        public string EventStatus { get; set; }

        public ApiException(string eventStatus)
        {
            EventStatus = eventStatus;
        }

        public ApiException(string eventStatus, string message) : base(message)
        {
            EventStatus = eventStatus;
        }

        public ApiException(string eventStatus, string message, Exception innerException) : base(message, innerException)
        {
            EventStatus = eventStatus;
        }
    }
}
