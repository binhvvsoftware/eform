﻿using System;
using System.Diagnostics;

namespace CMS.Shared
{
    /// <summary>
    /// Helper class that will throw exceptions when conditions are not satisfied.
    /// </summary>
    [DebuggerStepThrough]
    public static class Ensure
    {
        /// <summary>
        /// Ensures that the given expression is true
        /// </summary>
        /// <exception cref="System.Exception">Exception thrown if false condition</exception>
        /// <param name="condition">Condition to test/ensure</param>
        /// <param name="message">Message for the exception</param>
        /// <exception cref="System.Exception">Thrown when <paramref name="condition"/> is false</exception>
        public static void That(bool condition, string message = "")
        {
            That<Exception>(condition, message);
        }

        /// <summary>
        /// Ensures that the given expression is true
        /// </summary>
        /// <typeparam name="TException">Type of exception to throw</typeparam>
        /// <param name="condition">Condition to test/ensure</param>
        /// <param name="message">Message for the exception</param>
        /// <exception>Thrown when
        ///     <cref>TException</cref>
        ///     <paramref name="condition"/> is false</exception>
        /// <remarks><see cref="TException"/> must have a constructor that takes a single string</remarks>
        public static void That<TException>(bool condition, string message = "") where TException : Exception
        {
            if (!condition)
            {
                throw (TException)Activator.CreateInstance(typeof(TException), message);
            }
        }

        /// <summary>
        /// Argument-specific ensure methods
        /// </summary>
        public static class Argument
        {
            /// <summary>
            /// Ensures given value is not null
            /// </summary>
            /// <param name="value">Value to test for null</param>
            /// <param name="paramName">Name of the parameter in the method</param>
            /// <exception cref="System.ArgumentNullException">
            ///     Thrown if <paramref cref="value" /> is null
            /// </exception>
            public static void NotNull(object value, string paramName = "")
            {
                That<ArgumentNullException>(value != null, paramName);
            }
        }
    }
}
