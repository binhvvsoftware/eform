using System.IO;

namespace CMS.Services.Extensions
{
    public static class StreamExtensions
    {
        public static byte[] ToBytesArray(this Stream stream, long length)
        {
            var buffer = new byte[length*1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}