﻿using System.Collections.Generic;
using System.IO;
using CMS.Core.Extensions;
using CMS.Core.Serialization;
using StackExchange.Redis;

namespace CMS.Services.Extensions
{
    public static class SerializerExtensions
    {
        internal static ISerializer Serializer;

        public static void Boot(ISerializer serializer)
        {
            Serializer = serializer;
        }

        public static byte[] WireSerialize<T>(this T data, ISerializer serializer = null)
        {
            using (var ms = new MemoryStream())
            {
                (serializer ?? Serializer).Serialize(data, ms);
                return ms.ToArray();
            }
        }
        
        public static T WireDeserialize<T>(this byte[] buffer, ISerializer serializer = null)
        {
            if (buffer == null)
                return default(T);
            
            using (var ms = new MemoryStream(buffer))
            {
                return (serializer ?? Serializer).Deserialize<T>(ms);
            }
        }
        
        public static T WireDeserialize<T>(this RedisValue redisValue)
        {
            if (redisValue.IsNull)
                return default(T);
            
            return WireDeserialize<T>((byte[]) redisValue);
        }

        public static Dictionary<string, decimal> GetTaxInfo(this string value)
        {
            return string.IsNullOrEmpty(value) ? new Dictionary<string, decimal>() : value.JsonDeserialize<Dictionary<string, decimal>>();
        }
    }
}
