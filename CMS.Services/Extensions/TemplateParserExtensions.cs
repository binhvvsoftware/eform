﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Services.Templates;

namespace CMS.Services.Extensions
{
    public static class TemplateParserExtensions
    {
        public static async Task<string> RenderTemplate(this ITemplateParser templateParser, string template,
            IEnumerable<KeyValuePair<string, object>> tokens)
        {
            if (string.IsNullOrEmpty(template))
            {
                return template;
            }

            dynamic model = new ExpandoObject();
            var modelValues = (IDictionary<string, object>) model;
            foreach (var token in tokens)
            {
                modelValues[token.Key] = token.Value;
            }

            template = await templateParser.RenderTemplate(template, model);

            return template;
        }
    }
}