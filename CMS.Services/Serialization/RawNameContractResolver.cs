﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CMS.Services.Serialization
{
    /// <summary>
    /// This contract resolver is inherited from DefaultContractResolver.
    /// It will by pass property name in JsonProperty Attribute
    /// </summary>
    public class RawNameContractResolver: DefaultContractResolver
    {
        #region Overrides of DefaultContractResolver

        protected override JsonProperty CreateProperty(System.Reflection.MemberInfo member, MemberSerialization memberSerialization)
        {
            var p= base.CreateProperty(member, memberSerialization);
            p.PropertyName = p.UnderlyingName;
            return p;
        }

        #endregion
    }
}
