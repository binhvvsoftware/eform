﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IWorkflowStepService : IGenericService<WorkflowStep, int>
    {
        // Task<WorkflowStep> GetById(int workflowId);
        Task<IEnumerable<WorkflowStep>> GeStepsByWorkflowsId(int workflowId);
    }

    public class WorkflowStepService : GenericService<WorkflowStep, int>, IWorkflowStepService
    {
        public WorkflowStepService(IRepository<WorkflowStep, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<WorkflowStep> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }

        public Task<IEnumerable<WorkflowStep>> GeStepsByWorkflowsId(int workflowId)
        {
            return GetRecordsAsync(x => x.WorkflowId == workflowId);
        }
    }
}
