﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Extensions;
using CMS.Services.Extensions;
using CMS.Services.Templates;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using static CMS.Shared.Constants;

namespace CMS.Services.Services
{
    public interface IEmailService
    {
        Task SendMailAsync(MailMessage mailMessage, SmtpConfig smtpConfig = null, bool isDebug = false);

        Task SendRequestDocumentApproved(RequestDocument requestDocument, RequestDocumentAssignee stepApproved,
            IdentityUser user);

        Task SendNewRequestDocument(RequestDocument requestDocument, IEnumerable<RequestDocumentAssignee> userInSteps,
            IdentityUser requestor);

        Task SendRequestDocumentReject(RequestDocument requestDocument, RequestDocumentAssignee stepRejected,
            IdentityUser user);

        Task SendRequestDocumentCreated(RequestDocument requestDocument, IdentityUser user);

        Task SendEmailUserWorkedLate(WorkingTimesheet workingTime);
        Task SendEmailWorkingLate(WorkingLateRequest lateRequest);
        Task SendLateRequestExpiredNotification(WorkingLateRequest request);
        Task NotifyLateRequestHasUpdated(WorkingLateRequest request);
    }

    public class EmailService : IEmailService
    {
        private readonly IComponentContext componentContext;
        private readonly ILogger _logger;
        private readonly EmailSettings emailSetting;

        public EmailService(IComponentContext componentContext, IOptions<EmailSettings> options, ILogger logger)
        {
            this.componentContext = componentContext;
            _logger = logger;
            if (options == null)
            {
                options = new OptionsWrapper<EmailSettings>(new EmailSettings
                {
                    Email = "eform@fsb.edu.vn",
                    Host = "smtp.gmail.com",
                    Password = "fsb@123aB",
                    EnableSsl = true,
                    Port = 587
                });
            }

            emailSetting = options.Value;
        }

        private SmtpConfig DefaultSmtpConfig => new SmtpConfig()
        {
            Id = 0,
            Email = emailSetting.Email,
            EnableSsl = emailSetting.EnableSsl,
            Host = emailSetting.Host,
            Name = "Eform Smtp",
            Password = emailSetting.Password,
            Port = emailSetting.Port,
            Username = emailSetting.Email
        };

        public async Task SendMailAsync(MailMessage mailMessage, SmtpConfig smtpConfig = null, bool isDebug = false)
        {
            if (mailMessage == null)
                throw new Exception("Email message format wasn't correctly. Pls verify & try again, ");

            try
            {
                if (isDebug) return;

                using (var smtpClient = new SmtpClient())
                {
                    if (smtpConfig != null)
                    {
                        smtpClient.UseDefaultCredentials = smtpConfig.UseDefaultCredentials;
                        smtpClient.Host = smtpConfig.Host;
                        smtpClient.Port = smtpConfig.Port;
                        smtpClient.EnableSsl = smtpConfig.EnableSsl;
                        smtpClient.Credentials = smtpConfig.UseDefaultCredentials
                            ? CredentialCache.DefaultNetworkCredentials
                            : new NetworkCredential(smtpConfig.Username, smtpConfig.Password);

                        mailMessage.From = new MailAddress("eform@fsb.edu.vn", "Efom FSB");
                    }

                    await smtpClient.SendMailAsync(mailMessage);
                }
            }
            catch (Exception e)
            {
                _logger.Error($"SendingEmail. Unable to send email to {mailMessage.To}", e);
            }
        }

        public async Task SendNewRequestDocument(RequestDocument requestDocument,
            IEnumerable<RequestDocumentAssignee> userInSteps, IdentityUser requestor)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x =>
                x.Name == EmailTemplates.RequestDocumentNew);
            var templateParser = componentContext.Resolve<ITemplateParser>();
            var template = await componentContext.Resolve<IWorkflowService>().GetByIdAsync(requestDocument.WorkflowId);
            var userService = componentContext.Resolve<IUserService>();
            var reviewers = await userService.GetRecordsAsync(x => userInSteps.Select(u => u.UserId).Contains(x.Id));

            if (reviewers.IsNullOrEmpty())
                throw new ArgumentNullException("Không tìm thấy người duyệt kế tiếp.");

            if (requestor == null)
                requestor = await userService.GetByIdAsync(requestDocument.CreateByUserId);

            var mail = InitEmailMessage(emailTemplate);

            reviewers.ForEach(u => { mail.To.Add(u.Email); });
            mail.CC.Add(requestor.Email);

            var subject = emailTemplate.Subject;
            var body = emailTemplate.Body;
            var tokens = new Dictionary<string, object>();
            tokens["FormName"] = template.Name;
            tokens["Users"] = reviewers;
            tokens["User"] = requestor;

            if (!requestDocument.SignatureImage.IsNullOrEmpty())
            {
                var signatureImage =
                    $@"{Path.Combine(Directory.GetCurrentDirectory(), ImagePath)}\{requestDocument.SignatureImage}";
                AddImageInLine(mail, signatureImage);
            }

            mail.Subject = (await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens))
                .HtmlDecode();
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.BodyEncoding = Encoding.UTF8;

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        public async Task SendRequestDocumentApproved(RequestDocument requestDocument,
            RequestDocumentAssignee stepApproved, IdentityUser user)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x =>
                x.Name == EmailTemplates.RequestDocumentApproved);
            var templateParser = componentContext.Resolve<ITemplateParser>();
            var workflow = await componentContext.Resolve<IWorkflowService>().GetByIdAsync(requestDocument.WorkflowId);

            var mail = InitEmailMessage(emailTemplate);
            mail.To.Add(user.Email);
            mail.CC.Add(stepApproved.User.Email);

            var subject = emailTemplate.Subject;
            var body = emailTemplate.Body;
            var tokens = new Dictionary<string, object>();
            tokens["FormName"] = workflow.Name;
            tokens["User"] = user;

            // if (!stepApproved.SignatureImage.IsNullOrEmpty())
            // {
            //     var signatureImage =
            //         $@"{Path.Combine(Directory.GetCurrentDirectory(), ImagePath)}\{stepApproved.SignatureImage}";
            //     AddImageInLine(mail, signatureImage);
            // }

            mail.Subject = (await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens))
                .HtmlDecode();
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.BodyEncoding = Encoding.UTF8;

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        public async Task SendRequestDocumentCreated(RequestDocument requestDocument, IdentityUser user)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x =>
                x.Name == EmailTemplates.RequestDocumentCreated);
            var templateParser = componentContext.Resolve<ITemplateParser>();
            var workflow = await componentContext.Resolve<IWorkflowService>().GetByIdAsync(requestDocument.WorkflowId);
            var mail = InitEmailMessage(emailTemplate);
            mail.To.Add(user.Email);

            var subject = emailTemplate.Subject;
            var body = emailTemplate.Body;
            var tokens = new Dictionary<string, object>();
            tokens["FormName"] = workflow.Name;
            tokens["User"] = user;

            // if (!requestDocument.SignatureImage.IsNullOrEmpty())
            // {
            //     var signatureImage =
            //         $@"{Path.Combine(Directory.GetCurrentDirectory(), ImagePath)}\{requestDocument.SignatureImage}";
            //     AddImageInLine(mail, signatureImage);
            // }

            mail.Subject = (await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens))
                .HtmlDecode();
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.BodyEncoding = Encoding.UTF8;

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        public async Task SendRequestDocumentReject(RequestDocument requestDocument,
            RequestDocumentAssignee stepRejected, IdentityUser user)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x =>
                x.Name == EmailTemplates.RequestDocumentRejected);
            var templateParser = componentContext.Resolve<ITemplateParser>();
            var workflow = await componentContext.Resolve<IWorkflowService>().GetByIdAsync(requestDocument.WorkflowId);

            var mail = InitEmailMessage(emailTemplate);

            mail.To.Add(user.Email);
            mail.CC.Add(stepRejected.User.Email);

            var subject = emailTemplate.Subject;
            var body = emailTemplate.Body;
            var tokens = new Dictionary<string, object>();
            tokens["FormName"] = workflow.Name;
            tokens["User"] = user;
            // if (!stepRejected.SignatureImage.IsNullOrEmpty())
            // {
            //     var signatureImage =
            //         $@"{Path.Combine(Directory.GetCurrentDirectory(), ImagePath)}\{stepRejected.SignatureImage}";
            //     AddImageInLine(mail, signatureImage);
            // }

            mail.Subject = (await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens))
                .HtmlDecode();
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.BodyEncoding = Encoding.UTF8;

            await SendMailAsync(mail, DefaultSmtpConfig, true);
        }

        public async Task SendEmailUserWorkedLate(WorkingTimesheet workingTime)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>()
                .GetRecordAsync(x => x.Name == EmailTemplates.WorkingLateTemplate);

            if (emailTemplate is null)
                throw new ArgumentException("Working late email template not found.");

            var templateParser = componentContext.Resolve<ITemplateParser>();

            var mail = new MailMessage();
            mail.To.Add(workingTime.Requester?.Email);
            var subject = emailTemplate.Subject;
            var body = emailTemplate.Body;
            var tokens = new Dictionary<string, object>();
            mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, subject, tokens);
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, body, tokens);
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.BodyEncoding = Encoding.UTF8;

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        private static void AddImageInLine(MailMessage message, string attachmentPath)
        {
            Attachment attachment = new Attachment(attachmentPath);
            attachment.ContentDisposition.Inline = true;
            attachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            attachment.ContentId = Guid.NewGuid().ToString();
            attachment.ContentType.MediaType = "image/png";
            attachment.ContentType.Name = Path.GetFileName(attachmentPath);
            message.Attachments.Add(attachment);
        }

        public async Task SendEmailWorkingLate(WorkingLateRequest lateRequest)
        {
            if (lateRequest.Requester == null)
                throw new ArgumentNullException($"{nameof(WorkingLateRequest.Requester)} not found !");

            if (lateRequest.Requester.IgnoreWorkingRule)
            {
                if (lateRequest.TotalLateMinutes <= 120)
                    await NotifyWorkLateToUser(lateRequest);
                else
                    await NotifyWorkLateToUserAndManagers(lateRequest);
                return;
            }

            await NotifyWorkLateToUserAndManagers(lateRequest);
        }

        private async Task NotifyWorkLateToUser(WorkingLateRequest request)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x => x.Name == EmailTemplates.LateRequestToUserOnly);

            if (emailTemplate is null)
                throw new ArgumentException("Working late email template not found.");

            var templateParser = componentContext.Resolve<ITemplateParser>();

            var mail = InitEmailMessage(emailTemplate);

            mail.To.Add(request.Requester.Email);

            var tokens = new Dictionary<string, object>();

            tokens["TotalLateMinutes"] = request.TotalLateMinutes;
            tokens["ActualWorkTime"] = request.ActualWorkTime;
            tokens["User"] = request.Requester;

            mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Subject, tokens);
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Body, tokens);

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        private async Task NotifyWorkLateToUserAndManagers(WorkingLateRequest request)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x => x.Name == EmailTemplates.LateRequestToUserAndManagers);

            if (emailTemplate is null)
                throw new ArgumentException("Working late email template not found.");

            var templateParser = componentContext.Resolve<ITemplateParser>();

            var mail = InitEmailMessage(emailTemplate);

            mail.To.Add(new MailAddress(request.Requester.Email, request.Requester.FullName));

            if (!request.Managers.IsNullOrEmpty())
            {
                foreach (var manager in request.Managers)
                {
                    mail.To.Add(new MailAddress(manager.Email, manager.FullName));
                }
            }

            var tokens = new Dictionary<string, object>();

            tokens["TotalLateMinutes"] = request.TotalLateMinutes;
            tokens["ActualWorkTime"] = request.ActualWorkTime;
            tokens["Requester"] = request.Requester;
            tokens["Managers"] = request.Managers;

            mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Subject, tokens);
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Body, tokens);

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        public async Task SendLateRequestExpiredNotification(WorkingLateRequest request)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x => x.Name == EmailTemplates.LateRequestExpired);

            if (emailTemplate is null)
                throw new ArgumentException("Working late email template not found.");

            var templateParser = componentContext.Resolve<ITemplateParser>();

            var mail = InitEmailMessage(emailTemplate);

            mail.To.Add(new MailAddress(request.Requester.Email, request.Requester.FullName));

            if (!request.Managers.IsNullOrEmpty())
            {
                foreach (var manager in request.Managers)
                {
                    mail.To.Add(new MailAddress(manager.Email, manager.FullName));
                }
            }

            var tokens = new Dictionary<string, object>
            {
                ["LateTime"] = request.TotalLateMinutes,
                ["User"] = request.Requester
            };
            mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Subject, tokens);
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Body, tokens);

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }

        private static MailMessage InitEmailMessage(EmailTemplate emailTemplate)
        {
            var mail = new MailMessage
            {
                IsBodyHtml = true,
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                Subject = emailTemplate.Subject,
                Body = emailTemplate.Body
            };

            if (!emailTemplate.EmailAddress.IsNullOrEmpty())
                emailTemplate.EmailAddress.SafeSplit().ForEach(x => { mail.To.Add(x); });

            if (!emailTemplate.CcEmailAddress.IsNullOrEmpty())
                emailTemplate.CcEmailAddress.SafeSplit().ForEach(x => { mail.CC.Add(x); });

            if (!emailTemplate.BccEmailAddress.IsNullOrEmpty())
                emailTemplate.BccEmailAddress.SafeSplit().ForEach(x => { mail.Bcc.Add(x); });

            return mail;
        }

        public async Task NotifyLateRequestHasUpdated(WorkingLateRequest request)
        {
            var emailTemplate = await componentContext.Resolve<IEmailTemplateService>().GetRecordAsync(x => x.Name == EmailTemplates.LateRequestUpdated);

            if (emailTemplate is null)
                throw new ArgumentException("Working late email template not found.");

            var templateParser = componentContext.Resolve<ITemplateParser>();

            var tokens = new Dictionary<string, object>();

            var mail = InitEmailMessage(emailTemplate);

            mail.To.Add(new MailAddress(request.Requester.Email, request.Requester.FullName));

            if (!request.Managers.IsNullOrEmpty())
            {
                foreach (var manager in request.Managers)
                {
                    mail.To.Add(new MailAddress(manager.Email, manager.FullName));
                }
                tokens["Managers"] = request.Managers;
            }


            tokens["LateTime"] = request.TotalLateMinutes;
            tokens["User"] = request.Requester;
            tokens["CurrentStatus"] = request.ConfirmedStatus.GetDisplayName();
            tokens["PenaltyRule"] = request.PenaltyRule;
            mail.Subject = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Subject, tokens);
            mail.Body = await TemplateParserExtensions.RenderTemplate(templateParser, mail.Body, tokens);

            await SendMailAsync(mail, DefaultSmtpConfig, emailSetting.IsDebug);
        }
    }
}