﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IMemberRelationService : IGenericService<MemberRelation, int>
    {
    }

    public class MemberRelationService : GenericService<MemberRelation, int>, IMemberRelationService
    {
        public MemberRelationService(IRepository<MemberRelation, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<MemberRelation> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
