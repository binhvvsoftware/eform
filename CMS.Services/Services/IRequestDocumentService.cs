﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Linq;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;
using CMS.Shared.Models;
using Microsoft.Extensions.Logging;

namespace CMS.Services.Services
{
    public interface IRequestDocumentService : IGenericService<RequestDocument, int>
    {
        Task<ICollection<RequestDocument>> GetReviewDocuments(RequestDocumentFilterRequest request);
        Task<RequestDocument> GetReviewDocumentById(int documentId, int userId);
    }

    public class RequestDocumentService : GenericService<RequestDocument, int>, IRequestDocumentService
    {
        private readonly IUserService _userService;
        private readonly ILogger<RequestDocumentService> _logger;

        public RequestDocumentService(IRepository<RequestDocument, int> repository, IUserService userService,
            ILogger<RequestDocumentService> logger) :
            base(repository)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task<RequestDocument> GetReviewDocumentById(int documentId, int userId)
        {
            var record = await GetRecordAsync(x => x.Id == documentId && x.Enabled, null, x => x.Workflow,
                x => x.RequestDocumentAssignees, x => x.CreateByUser);
            if (record == null) throw new ArgumentException("Request document not found");
            try
            {
                var allUsers = await _userService.GetRecordsAsync(x => !x.Disabled).ContinueWith(x => x.Result.ToList());
                var currentUser = allUsers.FirstOrDefault(x => x.Id == userId);
                if (currentUser == null) throw new ArgumentException("User not found");

                record.RequestDocumentAssignees = record.RequestDocumentAssignees
                    .ForEach(x => x.User = allUsers
                    .FirstOrDefault(u => u.Id == x.UserId))
                    .OrderBy(x => x.Step).ToList();

                record.CompleteReviewSteps = record.RequestDocumentAssignees
                    .Where(x => x.Status != RequestStatus.Inprogress)
                    .OrderBy(x => x.Step)
                    .ToArray();

                var currentStepIdx = record.CompleteReviewSteps.IsNullOrEmpty()
                    ? 1
                    : (record.CompleteReviewSteps.Max(s => s.Step) + 1);

                record.CurrentReviewSteps =
                    record.RequestDocumentAssignees.Where(x => x.Step == currentStepIdx).ToList();
            }
            catch (Exception e)
            {
                _logger.Error($"RequestDocumentService.GetReviewDocumentById.Error", e);
                throw;
            }

            return record;
        }

        protected override void ApplyDefaultSort(FindOptions<RequestDocument> findOptions)
        {
            findOptions.SortAscending(x => (byte) x.Status);
            findOptions.SortDescending(x => x.CreateDate);
        }

        public async Task<ICollection<RequestDocument>> GetReviewDocuments(RequestDocumentFilterRequest request)
        {
            IEnumerable<RequestDocument> records = Array.Empty<RequestDocument>();
            try
            {
                var allUsers = await _userService.GetRecordsAsync(x => !x.Disabled);

                var currentUser = allUsers.FirstOrDefault(x => x.Id == request.UserId);

                if (currentUser == null)
                    throw new ArgumentException("Can not found user.");

                var filterExpression = PredicateBuilder.Create<RequestDocument>(x => x.Enabled);

                if (request.FromDate.HasValue)
                {
                    filterExpression = filterExpression.And(x => x.CreateDate >= request.FromDate);
                }

                if (request.ToDate.HasValue)
                {
                    filterExpression = filterExpression.And(x => x.CreateDate <= request.ToDate);
                }

                if (request.FormFilterStatus.HasValue)
                {
                    filterExpression = filterExpression.And(x => x.Status == request.FormFilterStatus);
                }

                if (request.WorkflowId.HasValue)
                {
                    filterExpression = filterExpression.And(x => x.WorkflowId == request.WorkflowId);
                }

                var findOpts = new FindOptions<RequestDocument>();
                findOpts.SortDescending(x => x.CreateDate);

                records = await GetRecordsAsync(filterExpression, findOpts, x => x.Workflow,
                    x => x.RequestDocumentAssignees, x => x.CreateByUser);

                foreach (var item in records)
                {
                    item.RequestDocumentAssignees = item.RequestDocumentAssignees
                        .Each(x => x.User = allUsers.FirstOrDefault(u => u.Id == x.UserId))
                        .OrderBy(x => x.Step).ToList();

                    item.CompleteReviewSteps = item.RequestDocumentAssignees
                        .Where(x => x.Status != RequestStatus.Inprogress)
                        .OrderBy(x => x.Step)
                        .ToArray();

                    var currentStepIdx = item.CompleteReviewSteps.IsNullOrEmpty() ? 1 : item.CompleteReviewSteps.Max(s => s.Step) + 1;

                    item.CurrentReviewSteps =
                        item.RequestDocumentAssignees.Where(x => x.Step == currentStepIdx).ToList();
                }

                if (currentUser.IsManageRequestDocument)
                {
                    return records.ToArray();
                }

                if (request.CustomFilterStatus.HasValue)
                {
                    return records.Where(x => x.RequestDocumentAssignees.Any(r => r.UserId == currentUser.Id && r.Status == request.CustomFilterStatus.Value)).ToArray();
                }
                records = records.Where(x => x.CreateByUserId == currentUser.Id ||
                                             x.CurrentReviewSteps.Any(r => r.UserId == currentUser.Id));
            }
            catch (Exception e)
            {
                _logger.Error("RequestDocument.GetReviewDocuments.Error", e);
                throw;
            }

            return records.ToArray();
        }
    }
}