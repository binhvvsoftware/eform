﻿using CMS.Core.Data;
using CMS.Core.Repository;
using IdentityRole = CMS.Shared.Data.Domain.IdentityRole;

namespace CMS.Services.Services
{
    public interface IRoleService : IGenericService<IdentityRole, int>
    {
    }

    public class RoleService : GenericService<IdentityRole, int>, IRoleService
    {
        public RoleService(IRepository<IdentityRole, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<IdentityRole> findOptions)
        {
            findOptions.SortAscending(x => x.Name);
        }
    }
}
