﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IUserRoleService : IGenericService<IdentityUserRole, int>
    {
    }

    public class UserRoleService : GenericService<IdentityUserRole, int>, IUserRoleService
    {
        public UserRoleService(IRepository<IdentityUserRole, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<IdentityUserRole> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
