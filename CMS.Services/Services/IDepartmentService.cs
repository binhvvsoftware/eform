﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IDepartmentService : IGenericService<Department, int>
    {
    }

    public class DepartmentService : GenericService<Department, int>, IDepartmentService
    {
        public DepartmentService(IRepository<Department, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<Department> findOptions)
        {
            findOptions.SortAscending(x => x.Order);
        }
    }
}
