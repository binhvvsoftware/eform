﻿using System;
using System.Threading.Tasks;
using Autofac;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;
using CMS.Shared.Models;
using Microsoft.Extensions.Logging;

namespace CMS.Services.Services
{
    public interface ILeaveDocumentService : IGenericService<UserLeaveApplication, int>
    {
        Task StoreAbsentRequest(AbsentModel model);
        Task StoreAbsentRequest(UserLeaveApplication entity);
    }

    public class LeaveDocumentService : GenericService<UserLeaveApplication, int>, ILeaveDocumentService
    {
        public LeaveDocumentService(IRepository<UserLeaveApplication, int> repository) : base(repository)
        {
        }

        public async Task StoreAbsentRequest(AbsentModel model)
        {
            if (!model.IsValidateModel())
            {
                throw new ArgumentException("Store Absent Request Failed. Absent model is not valid to storing.");
            }

            if (model.IsNew)
            {
                await Repository.InsertAsync(model.ToEntity());
            }
        }

        public async Task StoreAbsentRequest(UserLeaveApplication entity)
        {
            await Repository.InsertAsync(entity);
        }

        protected override void ApplyDefaultSort(FindOptions<UserLeaveApplication> findOptions)
        {
            findOptions.SortDescending(x => x.CreateAt);
        }
    }
}