﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IEmailTemplateService : IGenericService<EmailTemplate, int>
    {
    }

    public class EmailTemplateService : GenericService<EmailTemplate, int>, IEmailTemplateService
    {
        public EmailTemplateService(IRepository<EmailTemplate, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<EmailTemplate> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
