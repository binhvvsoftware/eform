﻿using System.Threading.Tasks;
using Autofac;
using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;
using Microsoft.Extensions.Logging;

namespace CMS.Services.Services
{
    public interface IWorkflowService : IGenericService<Workflow, int>
    {
        Task ApprovedRequest(int documentIdx, int stepReviewIdx);
        Task RejectRequest(int documentIdx, int stepReviewIdx);
    }

    public class WorkflowService : GenericService<Workflow, int>, IWorkflowService
    {
        private readonly IComponentContext componentContext;
        private readonly ILogger<WorkflowService> logger;

        public WorkflowService(IRepository<Workflow, int> repository, IComponentContext componentContext, ILogger<WorkflowService> logger) : base(repository)
        {
            this.componentContext = componentContext;
            this.logger = logger;
        }

        public Task ApprovedRequest(int documentIdx, int stepReviewIdx)
        {
            throw new System.NotImplementedException();
        }

        public Task RejectRequest(int documentIdx, int stepReviewIdx)
        {
            throw new System.NotImplementedException();
        }

        protected override void ApplyDefaultSort(FindOptions<Workflow> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
