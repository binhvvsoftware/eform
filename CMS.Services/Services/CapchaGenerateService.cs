using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using CMS.Services.Extensions;

namespace CMS.Services.Services
{
    public interface ICapchaGenerator
    {
        Task<string> GenerateCapchaImageAsync(int totalWords = 5, int width = 190, int height = 80);
        Task<NewCaptcha> ValidateCapchaAsync(string captchaBase64, string attemptedCaptchaValue);
        string GenerateCapchaImage(int totalWords = 5, int width = 190, int height = 80);
        NewCaptcha ValidateCapcha(string captchaBase64, string attemptedCaptchaValue);
    }

    public class CapchaGenerator : ICapchaGenerator
    {
        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        public Task<string> GenerateCapchaImageAsync(int totalWords = 5, int width = 190, int height = 80)
        {
            return Task.FromResult(GenerateCapchaImage(totalWords, width, height));
        }

        public Task<NewCaptcha> ValidateCapchaAsync(string captchaBase64, string attemptedCaptchaValue)
        {
            return Task.FromResult(ValidateCapcha(captchaBase64, attemptedCaptchaValue));
        }

        public string GenerateCapchaImage(string capchaCode, int width = 190, int height = 80)
        {
            string capchaBase64;

            try
            {
                _semaphoreSlim.Wait(TimeSpan.FromSeconds(10), CancellationToken.None);

                var newCapcha = CreateNewCaptcha(capchaCode);

                var captchaValue = DeserializeCaptchaValue(newCapcha.ValueString);

                if (captchaValue == null)
                    throw new Exception("Deserialize base64 failed.");

                var fontEmSizes = new[] {30, 35, 40, 45, 50};

                var fontNames = new[]
                {
                    "Trebuchet MS",
                    "Arial",
                    "Times New Roman",
                    "Georgia",
                    "Verdana",
                    "Geneva"
                };

                FontStyle[] fontStyles =
                {
                    FontStyle.Bold,
                    FontStyle.Italic,
                    FontStyle.Regular,
                    FontStyle.Strikeout,
                    FontStyle.Underline
                };

                HatchStyle[] hatchStyles =
                {
                    HatchStyle.BackwardDiagonal, HatchStyle.Cross,
                    HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
                    HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical,
                    HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross,
                    HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid,
                    HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                    HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard,
                    HatchStyle.LargeConfetti, HatchStyle.LargeGrid,
                    HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal,
                    HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
                    HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal,
                    HatchStyle.NarrowVertical, HatchStyle.OutlinedDiamond,
                    HatchStyle.Plaid, HatchStyle.Shingle, HatchStyle.SmallCheckerBoard,
                    HatchStyle.SmallConfetti, HatchStyle.SmallGrid,
                    HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis,
                    HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
                    HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
                };

                var bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

                var graphics = Graphics.FromImage(bitmap);
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

                var rectangleF = new RectangleF(0, 0, width, height);

                var random = new Random();

                //Draw background (Lighter colors RGB 100 to 255)
                var brush = new HatchBrush(hatchStyles[random.Next
                    (hatchStyles.Length - 1)], Color.FromArgb((random.Next(100, 255)),
                    (random.Next(100, 255)), (random.Next(100, 255))), Color.White);
                graphics.FillRectangle(brush, rectangleF);

                // There is no spoon
                var theMatrix = new Matrix();

                for (var i = 0; i <= captchaValue.Value.Length - 1; i++)
                {
                    theMatrix.Reset();

                    var charLength = captchaValue.Value.Length;
                    var x = width / (charLength + 1) * i;
                    var y = height / 2;

                    //Rotate text Random
                    theMatrix.RotateAt(random.Next(-20, 20), new PointF(x, y));

                    graphics.Transform = theMatrix;

                    //Draw the letters with Random Font Type, Size and Color
                    graphics.DrawString
                    (
                        //Text
                        captchaValue.Value.Substring(i, 1),

                        //Random Font Name and Style
                        new Font(fontNames[random.Next(fontNames.Length - 1)],
                            fontEmSizes[random.Next(fontEmSizes.Length - 1)],
                            fontStyles[random.Next(fontStyles.Length - 1)]),

                        //Random Color (Darker colors RGB 0 to 100)
                        new SolidBrush(Color.FromArgb(random.Next(0, 100),
                            random.Next(0, 100), random.Next(0, 100))),
                        x,
                        random.Next(10, 40)
                    );

                    graphics.ResetTransform();
                }

                var buffer = new byte[16 * 1024];

                // Create the base64 string from the bitmap
                using (var ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Png);

                    int read;

                    while ((read = ms.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }

                    var base64String = Convert.ToBase64String(ms.ToArray());

                    capchaBase64 = base64String;
                }
            }
            finally
            {
                _semaphoreSlim.Release(1);
            }

            return capchaBase64;
        }

        public string GenerateCapchaImage(int totalWords = 5, int width = 190, int height = 80)
        {
            string capchaBase64;

            try
            {
                _semaphoreSlim.Wait(TimeSpan.FromSeconds(10), CancellationToken.None);

                var newCapcha = CreateNewCaptcha(totalWords);

                var captchaValue = DeserializeCaptchaValue(newCapcha.ValueString);

                if (captchaValue == null)
                    throw new Exception("Deserialize base64 failed.");

                var fontEmSizes = new[] { 30, 35, 40, 45, 50 };

                var fontNames = new[]
                {
                    "Trebuchet MS",
                    "Arial",
                    "Times New Roman",
                    "Georgia",
                    "Verdana",
                    "Geneva"
                };

                FontStyle[] fontStyles =
                {
                    FontStyle.Bold,
                    FontStyle.Italic,
                    FontStyle.Regular,
                    FontStyle.Strikeout,
                    FontStyle.Underline
                };

                HatchStyle[] hatchStyles =
                {
                    HatchStyle.BackwardDiagonal, HatchStyle.Cross,
                    HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
                    HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical,
                    HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross,
                    HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid,
                    HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                    HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard,
                    HatchStyle.LargeConfetti, HatchStyle.LargeGrid,
                    HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal,
                    HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
                    HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal,
                    HatchStyle.NarrowVertical, HatchStyle.OutlinedDiamond,
                    HatchStyle.Plaid, HatchStyle.Shingle, HatchStyle.SmallCheckerBoard,
                    HatchStyle.SmallConfetti, HatchStyle.SmallGrid,
                    HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis,
                    HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
                    HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
                };

                var bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

                var graphics = Graphics.FromImage(bitmap);
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

                var rectangleF = new RectangleF(0, 0, width, height);

                var random = new Random();

                //Draw background (Lighter colors RGB 100 to 255)
                var brush = new HatchBrush(hatchStyles[random.Next
                    (hatchStyles.Length - 1)], Color.FromArgb((random.Next(100, 255)),
                    (random.Next(100, 255)), (random.Next(100, 255))), Color.White);
                graphics.FillRectangle(brush, rectangleF);

                // There is no spoon
                var theMatrix = new Matrix();

                for (var i = 0; i <= captchaValue.Value.Length - 1; i++)
                {
                    theMatrix.Reset();

                    var charLength = captchaValue.Value.Length;
                    var x = width / (charLength + 1) * i;
                    var y = height / 2;

                    //Rotate text Random
                    theMatrix.RotateAt(random.Next(-20, 20), new PointF(x, y));

                    graphics.Transform = theMatrix;

                    //Draw the letters with Random Font Type, Size and Color
                    graphics.DrawString
                    (
                        //Text
                        captchaValue.Value.Substring(i, 1),

                        //Random Font Name and Style
                        new Font(fontNames[random.Next(fontNames.Length - 1)],
                            fontEmSizes[random.Next(fontEmSizes.Length - 1)],
                            fontStyles[random.Next(fontStyles.Length - 1)]),

                        //Random Color (Darker colors RGB 0 to 100)
                        new SolidBrush(Color.FromArgb(random.Next(0, 100),
                            random.Next(0, 100), random.Next(0, 100))),
                        x,
                        random.Next(10, 40)
                    );

                    graphics.ResetTransform();
                }

                var buffer = new byte[16 * 1024];

                // Create the base64 string from the bitmap
                using (var ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Png);

                    int read;

                    while ((read = ms.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }

                    var base64String = Convert.ToBase64String(ms.ToArray());

                    capchaBase64 = base64String;
                }
            }
            finally
            {
                _semaphoreSlim.Release(1);
            }

            return capchaBase64;
        }

        public NewCaptcha ValidateCapcha(string captchaBase64, string attemptedCaptchaValue)
        {
            var captchaValue = DeserializeCaptchaValue(captchaBase64);

            if (captchaValue == null)
                return null;

            var capcha = new NewCaptcha()
            {
                ValueString = captchaBase64
            };

            if (captchaValue.NumberOfTimesAttempted >= 4)
            {
                capcha.AttemptFailed = true;
                capcha.AttemptFailedMessage = "the captcha was wrong for too many times, refresh the captcha!";
            }
            else if (attemptedCaptchaValue != captchaValue.Value)
            {
                capcha.AttemptFailed = true;
                capcha.AttemptFailedMessage = "the captcha was not correct, try again...";
            }
            else
            {
                capcha.AttemptFailed = false;
                capcha.AttemptSucceeded = true;
            }

            captchaValue.LastTimeAttempted = DateTime.Now;
            captchaValue.NumberOfTimesAttempted += 1;

            capcha.ValueString = SerializeCaptchaValue(captchaValue);

            return capcha;
        }

        private static NewCaptcha CreateNewCaptcha(int length)
        {
            var captchaValue = new NewCaptchaValue
            {
                Value = GenerateRandomString(length),
                LastTimeAttempted = DateTime.Now,
                FirstTimeAttempted = DateTime.Now,
                NumberOfTimesAttempted = 0
            };

            var captcha = new NewCaptcha
            {
                ValueString = SerializeCaptchaValue(captchaValue)
            };


            return captcha;
        }

        private static NewCaptcha CreateNewCaptcha(string capchaValue)
        {
            var captchaValue = new NewCaptchaValue
            {
                Value = capchaValue,
                LastTimeAttempted = DateTime.Now,
                FirstTimeAttempted = DateTime.Now,
                NumberOfTimesAttempted = 0
            };

            var captcha = new NewCaptcha
            {
                ValueString = SerializeCaptchaValue(captchaValue)
            };


            return captcha;
        }

        private static string SerializeCaptchaValue(NewCaptchaValue value)
        {
            byte[] capchaByteVal;
            try
            {

                var formatter = new BinaryFormatter();

                using (var memStream = new MemoryStream())
                {
                    formatter.Serialize(memStream, value);
                    capchaByteVal = memStream.ToArray();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return capchaByteVal != null && capchaByteVal.Length > 0 ? Convert.ToBase64String(capchaByteVal) : string.Empty;
        }

        private static NewCaptchaValue DeserializeCaptchaValue(string base64String)
        {
            NewCaptchaValue captchaValue;

            var bytes = Convert.FromBase64String(base64String);

            if (bytes == null || bytes.Length == 0)
                throw new ArgumentException("Can't desirialize this base64.");

            try
            {
                var formatter = new BinaryFormatter();
                using (var memStream = new MemoryStream(bytes, 0, bytes.Length))
                {
                    captchaValue = formatter.Deserialize(memStream) as NewCaptchaValue;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return captchaValue;
        }

        private static string GenerateRandomString(int length)
        {
            var random = new Random();

            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    public class NewCaptcha
    {
        public string ValueString { get; set; }

        public bool AttemptSucceeded { get; set; }

        public bool AttemptFailed { get; set; }

        public string AttemptFailedMessage { get; set; }
    }

    [Serializable]
    public class NewCaptchaValue
    {
        public string Value { get; set; }

        public DateTime FirstTimeAttempted { get; set; }

        public DateTime LastTimeAttempted { get; set; }

        public int NumberOfTimesAttempted { get; set; }
    }
}