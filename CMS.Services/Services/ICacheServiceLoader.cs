﻿using System;
using System.Threading.Tasks;

namespace CMS.Services.Services
{
    public interface ICacheServiceLoader
    {
        Task LoadCacheKeys(int userId);
    }
}
