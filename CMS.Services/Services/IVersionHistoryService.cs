﻿using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;
using System;
using System.Threading.Tasks;

namespace CMS.Services.Services
{
    public interface IVersionHistoryService : IGenericService<VersionHistory, int>
    {
        int GetEntityTypeId(Type type);

        Task CreateNewVersion<T>(T entity, string entityId, string createdBy);
    }

    public class VersionHistoryService : GenericService<VersionHistory, int>, IVersionHistoryService
    {
        private readonly IRepository<EntityVersion, int> entityVersionRepository;
        private readonly IRepository<VersionHistory, int> versionHistoryRepository;

        public VersionHistoryService(IRepository<EntityVersion, int> entityVersionRepository, IRepository<VersionHistory, int> versionHistoryRepository) : base(versionHistoryRepository)
        {
            this.entityVersionRepository = entityVersionRepository;
            this.versionHistoryRepository = versionHistoryRepository;
        }

        public int GetEntityTypeId(Type type)
        {
            var typeName = type.FullName;
            var record = entityVersionRepository.FindOne(x => x.Type == typeName);
            if (record == null)
            {
                record = new EntityVersion
                {
                    Type = typeName
                };

                entityVersionRepository.Insert(record);
            }

            return record.Id;
        }

        public async Task CreateNewVersion<T>(T entity, string entityId, string createdBy)
        {
            var entityTypeId = GetEntityTypeId(typeof(T));
            var values = entity.JsonSerialize();

            var record = new VersionHistory
            {
                TypeId = entityTypeId,
                EntityId = entityId,
                Values = values,
                CreatedDate = DateTime.Now,
                CreatedBy = createdBy
            };

            await versionHistoryRepository.InsertAsync(record);
        }

        protected override void ApplyDefaultSort(FindOptions<VersionHistory> findOptions)
        {
            findOptions.SortDescending(x => x.CreatedDate);
        }
    }
}
