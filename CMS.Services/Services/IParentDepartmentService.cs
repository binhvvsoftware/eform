﻿using CMS.Core.Data;
using CMS.Core.Repository;

using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IParentDepartmentService : IGenericService<ParentDepartment, int>
    {
    }

    public class ParentDepartmentService : GenericService<ParentDepartment, int>, IParentDepartmentService
    {
        public ParentDepartmentService(IRepository<ParentDepartment, int> repository) : base(repository)
        {

        }

        protected override void ApplyDefaultSort(FindOptions<ParentDepartment> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
