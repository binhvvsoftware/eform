﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface ISmtpConfigService : IGenericService<SmtpConfig, int>
    {
    }

    public class SmtpConfigService : GenericService<SmtpConfig, int>, ISmtpConfigService
    {
        public SmtpConfigService(IRepository<SmtpConfig, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<SmtpConfig> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
