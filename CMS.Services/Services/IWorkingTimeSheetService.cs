using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CMS.Services.Services
{
    public interface IWorkingTimeSheetService : IGenericService<WorkingTimesheet, long>
    {
        Task<ICollection<WorkingTimesheet>> GetUserWorkingTimes(ICollection<int> userIds, DateTime? today = null, bool lateOnly = false);
        Task<ICollection<WorkingTimesheet>> GetUserWorkingTimes(Expression<Func<WorkingTimesheet, bool>> predicate);
        Task<ICollection<WorkingTimesheet>> GetLateTimes(DateTime workingDay);
    }

    public class WorkingTimeSheetService : GenericService<WorkingTimesheet, long>, IWorkingTimeSheetService
    {
        private readonly AppSettings _settings;

        public WorkingTimeSheetService(IRepository<WorkingTimesheet, long> repository, IOptions<AppSettings> options) : base(repository)
        {
            if (options == null)
            {
                options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
            }

            _settings = options.Value;
        }

        #region Overrides of GenericService<WorkingTimesheet,long>

        protected override void ApplyDefaultSort(FindOptions<WorkingTimesheet> findOptions)
        {
            findOptions.SortDescending(x => x.StartTime);
        }

        public async Task<ICollection<WorkingTimesheet>> GetUserWorkingTimes(ICollection<int> userIds, DateTime? today = null, bool lateOnly = false)
        {
            using (var connection = Repository.OpenConnection())
            {
                return await connection.Set<WorkingTimesheet>()
                    .Where(x => userIds.IsNullOrEmpty() || userIds.Contains(x.UserId))
                    .Where(x => !today.HasValue || x.WorkingDate.Date == today.Value.Date)
                    .Include(i => i.Requester)
                    .Include(i => i.LateRequest)
                    .OrderByDescending(o => o.WorkingDate)
                    .ToListAsync();
            }
        }

        public async Task<ICollection<WorkingTimesheet>> GetUserWorkingTimes(Expression<Func<WorkingTimesheet, bool>> predicate)
        {
            using (var connection = Repository.OpenConnection())
            {
                var records = await connection.Set<WorkingTimesheet>()
                    .Where(predicate)
                    .Include(x => x.Requester)
                    .Include(x => x.LateRequest)
                    .OrderByDescending(o => o.WorkingDate)
                    .ToListAsync();
                return records;
            }
        }

        public async Task<ICollection<WorkingTimesheet>> GetLateTimes(DateTime workingDay)
        {
            using (var connection = Repository.OpenConnection())
            {
                return await connection.Set<WorkingTimesheet>()
                    .Where(x => x.WorkingDate.Date == workingDay.Date)
                    .Where(x => DateTime.ParseExact(x.StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) > (x.WorkingDate.Date + _settings.WorkingTime))
                    .Include(x => x.LateRequest)
                    .ToListAsync();
            }
        }

        #endregion
    }
}