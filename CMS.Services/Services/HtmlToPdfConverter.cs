﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Services.Services
{
    public interface IPdfConvertorService
    {
        Task<byte[]> ConvertFromUrl(string url);
        Task<byte[]> Convert(string html);
        Task Convert(string html, Stream outputStream);
        Task Convert(Stream inputStream, Stream outputStream);
    }

    public class PdfConverterService : IPdfConvertorService
    {
        #region Implementation of IPdfConvertorService

        readonly string ExecutePath = "xvfb-run";

        async Task ConvertProcess(Stream input, Stream output)
        {
            try
            {
                using (var p = new Process())
                {
                    p.StartInfo = GetProcessStartInfo();
                    p.Start();

                    p.StandardInput.AutoFlush = true;
                    await input.CopyToAsync(p.StandardInput.BaseStream);
                    p.StandardInput.Dispose();

                    var buffer = new byte[1024];
                    var length = 0;
                    do
                    {
                        var readed = await p.StandardOutput.BaseStream.ReadAsync(buffer, 0, buffer.Length);
                        if (readed <= 0) break;
                        length += readed;
                        output.Write(buffer, 0, readed);

                    } while (true);

                    if (length == 0)
                    {
                        var sb = new StringBuilder();
                        while (!p.StandardError.EndOfStream)
                        {
                            sb.AppendLine(await p.StandardError.ReadLineAsync());
                        }

                        var err = sb.ToString();

                        if (!string.IsNullOrEmpty(err))
                            throw new Exception("Unable to render pdf:" + err);
                    }

                    p.WaitForExit();
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Problem generating PDF from HTML", exc);
            }
        }

        private ProcessStartInfo GetProcessStartInfo()
        {
            ProcessStartInfo startInfo;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                startInfo = new ProcessStartInfo("wkhtmltopdf")
                {
                    Arguments = "-q -n --encoding UTF-8 --page-size A4 - -", // tell wkhtmltopdf bypass input output as stream
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                };
            }
            else
            {
                startInfo = new ProcessStartInfo(ExecutePath)
                {
                    Arguments = "-a wkhtmltopdf -q -n --encoding UTF-8 --page-size A4 - -", // tell wkhtmltopdf bypass input output as stream
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                };
            }

            return startInfo;
        }

        public async Task<byte[]> ConvertFromUrl(string url)
        {
            try
            {
                using(var output= new MemoryStream())
                using (var p = new Process())
                {
                    p.StartInfo = GetProcessStartInfo();
                    p.Start();

                    p.StandardInput.AutoFlush = true;
                    p.StandardInput.Dispose();

                    var buffer = new byte[1024];
                    var length = 0;
                    do
                    {
                        var readed = await p.StandardOutput.BaseStream.ReadAsync(buffer, 0, buffer.Length);
                        if (readed <= 0) break;
                        length += readed;
                        output.Write(buffer, 0, readed);

                    } while (true);

                    if (length == 0)
                    {
                        var sb = new StringBuilder();
                        while (!p.StandardError.EndOfStream)
                        {
                            sb.AppendLine(await p.StandardError.ReadLineAsync());
                        }

                        var err = sb.ToString();

                        if (!string.IsNullOrEmpty(err))
                            throw new Exception("Unable to render pdf:" + err);
                    }

                    p.WaitForExit();
                    return output.ToArray();
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Problem generating PDF from HTML", exc);
            }
        }

        public async Task<byte[]> Convert(string html)
        {
            using (var outputStream = new MemoryStream())
            {
                using (var inputStream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(inputStream){AutoFlush = true})
                    {
                        await writer.WriteAsync(html);

                        inputStream.Seek(0, SeekOrigin.Begin);
                        await ConvertProcess(inputStream, outputStream);
                        return outputStream.ToArray();
                    }
                }
            }
        }
      
        public async Task Convert(string html, Stream outputStream)
        {
            using (var inputStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(inputStream) { AutoFlush = true })
                {
                    await writer.WriteAsync(html);
                    inputStream.Seek(0, SeekOrigin.Begin);
                    await ConvertProcess(inputStream, outputStream);
                }
            }
        }
        
        public Task Convert(Stream inputStream, Stream outputStream)
        {
            return ConvertProcess(inputStream, outputStream);
        }

        #endregion
    }
    
}
