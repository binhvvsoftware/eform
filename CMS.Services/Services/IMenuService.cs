﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IMenuService : IGenericService<Menu, int>
    {
    }

    public class MenuService : GenericService<Menu, int>, IMenuService
    {
        public MenuService(IRepository<Menu, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<Menu> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
