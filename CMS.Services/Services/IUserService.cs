﻿using System.Threading;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Repository;
using Microsoft.AspNetCore.Identity;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;

namespace CMS.Services.Services
{
    public interface IUserService : IGenericService<IdentityUser, int>
    {
        
    }

    public class UserService : GenericService<IdentityUser, int>, IUserService
    {
        public UserService(IRepository<IdentityUser, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<IdentityUser> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
