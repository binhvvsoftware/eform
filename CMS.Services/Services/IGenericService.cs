﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Entity;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using Microsoft.EntityFrameworkCore;

namespace CMS.Services.Services
{
    public interface IGenericService<TRecord, in TKey>
    {
        DbContext OpenConnection();

        void ApplyDefaultSort(FindOptions<TRecord> findOptions);

        #region Count

        Task<long> CountAsync(Expression<Func<TRecord, bool>> predicate = null);

        #endregion Count

        #region Delete

        Task DeleteAsync(TRecord record);

        Task DeleteManyAsync(IEnumerable<TRecord> records);

        Task DeleteAsync(Expression<Func<TRecord, bool>> predicate);

        #endregion Delete

        #region Find

        Task<TRecord> GetByIdAsync(TKey id);

        Task<TRecord> GetRecordAsync(Expression<Func<TRecord, bool>> predicate = null, FindOptions<TRecord> findOptions = null);

        Task<TRecord> GetRecordAsync(Expression<Func<TRecord, bool>> predicate = null, FindOptions<TRecord> findOptions = null, params Expression<Func<TRecord, dynamic>>[] includes);

        Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate = null);

        Task<IEnumerable<TProjection>> GetRecordsAsync<TProjection>(Expression<Func<TRecord, bool>> predicate, Expression<Func<TRecord, TProjection>> projection, FindOptions<TRecord> findOptions = null);

        Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate, FindOptions<TRecord> findOptions);

        Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate, FindOptions<TRecord> findOptions, params Expression<Func<TRecord, dynamic>>[] includes);

        #endregion Find

        #region Insert

        Task InsertAsync(TRecord record);

        Task InsertManyAsync(IEnumerable<TRecord> records);

        #endregion Insert

        #region Update

        Task UpdateAsync(TRecord record);

        Task UpdateManyAsync(IEnumerable<TRecord> records);

        Task UpdateManyAsync(Expression<Func<TRecord, bool>> predicate, Expression<Func<TRecord, TRecord>> updateExpression);

        #endregion Update
    }

    public abstract class GenericService<TRecord, TKey> : IGenericService<TRecord, TKey> where TRecord : class, IBaseEntity<TKey>
    {
        protected readonly IRepository<TRecord, TKey> Repository;

        protected GenericService(IRepository<TRecord, TKey> repository)
        {
            Repository = repository;
        }

        #region Count

        public DbContext OpenConnection()
        {
            return Repository.OpenConnection();
        }

        public long Count(Expression<Func<TRecord, bool>> predicate = null)
        {
            return Repository.Count(predicate);
        }

        public async Task<long> CountAsync(Expression<Func<TRecord, bool>> predicate = null)
        {
            return await Repository.CountAsync(predicate);
        }

        #endregion Count

        #region Delete

        public virtual async Task DeleteAsync(TRecord record)
        {
            await Repository.DeleteAsync(record);
        }

        protected virtual void DeleteMany(IEnumerable<TRecord> records)
        {
            Repository.DeleteMany(records);
        }

        public virtual async Task DeleteManyAsync(IEnumerable<TRecord> records)
        {
            await Repository.DeleteManyAsync(records);
        }

        public void Delete(Expression<Func<TRecord, bool>> predicate)
        {
            Repository.DeleteMany(predicate);
        }

        public virtual async Task DeleteAsync(Expression<Func<TRecord, bool>> predicate)
        {
            await Repository.DeleteManyAsync(predicate);
        }

        #endregion Delete

        #region Find

        public virtual async Task<TRecord> GetByIdAsync(TKey id)
        {
            return await Repository.GetByIdAsync(id);
        }

        public async Task<TRecord> GetRecordAsync(Expression<Func<TRecord, bool>> predicate = null, FindOptions<TRecord> findOptions = null)
        {
            return await Repository.FindOneAsync(predicate, findOptions);
        }

        public async Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate = null)
        {
            var findOptions = new FindOptions<TRecord>();
            ApplyDefaultSort(findOptions);

            return await Repository.FindAsync(predicate, findOptions);
        }

        public async Task<IEnumerable<TProjection>> GetRecordsAsync<TProjection>(Expression<Func<TRecord, bool>> predicate, Expression<Func<TRecord, TProjection>> projection, FindOptions<TRecord> findOptions = null)
        {
            return await Repository.FindAsync(predicate, projection, findOptions);
        }

        public async Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate, FindOptions<TRecord> findOptions)
        {
            return await Repository.FindAsync(predicate, findOptions);
        }

        public Task<IEnumerable<TRecord>> GetRecordsAsync(Expression<Func<TRecord, bool>> predicate, FindOptions<TRecord> findOptions = null, params Expression<Func<TRecord, dynamic>>[] includePaths)
        {
            var records = Repository.Find(predicate, findOptions, includePaths);
            return Task.FromResult(records);
        }

        #endregion Find

        #region Insert

        public virtual async Task InsertAsync(TRecord record)
        {
            try
            {
                await Repository.InsertAsync(record);
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException == null ? "Have exception when insert entity" : ex.InnerException.Message;
                throw new Exception($"{msg}: {record.GetType().Name} with data: {record.JsonSerialize()}", ex);
            }
        }

        public virtual async Task InsertManyAsync(IEnumerable<TRecord> records)
        {
            if (records == null)
            {
                return;
            }

            await Repository.InsertManyAsync(records);
        }

        #endregion Insert

        #region Update

        public virtual async Task UpdateAsync(TRecord record)
        {
            try
            {
                await Repository.UpdateAsync(record);
            }
            catch (Exception ex)
            {
                throw new Exception("Have exception when update entity: " + record.GetType().Name + " with data: " + record.JsonSerialize(), ex);
            }
        }

        public virtual async Task UpdateManyAsync(IEnumerable<TRecord> records)
        {
            await Repository.UpdateManyAsync(records);
        }

        public virtual async Task UpdateManyAsync(Expression<Func<TRecord, bool>> predicate, Expression<Func<TRecord, TRecord>> updateExpression)
        {
            await Repository.UpdateManyAsync(predicate, updateExpression);
        }

        #endregion Update

        #region Non-Public Methods

        void IGenericService<TRecord, TKey>.ApplyDefaultSort(FindOptions<TRecord> findOptions)
        {
            ApplyDefaultSort(findOptions);
        }

        protected abstract void ApplyDefaultSort(FindOptions<TRecord> findOptions);

        public async Task<TRecord> GetRecordAsync(Expression<Func<TRecord, bool>> predicate = null, FindOptions<TRecord> findOptions = null, params Expression<Func<TRecord, dynamic>>[] includes)
        {
            return await Repository.FindOneAsync(predicate, findOptions, includes);
        }

        #endregion Non-Public Methods
    }

    public abstract class CachedGenericService<T, TKey> : GenericService<T, TKey>, ICacheServiceLoader where T : class, IBaseEntity<TKey>
    {
        protected CachedGenericService(IRepository<T, TKey> repository) : base(repository)
        {
        }

        private async Task RebuildCacheFor(T record)
        {
            await LoadCacheKeys(UserIdGetter(record));
        }

        private async Task RebuildCacheFor(IEnumerable<T> records)
        {
            foreach (var g in records.GroupBy(p => UserIdGetter(p)))
            {
                await LoadCacheKeys(g.Key);
            }
        }

        public override async Task InsertAsync(T record)
        {
            await base.InsertAsync(record);
            await RebuildCacheFor(record);
        }

        public override async Task InsertManyAsync(IEnumerable<T> records)
        {
            var arr = records?.ToArray();
            if (arr == null || arr.Length == 0) return;

            await base.InsertManyAsync(arr);

            await RebuildCacheFor(arr);
        }

        public override async Task UpdateAsync(T record)
        {
            await base.UpdateAsync(record);

            await RebuildCacheFor(record);
        }

        public override async Task DeleteAsync(T entity)
        {
            await base.DeleteAsync(entity);

            await RebuildCacheFor(entity);
        }

        protected override void DeleteMany(IEnumerable<T> records)
        {
            var arr = records?.ToArray();
            if (arr == null || arr.Length == 0) return;

            base.DeleteMany(arr);

            Task.Run(() => RebuildCacheFor(arr));
        }

        public override async Task UpdateManyAsync(IEnumerable<T> records)
        {
            var arr = records?.ToArray();
            if (arr != null && arr.Length > 0)
            {
                await base.UpdateManyAsync(arr);
                await RebuildCacheFor(arr);
            }
        }

        public abstract Task LoadCacheKeys(int userId);

        public abstract Func<T, int> UserIdGetter { get; }
    }
}
