﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface ICampusService : IGenericService<Campus, int>
    {
    }

    public class CampusService : GenericService<Campus, int>, ICampusService
    {
        public CampusService(IRepository<Campus, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<Campus> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
