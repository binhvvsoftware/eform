﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;
using CMS.Shared.Extensions;
using Microsoft.Extensions.Logging;

namespace CMS.Services.Services
{
    public interface IRequestDocumentAssigneeService : IGenericService<RequestDocumentAssignee, int>
    {
        Task<IEnumerable<RequestDocumentAssignee>> GetReviewerSignatureAndComment(int formRequestId);
        Task UpdateGroupReviewStatus(RequestDocumentAssignee stepReview);
    }

    public class RequestDocumentAssigneeService : GenericService<RequestDocumentAssignee, int>,
        IRequestDocumentAssigneeService
    {
        private readonly ILogger<RequestDocumentAssigneeService> logger;

        public RequestDocumentAssigneeService(IRepository<RequestDocumentAssignee, int> repository,
            ILogger<RequestDocumentAssigneeService> logger) : base(repository)
        {
            this.logger = logger;
        }

        protected override void ApplyDefaultSort(FindOptions<RequestDocumentAssignee> findOptions)
        {
            findOptions.SortAscending(x => x.Step);
        }

        public Task<IEnumerable<RequestDocumentAssignee>> GetReviewerSignatureAndComment(int formRequestId)
        {
            return GetRecordsAsync(x => x.RequestDocumentId == formRequestId &&
                                        !x.SignatureImage.IsNullOrEmpty() &&
                                        x.Status == RequestStatus.Approved,
                new FindOptions<RequestDocumentAssignee>().SortAscending(x => x.Step), x => x.User);
        }

        public async Task UpdateGroupReviewStatus(RequestDocumentAssignee stepReview)
        {
            var reviewerInGroups = await GetRecordsAsync(x => x.Step == stepReview.Step
                                                              && x.RequestDocumentId == stepReview.RequestDocumentId
                                                              && x.Id != stepReview.Id);

            var groupsToUpdate = new List<RequestDocumentAssignee>()
            {
                stepReview
            };

            if (!reviewerInGroups.IsNullOrEmpty())
            {
                foreach (var step in reviewerInGroups)
                {
                    step.UpdateDate = DateTime.Now;
                    step.Status = stepReview.Status;
                }

                groupsToUpdate.AddRange(reviewerInGroups);
            }

            try
            {
                await UpdateManyAsync(groupsToUpdate);
            }
            catch (System.Exception e)
            {
                logger.Error($"UpdateReviewStep.Error", e);
                throw e;
            }
        }
    }
}