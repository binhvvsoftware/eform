﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Services.Data;
using CMS.Shared.Data.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Services.Services
{
    public interface ILogService : IGenericService<Log, int>
    {
        Task<IEnumerable<string>> GetEntityTypes();
    }

    public class LogService : GenericService<Log, int>, ILogService
    {
        public LogService(IRepository<Log, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<Log> findOptions)
        {
            findOptions.SortDescending(x => x.LogTime);
        }

        public async Task<IEnumerable<string>> GetEntityTypes()
        {
            using (var connection = (EformDbContext)OpenConnection())
            {
                return await connection.Logs.Select(x => x.EntityName).Distinct().ToListAsync();
            }
        }
    }
}
