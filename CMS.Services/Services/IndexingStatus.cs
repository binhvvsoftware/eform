﻿namespace CMS.Services.Services
{
    public enum IndexingStatus
    {
        Rebuilding,
        Updating,
        Idle
    }
}