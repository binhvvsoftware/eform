﻿using CMS.Core.Data;
using CMS.Core.Repository;
using CMS.Shared.Data.Domain;

namespace CMS.Services.Services
{
    public interface IFormTemplateService : IGenericService<FormTemplate, int>
    {
    }

    public class FormTemplateService : GenericService<FormTemplate, int>, IFormTemplateService
    {
        public FormTemplateService(IRepository<FormTemplate, int> repository) : base(repository)
        {
        }

        protected override void ApplyDefaultSort(FindOptions<FormTemplate> findOptions)
        {
            findOptions.SortAscending(x => x.Id);
        }
    }
}
