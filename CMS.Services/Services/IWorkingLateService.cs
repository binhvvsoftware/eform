using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.Data;
using CMS.Core.Extensions;
using CMS.Core.Repository;
using CMS.Shared;
using CMS.Shared.Data.Domain;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

namespace CMS.Services.Services
{
    public interface IWorkingLateService : IGenericService<WorkingLateRequest, long>
    {
        Task<IEnumerable<WorkingLateRequest>> TransformAndInsertRequests(IEnumerable<IdentityUser> userLists, ICollection<WorkingTimesheet> userLates);
        Task<IEnumerable<WorkingLateRequest>> GetOrUpdateExpiredRequests(IEnumerable<IdentityUser> userLists, DateTime validPoint);
        Task UpdateRequestStatus(long requestId, ConfirmStatus confirmStatus);
    }

    public class WorkingLateService : GenericService<WorkingLateRequest, long>, IWorkingLateService
    {
        private readonly IEmailService _emailService;
        private readonly IMemberRelationService _memberRelationService;
        private readonly IUserService _userService;
        private AppSettings _appSetting;

        public WorkingLateService(IRepository<WorkingLateRequest, long> repository,
            IEmailService emailService,
            IMemberRelationService memberRelationService,
            IUserService userService,
            IOptions<AppSettings> options) : base(repository)
        {
            _emailService = emailService;
            _memberRelationService = memberRelationService;
            _userService = userService;
            if (options == null)
            {
                options = new OptionsWrapper<AppSettings>(new AppSettings()
                {
                    WorkingTime = new TimeSpan(8, 30, 0)
                });
            }

            _appSetting = options.Value;
        }

        public async Task<IEnumerable<WorkingLateRequest>> TransformAndInsertRequests(IEnumerable<IdentityUser> userLists, ICollection<WorkingTimesheet> userLates)
        {
            var updateTasks = new List<Task>();
            var lateRequests = await userLates.SelectAsync(async x =>
            {
                var relations = await _memberRelationService.GetRecordsAsync(s => s.UserId == x.UserId, projection => projection.ManagerId);
                var actualTime = DateTime.ParseExact(x.StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                var workingTime = x.WorkingDate.Date + _appSetting.WorkingTime; ;
                var request = new WorkingLateRequest
                {
                    TimeSheetId = x.Id,
                    RequesterId = x.UserId,
                    Requester = userLists.FirstOrDefault(u => u.Id == x.UserId),
                    ManagerIds = relations.ToArray(),
                    Managers = userLists.Where(u => relations.Contains(u.Id)).ToArray(),
                    ActualWorkTime = actualTime,
                    TotalLateMinutes = (int)(actualTime - workingTime).TotalMinutes
                };

                var expiredTime = (request.CreateDate.Date + TimeSpan.Parse("12:00:00")).AddDays(2);
                request.UpdateReviewTimeToExpired(expiredTime);

                return request;
            });

            // store user's late request
            if (!lateRequests.IsNullOrEmpty())
                await InsertManyAsync(lateRequests);

            return lateRequests;
        }

        public async Task<IEnumerable<WorkingLateRequest>> GetOrUpdateExpiredRequests(IEnumerable<IdentityUser> userLists, DateTime validPoint)
        {
            var expiredItems = await GetRecordsAsync(x => x.ExpiredDate > validPoint && x.ConfirmedStatus == ConfirmStatus.Inprogress);

            if (!expiredItems.IsNullOrEmpty())
            {
                var emailExpiredTasks = new List<Task>();

                foreach (var expired in expiredItems)
                {
                    expired.Requester = userLists.FirstOrDefault(x => x.Id == expired.RequesterId);
                    expired.Managers = userLists.Where(x => expired.ManagerIds.Contains(x.Id)).ToArray();
                    expired.UpdateConfirmStatus(ConfirmStatus.Expired);
                }

                await UpdateManyAsync(expiredItems);
            }

            return expiredItems;
        }

        public async Task UpdateRequestStatus(long requestId, ConfirmStatus confirmStatus)
        {
            var lateRequest = await GetByIdAsync(requestId);
            var userLists = await _userService.GetRecordsAsync(x => !x.Disabled);

            if (lateRequest == null)
                throw new ArgumentNullException("Không tìm thấy đơn cần duyệt. Xin hãy thử lại !");

            lateRequest.UpdateConfirmStatus(confirmStatus);

            await UpdateAsync(lateRequest);
            lateRequest.Requester = userLists.FirstOrDefault(x => x.Id == lateRequest.RequesterId);
            lateRequest.Managers = userLists.Where(x => lateRequest.ManagerIds.Contains(x.Id)).ToArray();

            await _emailService.NotifyLateRequestHasUpdated(lateRequest);
        }

        protected override void ApplyDefaultSort(FindOptions<WorkingLateRequest> findOptions)
        {
            findOptions.SortDescending(x => x.CreateDate);
        }
    }
}