using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RazorLight;
using RazorLight.Razor;

namespace CMS.Services.Templates
{
    public interface ITemplateParser
    {
        Task<string> RenderTemplate(string html, object model);
    }
    public class TemplateParser : ITemplateParser
    {
        private readonly object sync = new object();
        private IRazorLightEngine razorEngine;
        private readonly IDictionary<string, string> templates = new Dictionary<string, string>();

        public async Task<string> RenderTemplate(string html, object model)
        {
            if (string.IsNullOrEmpty(html))
            {
                return string.Empty;
            }

            lock (sync)
            {
                if (razorEngine == null)
                {
                    var builder = new RazorLightEngineBuilder();
                    builder.UseProject(new StringContentRazorProject(key => templates[key], key => templates.ContainsKey(key)));
                    razorEngine = builder.Build();
                }
            }

            try
            {
                var key = "template-" + html.GetHashCode();
                if (!templates.ContainsKey(key))
                {
                    templates[key] = html;
                }

                return await razorEngine.CompileRenderAsync(key, html, model);
            }
            catch (RazorLightException ex)
            {
                throw ex;
            }
        }

        public class StringContentRazorProject : RazorLightProject
        {
            public StringContentRazorProject(Func<string, string> getContentFunc, Func<string, bool> existsCheckFunc)
            {
                GetContentFunc = getContentFunc;
                ExistsCheckFunc = existsCheckFunc;
            }

            public Func<string, string> GetContentFunc { get; }

            public Func<string, bool> ExistsCheckFunc { get; }

            public override Task<RazorLightProjectItem> GetItemAsync(string templateKey)
            {
                var item = new StringContentRazorProjectItem(templateKey, GetContentFunc, ExistsCheckFunc);

                return Task.FromResult((RazorLightProjectItem)item);
            }

            public override Task<IEnumerable<RazorLightProjectItem>> GetImportsAsync(string templateKey)
            {
                return Task.FromResult(Enumerable.Empty<RazorLightProjectItem>());
            }
        }

        public class StringContentRazorProjectItem : RazorLightProjectItem
        {
            public StringContentRazorProjectItem(string key, Func<string, string> getContentFunc, Func<string, bool> existsCheckFunc)
            {
                Key = key;
                GetContentFunc = getContentFunc;
                ExistsCheckFunc = existsCheckFunc;
            }

            public sealed override string Key { get; }

            public Func<string, string> GetContentFunc { get; }

            public Func<string, bool> ExistsCheckFunc { get; }

            public override bool Exists => ExistsCheckFunc(Key);

            public override Stream Read()
            {
                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);

                var str = GetContentFunc(Key);
                if (!string.IsNullOrEmpty(str))
                {
                    writer.Write(str);
                }
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                return stream;
            }
        }
    }
}

