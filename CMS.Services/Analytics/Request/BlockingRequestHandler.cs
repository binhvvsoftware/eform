using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CMS.Services.Analytics.Exception;
using CMS.Services.Analytics.Model;
using CMS.Services.Analytics.Stats;
using Newtonsoft.Json;

namespace CMS.Services.Analytics.Request
{
    internal class BlockingRequestHandler : IRequestHandler
    {
        /// <summary>
        /// JSON serialization settings
        /// </summary>
        private JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            // Converters = new List<JsonConverter> { new ContextSerializer() }
        };

        /// <summary>
        /// Segment.io client to mark statistics
        /// </summary>
        private Client _client;

        /// <summary>
        /// The maximum amount of time to wait before calling
        /// the HTTP flush a timeout failure.
        /// </summary>
        public TimeSpan Timeout { get; set; }

        internal BlockingRequestHandler(Client client, TimeSpan timeout)
        {
            this._client = client;
            this.Timeout = timeout;
        }

        public void MakeRequest(Batch batch)
        {


            Task.Factory.StartNew(async () =>
            {
                Stopwatch watch = new Stopwatch();

                try
                {
                    Uri uri = new Uri(_client.Config.Host + "/v1/import");

                        // set the current request time
                        batch.SentAt = DateTime.Now.ToString("o");

                    string json = JsonConvert.SerializeObject(batch, settings);

                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", BasicAuthHeader(batch.WriteKey, ""));
                        client.DefaultRequestHeaders.ExpectContinue = false;
                        client.Timeout = Timeout;
                        var content = new StringContent(json, Encoding.GetEncoding(0), "application/json");
                        watch.Start();

                        using (var response = await client.PostAsync(uri, content))
                        {
                            watch.Stop();
                            if (response.IsSuccessStatusCode)
                            {
                                Succeed(batch, watch.ElapsedMilliseconds);
                            }
                            else
                            {
                                var responseStr = String.Format("Status Code {0}. ", response.StatusCode);
                                responseStr += await ReadResponse(response);
                                Fail(batch, new APIException("Unexpected Status Code", responseStr), watch.ElapsedMilliseconds);
                            }
                        }
                    }
                }
                catch (WebException e)
                {
                    watch.Stop();
                    Fail(batch, ParseException(e), watch.ElapsedMilliseconds);
                }
                catch (System.Exception e)
                {
                    watch.Stop();
                    Fail(batch, e, watch.ElapsedMilliseconds);
                }
            });


        }

        private void Fail(Batch batch, System.Exception e, long duration)
        {
            foreach (BaseAction action in batch.batch)
            {
                _client.Statistics.Failed = Statistics.Increment(_client.Statistics.Failed);
                _client.RaiseFailure(action, e);
            }

            Logger.Info("Segment.io request failed.", new Dict
            {
                { "batch id", batch.MessageId },
                { "reason", e.Message },
                { "duration (ms)", duration }
            });
        }


        private void Succeed(Batch batch, long duration)
        {
            foreach (BaseAction action in batch.batch)
            {
                _client.Statistics.Succeeded = Statistics.Increment(_client.Statistics.Succeeded);
                _client.RaiseSuccess(action);
            }

            Logger.Info("Segment.io request successful.", new Dict
            {
                { "batch id", batch.MessageId },
                { "duration (ms)", duration }
            });
        }

        private System.Exception ParseException(WebException e)
        {

            if (e.Response != null && ((HttpWebResponse)e.Response).StatusCode == HttpStatusCode.BadRequest)
            {
                return new APIException("Bad Request", ReadResponse(e.Response));
            }
            else
            {
                return e;
            }
        }

        private Task<string> ReadResponse(HttpResponseMessage response)
        {
            return response.Content.ReadAsStringAsync();
        }

        private string ReadResponse(WebResponse response)
        {
            if (response != null)
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            else
            {
                return null;
            }
        }

        private static string BasicAuthHeader(string user, string pass)
        {
            string val = user + ":" + pass;
            return "Basic " + Convert.ToBase64String(Encoding.GetEncoding(0).GetBytes(val));
        }
    }
}

