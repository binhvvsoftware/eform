﻿using CMS.Services.Analytics.Model;

namespace CMS.Services.Analytics.Request
{
    internal interface IRequestHandler
    {
		void MakeRequest(Batch batch); 
    }
}
