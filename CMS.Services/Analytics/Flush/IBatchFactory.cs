using System.Collections.Generic;
using CMS.Services.Analytics.Model;

namespace CMS.Services.Analytics.Flush
{
	internal interface IBatchFactory
	{
		Batch Create(List<BaseAction> actions);
	}
}

