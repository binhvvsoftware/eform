﻿using System;
using System.Collections.Generic;

namespace CMS.Services.Analytics.Trackings.Models
{
    namespace Models
    {
        public class ProductCategoryProperties
        {
            public string Category { get; set; }
        }

        public class OrderProperties
        {
            public string OrderId { get; set; }
            public decimal Total { get; set; }
            public decimal Revenue { get; set; }
            public decimal Shipping { get; set; }
            public decimal Tax { get; set; }
            public decimal Discount { get; set; }
            public string Coupon { get; set; }
            public string Currency { get; set; }
            public List<ProductProperties> Products { get; set; }
        }

        public class ProductProperties
        {
            public string Id { get; set; }
            public string Sku { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public string Category { get; set; }
        }

        public class UserContextProperties
        {
            public string Ip { get; set; }
            public string Language { get; set; }
            public string UserAgentString { get; set; }
            public string Timezone { get; set; }
            public string AnonymousId { get; set; }
            public string Key { get; set; }
            public Dictionary<string,string> Cookies { get; set; }
        }

        public class ScreenProperties
        {
            public string Name { get; set; }
        }

        public class UserTrails
        {
            public string Avatar { get; set; }
            public DateTime Birthday { get; set; }
            public DateTime CreatedAt { get; set; }
            public string Description { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string Gender { get; set; }
            public string Id { get; set; }
            public string LastName { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Title { get; set; }
            public string Username { get; set; }
            public string Website { get; set; }
        }

        public class GroupTrails
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Website { get; set; }
        }

        public class PageTrails
        {
            public string Name { get; set; }

            public string Path { get; set; }

            public string Referrer { get; set; }

            public string Search { get; set; }

            public string Title { get; set; }

            public string Url { get; set; }

        }
    }
}
