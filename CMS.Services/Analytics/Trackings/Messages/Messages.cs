using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using CMS.Core.Extensions;
using CMS.Services.Analytics.Trackings.Models.Models;
using Newtonsoft.Json;

namespace CMS.Services.Analytics.Trackings.Messages
{
    namespace Messages
    {
        public abstract class BaseTrackingRequest
        {
            protected BaseTrackingRequest(string sessionId)
            {
                Context = new UserContextProperties();
                Context.AnonymousId = sessionId;
            }

            public string UserId { get; set; }
            public UserContextProperties Context { get; set; }
            public object Options { get; set; }
        }

        public class IdentityTrackingRequest : BaseTrackingRequest
        {
            public IdentityTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string Pnr { get; set; }
            public UserTrails UserTrails { get; set; }
        }

        public class TrackTrackingRequest : BaseTrackingRequest
        {
            public TrackTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string Action { get; set; }
            public object Properties { get; set; }
        }

        public class GroupTrackingRequest : BaseTrackingRequest
        {
            public GroupTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public GroupTrails GroupTrails { get; set; }
        }

        public class PageTrackingRequest : BaseTrackingRequest
        {
            public PageTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string PageName { get; set; }
            public string Category { get; set; }
            public PageTrails PageTrails { get; set; }
        }

        public class ScreenTrackingRequest : BaseTrackingRequest
        {
            public ScreenTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string ScreenName { get; set; }
            public ScreenProperties ScreenProperties { get; set; }
        }

        public class AliasTrackingRequest : BaseTrackingRequest
        {
            public AliasTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string AnonymousId { get; set; }
        }

        public class ViewProductCategoryTracking : BaseTrackingRequest
        {
            public ViewProductCategoryTracking(string sessionId) : base(sessionId)
            {
            }

            public ProductCategoryProperties CategoryProperties { get; set; }
        }

        public class ViewProductTrackingRequest : BaseTrackingRequest
        {
            public ViewProductTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public ProductProperties ProductProperties { get; set; }
        }

        public class AddProductTrackingRequest : BaseTrackingRequest
        {
            public AddProductTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string Category { get; set; }
            public ProductProperties ProductProperties { get; set; }
        }

        public class RemoveProductTrackingRequest : BaseTrackingRequest
        {
            public RemoveProductTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public string Category { get; set; }
            public ProductProperties ProductProperties { get; set; }
        }

        public class CompleteOrderTrackingRequest : BaseTrackingRequest
        {
            public CompleteOrderTrackingRequest(string sessionId) : base(sessionId)
            {
            }

            public OrderProperties OrderProperties { get; set; }
        }

        public static class TrackingPropertyExtension
        {
            static readonly IDictionary<Type, Action<object>> CleanIgnoreHandlers = new Dictionary<Type, Action<object>>();

            static TrackingPropertyExtension()
            {
             
            }

            static Type GetEnumerableType(Type type)
            {
                foreach (var intType in type.GetInterfaces())
                {
                    if (intType.GetTypeInfo().IsGenericType && intType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    {
                        var enumerableType = intType.GetGenericArguments()[0];
                        return enumerableType;
                    }
                }
                return null;
            }

            private static void AddProperties(TrackingProperties properties, string name, object collection, Type type)
            {
                var listType = typeof(List<>).MakeGenericType(type);
                var list = Activator.CreateInstance(listType);

                var addMethod = listType.GetMethod("Add");

                var enumerable = collection as IEnumerable;
                if (enumerable != null)
                {
                    foreach (var item in enumerable)
                    {
                        var clone = Clone(item, type);

                        CleanIgnored(clone, type);

                        addMethod.Invoke(list, new[] { clone });
                    }
                }

                properties.AddProperty(name, list);
            }

            private static object Clone(object item, Type type)
            {
                var json = JsonConvert.SerializeObject(item);
                return JsonConvert.DeserializeObject(json, type);
            }

            private static T Clone<T>(T item)
            {
                return (T)Clone(item, typeof(T));
            }

            public static void AddProperty<T>(this TrackingProperties properties, string name, T obj) where T : class
            {
                var type = typeof(T);
                var enumerableType = GetEnumerableType(type);

                if (enumerableType != null)
                {
                    AddProperties(properties, name, obj, enumerableType);
                    return;
                }

                T clone = null;

                try
                {
                    clone = Clone(obj);

                    CleanIgnored(clone, type);
                }
                catch (System.Exception)
                {
                    // ignored
                }

                if (clone != null)
                    properties.Add(name, clone);
            }

            private static void CleanIgnored(object obj, Type type)
            {
                Action<object> handler;
                if (CleanIgnoreHandlers.TryGetValue(type, out handler))
                {
                    handler(obj);
                }
            }

            private static void Register<T>(params Expression<Func<T, object>>[] ignores)
            {
                var type = typeof(T);
                var ignoresProperties = new List<Action<object>>();

                foreach (var e in ignores)
                {
                    var pName = e.GetPropertyName();
                    var p = type.GetProperty(pName, BindingFlags.Instance | BindingFlags.Public);

                    var set = p.GetSetMethod();
                    var get = p.GetGetMethod();

                    if (set == null || get == null) continue;

                    var handler = p.PropertyType.GetTypeInfo().IsValueType ? (Action<object>)(obj => set.Invoke(obj, new[] { Activator.CreateInstance(p.PropertyType) })) : (obj => set.Invoke(obj, new object[] { null }));

                    ignoresProperties.Add(handler);
                }

                CleanIgnoreHandlers.Add(type, obj =>
                {
                    foreach (var property in ignoresProperties)
                    {
                        property.Invoke(obj);
                    }
                });
            }
        }

        public class TrackingProperties : Dictionary<string, object>
        {

        }

        public static class TrackingExt
        {
            public static object ToDynamicObject(this NameValueCollection namvCollection)
            {
                var obj = new ExpandoObject();
                var dic = (IDictionary<string, object>)obj;

                foreach (var p in namvCollection.AllKeys)
                {
                    dic.Add(p, namvCollection[p]);
                }

                return obj;
            }

            public static object ToDynamicObject(this IEnumerable<KeyValuePair<string, object>> collection)
            {
                var obj = new ExpandoObject();
                var dic = (IDictionary<string, object>)obj;

                foreach (var p in collection)
                {
                    dic.Add(p.Key, p.Value);
                }

                return obj;
            }
        }
    }


}