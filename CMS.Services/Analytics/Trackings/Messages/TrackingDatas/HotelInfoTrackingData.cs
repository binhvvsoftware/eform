﻿using System.Collections.Generic;

namespace CMS.Services.Analytics.Trackings.Messages.TrackingDatas
{
    public class HotelInfoTrackingData
    {
        public string HotelId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string StateProvinceCode { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string AirportCode { get; set; }
        public int PropertyCategory { get; set; }
        public float? HotelRating { get; set; }
        public decimal? LowRate { get; set; }
        public decimal? HighRate { get; set; }
        public string RateCurrencyCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double TripAdvisorRating { get; set; }
        public long TripAdvisorReviewCount { get; set; }
        public string TripAdvisorRatingUrl { get; set; }
        public float HotelRanking { get; set; }
        public ICollection<string> PropertyAmenities { get; set; }
//
//        public static implicit operator HotelInfoTrackingData(HotelInfo hotel)
//        {
//            var model = new HotelInfoTrackingData
//            {
//                HotelId = hotel.HotelId.ToString(),
//                Name = hotel.Name,
//                Address1 = hotel.Address1,
//                City = hotel.City,
//                StateProvinceCode = hotel.StateProvinceCode,
//                CountryCode = hotel.CountryCode,
//                PostalCode = hotel.PostalCode,
//                AirportCode = hotel.AirportCode,
//                PropertyCategory = hotel.PropertyCategory,
//                HotelRating = hotel.HotelRating,
//                LowRate = hotel.LowRate,
//                HighRate = hotel.HighRate,
//                RateCurrencyCode = hotel.RateCurrencyCode,
//                Latitude = hotel.Latitude,
//                Longitude = hotel.Longitude,
//                TripAdvisorRating = hotel.TripAdvisorRating,
//                TripAdvisorReviewCount = hotel.TripAdvisorReviewCount,
//                TripAdvisorRatingUrl = hotel.TripAdvisorRatingUrl,
//                HotelRanking = hotel.HotelRanking,
//                PropertyAmenities = hotel.PropertyAmenities
//            };
//
//            return model;
//        }
    }
}
