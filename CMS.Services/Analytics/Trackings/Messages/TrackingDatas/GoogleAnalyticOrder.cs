﻿using System.Collections.Generic;

namespace CMS.Services.Analytics.Trackings.Messages.TrackingDatas
{
    public class GoogleAnalyticOrder
    {
        public GoogleAnalyticOrder()
        {
            Items= new List<GoogleAnalyticItem>();
        }

        public string Id { get; set; }
        public decimal Revenue { get; set; }
        public string Currency { get; set; }
        public IList<GoogleAnalyticItem> Items { get; set; }

        public void AddItem(GoogleAnalyticItem item)
        {
            Items.Add(item);
        }

        public void AddItem(string id, string name, string sku, string category, int quanlity, decimal price, string currency)
        {
            AddItem(new GoogleAnalyticItem
                {
                    Id = id,
                    Name= name,
                    Sku = sku,
                    Category =  category,
                    Currency = currency,
                    Quanlity = quanlity,
                    Price = price
                });
        }
    }

    public class GoogleAnalyticItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Category { get; set; }
        public string Currency { get; set; }
        public int Quanlity { get; set; }
        public decimal Price { get; set; }
    }
}
