﻿using System;
using Microsoft.Extensions.Options;

namespace CMS.Services.Analytics.Trackings.Segment
{
    public interface ISegmentTrackingConfigProvider
    {
        Config GetConfig();
    }

    public class SegmentTrackingConfigProvider : ISegmentTrackingConfigProvider
    {
        private readonly IOptions<SegmentConfiguaration> options;

        public SegmentTrackingConfigProvider(IOptions<SegmentConfiguaration> options)
        {
            this.options = options;
        }

        public Config GetConfig()
        {
            return new Config()
                .SetAsync(true)
                .SetTimeout(TimeSpan.FromSeconds(options.Value.Timeout))
                .SetMaxQueueSize(options.Value.MaxQueueSize);
        }
    }

    public class SegmentConfiguaration
    {
        /// <summary>
        /// in seconds
        /// </summary>
        public int Timeout { get; set; }

        public int MaxQueueSize { get; set; }
    }

    public interface ITrackingClientFactory
    {
        ITrackingClient Build(string apikey);
    }

    public class SegmentTrackingClientFactory : ITrackingClientFactory
    {
        private readonly ISegmentTrackingConfigProvider configProvider;

        public SegmentTrackingClientFactory(ISegmentTrackingConfigProvider configProvider)
        {
            this.configProvider = configProvider;
        }

        public ITrackingClient Build(string apikey)
        {
            return new SegmentTrackingClient(apikey,configProvider.GetConfig());
        }
    }
}