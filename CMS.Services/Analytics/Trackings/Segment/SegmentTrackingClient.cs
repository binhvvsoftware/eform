using CMS.Services.Analytics.Model;

namespace CMS.Services.Analytics.Trackings.Segment
{
    public class SegmentTrackingClient : ITrackingClient
    {
        private readonly Client client;

        public SegmentTrackingClient(string key, Config config)
        {
            client =  new Client(key,config);
        }

        public void Identify(string userId, Traits traits)
        {
            client.Identify(userId, traits);
        }
        public void Identify(string userId, Traits traits, Options options)
        {
            client.Identify(userId, traits, options);
        }
        public void Group(string userId, string groupId, Options options)
        {
            client.Group(userId, groupId, options);
        }
        public void Group(string userId, string groupId, Traits traits)
        {
            client.Group(userId, groupId, traits);
        }
        public void Group(string userId, string groupId, Traits traits, Options options)
        {
            client.Group(userId, groupId, traits, options);
        }
        public void Track(string userId, string eventName)
        {
            client.Track(userId, eventName);
        }
        public void Track(string userId, string eventName, Properties properties)
        {
            client.Track(userId, eventName, properties);
        }
        public void Track(string userId, string eventName, Options options)
        {
            client.Track(userId, eventName, options);
        }
        public void Track(string userId, string eventName, Properties properties, Options options)
        {
            client.Track(userId, eventName, properties, options);
        }
        public void Alias(string previousId, string userId)
        {
            client.Alias(previousId, userId);
        }
        public void Alias(string previousId, string userId, Options options)
        {
            client.Alias(previousId, userId, options);
        }
        public void Page(string userId, string name)
        {
            client.Page(userId, name);
        }
        public void Page(string userId, string name, Options options)
        {
            client.Page(userId, name, options);
        }
        public void Page(string userId, string name, string category)
        {
            client.Page(userId, name, category);
        }
        public void Page(string userId, string name, Properties properties)
        {
            client.Page(userId, name, properties);
        }
        public void Page(string userId, string name, Properties properties, Options options)
        {
            client.Page(userId, name, properties, options);
        }
        public void Page(string userId, string name, string category, Properties properties, Options options)
        {
            client.Page(userId, name, category, properties, options);
        }
        public void Screen(string userId, string name)
        {
            client.Screen(userId, name);
        }
        public void Screen(string userId, string name, Options options)
        {
            client.Screen(userId, name, options);
        }
        public void Screen(string userId, string name, string category)
        {
            client.Screen(userId, name, category);
        }
        public void Screen(string userId, string name, Properties properties)
        {
            client.Screen(userId, name, properties);
        }
        public void Screen(string userId, string name, Properties properties, Options options)
        {
            client.Screen(userId, name, properties, options);
        }
        public void Screen(string userId, string name, string category, Properties properties, Options options)
        {
            client.Screen(userId, name, category, properties, options);
        }

        public void Flush()
        {
            client.Flush();
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}