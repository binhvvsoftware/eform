using System;
using CMS.Services.Analytics.Model;

namespace CMS.Services.Analytics.Trackings
{
    public interface ITrackingClient:IDisposable
    {
        void Identify(string userId, Traits traits);
        void Identify(string userId, Traits traits, Options options);
        void Group(string userId, string groupId, Options options);
        void Group(string userId, string groupId, Traits traits);
        void Group(string userId, string groupId, Traits traits, Options options);
        void Track(string userId, string eventName);
        void Track(string userId, string eventName, Properties properties);
        void Track(string userId, string eventName, Options options);
        void Track(string userId, string eventName, Properties properties, Options options);
        void Alias(string previousId, string userId);
        void Alias(string previousId, string userId, Options options);
        void Page(string userId, string name);
        void Page(string userId, string name, Options options);
        void Page(string userId, string name, string category);
        void Page(string userId, string name, Properties properties);
        void Page(string userId, string name, Properties properties, Options options);
        void Page(string userId, string name, string category, Properties properties, Options options);
        void Screen(string userId, string name);
        void Screen(string userId, string name, Options options);
        void Screen(string userId, string name, string category);
        void Screen(string userId, string name, Properties properties);
        void Screen(string userId, string name, Properties properties, Options options);
        void Screen(string userId, string name, string category, Properties properties, Options options);
        void Flush();
    }
}