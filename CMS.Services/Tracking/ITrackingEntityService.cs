﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using CMS.Services.CompareObjects;
using CMS.Services.Services;
using CMS.Shared.Data.Domain;
using Newtonsoft.Json;

namespace CMS.Services.Tracking
{
    public interface ITrackingEntityService
    {
        Task OnInsert(ComparisonResult result, string user);

        Task OnUpdate(ComparisonResult result, string user);

        Task OnDelete(ComparisonResult result, string user);
    }

    public class TrackingEntityService : ITrackingEntityService
    {
        private readonly ILogService logService;

        public TrackingEntityService(ILogService logService)
        {
            this.logService = logService;
        }

        public async Task OnInsert(ComparisonResult result, string user)
        {
            var siteId = Guid.Empty;

            if (result.Data.ContainsKey("SiteId"))
            {
                siteId = (Guid)result.Data["SiteId"];
            }

            var systemLog = new Log
            {
                EntityName = result.EntityName,
                LogTime = DateTime.Now,
                ActionName = "Insert",
                UserName = user,
                Values = result.DifferencesString
            };

            await logService.InsertAsync(systemLog);
        }

        public async Task OnUpdate(ComparisonResult result, string user)
        {
            var siteId = Guid.Empty;

            if (result.Data.ContainsKey("SiteId"))
            {
                siteId = (Guid)result.Data["SiteId"];
            }

            var systemLog = new Log
            {
                EntityName = result.EntityName,
                LogTime = DateTime.Now,
                ActionName = "Update",
                UserName = user,
                Values = result.DifferencesString
            };

            await logService.InsertAsync(systemLog);
        }

        public async Task OnDelete(ComparisonResult result, string user)
        {
            var siteId = Guid.Empty;

            if (result.Data.ContainsKey("SiteId"))
            {
                siteId = (Guid)result.Data["SiteId"];
            }

            var systemLog = new Log
            {
                EntityName = result.EntityName,
                LogTime = DateTime.Now,
                ActionName = "Delete",
                UserName = user,
                Values = result.DifferencesString
            };

            await logService.InsertAsync(systemLog);
        }
    }

    public class TrackingEntityManager<TEntity> : IDisposable// where TEntity : BaseEntity
    {
        private bool completed;
        private readonly IComponentContext componentContext;
        private readonly TEntity entity;
        private readonly bool isNew;
        private readonly string entitySerialized;
        private Exception exception;

        public TrackingEntityManager(IComponentContext componentContext, TEntity entity, bool isNew)
        {
            this.componentContext = componentContext;
            this.entity = entity;
            this.isNew = isNew;
            Name = entity.GetType().Name;
            entitySerialized = JsonConvert.SerializeObject(entity);
        }

        public string Name { get; set; }

        public bool EnableTracking { get; set; }

        public bool EnableVersion { get; set; }

        public Func<TEntity, IDictionary<string, object>> GetTrackingData { get; set; }

        public async Task Complete(string user)
        {
            if (!EnableTracking && !EnableVersion)
            {
                completed = true;
                return;
            }

            try
            {
                var oldEntity = JsonConvert.DeserializeObject<TEntity>(entitySerialized);
                var compareLogic = new CompareLogic(new ComparisonConfig
                {
                    MaxDifferences = 9999
                });

                var result = compareLogic.Compare(entity, oldEntity);
                if (!result.AreEqual)
                {
                    result.Differences.Insert(0, new Difference
                    {
                        PropertyName = "Id",
                        //Object1Value = entity.GetIdValue().ToString()
                    });
                    result.EntityName = Name;
                    result.Data = GetTrackingData != null ? GetTrackingData(entity) : new Dictionary<string, object>();

                    if (EnableTracking)
                    {
                        var trackingEntityService = componentContext.Resolve<ITrackingEntityService>();
                        if (isNew)
                        {
                            await trackingEntityService.OnInsert(result, user);
                        }
                        else
                        {
                            await trackingEntityService.OnUpdate(result, user);
                        }
                    }

                    if (EnableVersion && !isNew)
                    {
                        var versionHistoryService = componentContext.Resolve<IVersionHistoryService>();

                        if (string.IsNullOrEmpty(user))
                        {
                            user = "Anonymous";
                        }

                        await versionHistoryService.CreateNewVersion(oldEntity, entity.ToString(), user);
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            completed = true;
        }

        public void Abort()
        {
            completed = true;
        }

        public void Dispose()
        {
            if (exception != null)
            {
                throw exception;
            }

            if (!completed)
            {
                throw new ArgumentException("Please call Complete method.");
            }
        }
    }
}
