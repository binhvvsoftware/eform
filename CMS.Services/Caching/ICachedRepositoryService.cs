﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Core.Extensions;
using CMS.Core.Threading;
using CMS.Shared.Extensions;
using Microsoft.Extensions.Options;

namespace CMS.Services.Caching
{
    public interface ICachedRepository<T>
    {
        T Get(string key);

        Task<T> GetAsync(string key);

        T GetOrAdd(string key, Func<T> func);

        Task<T> GetOrAddAsync(string key, Func<Task<T>> func);

        void Put(string key, T value, TimeSpan? ttl = null);

        Task PutAsync(string key, T value);

        Task PutAllAsync(IEnumerable<KeyValuePair<string, T>> values);

        void PutAll(IEnumerable<KeyValuePair<string, T>> values);

        void Remove(string key);

        Task RemoveAsync(string key);

        void Clear(string pattern);

        Task ClearAsync(string pattern);

        int InMemoryItemsCount { get; }
    }

    //public class CachedRepository<T> : ICachedRepository<T>
    //{
    //    private readonly ConcurrentDictionary<string, LazyItem<T>> concurrentDictionary = new ConcurrentDictionary<string, LazyItem<T>>();
    //    private readonly ConcurrentQueue<LazyItem<T>> expirationsQueue = new ConcurrentQueue<LazyItem<T>>();

    //    private readonly IScheduler scheduler;
       
    //    private readonly ILazyBuilder<T> lazyBuilder;
    //    private readonly CacheConfiguration config;

    //    public CachedRepository(ICacheFactory cacheFactory, IScheduler scheduler,
    //        ICacheSynchronizerFactory synchronizerFactory, IOptions<CacheConfiguration> config)
    //    {
    //        this.scheduler = scheduler;
    //        this.config = config.Value;

    //        var regionValue = typeof(T).GetRegionValue();

    //        cacheStorage = cacheFactory.CreateCacheStorage(new CacheStorageConfig
    //        {
    //            Prefix = string.IsNullOrEmpty(this.config.Prefix) ? regionValue : $"{this.config.Prefix}:{regionValue}",
    //            Database = this.config.RedisDatabase
    //        });

    //        synchronizer = synchronizerFactory.Create(regionValue);
    //        synchronizer.OnUpdated += OnUpdated;

    //        lazyBuilder = new LazyBuilder<T>(SyncFactory, AsyncFactory);
    //        ExpiredItemCleanerCallback();
    //    }

    //    public int InMemoryItemsCount => concurrentDictionary.Values.Count;

    //    private void ExpiredItemCleanerCallback()
    //    {
    //        scheduler.Schedule(obj => CleanExpiredValue(), null, TimeSpan.Zero, TimeSpan.FromSeconds(1));
    //    }

    //    private void OnUpdated(string[] keys)
    //    {
    //        if (keys.IsNullOrEmpty())
    //        {
    //            concurrentDictionary.Clear();
    //            expirationsQueue.Clear();
    //        }
    //        else
    //        {
    //            foreach (var k in keys)
    //            {
    //                ExpiredValue(k);
    //            }
    //        }
    //    }

    //    private LazyItem<T> Build(string key, Func<Task<T>> asyncFac = null, Func<T> syncFac = null)
    //    {
    //        var item = lazyBuilder.Build(key, syncFac, asyncFac);

    //        expirationsQueue.Enqueue(item);

    //        return item;
    //    }

    //    private LazyItem<T> Build(string key, T value)
    //    {
    //        return Build(key, () => Task.FromResult(value), () => value);
    //    }

    //    private void CleanExpiredValue()
    //    {
    //        var expiredKeys = new List<string>();
    //        var now = DateTimeOffset.UtcNow;

    //        while (expirationsQueue.TryPeek(out var p))
    //        {
    //            if (p.IsDead)
    //            {
    //                expirationsQueue.TryDequeue(out _);
    //                continue;
    //            }

    //            if ((p.CreateAt + config.MemoryTtl) <= now)
    //            {
    //                expiredKeys.Add(p.Key);
    //                expirationsQueue.TryDequeue(out _);
    //                //expired
    //            }
    //            else
    //            {
    //                break;
    //                //not yet expired
    //            }
    //        }

    //        foreach (var key in expiredKeys)
    //        {
    //            ExpiredValue(key);
    //        }

    //        if (expiredKeys.Count > 0)
    //            synchronizer.NotifyUpdate(expiredKeys.ToArray());
    //    }

    //    private void ExpiredValue(string key)
    //    {
    //        if (concurrentDictionary.TryRemove(key, out var lazyItem))
    //        {
    //            lazyItem.IsDead = true;
    //        }
    //    }

    //    private T SyncFactory(string key)
    //    {
    //        return cacheStorage.Get<T>(key).Value;
    //    }

    //    private async Task<T> AsyncFactory(string key)
    //    {
    //        var records = await cacheStorage.GetAsync<T>(key);
    //        return records.Value;
    //    }

    //    public T Get(string key)
    //    {
    //        return concurrentDictionary.GetOrAdd(key, s => Build(key)).ValueEntry.Value;
    //    }

    //    public Task<T> GetAsync(string key)
    //    {
    //        return concurrentDictionary.GetOrAdd(key, s => Build(key)).TaskEntry.Value;
    //    }

    //    public T GetOrAdd(string key, Func<T> valueFactory)
    //    {
    //        T GetFromOuterFactory()
    //        {
    //            var itemFromRedis = cacheStorage.Get<T>(key);

    //            if (itemFromRedis.Value != null)
    //                return itemFromRedis.Value;

    //            var item = valueFactory();
    //            cacheStorage.Put(key, item, config.Ttl);
    //            synchronizer.NotifyUpdate(new[] { key });
    //            return item;
    //        }

    //        var lazyItem = concurrentDictionary.GetOrAdd(key, k => Build(k, syncFac: GetFromOuterFactory));

    //        return lazyItem.ValueEntry.Value;
    //    }

    //    public Task<T> GetOrAddAsync(string key, Func<Task<T>> valueFactory)
    //    {
    //        async Task<T> GetFromOuterFactory()
    //        {
    //            var itemFromRedis = await cacheStorage.GetAsync<T>(key);

    //            if (itemFromRedis.Value != null)
    //                return itemFromRedis.Value;

    //            var item = await valueFactory();
    //            await cacheStorage.PutAsync(key, item, config.Ttl);
    //            await synchronizer.NotifyUpdateAsync(new[] { key });
    //            return item;
    //        }

    //        var lazyItem = concurrentDictionary.GetOrAdd(key, k => Build(k, GetFromOuterFactory));

    //        return lazyItem.TaskEntry.Value;
    //    }

    //    public void Put(string key, T value, TimeSpan? ttl = null)
    //    {
    //        var lazyItem = concurrentDictionary.AddOrUpdate(key, s => Build(key, value), (s, item) => Build(key, value));

    //        cacheStorage.Put(key, lazyItem.ValueEntry.Value, ttl ?? config.Ttl);

    //        synchronizer.NotifyUpdate(new[] { key });
    //    }

    //    public void Put(string key, T value, string region)
    //    {
    //        Put(region + ":" + key, value);
    //    }

    //    public async Task PutAsync(string key, T value)
    //    {
    //        var lazyItem = concurrentDictionary.AddOrUpdate(key, s => Build(s, value), (s, item) => Build(s, value));
    //        await cacheStorage.PutAsync(key, lazyItem.ValueEntry.Value, config.Ttl);
    //        await synchronizer.NotifyUpdateAsync(new[] { key });
    //    }

    //    public async Task PutAllAsync(IEnumerable<KeyValuePair<string, T>> values)
    //    {
    //        await cacheStorage.PutAllAsync(AddToMemory(values), config.Ttl);
    //        await synchronizer.NotifyUpdateAsync(values.Select(x => x.Key).ToArray());
    //    }

    //    private IEnumerable<KeyValuePair<string, T>> AddToMemory(IEnumerable<KeyValuePair<string, T>> values)
    //    {
    //        foreach (var valuePair in values)
    //        {
    //            concurrentDictionary.AddOrUpdate(valuePair.Key, s => Build(valuePair.Key, valuePair.Value), (s, item) => Build(s, valuePair.Value));

    //            yield return valuePair;
    //        }
    //    }

    //    public void PutAll(IEnumerable<KeyValuePair<string, T>> values)
    //    {
    //        cacheStorage.PutAll(AddToMemory(values), config.Ttl);
    //        synchronizer.NotifyUpdate(values.Select(x => x.Key).ToArray());
    //    }

    //    public void Remove(string key)
    //    {
    //        concurrentDictionary.TryRemove(key, out _);
    //        cacheStorage.Remove(key);
    //        synchronizer.NotifyUpdate(new[] { key });
    //    }

    //    public async Task RemoveAsync(string key)
    //    {
    //        concurrentDictionary.TryRemove(key, out _);
    //        await cacheStorage.RemoveAsync(key);
    //        await synchronizer.NotifyUpdateAsync(new[] { key });
    //    }

    //    public void Clear(string pattern)
    //    {
    //        var removedKeys = RemoveFromMemory(pattern);
    //        cacheStorage.Clear(pattern);
    //        synchronizer.NotifyUpdate(removedKeys.ToArray());
    //    }

    //    public async Task ClearAsync(string pattern)
    //    {
    //        if (string.IsNullOrEmpty(pattern))
    //            throw new NullReferenceException(nameof(pattern));

    //        var removedKeys = RemoveFromMemory(pattern);

    //        await cacheStorage.ClearAsync(pattern);
    //        await synchronizer.NotifyUpdateAsync(removedKeys.ToArray());
    //    }

    //    private IEnumerable<string> RemoveFromMemory(string pattern)
    //    {
    //        if (pattern != "*")
    //        {
    //            var expired = new List<string>();
    //            foreach (var item in concurrentDictionary)
    //            {
    //                if (item.Key.IsLike(pattern, true))
    //                    expired.Add(item.Key);
    //            }

    //            foreach (var item in expired)
    //            {
    //                concurrentDictionary.TryRemove(item, out _);
    //            }

    //            return expired;
    //        }
    //        else
    //        {
    //            concurrentDictionary.Clear();
    //            return concurrentDictionary.Keys;
    //        }
    //    }
    //}
}
