﻿using System;
using System.Threading.Tasks;

namespace CMS.Services.Caching
{
    public interface ILazyBuilder<TValue>
    {
        LazyItem<TValue> Build(string key);
        LazyItem<TValue> Build(string key, Func<TValue> syncFactory);
        LazyItem<TValue> Build(string key, Func<Task<TValue>> asyncFactory);
        LazyItem<TValue> Build(string key, Func<TValue> syncFactory, Func<Task<TValue>> asyncFactory);
    }

    public class LazyBuilder<TValue> : ILazyBuilder<TValue>
    {
        private readonly Func<string, TValue> _syncFactory;
        private readonly Func<string, Task<TValue>> _asyncFactory;

        public LazyBuilder(Func<string, TValue> syncFactory, Func<string, Task<TValue>> asyncFactory)
        {
            _syncFactory = syncFactory ?? throw new ArgumentNullException($"{nameof(syncFactory)} can't be null");
            _asyncFactory = asyncFactory ?? throw new ArgumentNullException($"{nameof(asyncFactory)} can't be null");
        }

        public LazyItem<TValue> Build(string key)
        {
            var item = new LazyItem<TValue>(key, () => _syncFactory(key), () => _asyncFactory(key));
            return item;
        }

        public LazyItem<TValue> Build(string key, Func<TValue> syncFactory)
        {
            var item = new LazyItem<TValue>(key, syncFactory, () => _asyncFactory(key));
            return item;
        }

        public LazyItem<TValue> Build(string key, Func<Task<TValue>> asyncFactory)
        {
            var item = new LazyItem<TValue>(key, () => _syncFactory(key), asyncFactory);
            return item;
        }

        public LazyItem<TValue> Build(string key, Func<TValue> syncFactory, Func<Task<TValue>> asyncFactory)
        {
            if (syncFactory == null && asyncFactory == null)
                return Build(key);

            var syncFac = syncFactory ?? LazyItem<TValue>.NotSupportSync;
            var asyncFac = asyncFactory ?? LazyItem<TValue>.NotSupportAsync;

            var item = new LazyItem<TValue>(key, syncFac, asyncFac);
            return item;
        }
    }

    public interface ILazyItem<TValue>
    {
        Lazy<Task<TValue>> TaskEntry { get; }
        Lazy<TValue> ValueEntry { get; }
        DateTimeOffset CreateAt { get; }
        string Key { get; }
    }

    public class LazyItem<TValue> : ILazyItem<TValue>
    {
        internal static readonly Func<TValue> NotSupportSync = () => throw new ArgumentNullException("Synchronous factory can't be null.");
        internal static readonly Func<Task<TValue>> NotSupportAsync = () => throw new ArgumentNullException("Asynchronous factory can't be null.");

        public LazyItem(string key, Func<TValue> syncFactory, Func<Task<TValue>> asyncFactory)
        {
            ValueEntry = new Lazy<TValue>(() =>
            {
                if (asyncFactory!=NotSupportAsync && TaskEntry.IsValueCreated && TaskEntry.Value.Status == TaskStatus.RanToCompletion)
                {
                    return TaskEntry.Value.Result;
                }

                return syncFactory();
            });

            TaskEntry = new Lazy<Task<TValue>>(() =>
            {
                if (syncFactory!=NotSupportSync && ValueEntry.IsValueCreated)
                {
                    return Task.FromResult(ValueEntry.Value);
                }

                return asyncFactory();
            });

            Key = key;

            CreateAt = DateTimeOffset.UtcNow;
        }

        public Lazy<Task<TValue>> TaskEntry { get; }

        public string Key { get; }

        public Lazy<TValue> ValueEntry { get; }

        public DateTimeOffset CreateAt { get; }

        public bool IsDead { get; set; }
    }
}