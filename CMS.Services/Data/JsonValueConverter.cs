﻿using System;
using System.Collections.Concurrent;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

namespace CMS.Services.Data
{
    static class JsonValueConverter
    {
        private static readonly ConcurrentDictionary<Type, ValueConverter> Converters = new ConcurrentDictionary<Type, ValueConverter>();

        static readonly JsonSerializerSettings DefaultJsonSetting = new JsonSerializerSettings();

        private static T ConvertFromProvider<T>(string value, JsonSerializerSettings jsonSettings)
        {
            return JsonConvert.DeserializeObject<T>(value, jsonSettings);
        }

        private static string ConvertToProvider<T>(T value, JsonSerializerSettings jsonSettings)
        {
            return JsonConvert.SerializeObject(value, jsonSettings);
        }

        public static ValueConverter GetConverter(Type type)
        {
            return Converters.GetOrAdd(type, (t) =>
            {
                var converterType = typeof(JsonConverter<>).MakeGenericType(t);
                return (ValueConverter) Activator.CreateInstance(converterType, new object[] {DefaultJsonSetting});
            });
        }

        public class JsonConverter<T> : ValueConverter<T, string>
        {
            private readonly JsonSerializerSettings _jsonSerializerSettings;

            public JsonConverter(JsonSerializerSettings jsonSerializerSettings) : base(
                x => JsonValueConverter.ConvertToProvider(x, jsonSerializerSettings),
                x => ConvertFromProvider<T>(x, jsonSerializerSettings),
                null)
            {
                _jsonSerializerSettings = jsonSerializerSettings;
            }
        }
    }
  
}



