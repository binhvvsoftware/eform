﻿using System;
using System.Linq.Expressions;
using CMS.Core.Security;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Options;

namespace CMS.Services.Data
{
    public class EncryptedValueConverter : ValueConverter<string, string>
    {
        private static readonly EncryptionSettings encryptionSettings = new EncryptionSettings
        {
            EncryptionAlgorithm = "AES",
            EncryptionKey = "CC5A18522A44FEA543168174A60D68C960DE24C28049F88F639631729D51A806",
            HashAlgorithm = "HMACSHA256",
            HashKey = "EE1D2DB2518CD27A4EB5BE72F91B0823E45F565EAB9891FEE32867F5A66050F5877CF6A0E790290A7CACFCF9BC371E979FE0468453CAFE8C328EA2DCF1B40FF4"
        };

        private static readonly IEncryptionService encryptionService =
            new DefaultEncryptionService(new OptionsWrapper<EncryptionSettings>(encryptionSettings));

        public EncryptedValueConverter(ConverterMappingHints mappingHints = null)
            : base(EncryptExpression, DecryptExpression, mappingHints)
        {
        }

        static readonly Expression<Func<string, string>> DecryptExpression = x => DecryptString(x);

        static readonly Expression<Func<string, string>> EncryptExpression = x => EncryptString(x);

        private static string EncryptString(string value)
        {
            return encryptionService.Encode(value);
        }

        private static string DecryptString(string value)
        {
            try
            {
                return encryptionService.Decode(value);
            }
            catch (Exception)
            {
                return value;
            }
        }
    }
}
