﻿using System.Reflection;
using CMS.Shared.Data.Annotations;
using CMS.Shared.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IdentityRole = CMS.Shared.Data.Domain.IdentityRole;
using IdentityUser = CMS.Shared.Data.Domain.IdentityUser;

namespace CMS.Services.Data
{
    public class EformDbContext : DbContext
    {
        public EformDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<IdentityUser> IdentityUsers { get; set; }
        public DbSet<MemberRelation> MemberRelations { get; set; }
        public DbSet<IdentityRole> IdentityRoles { get; set; }
        public DbSet<IdentityUserLogin> IdentityUserLogins { get; set; }
        public DbSet<IdentityUserRole> IdentityUserRoles { get; set; }
        public DbSet<IdentityUserClaim<int>> IdentityUserClaims { get; set; }
        public DbSet<IdentityRoleClaim<int>> IdentityRoleClaims { get; set; }
        public DbSet<IdentityUserToken<int>> IdentityUserTokens { get; set; }
        public DbSet<ParentDepartment> ParentDepartments { get; set; }
        public DbSet<Campus> Campuses { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<VersionHistory> VersionHistorys { get; set; }
        public DbSet<EntityVersion> EntityVersions { get; set; }
        public DbSet<SmtpConfig> SmtpConfigs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<FormTemplate> FormTemplates { get; set; }
        public DbSet<RequestDocument> RequestDocuments { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowStep> WorkflowSteps { get; set; }
        public DbSet<RequestDocumentAssignee> RequestDocumentAssignees { get; set; }
        public DbSet<WorkingTimesheet> WorkingTimesheets { get; set; }
        public DbSet<UserLeaveApplication> UserLeaveApplications { get; set; }
        public DbSet<WorkingLateRequest> WorkingsLateRequests { get; set; }

        private void SetConverterForJsonColumn(ModelBuilder builder)
        {
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var properties = entityType.ClrType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                foreach (var property in properties)
                {
                    var columnAttr = property.GetCustomAttribute<JsonColumnAttribute>();
                    if (columnAttr != null)
                    {
                        var pBuilder = builder
                            .Entity(entityType.ClrType)
                            .Property(property.PropertyType, property.Name)
                            .HasColumnName(string.IsNullOrEmpty(columnAttr.Name) ? property.Name : columnAttr.Name)
                            .UsePropertyAccessMode(PropertyAccessMode.Property)
                            .HasConversion(JsonValueConverter.GetConverter(property.PropertyType));

                        if (!string.IsNullOrEmpty(columnAttr.TypeName))
                            pBuilder.HasColumnType(columnAttr.TypeName);
                    }
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)

        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>()
                .HasOne(s => s.Department)
                .WithMany(g => g.Users);

            modelBuilder.Entity<Campus>()
                .HasOne(s => s.RootDepartment)
                .WithMany(s => s.Campuses);

            modelBuilder.Entity<Department>()
                .HasOne(s => s.RootCampus)
                .WithMany(s => s.Departments);

            modelBuilder.Entity<IdentityUserRole<int>>()
                .ToTable("CMS_IdentityUserRoles").HasKey(x => new
                {
                    x.UserId,
                    x.RoleId
                });

            modelBuilder.Entity<IdentityUserClaim<int>>()
             .ToTable("CMS_IdentityUserClaims").HasKey(x => x.Id);

            modelBuilder.Entity<IdentityRoleClaim<int>>()
                .ToTable("CMS_IdentityRoleClaims").HasKey(x => x.Id);

            modelBuilder.Entity<IdentityUserToken<int>>()
                          .ToTable("Cms_IdentityUserTokens").HasKey(x => new
                          {
                              x.UserId,
                              x.LoginProvider,
                              x.Name
                          });

            SetConverterForJsonColumn(modelBuilder);
        }
    }
}
