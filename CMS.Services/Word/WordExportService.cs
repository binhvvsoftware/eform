﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Services.Word
{
    public class WordExportService : IExport
    {
        public async Task<byte[]> ExportAsync(Dictionary<string, string> dictionary, string path)
        {
            var readAllText = await File.ReadAllTextAsync(path);

            foreach (var item in dictionary)
            {
                readAllText = readAllText.Replace(item.Key, item.Value);
            }

            return Encoding.UTF8.GetBytes(readAllText);
        }

        Task<byte[]> IExport.Export(Dictionary<string, string> dictionary, int FormTemplateId)
        {
            throw new NotImplementedException();
        }
    }
}
