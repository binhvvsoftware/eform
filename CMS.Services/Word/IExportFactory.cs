﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CMS.Services.Word
{
    interface IExportFactory
    {
        IExport Execute();
    }

    public class ExportFactory<T> : IExportFactory where T : new()
    {
        public IExport Execute()
        {
            return (IExport)new T();
        }
    }

    public interface IExport
    {
        Task<byte[]> ExportAsync(Dictionary<string, string> dictionary, string path);

        Task<byte[]> Export(Dictionary<string, string> dictionary, int FormTemplateId);
    }
}
